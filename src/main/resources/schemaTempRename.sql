--STATE TABLE

DROP TRIGGER IF EXISTS before_state_update_delete;
CREATE TRIGGER before_state_update_delete BEFORE UPDATE ON state
FOR EACH ROW
BEGIN
IF (NEW.area_covered <> OLD.area_covered) OR (NEW.area_covered IS NOT NULL AND OLD.area_covered IS NULL) OR (NEW.area_covered IS NULL AND OLD.area_covered IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "STATE", "area_covered",OLD.area_covered,NEW.area_covered,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.code <> OLD.code) OR (NEW.code IS NOT NULL AND OLD.code IS NULL) OR (NEW.code IS NULL AND OLD.code IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "STATE", "code",OLD.code,NEW.code,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.core_id <> OLD.core_id) OR (NEW.core_id IS NOT NULL AND OLD.core_id IS NULL) OR (NEW.core_id IS NULL AND OLD.core_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "STATE", "core_id",OLD.core_id,NEW.core_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.country_id <> OLD.country_id) OR (NEW.country_id IS NOT NULL AND OLD.country_id IS NULL) OR (NEW.country_id IS NULL AND OLD.country_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "STATE", "country_id",OLD.country_id,NEW.country_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.latitude <> OLD.latitude) OR (NEW.latitude IS NOT NULL AND OLD.latitude IS NULL) OR (NEW.latitude IS NULL AND OLD.latitude IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "STATE", "latitude",OLD.latitude,NEW.latitude,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.longitude <> OLD.longitude) OR (NEW.longitude IS NOT NULL AND OLD.longitude IS NULL) OR (NEW.longitude IS NULL AND OLD.longitude IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "STATE", "longitude",OLD.longitude,NEW.longitude,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.name <> OLD.name) OR (NEW.name IS NOT NULL AND OLD.name IS NULL) OR (NEW.name IS NULL AND OLD.name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "STATE", "name",OLD.name,NEW.name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.zoom_level <> OLD.zoom_level) OR (NEW.zoom_level IS NOT NULL AND OLD.zoom_level IS NULL) OR (NEW.zoom_level IS NULL AND OLD.zoom_level IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "STATE", "zoom_level",OLD.zoom_level,NEW.zoom_level,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.is_deleted = 1) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "DELETE", "STATE", "id",OLD.id,null,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
END;


--BASIN table


DROP TRIGGER IF EXISTS before_basin_update_delete;
CREATE TRIGGER before_basin_update_delete BEFORE UPDATE ON basin
FOR EACH ROW
BEGIN
IF (NEW.area <> OLD.area) OR (NEW.area IS NOT NULL AND OLD.area IS NULL) OR (NEW.area IS NULL AND OLD.area IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "BASIN", "area",OLD.area,NEW.area,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.code <> OLD.code) OR (NEW.code IS NOT NULL AND OLD.code IS NULL) OR (NEW.code IS NULL AND OLD.code IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "BASIN", "code",OLD.code,NEW.code,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.core_id <> OLD.core_id) OR (NEW.core_id IS NOT NULL AND OLD.core_id IS NULL) OR (NEW.core_id IS NULL AND OLD.core_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "BASIN", "core_id",OLD.core_id,NEW.core_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.description <> OLD.description) OR (NEW.description IS NOT NULL AND OLD.description IS NULL) OR (NEW.description IS NULL AND OLD.description IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "BASIN", "description",OLD.description,NEW.description,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.name <> OLD.name) OR (NEW.name IS NOT NULL AND OLD.name IS NULL) OR (NEW.name IS NULL AND OLD.name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "BASIN", "name",OLD.name,NEW.name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.status <> OLD.status) OR (NEW.status IS NOT NULL AND OLD.status IS NULL) OR (NEW.status IS NULL AND OLD.status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "BASIN", "status",OLD.status,NEW.status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.type <> OLD.type) OR (NEW.type IS NOT NULL AND OLD.type IS NULL) OR (NEW.type IS NULL AND OLD.type IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "BASIN", "type",OLD.type,NEW.type,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.state_id <> OLD.state_id) OR (NEW.state_id IS NOT NULL AND OLD.state_id IS NULL) OR (NEW.state_id IS NULL AND OLD.state_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "BASIN", "state_id",OLD.state_id,NEW.state_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.is_deleted = 1) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "DELETE", "BASIN", "id",OLD.id,null,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
END;

--CONTARCT_AREA TABLE

DROP TRIGGER IF EXISTS before_contract_area_update_delete;
CREATE TRIGGER before_contract_area_update_delete BEFORE UPDATE ON contract_area
FOR EACH ROW
BEGIN
IF (NEW.area_covered <> OLD.area_covered) OR (NEW.area_covered IS NOT NULL AND OLD.area_covered IS NULL) OR (NEW.area_covered IS NULL AND OLD.area_covered IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "area_covered",OLD.area_covered,NEW.area_covered,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.area_type <> OLD.area_type) OR (NEW.area_type IS NOT NULL AND OLD.area_type IS NULL) OR (NEW.area_type IS NULL AND OLD.area_type IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "area_type",OLD.area_type,NEW.area_type,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.core_id <> OLD.core_id) OR (NEW.core_id IS NOT NULL AND OLD.core_id IS NULL) OR (NEW.core_id IS NULL AND OLD.core_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "core_id",OLD.core_id,NEW.core_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.basin_id <> OLD.basin_id) OR (NEW.basin_id IS NOT NULL AND OLD.basin_id IS NULL) OR (NEW.basin_id IS NULL AND OLD.basin_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "basin_id",OLD.basin_id,NEW.basin_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.name <> OLD.name) OR (NEW.name IS NOT NULL AND OLD.name IS NULL) OR (NEW.name IS NULL AND OLD.name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "name",OLD.name,NEW.name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.status <> OLD.status) OR (NEW.status IS NOT NULL AND OLD.status IS NULL) OR (NEW.status IS NULL AND OLD.status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "status",OLD.status,NEW.status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.type <> OLD.type) OR (NEW.type IS NOT NULL AND OLD.type IS NULL) OR (NEW.type IS NULL AND OLD.type IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "type",OLD.type,NEW.type,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.code <> OLD.code) OR (NEW.code IS NOT NULL AND OLD.code IS NULL) OR (NEW.code IS NULL AND OLD.code IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "code",OLD.code,NEW.code,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.producing_status <> OLD.producing_status) OR (NEW.producing_status IS NOT NULL AND OLD.producing_status IS NULL) OR (NEW.producing_status IS NULL AND OLD.producing_status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "CONTRACT_AREA", "producing_status",OLD.producing_status,NEW.producing_status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.is_deleted = 1) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "DELETE", "CONTRACT_AREA", "id",OLD.id,null,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
END;


--FACILITY TABLE

DROP TRIGGER IF EXISTS before_facility_update_delete;
CREATE TRIGGER before_facility_update_delete BEFORE UPDATE ON facility
FOR EACH ROW
BEGIN
IF (NEW.contract_area_id <> OLD.contract_area_id) OR (NEW.contract_area_id IS NOT NULL AND OLD.contract_area_id IS NULL) OR (NEW.contract_area_id IS NULL AND OLD.area_covered IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "contract_area_id",OLD.contract_area_id,NEW.contract_area_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.facility_operator_id <> OLD.facility_operator_id) OR (NEW.facility_operator_id IS NOT NULL AND OLD.facility_operator_id IS NULL) OR (NEW.facility_operator_id IS NULL AND OLD.facility_operator_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "facility_operator_id",OLD.facility_operator_id,NEW.facility_operator_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.core_id <> OLD.core_id) OR (NEW.core_id IS NOT NULL AND OLD.core_id IS NULL) OR (NEW.core_id IS NULL AND OLD.core_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "core_id",OLD.core_id,NEW.core_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.field_area <> OLD.field_area) OR (NEW.field_area IS NOT NULL AND OLD.field_area IS NULL) OR (NEW.field_area IS NULL AND OLD.field_area IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "field_area",OLD.field_area,NEW.field_area,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.name <> OLD.name) OR (NEW.name IS NOT NULL AND OLD.name IS NULL) OR (NEW.name IS NULL AND OLD.name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "name",OLD.name,NEW.name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.status <> OLD.status) OR (NEW.status IS NOT NULL AND OLD.status IS NULL) OR (NEW.status IS NULL AND OLD.status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "status",OLD.status,NEW.status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.field_status <> OLD.field_status) OR (NEW.field_status IS NOT NULL AND OLD.field_status IS NULL) OR (NEW.field_status IS NULL AND OLD.field_status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "field_status",OLD.field_status,NEW.field_status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.code <> OLD.code) OR (NEW.code IS NOT NULL AND OLD.code IS NULL) OR (NEW.code IS NULL AND OLD.code IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "code",OLD.code,NEW.code,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.target_production <> OLD.target_production) OR (NEW.target_production IS NOT NULL AND OLD.target_production IS NULL) OR (NEW.target_production IS NULL AND OLD.target_production IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "target_production",OLD.target_production,NEW.target_production,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.address_id <> OLD.address_id) OR (NEW.address_id IS NOT NULL AND OLD.address_id IS NULL) OR (NEW.address_id IS NULL AND OLD.address_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "FACILITY", "address_id",OLD.address_id,NEW.address_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.is_deleted = 1) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "DELETE", "FACILITY", "id",OLD.id,null,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
END;

--WELL table;


DROP TRIGGER IF EXISTS before_well_update_delete;
CREATE TRIGGER before_well_update_delete BEFORE UPDATE ON well
FOR EACH ROW
BEGIN
IF (NEW.facility_id <> OLD.facility_id) OR (NEW.facility_id IS NOT NULL AND OLD.facility_id IS NULL) OR (NEW.facility_id IS NULL AND OLD.facility_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "WELL", "facility_id",OLD.facility_id,NEW.facility_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.well_production_type <> OLD.well_production_type) OR (NEW.well_production_type IS NOT NULL AND OLD.well_production_type IS NULL) OR (NEW.well_production_type IS NULL AND OLD.well_production_type IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "WELL", "well_production_type",OLD.well_production_type,NEW.well_production_type,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.core_id <> OLD.core_id) OR (NEW.core_id IS NOT NULL AND OLD.core_id IS NULL) OR (NEW.core_id IS NULL AND OLD.core_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "WELL", "core_id",OLD.core_id,NEW.core_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.well_status <> OLD.well_status) OR (NEW.well_status IS NOT NULL AND OLD.well_status IS NULL) OR (NEW.well_status IS NULL AND OLD.well_status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "WELL", "well_status",OLD.well_status,NEW.well_status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.name <> OLD.name) OR (NEW.name IS NOT NULL AND OLD.name IS NULL) OR (NEW.name IS NULL AND OLD.name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "WELL", "name",OLD.name,NEW.name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.status <> OLD.status) OR (NEW.status IS NOT NULL AND OLD.status IS NULL) OR (NEW.status IS NULL AND OLD.status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "WELL", "status",OLD.status,NEW.status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.well_type <> OLD.well_type) OR (NEW.well_type IS NOT NULL AND OLD.well_type IS NULL) OR (NEW.well_type IS NULL AND OLD.well_type IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "WELL", "well_type",OLD.well_type,NEW.well_type,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.is_deleted = 1) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "DELETE", "WELL", "id",OLD.id,null,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
END;


--TANK TABLE


DROP TRIGGER IF EXISTS before_tank_update_delete;
CREATE TRIGGER before_tank_update_delete BEFORE UPDATE ON tank
FOR EACH ROW
BEGIN
IF (NEW.facility_id <> OLD.facility_id) OR (NEW.facility_id IS NOT NULL AND OLD.facility_id IS NULL) OR (NEW.facility_id IS NULL AND OLD.facility_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "facility_id",OLD.facility_id,NEW.facility_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.capacity <> OLD.capacity) OR (NEW.capacity IS NOT NULL AND OLD.capacity IS NULL) OR (NEW.capacity IS NULL AND OLD.capacity IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "capacity",OLD.capacity,NEW.capacity,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.core_id <> OLD.core_id) OR (NEW.core_id IS NOT NULL AND OLD.core_id IS NULL) OR (NEW.core_id IS NULL AND OLD.core_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "core_id",OLD.core_id,NEW.core_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.diameter <> OLD.diameter) OR (NEW.diameter IS NOT NULL AND OLD.diameter IS NULL) OR (NEW.diameter IS NULL AND OLD.diameter IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "diameter",OLD.diameter,NEW.diameter,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.name <> OLD.name) OR (NEW.name IS NOT NULL AND OLD.name IS NULL) OR (NEW.name IS NULL AND OLD.name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "name",OLD.name,NEW.name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.status <> OLD.status) OR (NEW.status IS NOT NULL AND OLD.status IS NULL) OR (NEW.status IS NULL AND OLD.status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "status",OLD.status,NEW.status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.height <> OLD.height) OR (NEW.height IS NOT NULL AND OLD.height IS NULL) OR (NEW.height IS NULL AND OLD.height IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "height",OLD.height,NEW.height,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.last_calibration_date <> OLD.last_calibration_date) OR (NEW.last_calibration_date IS NOT NULL AND OLD.last_calibration_date IS NULL) OR (NEW.last_calibration_date IS NULL AND OLD.last_calibration_date IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "last_calibration_date",OLD.last_calibration_date,NEW.last_calibration_date,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.tag_number <> OLD.tag_number) OR (NEW.tag_number IS NOT NULL AND OLD.tag_number IS NULL) OR (NEW.tag_number IS NULL AND OLD.tag_number IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "TANK", "tag_number",OLD.tag_number,NEW.tag_number,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.is_deleted = 1) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "DELETE", "TANK", "id",OLD.id,null,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
END;


--DEVICE TABLE


DROP TRIGGER IF EXISTS before_device_update_delete;
CREATE TRIGGER before_device_update_delete BEFORE UPDATE ON device
FOR EACH ROW
BEGIN
IF (NEW.facility_id <> OLD.facility_id) OR (NEW.facility_id IS NOT NULL AND OLD.facility_id IS NULL) OR (NEW.facility_id IS NULL AND OLD.facility_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "facility_id",OLD.facility_id,NEW.facility_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.device_type <> OLD.device_type) OR (NEW.device_type IS NOT NULL AND OLD.device_type IS NULL) OR (NEW.device_type IS NULL AND OLD.device_type IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "device_type",OLD.device_type,NEW.device_type,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.core_id <> OLD.core_id) OR (NEW.core_id IS NOT NULL AND OLD.core_id IS NULL) OR (NEW.core_id IS NULL AND OLD.core_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "core_id",OLD.core_id,NEW.core_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.model_id <> OLD.model_id) OR (NEW.model_id IS NOT NULL AND OLD.model_id IS NULL) OR (NEW.model_id IS NULL AND OLD.model_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "model_id",OLD.model_id,NEW.model_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.name <> OLD.name) OR (NEW.name IS NOT NULL AND OLD.name IS NULL) OR (NEW.name IS NULL AND OLD.name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "name",OLD.name,NEW.name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.status <> OLD.status) OR (NEW.status IS NOT NULL AND OLD.status IS NULL) OR (NEW.status IS NULL AND OLD.status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "status",OLD.status,NEW.status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.serial_number <> OLD.serial_number) OR (NEW.serial_number IS NOT NULL AND OLD.serial_number IS NULL) OR (NEW.serial_number IS NULL AND OLD.serial_number IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "serial_number",OLD.serial_number,NEW.serial_number,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.well_id <> OLD.well_id) OR (NEW.well_id IS NOT NULL AND OLD.well_id IS NULL) OR (NEW.well_id IS NULL AND OLD.well_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "well_id",OLD.well_id,NEW.well_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.tank_id <> OLD.tank_id) OR (NEW.tank_id IS NOT NULL AND OLD.tank_id IS NULL) OR (NEW.tank_id IS NULL AND OLD.tank_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "DEVICE", "tank_id",OLD.tank_id,NEW.tank_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.is_deleted = 1) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "DELETE", "DEVICE", "id",OLD.id,null,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
END;

--USER TABLE


DROP TRIGGER IF EXISTS before_user_update_delete;
CREATE TRIGGER before_user_update_delete BEFORE UPDATE ON user
FOR EACH ROW
BEGIN
IF (NEW.comment <> OLD.comment) OR (NEW.comment IS NOT NULL AND OLD.comment IS NULL) OR (NEW.comment IS NULL AND OLD.comment IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "comment",OLD.comment,NEW.comment,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.core_user_id <> OLD.core_user_id) OR (NEW.core_user_id IS NOT NULL AND OLD.core_user_id IS NULL) OR (NEW.core_user_id IS NULL AND OLD.core_user_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "core_user_id",OLD.core_user_id,NEW.core_user_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.email <> OLD.email) OR (NEW.email IS NOT NULL AND OLD.email IS NULL) OR (NEW.email IS NULL AND OLD.email IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "email",OLD.email,NEW.email,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.failed_attempt_count <> OLD.failed_attempt_count) OR (NEW.failed_attempt_count IS NOT NULL AND OLD.failed_attempt_count IS NULL) OR (NEW.failed_attempt_count IS NULL AND OLD.failed_attempt_count IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "failed_attempt_count",OLD.failed_attempt_count,NEW.failed_attempt_count,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.name <> OLD.name) OR (NEW.name IS NOT NULL AND OLD.name IS NULL) OR (NEW.name IS NULL AND OLD.name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "name",OLD.name,NEW.name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.status <> OLD.status) OR (NEW.status IS NOT NULL AND OLD.status IS NULL) OR (NEW.status IS NULL AND OLD.status IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "status",OLD.status,NEW.status,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.password_last_change_date <> OLD.password_last_change_date) OR (NEW.password_last_change_date IS NOT NULL AND OLD.password_last_change_date IS NULL) OR (NEW.password_last_change_date IS NULL AND OLD.password_last_change_date IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "password_last_change_date",OLD.password_last_change_date,NEW.password_last_change_date,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.phone <> OLD.phone) OR (NEW.phone IS NOT NULL AND OLD.phone IS NULL) OR (NEW.phone IS NULL AND OLD.phone IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "phone",OLD.phone,NEW.phone,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.user_name <> OLD.user_name) OR (NEW.user_name IS NOT NULL AND OLD.user_name IS NULL) OR (NEW.user_name IS NULL AND OLD.user_name IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "user_name",OLD.user_name,NEW.user_name,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.address_id <> OLD.address_id) OR (NEW.address_id IS NOT NULL AND OLD.address_id IS NULL) OR (NEW.address_id IS NULL AND OLD.address_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "address_id",OLD.address_id,NEW.address_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.operator_id <> OLD.operator_id) OR (NEW.operator_id IS NOT NULL AND OLD.operator_id IS NULL) OR (NEW.operator_id IS NULL AND OLD.operator_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "operator_id",OLD.operator_id,NEW.operator_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.role_id <> OLD.role_id) OR (NEW.role_id IS NOT NULL AND OLD.role_id IS NULL) OR (NEW.role_id IS NULL AND OLD.role_id IS NOT NULL) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "UPDATE", "USER", "role_id",OLD.role_id,NEW.role_id,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
IF (NEW.is_deleted = 1) THEN
 INSERT INTO `audit_log` (`id_user`, `action`, `entity`, `name`,`old_value`,`new_value`,`changed_on`,`changed_at`)
 VALUES (NEW.modified_by, "DELETE", "USER", "id",OLD.id,null,CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP());
END IF;
END;