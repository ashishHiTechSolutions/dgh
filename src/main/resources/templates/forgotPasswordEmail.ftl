<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>DGH-RTMS Registration</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- use the font -->
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            font-size: 48px;
        }
    </style>
</head>
<body style="margin: 0; padding: 0;">

    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
        <tr>
            <td align="center" bgcolor="#78ab46" style="padding: 40px 0 30px 0;">
				<h3>Directorate General of Hydrocarbons</h3>
            </td>
        </tr>
        <tr>
            <td bgcolor="#eaeaea" style="padding: 40px 30px 40px 30px;">
                <p>Dear ${email.name},</b></p>
               	<br>
                <p>We have received your password change request. Your password should be at least 9 characters (alpha & numeric) in length. Passwords are case-sensitive.</p>
                <p>Click on the below link to enter your new password and the OTP received on your registered mobile number.</p>
               	<br>
                <p><span><a href="${email.passwordLink}">Click Here</a> to set your new password.</span></p>
                <p>This link is active only for 24 hours. </p>
            </td>
        </tr>
        <tr>
            <td bgcolor="#777777" style="padding: 30px 30px 30px 30px;">
                <p>Admin</p>
                <p>DGH-RTMS Team</p>
            </td>
        </tr>
    </table>

</body>
</html>