package com.logicladder.dgh.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

import lombok.ToString;

@ToString
public class TariffDto extends SuperDto {

	@NotEmpty
	@Size(max = 60)
	private String name;

	@NotNull
	private Long deviceId;

	private EntityStatus status = EntityStatus.ACTIVE;

	@NotNull
	private Date tariffStartDate;

	@NotNull
	private Date tariffEndDate;

	@NotNull
	private Double rate;

	private Integer waterCutPercentage = 0;
	
	private DeviceDto deviceDto;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Long deviceId) {
		this.deviceId = deviceId;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}


	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Integer getWaterCutPercentage() {
		return waterCutPercentage;
	}

	public void setWaterCutPercentage(Integer waterCutPercentage) {
		this.waterCutPercentage = waterCutPercentage;
	}

	public DeviceDto getDeviceDto() {
		return deviceDto;
	}

	public void setDeviceDto(DeviceDto deviceDto) {
		this.deviceDto = deviceDto;
	}

	public Date getTariffStartDate() {
		return tariffStartDate;
	}

	public void setTariffStartDate(Date tariffStartDate) {
		this.tariffStartDate = tariffStartDate;
	}

	public Date getTariffEndDate() {
		return tariffEndDate;
	}

	public void setTariffEndDate(Date tariffEndDate) {
		this.tariffEndDate = tariffEndDate;
	}

	
}
