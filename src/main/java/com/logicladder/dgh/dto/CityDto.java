/*
 * 
 */
package com.logicladder.dgh.dto;

public class CityDto extends SuperDto {

	private String name;

	private StateDto state;

	public CityDto() {
		super();
	}
	
	public CityDto(String name, StateDto stateDto, Long id) {
		this.setId(id);
		this.name = name;
		this.state = stateDto;
	}

	public String getName() {
		return this.name;
	}

	public StateDto getState() {
		return this.state;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setState(StateDto state) {
		this.state = state;
	}
}
