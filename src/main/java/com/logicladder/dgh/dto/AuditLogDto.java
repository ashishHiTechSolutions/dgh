package com.logicladder.dgh.dto;

import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.EnumUtils;

import javax.persistence.*;
import java.util.Date;

public class AuditLogDto {

    private Long id;
    private String email;
    private String userType;
    private Long userId;
    private String userName;
    private EnumUtils.AuditAction action;
    private String entity;
    private String name;
    private String oldValue;
    private String newValue;
    @Temporal(TemporalType.DATE)
    private Date changedOn;
    @Temporal(TemporalType.TIME)
    private Date changedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public EnumUtils.AuditAction getAction() {
        return action;
    }

    public void setAction(EnumUtils.AuditAction action) {
        this.action = action;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public Date getChangedOn() {
        return changedOn;
    }

    public void setChangedOn(Date changedOn) {
        this.changedOn = changedOn;
    }

    public Date getChangedAt() {
        return changedAt;
    }

    public void setChangedAt(Date changedAt) {
        this.changedAt = changedAt;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
