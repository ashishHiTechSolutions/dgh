/*
 * 
 */
package com.logicladder.dgh.dto;

import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.ProducingStatus;

import lombok.ToString;

@ToString
public class FacilityDto extends SuperDto {

	@NotEmpty
	@Size(max = 60)
	private String name;

	@NotNull
	private Integer fieldArea;

	@NotEmpty
	@Size(max = 60)
	private String code;

	private EntityStatus status;

	private ProducingStatus fieldStatus;

	private Long coreId;

	private AddressDto addressDto;

	@NotNull
	private Long contractAreaId;

	private String contractStartDate;

	private String contractEndDate;

	private Long operatorId;

	private Set<TankDto> tanks;

	private Set<WellDto> wells;

	private Long facilityOperatorId;

	public Long getContractAreaId() {
		return this.contractAreaId;
	}

	public String getContractEndDate() {
		return this.contractEndDate;
	}

	public String getContractStartDate() {
		return this.contractStartDate;
	}

	public Long getCoreId() {
		return this.coreId;
	}

	public Integer getFieldArea() {
		return this.fieldArea;
	}

	public String getName() {
		return this.name;
	}

	public Long getOperatorId() {
		return this.operatorId;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public Set<TankDto> getTanks() {
		return this.tanks;
	}

	public Set<WellDto> getWells() {
		return this.wells;
	}

	public void setContractAreaId(Long contractAreaId) {
		this.contractAreaId = contractAreaId;
	}

	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setFieldArea(Integer fieldArea) {
		this.fieldArea = fieldArea;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setTanks(Set<TankDto> tanks) {
		this.tanks = tanks;
	}

	public void setWells(Set<WellDto> wells) {
		this.wells = wells;
	}

	public Long getFacilityOperatorId() {
		return facilityOperatorId;
	}

	public void setFacilityOperatorId(Long facilityOperatorId) {
		this.facilityOperatorId = facilityOperatorId;
	}

	public AddressDto getAddressDto() {
		return addressDto;
	}

	public void setAddressDto(AddressDto addressDto) {
		this.addressDto = addressDto;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ProducingStatus getFieldStatus() {
		return fieldStatus;
	}

	public void setFieldStatus(ProducingStatus fieldStatus) {
		this.fieldStatus = fieldStatus;
	}

}
