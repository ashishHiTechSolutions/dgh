package com.logicladder.dgh.dto;

public class MakeDto extends SuperDto {

	private String name;

	private Long coreId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCoreId() {
		return coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}
	
	
}
