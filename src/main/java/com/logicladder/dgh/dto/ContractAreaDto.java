/*
 * 
 */
package com.logicladder.dgh.dto;

import java.util.Set;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.logicladder.dgh.enums.EnumUtils.ContractAreaType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.ProducingStatus;

public class ContractAreaDto extends SuperDto {

	@NotEmpty
	@Size(max=60)
	private String name;

	private ContractAreaType type;

	private Integer areaCovered;

	@NotEmpty
	@Size(max=100)
	private String code;

	private ProducingStatus producingStatus;

	private EntityStatus status;

	private Long coreId;

	private Set<FacilityDto> facilities;

	private Long basinId;
	
	@Enumerated(EnumType.STRING)
	private com.logicladder.dgh.enums.ContractAreaType areaType;

	public Integer getAreaCovered() {
		return this.areaCovered;
	}

	public Long getBasinId() {
		return this.basinId;
	}

	public Long getCoreId() {
		return this.coreId;
	}

	public Set<FacilityDto> getFacilities() {
		return this.facilities;
	}

	public String getName() {
		return this.name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public ContractAreaType getType() {
		return this.type;
	}

	public void setAreaCovered(Integer areaCovered) {
		this.areaCovered = areaCovered;
	}

	public void setBasinId(Long basinId) {
		this.basinId = basinId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setFacilities(Set<FacilityDto> facilities) {
		this.facilities = facilities;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setType(ContractAreaType type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ProducingStatus getProducingStatus() {
		return producingStatus;
	}

	public void setProducingStatus(ProducingStatus producingStatus) {
		this.producingStatus = producingStatus;
	}

	public com.logicladder.dgh.enums.ContractAreaType getAreaType() {
		return areaType;
	}

	public void setAreaType(com.logicladder.dgh.enums.ContractAreaType areaType) {
		this.areaType = areaType;
	}

}
