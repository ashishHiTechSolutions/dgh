/*
 * 
 */
package com.logicladder.dgh.dto;

import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.WellProductionType;
import com.logicladder.dgh.enums.WellStatus;
import com.logicladder.dgh.enums.WellType;

public class WellDto extends SuperDto {

	private Long coreId;

	@NotNull
	private Long facilityId;

	@NotEmpty
	@Size(max=60)
	private String name;

	private EntityStatus status;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Set<TankDto> tanks;

	private WellProductionType wellProductionType;

	private WellStatus wellStatus;

	private WellType wellType;

	public Long getCoreId() {
		return this.coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public Set<TankDto> getTanks() {
		return this.tanks;
	}

	public void setTanks(Set<TankDto> tanks) {
		this.tanks = tanks;
	}

	public WellProductionType getWellProductionType() {
		return this.wellProductionType;
	}

	public void setWellProductionType(WellProductionType wellProductionType) {
		this.wellProductionType = wellProductionType;
	}

	public WellStatus getWellStatus() {
		return this.wellStatus;
	}

	public void setWellStatus(WellStatus wellStatus) {
		this.wellStatus = wellStatus;
	}

	public WellType getWellType() {
		return this.wellType;
	}

	public void setWellType(WellType wellType) {
		this.wellType = wellType;
	}

}
