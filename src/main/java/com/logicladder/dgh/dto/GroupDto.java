package com.logicladder.dgh.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.GroupEntityType;

import lombok.ToString;

@ToString
public class GroupDto extends SuperDto{

	@NotEmpty
	@Size(max = 60)
	private String name;

	private EntityStatus status;
	
	private GroupEntityType type;

	@NotEmpty
	private String userList;

	@NotEmpty
	private String entityList;

	private List<Object> entities;

	private List<User> users;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public String getUserList() {
		return userList;
	}

	public void setUserList(String userList) {
		this.userList = userList;
	}

	public String getEntityList() {
		return entityList;
	}

	public void setEntityList(String entityList) {
		this.entityList = entityList;
	}

	public List<Object> getEntities() {
		return entities;
	}

	public void setEntities(List<Object> entities) {
		this.entities = entities;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public GroupEntityType getType() {
		return type;
	}

	public void setType(GroupEntityType type) {
		this.type = type;
	}

}
