package com.logicladder.dgh.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class DeviceParamDto extends SuperDto {

	/*private Object cPCBUnit;
	private Object max;
	private Object min;
	private Object persisted;
	private Object resetValue;
	private Object sPCBUnit;*/
	
	//private Long coreId;

	@NotNull
	private Long stdParamId;
	//private StdParam stdParam;

	@NotEmpty
	private String unit;

	@NotEmpty
	private String uSERUnit;

	public Long getStdParamId() {
		return stdParamId;
	}

	public void setStdParamId(Long stdParamId) {
		this.stdParamId = stdParamId;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getuSERUnit() {
		return uSERUnit;
	}

	public void setuSERUnit(String uSERUnit) {
		this.uSERUnit = uSERUnit;
	}
	
	
}
