package com.logicladder.dgh.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.logicladder.dgh.enums.DeviceType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

import lombok.ToString;

@ToString
public class DeviceDto extends SuperDto {

	@NotBlank
	private String name;

	private Long coreId;

	private DeviceType deviceType;

	@NotNull
	private Long modelId;
	// private Model model;

	private String serialNumber;

	@NotNull
	private Long facilityId;

	// private Facility facility;

	private Long wellId;
	// private Well well;
	private Long tankId;

	// Will save the User custom Unit Data Saved While Saving and Will link to
	// standard stdParam

	@NotNull
	@Valid
	private List<DeviceParamDto> deviceParam;

	private EntityStatus status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCoreId() {
		return coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public Long getModelId() {
		return modelId;
	}

	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Long getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public Long getWellId() {
		return wellId;
	}

	public void setWellId(Long wellId) {
		this.wellId = wellId;
	}

	public List<DeviceParamDto> getDeviceParam() {
		return deviceParam;
	}

	public void setDeviceParam(List<DeviceParamDto> deviceParam) {
		this.deviceParam = deviceParam;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public Long getTankId() {
		return tankId;
	}

	public void setTankId(Long tankId) {
		this.tankId = tankId;
	}

}
