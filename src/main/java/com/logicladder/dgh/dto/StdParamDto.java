package com.logicladder.dgh.dto;

import lombok.ToString;

@ToString
public class StdParamDto extends SuperDto {

	private String type;

	private String name;

	private String paramKey;

	private String stdUnit;

	private String alertLabel;

	private String label;

	private String paramMetric;

	private String molWt;

	private String defaultStdUnit;

	private String mtype;

	private Boolean isCounter;

	private Boolean isConversion;

	private Boolean isAlertEnabled;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParamKey() {
		return paramKey;
	}

	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}

	public String getStdUnit() {
		return stdUnit;
	}

	public void setStdUnit(String stdUnit) {
		this.stdUnit = stdUnit;
	}

	public String getAlertLabel() {
		return alertLabel;
	}

	public void setAlertLabel(String alertLabel) {
		this.alertLabel = alertLabel;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getParamMetric() {
		return paramMetric;
	}

	public void setParamMetric(String paramMetric) {
		this.paramMetric = paramMetric;
	}

	public String getMolWt() {
		return molWt;
	}

	public void setMolWt(String molWt) {
		this.molWt = molWt;
	}

	public String getDefaultStdUnit() {
		return defaultStdUnit;
	}

	public void setDefaultStdUnit(String defaultStdUnit) {
		this.defaultStdUnit = defaultStdUnit;
	}

	public String getMtype() {
		return mtype;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}

	public Boolean getIsCounter() {
		return isCounter;
	}

	public void setIsCounter(Boolean isCounter) {
		this.isCounter = isCounter;
	}

	public Boolean getIsConversion() {
		return isConversion;
	}

	public void setIsConversion(Boolean isConversion) {
		this.isConversion = isConversion;
	}

	public Boolean getIsAlertEnabled() {
		return isAlertEnabled;
	}

	public void setIsAlertEnabled(Boolean isAlertEnabled) {
		this.isAlertEnabled = isAlertEnabled;
	}

}
