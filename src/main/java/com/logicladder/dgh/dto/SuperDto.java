package com.logicladder.dgh.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SuperDto {

	private String createdBy;

	private Date createdDate;

	private Long id;

	@JsonIgnore
	private boolean isDeleted;

	private Date lastModifiedOn;

	private String modifiedBy;

	public String getCreatedBy() {
		return this.createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public Long getId() {
		return this.id;
	}

	public Date getLastModifiedOn() {
		return this.lastModifiedOn;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public boolean isDeleted() {
		return this.isDeleted;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
