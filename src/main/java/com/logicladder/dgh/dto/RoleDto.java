package com.logicladder.dgh.dto;

import java.util.Set;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "createdDate", "pageGroupDto" })
public class RoleDto extends SuperDto {

	@NotEmpty
	@JsonProperty("name")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<PageGroupDto> getPageGroupDto() {
		return pageGroupDto;
	}

	public void setPageGroupDto(Set<PageGroupDto> pageGroupDto) {
		this.pageGroupDto = pageGroupDto;
	}

	@NotEmpty
	@JsonProperty("pageGroupDto")
	private Set<PageGroupDto> pageGroupDto;
}
