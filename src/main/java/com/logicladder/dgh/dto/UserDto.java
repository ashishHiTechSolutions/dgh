/*
 * 
 */
package com.logicladder.dgh.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.logicladder.dgh.enums.ApprovalStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "userName", "name", "status", "phone", "address", "city", "zip", "home", "role",
		"createdDate", "email", "accessibleEntities", "coreUserId", "password", "operator" })
public class UserDto extends SuperDto {

	@JsonProperty("accessibleEntities")
	@NotNull
	private String accessibleEntities;

	@JsonProperty("address")
	private String addressLine;

	@PositiveOrZero()
	@JsonProperty("cityId")
	private Long cityId;

	@JsonProperty("coreUserId")
	private Long coreUserId;

	@NotEmpty
	@Email()
	@JsonProperty("email")
	private String email;

	@JsonProperty("home")
	private String home;

	@NotEmpty
	@Size(min = 2)
	@JsonProperty("name")
	private String name;

	@JsonProperty("operatorId")
	private Long operatorId;

	@NotEmpty
	@Pattern(regexp = "^[7-9][0-9]{9}$")
	@JsonProperty("phone")
	private String phone;

	@NotNull()
	@JsonProperty("status")
	private ApprovalStatus status;

	@NotEmpty
	@Size(min = 2)
	@Email()
	@JsonProperty("userName")
	private String userName;

	@JsonProperty("roleId")
	private Long userRoleId;

	@JsonProperty("zip")
	private String zip;
	
	@Size(max=1000)
	private String comment;

	public String getAccessibleEntities() {
		return this.accessibleEntities;
	}

	public String getAddressLine() {
		return this.addressLine;
	}

	public Long getCityId() {
		return this.cityId;
	}

	public Long getCoreUserId() {
		return this.coreUserId;
	}

	public String getEmail() {
		return this.email;
	}

	public String getHome() {
		return this.home;
	}

	public String getName() {
		return this.name;
	}

	public Long getOperatorId() {
		return this.operatorId;
	}

	public String getPhone() {
		return this.phone;
	}

	public ApprovalStatus getStatus() {
		return this.status;
	}

	public String getUserName() {
		return this.userName;
	}

	public Long getUserRoleId() {
		return this.userRoleId;
	}

	public String getZip() {
		return this.zip;
	}

	public void setAccessibleEntities(String accessibleEntities) {
		this.accessibleEntities = accessibleEntities;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public void setCoreUserId(Long coreUserId) {
		this.coreUserId = coreUserId;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setStatus(ApprovalStatus status) {
		this.status = status;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
