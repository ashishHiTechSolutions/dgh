package com.logicladder.dgh.dto;

import java.util.List;

public class AuditResponseDto {

    private List<AuditLogDto> auditLogs;
    private int page;
    private int size;
    private long totalElements;
    private int total;

    public List<AuditLogDto> getAuditLogs() {
        return auditLogs;
    }

    public void setAuditLogs(List<AuditLogDto> auditLogs) {
        this.auditLogs = auditLogs;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
