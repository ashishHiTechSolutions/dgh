/*
 * 
 */
package com.logicladder.dgh.dto;

import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.logicladder.dgh.enums.EnumUtils.BasinType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

import lombok.ToString;

@ToString
public class BasinDto extends SuperDto {

	@NotEmpty
	private String code;
	
	private StateDto stateDto;

	private Set<ContractAreaDto> contractAreas;

	private Long coreId;

	@NotEmpty
	private String description;

	@NotNull
	private Double latitude;

	@NotNull
	private Double longitude;

	// devices and shifts are also additional attributes is coming as Array in Resp
	// from logic ladder side

	@NotEmpty
	private String name;

	// TODO Values of Enum should get saved
	private EntityStatus status;

	private BasinType type;

	private Double zoomLevel;

	public String getCode() {
		return this.code;
	}

	public Set<ContractAreaDto> getContractAreas() {
		return this.contractAreas;
	}

	public Long getCoreId() {
		return this.coreId;
	}

	public String getDescription() {
		return this.description;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public String getName() {
		return this.name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public BasinType getType() {
		return this.type;
	}

	public Double getZoomLevel() {
		return this.zoomLevel;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setContractAreas(Set<ContractAreaDto> contractAreas) {
		this.contractAreas = contractAreas;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setType(BasinType type) {
		this.type = type;
	}

	public void setZoomLevel(Double zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

	public StateDto getStateDto() {
		return stateDto;
	}

	public void setStateDto(StateDto stateDto) {
		this.stateDto = stateDto;
	}

}
