
package com.logicladder.dgh.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.logicladder.dgh.enums.ApprovalStatus;

import lombok.ToString;

@ToString
public class OperatorDto extends SuperDto {

	@NotNull
	@Valid
	private AddressDto addressDto;

	private Long addressId;

	@NotEmpty
	private String companyName;

	private Long coreId;

	@NotEmpty
	private String designation;

	@Email
	@NotEmpty
	private String email;

	@NotEmpty
	private String name;

	@NotEmpty
	private String operatorLicense;

	@NotEmpty
	private String phone;

	private ApprovalStatus status;

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCoreId() {
		return coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOperatorLicense() {
		return operatorLicense;
	}

	public void setOperatorLicense(String operatorLicense) {
		this.operatorLicense = operatorLicense;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public ApprovalStatus getStatus() {
		return status;
	}

	public void setStatus(ApprovalStatus status) {
		this.status = status;
	}

	public AddressDto getAddressDto() {
		return addressDto;
	}

	public void setAddressDto(AddressDto addressDto) {
		this.addressDto = addressDto;
	}

}
