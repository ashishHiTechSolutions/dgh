/*
 * 
 */
package com.logicladder.dgh.dto;

public class CountryDto extends SuperDto {

	private String name;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
