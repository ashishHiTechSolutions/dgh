/*
 * 
 */
package com.logicladder.dgh.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "userName", "password", "confirmPassword", "oldPassword" })
public class UserPasswordChangeDto {

	@JsonProperty("confirmPassword")
	private String confirmPassword;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("oldPassword")
	private String oldPassword;

	@JsonProperty("password")
	private String password;

	@JsonProperty("userName")
	private String userName;

	public String getConfirmPassword() {
		return this.confirmPassword;
	}

	public Long getId() {
		return this.id;
	}

	public String getOldPassword() {
		return this.oldPassword;
	}

	public String getPassword() {
		return this.password;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
