/*
 * 
 */
package com.logicladder.dgh.dto;

import javax.validation.constraints.NotEmpty;

import lombok.ToString;

@ToString
public class AddressDto extends SuperDto {

	@NotEmpty
	private String addressLine;

	private CityDto cityDto;

	public AddressDto() {
		super();
	}

	public AddressDto(String addressLine, CityDto cityDto, String zip) {
		super();
		this.addressLine = addressLine;
		this.cityDto = cityDto;
		this.zip = zip;
	}

	@NotEmpty
	private String zip;

	public String getAddressLine() {
		return this.addressLine;
	}

	public CityDto getCityDto() {
		return this.cityDto;
	}

	public String getZip() {
		return this.zip;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

}
