package com.logicladder.dgh.dto;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "pageName", "createdDate", "functions" })
public class PageDto extends SuperDto {

	@NotEmpty
	@JsonProperty("pageName")
	private String pageName;

	@NotEmpty
	@JsonProperty("functions")
	private Set<FunctionDto> functions = new HashSet<>();

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public Set<FunctionDto> getFunctions() {
		return functions;
	}

	public void setFunctions(Set<FunctionDto> functions) {
		this.functions = functions;
	}

}
