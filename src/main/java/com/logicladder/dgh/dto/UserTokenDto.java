/*
 * 
 */
package com.logicladder.dgh.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserTokenDto extends SuperDto {

	// 30days expires for same device
	private String deviceId;

	private String ipAddress;

	private String jwtToken;

	private String llAuthToken;

	// Same user can login form multiple device
	private String userEmail;

	public String getDeviceId() {
		return this.deviceId;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public String getJwtToken() {
		return this.jwtToken;
	}

	public String getLlAuthToken() {
		return this.llAuthToken;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	public void setLlAuthToken(String llAuthToken) {
		this.llAuthToken = llAuthToken;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
}
