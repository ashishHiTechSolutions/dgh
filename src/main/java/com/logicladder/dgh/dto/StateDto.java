/*
 * 
 */
package com.logicladder.dgh.dto;

import java.util.Set;

public class StateDto extends SuperDto {

	private Integer areaCovered;

	private Set<BasinDto> basins;

	private String code;

	private Long coreSystemOperatorId;

	private CountryDto country;

	private Double latitude;

	private Double longitude;

	private String name;

	private Double zoomLevel;

	public Integer getAreaCovered() {
		return this.areaCovered;
	}

	public Set<BasinDto> getBasins() {
		return this.basins;
	}

	public String getCode() {
		return this.code;
	}

	public Long getCoreSystemOperatorId() {
		return this.coreSystemOperatorId;
	}

	public CountryDto getCountry() {
		return this.country;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public String getName() {
		return this.name;
	}

	public Double getZoomLevel() {
		return this.zoomLevel;
	}

	public void setAreaCovered(Integer areaCovered) {
		this.areaCovered = areaCovered;
	}

	public void setBasins(Set<BasinDto> basins) {
		this.basins = basins;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCoreSystemOperatorId(Long coreSystemOperatorId) {
		this.coreSystemOperatorId = coreSystemOperatorId;
	}

	public void setCountry(CountryDto country) {
		this.country = country;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setZoomLevel(Double zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

}
