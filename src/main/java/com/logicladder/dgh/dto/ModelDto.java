package com.logicladder.dgh.dto;

import java.util.HashSet;
import java.util.Set;

public class ModelDto extends SuperDto {

	private String name;

	private MakeDto make;
	
	private Long makeId;

	private Set<ParamDto> params = new HashSet<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MakeDto getMake() {
		return make;
	}

	public void setMake(MakeDto make) {
		this.make = make;
	}

	public Set<ParamDto> getParams() {
		return params;
	}

	public void setParams(Set<ParamDto> params) {
		this.params = params;
	}

	public Long getMakeId() {
		return makeId;
	}

	public void setMakeId(Long makeId) {
		this.makeId = makeId;
	}
	
}
