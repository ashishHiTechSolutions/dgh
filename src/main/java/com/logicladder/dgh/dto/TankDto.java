/*
 * 
 */
package com.logicladder.dgh.dto;

import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

import lombok.NonNull;
import lombok.ToString;

@ToString
public class TankDto extends SuperDto {

	private Long coreId;

	private Double diameter;

	@NonNull
	private Long facilityId;

	private Double height;

	private String lastCalibration;

	@NotEmpty
	@Size(max = 60)
	private String name;

	private EntityStatus status;

	private Double capacity;

	private String tagNumber;

	// Temp Var being user for taking input
	private Long[] wellIds;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Set<WellDto> wells;

	public Long getCoreId() {
		return this.coreId;
	}

	public Double getDiameter() {
		return this.diameter;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public Double getHeight() {
		return this.height;
	}

	public String getLastCalibration() {
		return this.lastCalibration;
	}

	public String getName() {
		return this.name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public Long[] getWellIds() {
		return this.wellIds;
	}

	public Set<WellDto> getWells() {
		return this.wells;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setDiameter(Double diameter) {
		this.diameter = diameter;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public void setLastCalibration(String lastCalibration) {
		this.lastCalibration = lastCalibration;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setWellIds(Long[] wellIds) {
		this.wellIds = wellIds;
	}

	public void setWells(Set<WellDto> wells) {
		this.wells = wells;
	}

	public Double getCapacity() {
		return capacity;
	}

	public void setCapacity(Double capacity) {
		this.capacity = capacity;
	}

	public String getTagNumber() {
		return tagNumber;
	}

	public void setTagNumber(String tagNumber) {
		this.tagNumber = tagNumber;
	}

}
