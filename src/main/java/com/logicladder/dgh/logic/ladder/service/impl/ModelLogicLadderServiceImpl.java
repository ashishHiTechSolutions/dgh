package com.logicladder.dgh.logic.ladder.service.impl;

import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import com.logicladder.dgh.app.exception.CustomInValidRequestException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.logic.ladder.service.IModelLogicLadderService;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.controller.CusAuthenticationException;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderConstant;
import com.logicladder.dgh.thirdparty.response.dto.Model;
import com.logicladder.dgh.util.Utility;

@Component
public class ModelLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements IModelLogicLadderService {

	@Override
	public Model[] getMakeModel(Long makeId) {

		HttpHeaders headers = getRequestHeaders(null);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		String url = Utility.makeLogicLadderApisUrl(LogicLadderConstant.LL_MODEL_ENITY_GET_URL);

		String parturl = url.replace("{{MAKE-ID}}", makeId.toString());
		Model[] models = null;
		ResponseEntity<String> tpModelListtResponse = null;
		try {

			tpModelListtResponse = restTemplate.exchange(parturl.toString(), HttpMethod.GET, entity, String.class);
			models = this.convertStringToModelArray(tpModelListtResponse.getBody());
		} catch (RestClientException httpError) {
			logger.error("Error from core system" + httpError);
			throw new CusAuthenticationException(Response.of(ErrorCode.LOGIC_LADDER_AUTHENTICATION,
					httpError.getMessage(), ApplicationConstants.ResponseMessage.FAILURE));
		} catch (IOException e) {
			logger.error("Error While parsing data ", e);
			throw new CustomInValidRequestException(Response.of(ErrorCode.GLOBAL, e.getMessage(),
					ApplicationConstants.ResponseMessage.FAILURE));
		}

		return models;
	}

	public Model[] convertStringToModelArray(String json) throws IOException {
		Model[] obj = null;
		obj = mapper.readValue(json, Model[].class);
		return obj;

	}

}
