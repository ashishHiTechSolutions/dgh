/*
 * 
 */
package com.logicladder.dgh.logic.ladder.service;

import java.io.IOException;

import org.springframework.http.ResponseEntity;

import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.response.dto.Entity;

public interface IGenericLogicLadderService {

	public Entity createEntity(GenricEntityCreationRequestDto entity, String authToken);

	public ResponseEntity<String> removeEntity(Integer id);

	public Entity updateEntity(GenricEntityCreationRequestDto entity, Long id, String authToken);

	public Entity convertStringToObject(String json) throws IOException;

	public Entity[] convertStringToObjectArray(String json) throws IOException;
}
