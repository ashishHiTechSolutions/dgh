package com.logicladder.dgh.logic.ladder.service.impl;

import java.util.Collections;
import java.util.HashMap;

import javax.xml.ws.http.HTTPException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.logicladder.dgh.components.RequestResponseLoggingInterceptor;
import com.logicladder.dgh.dto.UserPasswordChangeDto;
import com.logicladder.dgh.response.LoginResponse;
import com.logicladder.dgh.thirdparty.request.dto.UserPatchRequest;
import com.logicladder.dgh.thirdparty.request.dto.UserRequest;
import com.logicladder.dgh.thirdparty.response.dto.User;
import com.logicladder.dgh.util.Utility;

public class LogicLadderUserAPI {

	private static final Logger logger = LogManager.getLogger(LogicLadderUserAPI.class);

	public static User createUser(String token, com.logicladder.dgh.entity.app.User user)
			throws HTTPException {
		RestTemplate restTemplate = new RestTemplate();
		LogicLadderUserAPI.logger.info("Inside createUser : " + token);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.ll_auth_token_header"), token);

		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		UserRequest userRequest = new UserRequest();
		userRequest.userMapper(user);

		String requestJson = Utility.convertObjectToJsonString(userRequest);

		LogicLadderUserAPI.logger.info("Inside createUser1 : " + requestJson);

		HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);

		StringBuffer url = new StringBuffer((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append((String) Utility
						.getApplicationDefaultProperties().get("constant.logic.ladder.constants.url_users"));
		boolean isUpdate = (user.getCoreUserId() != null && user.getCoreUserId() > 0) ? true : false;
		if (isUpdate) {
			url.append("/");
			url.append(user.getCoreUserId());
		}
		User userResponse = null;
		ResponseEntity<User> tpUserResponse = null;

		try {
			tpUserResponse = restTemplate.exchange(url.toString(), isUpdate ? HttpMethod.PUT
					: HttpMethod.POST, entity, User.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

		LogicLadderUserAPI.logger.info("Inside createUser2 : " + tpUserResponse);

		userResponse = tpUserResponse.getBody();

		return userResponse;
	}

	public static LoginResponse login(String username, String password) {

		LogicLadderUserAPI.logger.info("Login Request recieved for Username : " + username);

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		MultiValueMap<String, String> parametersMap = new LinkedMultiValueMap<>();
		parametersMap.add("userName", username);
		parametersMap.add("password", password);

		StringBuffer url = new StringBuffer((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append("/login/json?type=p");
		LoginResponse loginResponse = null;

		try {
			loginResponse = restTemplate.postForObject(url.toString(), parametersMap, LoginResponse.class);

		} catch (RestClientException e) {
			e.printStackTrace();
			throw e;

		}

		LogicLadderUserAPI.logger.info("LogicLadderUserApi.LoginResponse : " + loginResponse);
		return loginResponse;
	}

	public static String resetUserPassword(UserPasswordChangeDto userPasswordRequest, String token) {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();

		// Set Password
		StringBuffer url = new StringBuffer((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append((String) Utility
						.getApplicationDefaultProperties().get(
								"constant.logic.ladder.constants.url_reset_password"));
		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.ll_auth_token_header"), token);
		HashMap<String, String> map = new HashMap<>();
		map.put("oldPassword", userPasswordRequest.getOldPassword());
		map.put("newPassword", userPasswordRequest.getPassword());
		map.put("userName", userPasswordRequest.getUserName());
		HttpEntity<HashMap<String, String>> entityMap = new HttpEntity<>(map, headers);
		ResponseEntity<String> setPassword = null;
		try {
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			setPassword = restTemplate.exchange(url.toString(), HttpMethod.PUT, entityMap, String.class);

		} catch (HttpClientErrorException httpError) {
			LogicLadderUserAPI.logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			LogicLadderUserAPI.logger.error("Error in middle layer" + e);
		}

		return setPassword.getBody();
	}

	public static String setPassword(com.logicladder.dgh.entity.app.User user, String password,
			String confirmPassword) {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();

		// Set Password
		StringBuffer url = new StringBuffer((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append((String) Utility
						.getApplicationDefaultProperties().get(
								"constant.logic.ladder.constants.url_password"));
		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("confirmPassword", confirmPassword);
		map.add("password", password);
		map.add("userId", user.getCoreUserId().toString());
		map.add("email", user.getUserName());
		HttpEntity<MultiValueMap<String, String>> entityMap = new HttpEntity<>(map, headers);
		ResponseEntity<String> setPassword = null;
		try {

			setPassword = restTemplate.exchange(url.toString(), HttpMethod.POST, entityMap, String.class);

		} catch (HttpClientErrorException httpError) {
			LogicLadderUserAPI.logger.error("Error from core system" + httpError);
			throw httpError;
		} catch (Exception e) {
			LogicLadderUserAPI.logger.error("Error in middle layer" + e);
			throw e;
		}

		return setPassword.getBody();
	}

	public static User setPatch(com.logicladder.dgh.entity.app.User user, UserPatchRequest[] requests,
			String authToken) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.ll_auth_token_header"), authToken);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		String requestJson = Utility.convertObjectToJsonString(requests);

		HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);

		StringBuffer url = new StringBuffer((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append((String) Utility
						.getApplicationDefaultProperties().get("constant.logic.ladder.constants.url_users"))
						.append("/").append(user.getCoreUserId());
		ResponseEntity<User> response = null;

		response = restTemplate.exchange(url.toString(), HttpMethod.PATCH, entity, User.class);

		return response.getBody();
	}
}
