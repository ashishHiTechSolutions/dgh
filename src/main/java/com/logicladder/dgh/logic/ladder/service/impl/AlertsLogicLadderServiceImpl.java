package com.logicladder.dgh.logic.ladder.service.impl;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.logic.ladder.service.IAlertsLogicLadderService;
import com.logicladder.dgh.thirdparty.request.dto.LLCreateAlertRuleRequest;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.AlertRule;
import com.logicladder.dgh.thirdparty.response.dto.TPEntity;
import com.logicladder.dgh.thirdparty.response.dto.TriggeredAlert;
import com.logicladder.dgh.util.Utility;

@Component
public class AlertsLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements
		IAlertsLogicLadderService {

	private static final Logger logger = Logger.getLogger(AlertsLogicLadderServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public AlertRule[] getUserAlerts() {

		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_ALERT_RULES).append("?status=ENABLED");
		HttpEntity<String> entity = new HttpEntity<>("parameters", this.getRequestHeaders(null));
		ResponseEntity<String> tpUserResponse = null;
		AlertRule[] alertsArr = null;
		logger.info("getUserAlerts() url :: " + url.toString());

		try {
			tpUserResponse = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, String.class);
			logger.info("getUserAlerts() tpUserResponse :: " + tpUserResponse);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer" + e);
		}
		try {
			if (tpUserResponse != null) {
				alertsArr = (AlertRule[]) Utility.convertStringToAlertRuleArray(tpUserResponse.getBody());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("unable to parse response into object :: " + e);
		}
		return alertsArr;

	}

	@Override
	public TriggeredAlert[] getTriggeredAlerts(int pageNumber) {
		// TODO - reduce count to 10 and get from constants
		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_ALERTS).append("?count=1000&pageNum=" + pageNumber);
		HttpEntity<String> entity = new HttpEntity<>("parameters", this.getRequestHeaders(null));
		ResponseEntity<String> tpUserResponse = null;
		TriggeredAlert[] alertsArr = null;
		logger.info("getTriggeredAlerts() url :: " + url.toString());
		try {
			tpUserResponse = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, String.class);
			logger.info("getTriggeredAlerts() tpUserResponse :: " + tpUserResponse);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer" + e);
		}

		try {
			if (tpUserResponse != null) {
				alertsArr = (TriggeredAlert[]) Utility.convertStringToTriggerArray(tpUserResponse.getBody());
			}
		} catch (IOException e) {
			logger.error("unable to parse response into object" + e);
		}
		return alertsArr;

	}

	@Override
	public AlertMetric[] getMetrics(String entityId) {

		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_METRICS).append("?nodeIds=" + entityId);
		HttpEntity<String> entity = new HttpEntity<>("parameters", this.getRequestHeaders(null));
		AlertMetric[] alertArr = null;
		ResponseEntity<String> tpUserResponse = null;
		try {
			tpUserResponse = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, String.class);
			logger.info("getMetrics() tpUserResponse :: " + tpUserResponse);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer" + e);
		}
		try {
			alertArr = (AlertMetric[]) Utility.convertStringToAlertMetricArray(tpUserResponse.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return alertArr;

	}

	@Override
	public String createAlertRule(LLCreateAlertRuleRequest request) {
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		String requestJson = Utility.convertObjectToJsonString(request);
		HttpEntity<String> entity = new HttpEntity<String>(requestJson, getRequestHeaders(null));
		logger.info("createAlertRule requestJson :: " + requestJson);
		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_ALERT_RULE);
		logger.info("createAlertRule url :: " + url);
		ResponseEntity<String> tpAlertRuleResponse = null;
		try {
			tpAlertRuleResponse = restTemplate.exchange(url.toString(), HttpMethod.POST, entity,
					String.class);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system " + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer " + e);
		}

		return tpAlertRuleResponse.getBody();
	}

	@Override
	public TPEntity[] getEntities(long entityId, boolean fullEnitites) {

		logger.info("Inside getEntities");
		HttpEntity<String> entity = new HttpEntity<>("parameters", this.getRequestHeaders(null));

		StringBuffer url = null;
		if (entityId == 0L) {
			if (fullEnitites) {
				url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
						ApplicationConstants.LogicLadderAPI.URL_FULL_ENTITIES);
			} else {
				url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
						ApplicationConstants.LogicLadderAPI.URL_ENTITIES);
			}
		} else {
			url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
					ApplicationConstants.LogicLadderAPI.URL_ENTITIES).append("?entityId=").append(entityId);
		}

		logger.debug("Core request url" + url.toString());

		ResponseEntity<String> tpEntityListResponse = null;
		TPEntity[] tpEntity = null;
		try {

			tpEntityListResponse = restTemplate.exchange(url.toString(), HttpMethod.GET, entity,
					String.class);

		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			logger.error("Error from core system" + e);
		}

		try {
			if (tpEntityListResponse != null) {
				tpEntity = Utility.convertStringToObjectArray(tpEntityListResponse.getBody());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("unable to parse core response into object" + e);
		}

		logger.info("Exiting getEntities" + url.toString());
		return tpEntity;

	}

}
