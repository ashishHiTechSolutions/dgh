package com.logicladder.dgh.logic.ladder.service.impl;

import org.springframework.stereotype.Component;
import com.logicladder.dgh.logic.ladder.service.IWellLogicLadderService;

@Component
public class WellLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements
		IWellLogicLadderService {

}
