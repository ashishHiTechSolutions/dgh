/*
 * 
 */
package com.logicladder.dgh.logic.ladder.service.impl;

import org.springframework.stereotype.Component;
import com.logicladder.dgh.logic.ladder.service.IBasinLogicLadderService;

/**
 * The Class BasinLogicLadderServiceImpl.
 *
 * Ideally Should not be marked as @Transactional , Should be managed at DGH
 * service end
 *
 */
@Component
public class BasinLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements
		IBasinLogicLadderService {

}
