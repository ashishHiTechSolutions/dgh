package com.logicladder.dgh.logic.ladder.service.impl;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.controller.CusAuthenticationException;

public abstract class AbstractLogicLadderService {

	public AbstractLogicLadderService(RestTemplate restTemplate2) {
		this.restTemplate = restTemplate2;
	}

	RestTemplate restTemplate;

	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, @Nullable HttpEntity<?> requestEntity,
			Class<T> responseType, Object... uriVariables) throws RestClientException {

		try {
			return restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
		} catch (Exception e) {
			throw new CusAuthenticationException(Response.of(ErrorCode.LOGIC_LADDER_AUTHENTICATION, e.getMessage(),
					ApplicationConstants.ResponseMessage.FAILURE));
		}
	}

}
