package com.logicladder.dgh.logic.ladder.service.impl;

import org.springframework.stereotype.Component;

import com.logicladder.dgh.logic.ladder.service.IFacilityLogicLadderService;

@Component
public class FacilityLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements
		IFacilityLogicLadderService {

}
