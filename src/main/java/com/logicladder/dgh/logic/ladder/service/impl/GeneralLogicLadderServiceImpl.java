package com.logicladder.dgh.logic.ladder.service.impl;

import org.springframework.stereotype.Component;

import com.logicladder.dgh.logic.ladder.service.IGeneralLogicLadderService;

@Component
public class GeneralLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements
		IGeneralLogicLadderService {

}
