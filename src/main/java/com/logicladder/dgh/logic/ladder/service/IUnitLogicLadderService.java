package com.logicladder.dgh.logic.ladder.service;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.thirdparty.response.dto.Units;

@Validated
public interface IUnitLogicLadderService {

	public Units[] getUnits(String stdParamMtype);
}
