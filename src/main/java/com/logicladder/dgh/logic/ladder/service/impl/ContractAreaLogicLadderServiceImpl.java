package com.logicladder.dgh.logic.ladder.service.impl;

import org.springframework.stereotype.Component;

import com.logicladder.dgh.logic.ladder.service.IContractAreaLogicLadderService;

/**
 * The Class ContractAreaLogicLadderServiceImpl.
 * 
 * Ideally Should not be marked as @Transactional , Should be managed
 * at @ContractAreaServiceImpl/Service end
 */

@Component
public class ContractAreaLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements
		IContractAreaLogicLadderService {

}
