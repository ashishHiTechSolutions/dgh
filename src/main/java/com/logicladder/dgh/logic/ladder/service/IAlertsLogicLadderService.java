package com.logicladder.dgh.logic.ladder.service;

import com.logicladder.dgh.thirdparty.request.dto.LLCreateAlertRuleRequest;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.AlertRule;
import com.logicladder.dgh.thirdparty.response.dto.TPEntity;
import com.logicladder.dgh.thirdparty.response.dto.TriggeredAlert;

public interface IAlertsLogicLadderService extends IGenericLogicLadderService {

	public AlertRule[] getUserAlerts();

	public TriggeredAlert[] getTriggeredAlerts(int pageNumber);

	public AlertMetric[] getMetrics(String entityId);

	public String createAlertRule(LLCreateAlertRuleRequest request);

	public TPEntity[] getEntities(long entityId, boolean fullEnitites);

}
