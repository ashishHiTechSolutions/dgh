package com.logicladder.dgh.logic.ladder.service.impl;

import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import com.logicladder.dgh.app.exception.CustomInValidRequestException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.logic.ladder.service.IDeviceLogicLadderService;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.controller.CusAuthenticationException;
import com.logicladder.dgh.thirdparty.request.dto.CreateDeviceRequest;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderConstant;
import com.logicladder.dgh.thirdparty.response.dto.Device;
import com.logicladder.dgh.util.Utility;

@Component
public class DeviceLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements IDeviceLogicLadderService {

	@Override
	public Device createDevice(CreateDeviceRequest request) {
		// http://stage.dgh.energylogiciq.com/api/entities/2387/locatedDevices

		HttpHeaders headers = getRequestHeaders(null);
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		String requestJson = Utility.convertObjectToJsonString(request);
		HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);

		// Create Url --"/entities/{{ENTITY-ID}}/locatedDevices
		// ENTITY-ID Should be Facility Id Whom should Device Should be Attached
		String url = Utility.makeLogicLadderApisUrl(LogicLadderConstant.LL_DEVICE_ENITY_CREATE_URL);
		String parturl = url.replace("{{ENTITY-ID}}", request.getLocation().getId());
		Device device = null;
		ResponseEntity<String> tpDeviceResponse = null;
		try {
			tpDeviceResponse = restTemplate.exchange(parturl, HttpMethod.POST, entity, String.class);
			if (tpDeviceResponse != null) {
				device = convertStringToDevice(tpDeviceResponse.getBody());
			}
		} catch (RestClientException httpError) {
			logger.error("Error from core system" + httpError);
			throw new CusAuthenticationException(Response.of(ErrorCode.LOGIC_LADDER_AUTHENTICATION,
					httpError.getMessage(), ApplicationConstants.ResponseMessage.FAILURE));
		} catch (IOException e) {
			logger.error("Error While parsing data ", e);
			throw new CustomInValidRequestException(
					Response.of(ErrorCode.GLOBAL, e.getMessage(), ApplicationConstants.ResponseMessage.FAILURE));
		}

		return device;
	}

	@Override
	public Device convertStringToDevice(String json) throws IOException {

		Device obj = null;
		obj = mapper.readValue(json, Device.class);
		return obj;

	}

	@Override
	public String deleteDevice(long entityId, long deviceId) {

		HttpHeaders headers = getRequestHeaders(null);
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		// Delete Url ="/entities/{{ENTITY-ID}}/locatedDevices/{{DEVICE-ID}}
		String url = Utility.makeLogicLadderApisUrl(LogicLadderConstant.LL_DEVICE_ENITY_DELETE_URL);
		String pathUrl = url.replace("{ENTITY-ID}}", String.valueOf(entityId)).replace("{{DEVICE-ID}}",
				String.valueOf(deviceId));

		ResponseEntity<String> response = null;
		String resp = null;
		try {
			response = restTemplate.exchange(pathUrl.toString(), HttpMethod.DELETE, entity, String.class);
			if (response != null) {
				resp = response.getBody();
			}
		} catch (RestClientException httpError) {
			logger.error("Error from core system" + httpError);
			throw new CusAuthenticationException(Response.of(ErrorCode.LOGIC_LADDER_AUTHENTICATION,
					httpError.getMessage(), ApplicationConstants.ResponseMessage.FAILURE));
		}

		return resp;

	}
}
