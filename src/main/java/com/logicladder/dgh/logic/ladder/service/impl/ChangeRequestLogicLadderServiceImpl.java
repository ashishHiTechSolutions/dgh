package com.logicladder.dgh.logic.ladder.service.impl;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.logicladder.dgh.app.exception.CustomInValidRequestException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.logic.ladder.service.IAlertsLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IChangeRequestLogicLadderService;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.controller.CusAuthenticationException;
import com.logicladder.dgh.thirdparty.request.dto.CreateDeviceRequest;
import com.logicladder.dgh.thirdparty.request.dto.DeviceDataRequest;
import com.logicladder.dgh.thirdparty.request.dto.LLCreateAlertRuleRequest;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderConstant;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.AlertRule;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequest;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequestMetricData;
import com.logicladder.dgh.thirdparty.response.dto.Device;
import com.logicladder.dgh.thirdparty.response.dto.TPEntity;
import com.logicladder.dgh.thirdparty.response.dto.TriggeredAlert;
import com.logicladder.dgh.util.Utility;

@Component
public class ChangeRequestLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements
		IChangeRequestLogicLadderService {

	private static final Logger logger = Logger.getLogger(ChangeRequestLogicLadderServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public ChangeRequest[] getChangeRequests() {

		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_CHANGE_REQUEST);
		HttpEntity<String> entity = new HttpEntity<>("parameters", this.getRequestHeaders(null));
		
		ResponseEntity<String> tpUserResponse = null;
		ChangeRequest[] changeRequestArr = null;
		logger.info("getChangeRequests() url :: " + url.toString());

		try {
			tpUserResponse = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, String.class);
			logger.info("getChangeRequests() tpUserResponse :: " + tpUserResponse);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer" + e);
		}
		try {
			if (tpUserResponse != null) {
				changeRequestArr = (ChangeRequest[]) Utility.convertStringToChangeRequestArray(tpUserResponse.getBody());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("unable to parse response into object :: " + e);
			e.printStackTrace();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} 
		return changeRequestArr;

	}
	
	
	@Override
	public ChangeRequest getChangeRequest(long id) {

		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_CHANGE_REQUEST).append("/"+id);
		HttpEntity<String> entity = new HttpEntity<>("parameters", this.getRequestHeaders(null));
		ResponseEntity<String> tpUserResponse = null;
		ChangeRequest changeRequest = null;
		logger.info("getChangeRequests() url :: " + url.toString());

		try {
			tpUserResponse = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, String.class);
			logger.info("getChangeRequest() tpUserResponse :: " + tpUserResponse);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer" + e);
		}
		try {
			if (tpUserResponse != null) {
				changeRequest = (ChangeRequest) Utility.convertStringToChangeRequestObj(tpUserResponse.getBody());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("unable to parse response into object :: " + e);
			e.printStackTrace();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} 
		return changeRequest;

	}

	@Override
	public ChangeRequestMetricData getMetricData(String entityId , String metricName, String fromDate, String toDate) {

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = this.getRequestHeaders(null);

		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		DeviceDataRequest dataRequest = new DeviceDataRequest();
		ArrayList<String> dimList = new ArrayList();
		dimList.add("time:instances");

		dataRequest.setMetric(metricName);
		dataRequest.setDims(dimList);

		dataRequest.setStartDate(fromDate);
		dataRequest.setEndDate(toDate);

		String requestJson = Utility.convertObjectToJsonString(dataRequest);

		HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);

		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(ApplicationConstants.LogicLadderAPI.URL_ENTITIES).append("/")
				.append(entityId).append("/data").append("?entityId=" + entityId);
		
		ResponseEntity<ChangeRequestMetricData> deviceData = null;

		deviceData = restTemplate.exchange(url.toString(), HttpMethod.POST, entity, ChangeRequestMetricData.class);

		return deviceData.getBody();

	}
	
	
	@Override
	public String editDataRequest(ChangeRequest request) {
		
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
		String requestJson = Utility.convertObjectToJsonString(request);
		
		HttpEntity<String> entity = new HttpEntity<String>(requestJson, getRequestHeaders(null));
		logger.info("editDataRequest requestJson :: " + requestJson);
		
		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_CHANGE_REQUEST);
		logger.info("editDataRequest url :: " + url);
		
		ResponseEntity<String> tpAlertRuleResponse = null;
		try {
			tpAlertRuleResponse = restTemplate.exchange(url.toString(), HttpMethod.POST, entity,
					String.class);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system " + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer " + e);
		}

		return tpAlertRuleResponse.getBody();
	}
	
	
	@Override
	public String deleteDataRequest(ChangeRequest request) {
		
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
		String requestJson = Utility.convertObjectToJsonString(request);
		
		HttpEntity<String> entity = new HttpEntity<String>(requestJson, getRequestHeaders(null));
		logger.info("editDataRequest requestJson :: " + requestJson);
		
		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_CHANGE_REQUEST);
		logger.info("editDataRequest url :: " + url);
		
		ResponseEntity<String> tpAlertRuleResponse = null;
		try {
			tpAlertRuleResponse = restTemplate.exchange(url.toString(), HttpMethod.DELETE, entity,
					String.class);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system " + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer " + e);
		}

		return tpAlertRuleResponse.getBody();
	}

}
