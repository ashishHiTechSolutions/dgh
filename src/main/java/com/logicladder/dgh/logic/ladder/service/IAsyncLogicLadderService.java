/*
 * 
 */
package com.logicladder.dgh.logic.ladder.service;

import org.springframework.http.ResponseEntity;

import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LLCreateAlertRuleRequest;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.AlertRule;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequest;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequestMetricData;
import com.logicladder.dgh.thirdparty.response.dto.Entity;
import com.logicladder.dgh.thirdparty.response.dto.TPEntity;
import com.logicladder.dgh.thirdparty.response.dto.TriggeredAlert;

public interface IAsyncLogicLadderService {

	public Entity createEntityRequest(GenricEntityCreationRequestDto entityCreationDto, String authToken);

	public ResponseEntity<String> removeEntity(Integer id);

	public Entity updateEntityRequest(GenricEntityCreationRequestDto entityCreationDto, Long id, String authToken);
	
	public AlertRule[] getUserAlerts();

	public TriggeredAlert[] getTriggeredAlerts(int pageNumber);

	public AlertMetric[] getMetrics(String entityId);

	public String createAlertRule(LLCreateAlertRuleRequest request);

	public TPEntity[] getEntities(long entityId, boolean fullEnitites);
	
	public ChangeRequest[] getChangeRequests();
	
	public ChangeRequest getChangeRequest(long id);
	
	public ChangeRequestMetricData getMetricData(String entityId , String metricName, String fromDate, String toDate);
	
	public String editDataRequest(ChangeRequest changeRequest);
	
	public String deleteDataRequest(ChangeRequest changeRequest);
}
