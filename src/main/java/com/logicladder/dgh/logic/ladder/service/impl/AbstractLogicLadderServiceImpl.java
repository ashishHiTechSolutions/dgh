package com.logicladder.dgh.logic.ladder.service.impl;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.logic.ladder.service.IGenericLogicLadderService;
import com.logicladder.dgh.service.IUserAuthDataService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderConstant;
import com.logicladder.dgh.thirdparty.response.dto.Entity;
import com.logicladder.dgh.util.Utility;

public class AbstractLogicLadderServiceImpl implements IGenericLogicLadderService {

	protected static Logger logger = LogManager.getLogger(AbstractLogicLadderServiceImpl.class);

	protected ObjectMapper mapper = new ObjectMapper();

	@Autowired
	protected IUserAuthDataService iUserAuthDataService;

	@Autowired
	protected SecurityContextUserDetails securityContextUserDetails;

	@Autowired
	protected RestTemplate restTemplate;

	public String convertObjectToJsonString(Object obj) {

		String jsonStringCreateEntity = "";

		try {
			jsonStringCreateEntity = this.mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return jsonStringCreateEntity;

	}

	@Override
	public Entity convertStringToObject(String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		Entity obj = null;

		obj = mapper.readValue(json, Entity.class);

		return obj;

	}

	@Override
	public Entity[] convertStringToObjectArray(String json) throws IOException {

		Entity[] obj = null;

		obj = this.mapper.readValue(json, Entity[].class);

		return obj;

	}

	@Override
	public Entity createEntity(GenricEntityCreationRequestDto entity, String authToken) {
		AbstractLogicLadderServiceImpl.logger.info(
				"Creating Entity in core system in AbstractLogicLadderServiceImpl.createEntity");

		this.restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		String requestJson = this.convertObjectToJsonString(entity).replace("}", ",\"properties\": []}");
		HttpEntity<String> requestObject = new HttpEntity<>(requestJson, getRequestHeaders(authToken));
		StringBuilder url = new StringBuilder((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append((String) Utility
						.getApplicationDefaultProperties().get(
								"constant.logic.ladder.constants.url_entities"));
		Entity llEntity = null;
		ResponseEntity<String> entityListResponse = null;
		try {
			entityListResponse = this.restTemplate.exchange(url.toString(), HttpMethod.POST, requestObject,
					String.class);

		} catch (HttpClientErrorException httpError) {
			AbstractLogicLadderServiceImpl.logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			AbstractLogicLadderServiceImpl.logger.error("Error in middle layer" + e);
		}

		try {
			if (entityListResponse != null) {
				llEntity = this.convertStringToObject(entityListResponse.getBody());
			}
		} catch (IOException e) {

			AbstractLogicLadderServiceImpl.logger.error("unable to parse response into object" + e);
		}

		return llEntity;
	}

	@Override
	public ResponseEntity<String> removeEntity(Integer id) {
		AbstractLogicLadderServiceImpl.logger.info(
				"Creating Entity in core system in AbstractLogicLadderServiceImpl.removeEntity");

		HttpEntity<String> entity = new HttpEntity<>("");

		StringBuilder url = new StringBuilder((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append((String) Utility
						.getApplicationDefaultProperties().get(
								"constant.logic.ladder.constants.url_entities")).append("/").append(id);
		ResponseEntity<String> response = null;
		try {
			response = this.restTemplate.exchange(url.toString(), HttpMethod.DELETE, entity, String.class);
		} catch (HttpClientErrorException httpError) {
			AbstractLogicLadderServiceImpl.logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			AbstractLogicLadderServiceImpl.logger.error("Error in middle layer" + e);
		}

		return response;
	}

	@Override
	public Entity updateEntity(GenricEntityCreationRequestDto entity, Long id, String authToken) {
		AbstractLogicLadderServiceImpl.logger.info(
				"Creating Entity in core system in AbstractLogicLadderServiceImpl.updateEntity");

		this.restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		String requestJson = this.convertObjectToJsonString(entity).replace("}",
				",\"properties\": [], \"devices\": [], \"id\":" + id + "}");
		HttpEntity<String> requestObject = new HttpEntity<>(requestJson, getRequestHeaders(authToken));

		StringBuilder url = new StringBuilder((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append((String) Utility
						.getApplicationDefaultProperties().get(
								"constant.logic.ladder.constants.url_entities")).append("/").append(id);
		Entity llEntity = null;
		ResponseEntity<String> response = null;
		try {
			response = this.restTemplate.exchange(url.toString(), HttpMethod.PUT, requestObject,
					String.class);

		} catch (HttpClientErrorException httpError) {
			AbstractLogicLadderServiceImpl.logger.error("Error from core system" + httpError);
		} catch (Exception e) {
			AbstractLogicLadderServiceImpl.logger.error("Error in middle layer" + e);
		}

		try {
			if (response != null) {
				llEntity = this.convertStringToObject(response.getBody());
			}
		} catch (IOException e) {
			AbstractLogicLadderServiceImpl.logger.error("unable to parse response into object" + e);
		}

		return llEntity;
	}


	// authTken will always be null when calling this method
	public HttpHeaders getRequestHeaders(String authToken) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		if (null == authToken || authToken.isEmpty()) {
			authToken = this.iUserAuthDataService.findLLAuthToken(this.securityContextUserDetails
					.getSecurityContextUserName());
			;
		}

		headers.set((String) Utility.getApplicationDefaultProperties().get(LogicLadderConstant.LL_AUTH_TOKEN),
				authToken);
		return headers;

	}
}
