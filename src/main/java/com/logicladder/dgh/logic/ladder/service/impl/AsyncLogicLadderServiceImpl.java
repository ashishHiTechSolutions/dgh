/*
 * 
 */
package com.logicladder.dgh.logic.ladder.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import com.logicladder.dgh.logic.ladder.service.IAlertsLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IChangeRequestLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IContractAreaLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IFacilityLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IGeneralLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IGenericLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.ITankLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IWellLogicLadderService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LLCreateAlertRuleRequest;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.AlertRule;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequest;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequestMetricData;
import com.logicladder.dgh.thirdparty.response.dto.Entity;
import com.logicladder.dgh.thirdparty.response.dto.TPEntity;
import com.logicladder.dgh.thirdparty.response.dto.TriggeredAlert;

/**
 * @author akashd
 *
 */
@Component
public class AsyncLogicLadderServiceImpl implements IAsyncLogicLadderService {

	@Autowired
	private IFacilityLogicLadderService iFacilityLogicLadderService;

	@Autowired
	private IGeneralLogicLadderService iGeneralLogicLadderService;

	@Autowired
	private IContractAreaLogicLadderService iContractAreaLogicLadderService;

	@Autowired
	private IWellLogicLadderService iWellLogicLadderService;

	@Autowired
	private ITankLogicLadderService iTankLogicLadderService;
	
	@Autowired
	private IAlertsLogicLadderService iAlertsLogicLadderService; 

	@Autowired
	private IChangeRequestLogicLadderService iChangeRequestLogicLadderService; 
	
	/* (non-Javadoc)
	 * authToken always should be sent as null. param needed for entity creation at application start up
	 */
	@Override
	public Entity createEntityRequest(GenricEntityCreationRequestDto entityCreationDto, String authToken) {
		return this.getAppropriateService(entityCreationDto).createEntity(entityCreationDto, authToken);
	}

	private IGenericLogicLadderService getAppropriateService(GenricEntityCreationRequestDto dto) {
		if (dto != null) {
			switch (dto.getType()) {
			case "Contract Area":
				return this.iContractAreaLogicLadderService;
			case "facility":
				return this.iFacilityLogicLadderService;
			case "Well":
				return this.iWellLogicLadderService;
			case "Tank":
				return this.iTankLogicLadderService;
			default:
				break;
			}
			return iGeneralLogicLadderService;
		} else {
			return iGeneralLogicLadderService;
		}
	}

	@Override
	public ResponseEntity<String> removeEntity(Integer id) {
		return this.iFacilityLogicLadderService.removeEntity(id);
	}

	@Override
	public Entity updateEntityRequest(GenricEntityCreationRequestDto entityCreationDto, Long id, String authToken) {
		return this.getAppropriateService(entityCreationDto).updateEntity(entityCreationDto, id, authToken);
	}

	@Override
	public AlertRule[] getUserAlerts() {
		return this.iAlertsLogicLadderService.getUserAlerts();
	}

	@Override
	public TriggeredAlert[] getTriggeredAlerts(int pageNumber) {
		return this.iAlertsLogicLadderService.getTriggeredAlerts(pageNumber);
	}

	@Override
	public AlertMetric[] getMetrics(String entityId) {
		return this.iAlertsLogicLadderService.getMetrics(entityId);
	}

	@Override
	public String createAlertRule(LLCreateAlertRuleRequest request) {
		return this.iAlertsLogicLadderService.createAlertRule(request);
	}

	@Override
	public TPEntity[] getEntities(long entityId, boolean fullEnitites) {
		return this.iAlertsLogicLadderService.getEntities(entityId, fullEnitites);
	}
	
	@Override
	public ChangeRequest[] getChangeRequests() {
		return this.iChangeRequestLogicLadderService.getChangeRequests();
	}
	
	@Override
	public ChangeRequest getChangeRequest(long id) {
		return this.iChangeRequestLogicLadderService.getChangeRequest(id);
	}
	
	@Override
	public ChangeRequestMetricData getMetricData(String entityId , String metricName, String fromDate, String toDate) {
		return this.iChangeRequestLogicLadderService.getMetricData(entityId, metricName,fromDate,toDate);
	}
	
	@Override
	public String editDataRequest(ChangeRequest changeRequest){
		return this.iChangeRequestLogicLadderService.editDataRequest(changeRequest);
	}
	
	@Override
	public String deleteDataRequest(ChangeRequest changeRequest){
		return this.iChangeRequestLogicLadderService.deleteDataRequest(changeRequest);
	}
}
