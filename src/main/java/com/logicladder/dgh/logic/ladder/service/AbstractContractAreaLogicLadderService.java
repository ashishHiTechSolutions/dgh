package com.logicladder.dgh.logic.ladder.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logicladder.dgh.configuration.RestTemplateConfig;
import com.logicladder.dgh.dto.ContractAreaDto;
import com.logicladder.dgh.logic.ladder.service.impl.AbstractLogicLadderService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderEnumUtil.EntityTypeLogicLadder;
import com.logicladder.dgh.util.Utility;

public abstract class AbstractContractAreaLogicLadderService extends AbstractLogicLadderService {

	@Autowired
	public AbstractContractAreaLogicLadderService(RestTemplate restTemplate2) {
		super(restTemplate2);
	}

	RestTemplate restTemplate;

	@Autowired
	RestTemplateConfig restTemplateConfig;

	@Autowired
	ObjectMapper objectMapper;
	
	public String makeRequestBody(ContractAreaDto contractAreaDto) {

		GenricEntityCreationRequestDto obj = new GenricEntityCreationRequestDto();
		obj.setName(contractAreaDto.getName());
		obj.setParentId(Integer.valueOf(contractAreaDto.getBasinId().intValue()));
		obj.setType(EntityTypeLogicLadder.CONTRACT_AREA.getDescription());
		return Utility.convertObjectToJsonString(obj);

	}

}
