package com.logicladder.dgh.logic.ladder.service;

import com.logicladder.dgh.thirdparty.response.dto.ChangeRequest;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequestMetricData;

public interface IChangeRequestLogicLadderService extends IGenericLogicLadderService {

	public ChangeRequest[] getChangeRequests();
	
	public ChangeRequest getChangeRequest(long id);
	
	public ChangeRequestMetricData getMetricData(String entityId , String metricName, String fromDate, String toDate);
	
	public String editDataRequest(ChangeRequest changeRequest);
	
	public String deleteDataRequest(ChangeRequest changeRequest);

}
