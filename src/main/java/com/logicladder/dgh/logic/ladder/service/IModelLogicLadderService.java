package com.logicladder.dgh.logic.ladder.service;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.thirdparty.response.dto.Model;

@Validated
public interface IModelLogicLadderService {

	public Model[] getMakeModel( Long makeId);

}
