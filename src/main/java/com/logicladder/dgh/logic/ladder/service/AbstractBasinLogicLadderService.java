package com.logicladder.dgh.logic.ladder.service;

import org.springframework.web.client.RestTemplate;

import com.logicladder.dgh.dto.BasinDto;
import com.logicladder.dgh.logic.ladder.service.impl.AbstractLogicLadderService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderEnumUtil.EntityTypeLogicLadder;
import com.logicladder.dgh.util.Utility;

public abstract class AbstractBasinLogicLadderService extends AbstractLogicLadderService {

	public AbstractBasinLogicLadderService(RestTemplate restTemplate) {
		super(restTemplate);
	}

	public String makeRequestBody(BasinDto basindto) {

		GenricEntityCreationRequestDto obj = new GenricEntityCreationRequestDto();
		obj.setName(basindto.getName());
		obj.setParentId(null);
		obj.setType(EntityTypeLogicLadder.BASIN.getDescription());
		return Utility.convertObjectToJsonString(obj);

	}

}
