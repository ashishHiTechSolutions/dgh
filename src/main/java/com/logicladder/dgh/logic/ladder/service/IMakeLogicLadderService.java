package com.logicladder.dgh.logic.ladder.service;

import com.logicladder.dgh.thirdparty.response.dto.Make;

public interface IMakeLogicLadderService {
	
	public Make[] getAllDeviceMake();

}
