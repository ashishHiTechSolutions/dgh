package com.logicladder.dgh.logic.ladder.service;

import java.io.IOException;

import com.logicladder.dgh.thirdparty.request.dto.CreateDeviceRequest;
import com.logicladder.dgh.thirdparty.response.dto.Device;

public interface IDeviceLogicLadderService extends IGenericLogicLadderService {

	public Device createDevice(CreateDeviceRequest request);

	public Device convertStringToDevice(String json) throws IOException;

	public String deleteDevice(long entityId, long deviceId);
}
