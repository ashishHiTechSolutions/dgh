package com.logicladder.dgh.logic.ladder.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logicladder.dgh.app.exception.CustomInValidRequestException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.logic.ladder.service.IMakeLogicLadderService;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.controller.CusAuthenticationException;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderConstant;
import com.logicladder.dgh.thirdparty.response.dto.Make;
import com.logicladder.dgh.util.Utility;

@Component
public class MakeLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements IMakeLogicLadderService {


	@Autowired
	ObjectMapper objectMapper;

	@Override
	public Make[] getAllDeviceMake() {

		HttpHeaders headers = getRequestHeaders(null);
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		String url = Utility.makeLogicLadderApisUrl(LogicLadderConstant.LL_MAKE_ENITY_GET_URL);

		ResponseEntity<String> tpMakeListResponse = null;
		Make[] make = null;
		try {

			tpMakeListResponse = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, String.class);
			make = this.convertStringToObjectMakeArray(tpMakeListResponse.getBody());
		} catch (RestClientException httpError) {
			logger.error("Error from core system" + httpError);
			throw new CusAuthenticationException(Response.of(ErrorCode.LOGIC_LADDER_AUTHENTICATION,
					"Error while Fething Make data ", ApplicationConstants.ResponseMessage.FAILURE));
		} catch (IOException e) {
			logger.error("Error While parsing data ", e);
			throw new CustomInValidRequestException(Response.of(ErrorCode.GLOBAL, "Error While parsing Make data ",
					ApplicationConstants.ResponseMessage.FAILURE));
		}

		return make;

	}

	public Make[] convertStringToObjectMakeArray(String json) throws IOException {

		Make[] obj = null;

		obj = objectMapper.readValue(json, Make[].class);

		return obj;

	}

}
