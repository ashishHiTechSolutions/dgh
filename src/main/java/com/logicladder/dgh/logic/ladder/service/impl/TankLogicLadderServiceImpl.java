package com.logicladder.dgh.logic.ladder.service.impl;

import org.springframework.stereotype.Component;
import com.logicladder.dgh.logic.ladder.service.ITankLogicLadderService;

@Component
public class TankLogicLadderServiceImpl extends AbstractLogicLadderServiceImpl implements
		ITankLogicLadderService {

}
