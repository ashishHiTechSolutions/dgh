/*
 * 
 */
package com.logicladder.dgh.configuration;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

	@Value("${restTemplate.connectionManagerMaxTotal}")
	private Integer connectionManagerMaxTotal;

	@Value("${restTemplate.connectionRequestTimeout}")
	private Integer connectionRequestTimeout;

	@Value("${restTemplate.connectTimeout}")
	private Integer connectTimeout;

	Logger logger = Logger.getLogger(RestTemplateConfig.class);
	
	@Value("${restTemplate.socketTimeout}")
	private Integer socketTimeout;

	@Bean
	public CloseableHttpClient httpClient(
			PoolingHttpClientConnectionManager poolingHttpClientConnectionManager,
			RequestConfig requestConfig) {
		this.logger.debug("creating CloseableHttpClient  class");

		CloseableHttpClient result = HttpClientBuilder.create().setConnectionManager(
				poolingHttpClientConnectionManager).setDefaultRequestConfig(requestConfig).build();
		return result;
	}

	@Bean
	public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
		this.logger.debug("creating PoolingHttpClientConnectionManager  class");
		PoolingHttpClientConnectionManager result = new PoolingHttpClientConnectionManager();
		result.setMaxTotal(this.connectionManagerMaxTotal == null ? 20 : this.connectionManagerMaxTotal);
		return result;
	}

	@Bean
	public RequestConfig requestConfig() {
		this.logger.debug(String.format(
				"creating RequestConfig setConnectionRequestTimeout = %s  , setConnectTimeout = %s, setSocketTimeout= %s",
				this.connectionRequestTimeout, this.connectTimeout, this.socketTimeout));

		RequestConfig result = RequestConfig.custom()
				.setConnectionRequestTimeout(this.connectionRequestTimeout == null ? 20000 : this.connectionRequestTimeout)
				.setConnectTimeout(this.connectTimeout == null ? 20000 : this.connectTimeout)
				.setSocketTimeout(this.socketTimeout == null ? 20000 : this.socketTimeout).build();
		return result;
	}

	@Bean
	public RestTemplate restTemplate(HttpClient httpClient) {
		this.logger.debug("creating RestTemplate  class ");

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClient);
		return new RestTemplate(requestFactory);
	}
}
