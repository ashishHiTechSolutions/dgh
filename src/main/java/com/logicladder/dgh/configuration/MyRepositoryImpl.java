/*
 * 
 */
package com.logicladder.dgh.configuration;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.logicladder.dgh.entity.app.SuperEntity;

public class MyRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> {

	private final EntityManager entityManager;

	@SuppressWarnings("rawtypes")
	MyRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager) {
		super(entityInformation, entityManager);

		// Keep the EntityManager around to used from the newly introduced methods.
		this.entityManager = entityManager;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <S extends T> S save(S entity) {
		if (((SuperEntity) entity).getId() != null && ((SuperEntity) entity).getId() > 0) {
			this.entityManager.unwrap(Session.class).update(entity);
			return entity;
		}
		return (S) this.entityManager.unwrap(Session.class).save(entity);
	}

}