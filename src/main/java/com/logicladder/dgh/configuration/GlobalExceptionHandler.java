/*
 * 
 */
package com.logicladder.dgh.configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.app.exception.CustomInValidRequestException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.response.ValidationErrorResponse;
import com.logicladder.dgh.security.controller.CusAuthenticationException;
import com.logicladder.dgh.util.Utility;

@RestControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger logger = LogManager.getLogger(GlobalExceptionHandler.class);

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler({ CusAuthenticationException.class })
	public ResponseEntity<?> CusAuthenticationExceptionError(CusAuthenticationException ex) {
		GlobalExceptionHandler.logger.error("Error: ", ex);
		ex.printStackTrace();
		return ResponseEntity.ok().body(ex.getResp());

	}

	@ExceptionHandler({ ObjectOptimisticLockingFailureException.class,
			org.hibernate.StaleObjectStateException.class })
	public ResponseEntity<?> CusAuthenticationExceptionError(org.hibernate.StaleObjectStateException ex) {
		GlobalExceptionHandler.logger.error("Error: ", ex);
		ex.printStackTrace();

		Response<?> responseData = new Response<>(ErrorCode.ENTITY_DOES_NOT_EXITS.getErrorCode(), String
				.format("Entity does not exits for identifies [%s]", ex.getIdentifier()),
				ApplicationConstants.ResponseMessage.FAILURE);

		return ResponseEntity.ok().body(responseData);

	}

	@ExceptionHandler(Throwable.class)
	public ResponseEntity<?> handleAllExceptions(HttpServletRequest request, Throwable ex) {
		GlobalExceptionHandler.logger.info("Exception occured:: URL=" + request.getRequestURL()
				+ " exception is " + ex.getMessage());
		GlobalExceptionHandler.logger.error("Error: ", ex);
		ex.printStackTrace();

		Response<?> responseData = new Response<>(HttpStatus.INTERNAL_SERVER_ERROR.value(),
				ApplicationConstants.ResponseMessage.FAILURE, ex.getMessage(), new HashMap<>());

		return ResponseEntity.ok().body(responseData);
	}

	@ExceptionHandler({ EntityNotFoundException.class })
	public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException ex) {
		GlobalExceptionHandler.logger.error("Error: ", ex);
		ex.printStackTrace();

		Response<?> responseData = new Response<>(ErrorCode.ENTITY_DOES_NOT_EXITS.getErrorCode(), ex
				.getMessage(), ApplicationConstants.ResponseMessage.FAILURE);

		return ResponseEntity.ok().body(responseData);

	}

	@ExceptionHandler({ org.hibernate.exception.ConstraintViolationException.class,
			javax.persistence.PersistenceException.class })
	public ResponseEntity<?> processDBConstraintsErrorHibernateBased(
			org.hibernate.exception.ConstraintViolationException ex) {
		GlobalExceptionHandler.logger.error("Error: ", ex);
		ex.printStackTrace();
		String message = ex.getSQLException().getMessage();

		String apiErrorMessage = null;

		if ((message != null) && message.contains("cannot insert NULL")) {
			apiErrorMessage = this.messageSource.getMessage("db.fields.required.exception", null,
					LocaleContextHolder.getLocale());
		} else if ((message != null) && message.contains("unique constraint")) {
			apiErrorMessage = this.messageSource.getMessage("db.unique.field.exception", null,
					LocaleContextHolder.getLocale());
		} else if ((message != null) && message.contains("Duplicate entry")) {
			apiErrorMessage = message.substring(0, message.lastIndexOf("for"));
			if (apiErrorMessage == null) {
				apiErrorMessage = this.messageSource.getMessage("db.unique.field.duplicate.entry", null,
						LocaleContextHolder.getLocale());
			}
		} else if ((message != null) && message.contains("Column")) {
			apiErrorMessage = ex.getSQLException().getMessage();
		} else if (apiErrorMessage == null) {
			apiErrorMessage = this.messageSource.getMessage("db.general.exception", null, LocaleContextHolder
					.getLocale());
		}

		Response<?> responseData = new Response<>(HttpStatus.BAD_REQUEST.value(), apiErrorMessage,
				ApplicationConstants.ResponseMessage.FAILURE);

		return ResponseEntity.ok().body(responseData);

	}

	@ExceptionHandler({ DataIntegrityViolationException.class,
			java.sql.SQLIntegrityConstraintViolationException.class })
	public ResponseEntity<?> processDBConstraintsErrorSpringBased(DataIntegrityViolationException ex) {
		GlobalExceptionHandler.logger.error("Error: ", ex);
		ex.printStackTrace();
		String message = ex.getMostSpecificCause().getMessage();

		String apiErrorMessage = null;

		if ((message != null) && message.contains("cannot insert NULL")) {
			apiErrorMessage = this.messageSource.getMessage("db.fields.required.exception", null,
					LocaleContextHolder.getLocale());
		} else if ((message != null) && message.contains("unique constraint")) {
			apiErrorMessage = this.messageSource.getMessage("db.unique.field.exception", null,
					LocaleContextHolder.getLocale());
		} else if ((message != null) && message.contains("Duplicate entry")) {
			apiErrorMessage = message.substring(0, message.lastIndexOf("for"));
			if (apiErrorMessage == null) {
				apiErrorMessage = this.messageSource.getMessage("db.unique.field.duplicate.entry", null,
						LocaleContextHolder.getLocale());
			}
		} else if ((message != null) && message.contains("Column")) {
			apiErrorMessage = ex.getMostSpecificCause().getMessage();
		} else if ((message != null) && message.contains("Cannot add or update")) {
			apiErrorMessage = message.substring(0, message.lastIndexOf("fails"));
		} else if (apiErrorMessage == null) {
			apiErrorMessage = this.messageSource.getMessage("db.general.exception", null, LocaleContextHolder
					.getLocale());
		}

		Response<?> responseData = new Response<>(HttpStatus.BAD_REQUEST.value(), apiErrorMessage,
				ApplicationConstants.ResponseMessage.FAILURE);

		return ResponseEntity.ok().body(responseData);

	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<?> processValidationError(ConstraintViolationException ex) {
		List<ValidationErrorResponse> fieldErrors = ex.getConstraintViolations().stream().map(
				constraintViolation -> {
					String modelName = constraintViolation.getLeafBean().getClass().getSimpleName()
							.toLowerCase();
					String field = modelName + "." + StringUtils.substringAfterLast(constraintViolation
							.getPropertyPath().toString(), ".");
					String resourceLookupProperty = StringUtils.substring(constraintViolation
							.getMessageTemplate(), 1, constraintViolation.getMessageTemplate().length() - 1)
							+ "." + field;
					String message = this.messageSource.getMessage(resourceLookupProperty, null,
							constraintViolation.getMessage(), LocaleContextHolder.getLocale());
					return new ValidationErrorResponse(field, message);
				}).collect(Collectors.toList());
		ex.printStackTrace();
		Map<String, Object> error = new HashMap<>();
		error.put("Validation Errors", fieldErrors);

		return ResponseEntity.ok(Response.of(HttpStatus.BAD_REQUEST.value(), "Validation errors",
				HttpStatus.BAD_REQUEST.name(), error));

	}

	@ExceptionHandler({ CustomInValidRequestException.class })
	public ResponseEntity<?> handleCustomInValidRequestException(CustomInValidRequestException ex) {
		GlobalExceptionHandler.logger.error("Error: ", ex);
		ex.printStackTrace();
		return ResponseEntity.ok().body(ex.getResp());

	}

	@ExceptionHandler({ CustomException.class })
	public ResponseEntity<?> handleCustomException(CustomException ex) {
		GlobalExceptionHandler.logger.error("Error: ", ex);
		logger.fatal(ex.getMessage());
		ex.printStackTrace();
		return ResponseEntity.ok().body(Utility.createFailure(ex.getErrorCode(), ex.getException()
				.getMessage()));

	}

}