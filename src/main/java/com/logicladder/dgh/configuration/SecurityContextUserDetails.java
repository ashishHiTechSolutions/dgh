package com.logicladder.dgh.configuration;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.logicladder.dgh.security.JwtUser;

@Component
public class SecurityContextUserDetails {

	public JwtUser getJwtUserFromContextUser() {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		JwtUser userContext = (JwtUser) authentication.getPrincipal();
		return userContext;
	}

	public String getSecurityContextUserName() {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		if (authentication.getPrincipal() != "anonymousUser") {
			JwtUser userContext = (JwtUser) authentication.getPrincipal();
			return userContext.getUsername();
		}
		return null;
	}

}
