/*
 * package com.logicladder.dgh.configuration;
 *
 * import java.util.ArrayList;
 *
 * import org.slf4j.Logger; import org.slf4j.LoggerFactory; import
 * org.springframework.context.annotation.Bean; import
 * org.springframework.context.annotation.Configuration;
 *
 * import springfox.documentation.builders.PathSelectors; import
 * springfox.documentation.builders.RequestHandlerSelectors; import
 * springfox.documentation.service.ApiInfo; import
 * springfox.documentation.service.Contact; import
 * springfox.documentation.service.VendorExtension; import
 * springfox.documentation.spi.DocumentationType; import
 * springfox.documentation.spring.web.plugins.Docket; import
 * springfox.documentation.swagger2.annotations.EnableSwagger2;
 *
 * @Configuration
 *
 * @EnableSwagger2 public class SwaggerConfig {
 *
 * protected Logger logger = LoggerFactory.getLogger(SwaggerConfig.class);
 *
 * private String basePackage = "com.logicladder.dgh.controller"; private String
 * basePackage1 = "com.logicladder.dgh.security.controller";
 *
 * @Bean public Docket api() { logger.info("init () of Swagger config : " +
 * logger.getName()); Docket docketObj = new Docket(DocumentationType.SWAGGER_2)
 * .select() .apis(RequestHandlerSelectors.any()) .paths(PathSelectors.any())
 * .build() .apiInfo(apiInfo());
 *
 * logger.debug("Apis/Resources eligible for Swagger API landscape package :" +
 * basePackage); logger.debug("Path Selector  : Any ");
 *
 * return docketObj;
 *
 * }
 *
 * private ApiInfo apiInfo() { ApiInfo apiInfo = new ApiInfo("TimesAward ",
 * "TimesAward Rest docs", "1.0", "#", new Contact("TimeAward", "", ""),
 * "License", "#", new ArrayList<VendorExtension>()); return apiInfo; } }
 */