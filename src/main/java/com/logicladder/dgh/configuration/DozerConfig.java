/*
 * 
 */
package com.logicladder.dgh.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class DozerConfig {

	private static final Logger logger = LogManager.getLogger(DozerConfig.class);

	@Bean
	public DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean(
			@Value("classpath*:mappings/*Mappings.xml") Resource[] resources) throws Exception {

		DozerConfig.logger.info("initilazationing Dozer configurations with resources" + resources);
		final DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean = new DozerBeanMapperFactoryBean();
		// Other configurations
		dozerBeanMapperFactoryBean.setMappingFiles(resources);
		DozerConfig.logger.info("initilazationing of Dozer configurations finished");
		return dozerBeanMapperFactoryBean;
	}

}
