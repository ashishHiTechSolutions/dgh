package com.logicladder.dgh.service;

import java.util.List;

import org.springframework.validation.annotation.Validated;
import com.logicladder.dgh.entity.app.Unit;

@Validated
public interface IUnitService {
	
	public List<Unit>  save();

	public List<Unit> getAllUnit();

}
