/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.Country;

public interface ICountryService extends IGenericService<Country, Long> {

}
