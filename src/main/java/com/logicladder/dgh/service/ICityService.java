/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.City;

public interface ICityService extends IGenericService<City, Long> {

	void createDefaultCities();

}
