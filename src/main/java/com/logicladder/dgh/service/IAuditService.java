package com.logicladder.dgh.service;

import com.logicladder.dgh.dto.AuditResponseDto;

import java.util.Date;

public interface IAuditService {

    AuditResponseDto getAuditLogs(Date startDate, Date endDate, String entity, int page, int size);
}
