package com.logicladder.dgh.service;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.entity.app.Model;

@Validated
public interface IModelService {

	public List<Model> save();

	public Model update();

	public List<Model> getAllModel();

	public List<Model> getAllByMakeId(@NotNull Long makeId);
}
