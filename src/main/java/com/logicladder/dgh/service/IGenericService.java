/*
 * 
 */
package com.logicladder.dgh.service;

import java.util.List;

public interface IGenericService<E, K> {
	E get(K id);

	List<E> getAll();

	boolean remove(E entity);

	void saveAll(List<E> entities);

	E saveOrUpdate(E entity);
	
}
