/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.controller.CusAuthenticationException;

public interface IAuthenticationService {

	void checkIfLoginValid(User user);

	Response<?> forgotPassword(String emailId) throws InterruptedException;

	String loginUser(String username, String password, String deviceId, String ipAddress) throws Exception;

	Response<?> verifyOtp(String otp, String username, String jwtToken) throws CusAuthenticationException;

}
