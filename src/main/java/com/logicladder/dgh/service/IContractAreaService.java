package com.logicladder.dgh.service;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.dto.ContractAreaDto;
import com.logicladder.dgh.entity.app.City;
import com.logicladder.dgh.entity.app.ContractArea;

@Validated
public interface IContractAreaService extends IGenricService<ContractAreaDto, ContractArea> {

	public List<City> retrieveCitiesForFacility(long id);

}
