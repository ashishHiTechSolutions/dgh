package com.logicladder.dgh.service;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.entity.app.Make;

@Validated
public interface IMakeService {

	public List<Make>  save();
	public Make update();
	public List<Make> getAllMake();

}
