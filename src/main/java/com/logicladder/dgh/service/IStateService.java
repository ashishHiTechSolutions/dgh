/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.State;

public interface IStateService extends IGenericService<State, Long> {

	void createDefaultStates(String authToken);

}
