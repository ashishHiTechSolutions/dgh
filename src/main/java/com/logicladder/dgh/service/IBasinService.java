/*
 * 
 */
package com.logicladder.dgh.service;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.dto.BasinDto;
import com.logicladder.dgh.entity.app.Basin;

@Validated
public interface IBasinService extends IGenricService<BasinDto, Basin> {

	void createDefaultBasin(String authToken);

}
