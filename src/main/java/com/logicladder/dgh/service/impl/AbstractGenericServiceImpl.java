/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;

import org.dozer.Mapper;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.security.repository.IGenericRepository;
import com.logicladder.dgh.service.IGenericService;

public abstract class AbstractGenericServiceImpl<E, K> implements IGenericService<E, K> {

	private EntityManager entityManager;

	private IGenericRepository genericRepository;

	private Mapper mapper;

	public AbstractGenericServiceImpl() {

	}

	@Autowired
	public AbstractGenericServiceImpl(IGenericRepository iGenericDao, Mapper mapper,
			EntityManager entityManager) {
		this.genericRepository = iGenericDao;
		this.mapper = mapper;
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public E get(K id) {
		return (E) this.genericRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public List<E> getAll() {
		return this.genericRepository.findAll();
	}

	public Session getHibernateSession() {
		return this.entityManager.unwrap(Session.class);
	}

	@Override
	@Transactional
	public boolean remove(E entity) {
		try {
			this.genericRepository.delete(entity);
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Transactional
	public void saveAll(List<E> entities) {
		this.genericRepository.saveAll(entities);
	}

	@Override
	@Transactional
	public E saveOrUpdate(E entity) {
		return (E) this.genericRepository.save(entity);
	}
}
