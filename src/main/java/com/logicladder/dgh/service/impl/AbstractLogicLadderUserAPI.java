/*
 * 
 */
package com.logicladder.dgh.service.impl;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.response.LoginResponse;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.controller.CusAuthenticationException;
import com.logicladder.dgh.util.Utility;

public abstract class AbstractLogicLadderUserAPI {

	private static final Logger logger = Logger.getLogger(AbstractLogicLadderUserAPI.class);

	private RestTemplate restTemplate;

	public AbstractLogicLadderUserAPI(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public LoginResponse login(String username, String password) {

		AbstractLogicLadderUserAPI.logger.info("Login Request recieved for Username : " + username);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		MultiValueMap<String, String> parametersMap = new LinkedMultiValueMap<>();
		parametersMap.add("userName", username);
		parametersMap.add("password", password);

		StringBuffer url = new StringBuffer((String) Utility.getApplicationDefaultProperties().get(
				"constant.logic.ladder.constants.tp_base_url")).append("/login/json?type=p");
		LoginResponse loginResponse = null;

		try {
			loginResponse = this.restTemplate.postForObject(url.toString(), parametersMap,
					LoginResponse.class);

		} catch (RestClientException e) {
			throw new CusAuthenticationException(Response.of(ErrorCode.LOGIC_LADDER_AUTHENTICATION, e
					.getMostSpecificCause().getMessage(), ApplicationConstants.ResponseMessage.FAILURE));

		}

		return loginResponse;
	}
}
