/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.entity.app.Country;
import com.logicladder.dgh.security.repository.ICountryRepository;
import com.logicladder.dgh.service.ICountryService;

@Service
@Transactional
public class CountryServiceImpl extends AbstractGenericServiceImpl<Country, Long> implements ICountryService {

	private ICountryRepository iCountryRepository;

	public CountryServiceImpl() {
	}

	@Autowired
	public CountryServiceImpl(ICountryRepository iCountryRepository, Mapper mapper,
			EntityManager entityManager) {
		super(iCountryRepository, mapper, entityManager);
		this.iCountryRepository = iCountryRepository;
	}
}
