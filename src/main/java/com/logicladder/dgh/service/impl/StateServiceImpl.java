/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.entity.app.Country;
import com.logicladder.dgh.entity.app.State;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.security.repository.IStateRepository;
import com.logicladder.dgh.service.IStateService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderEnumUtil.EntityTypeLogicLadder;
import com.logicladder.dgh.util.Utility;

@Service
@Transactional
public class StateServiceImpl extends AbstractGenericServiceImpl<State, Long> implements IStateService {

	private static Logger logger = LogManager.getLogger(StateServiceImpl.class);
	
	private IStateRepository iStateRepository;

	private IAsyncLogicLadderService iAsyncLogicLadderService;

	public StateServiceImpl() {
	}

	@Autowired
	public StateServiceImpl(IStateRepository iStateRepository, Mapper mapper, EntityManager entityManager,
			IAsyncLogicLadderService iAsyncLogicLadderService) {
		super(iStateRepository, mapper, entityManager);
		this.iStateRepository = iStateRepository;
		this.iAsyncLogicLadderService = iAsyncLogicLadderService;
	}

	@Override
	public void createDefaultStates(String authToken) {
		try {
			logger.info("Creating Default States");
			String[] states = Utility.getPreloadProperties().getProperty("app.state").split(",");
			Country country = new Country();
			country.setCreatedBy(ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA);
			country.setName("India");
			for (String stateName : states) {
				State state = new State();
				state.setName(stateName);
				state.setCountry(country);
				state.setCoreId(iAsyncLogicLadderService.createEntityRequest(new GenricEntityCreationRequestDto(
						5328, EntityTypeLogicLadder.STATE.getDescription(), state.getName()), authToken).getId());
				this.getHibernateSession().save(state);
				logger.info("State created :" + stateName);
			}
			
			logger.info("States created");
		}catch (Exception e) {
			logger.fatal(e.getMessage());
			throw e;
		}

	}

}
