package com.logicladder.dgh.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.Make;
import com.logicladder.dgh.logic.ladder.service.IMakeLogicLadderService;
import com.logicladder.dgh.security.repository.IMakeRepository;
import com.logicladder.dgh.service.IMakeService;

@Transactional
@Service
public class MakeServiceImpl extends AbstractGenricSevice implements IMakeService {

	@Autowired
	IMakeLogicLadderService iMakeLogicLadderService;
	
	@Autowired
	IMakeRepository iMakeRepository;

	/*
	 * Should be Executed out of boundary Transaction. Since it would be bulk update
	 * and may take time
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@Override
	public List<Make> save() {

		// TODO Need to move to Propertied Files
		int batchSize = 25;
		com.logicladder.dgh.thirdparty.response.dto.Make[] arrayMakeRespDto = iMakeLogicLadderService
				.getAllDeviceMake();
		List<Make> listMake = new ArrayList<>();
		for (com.logicladder.dgh.thirdparty.response.dto.Make makeRespDto : arrayMakeRespDto) {
			Make makeEntity = new Make();
			makeEntity.setName(makeRespDto.getName());
			makeEntity.setCoreId(Long.valueOf(makeRespDto.getId().longValue()));
			listMake.add(makeEntity);
		}
		EntityManager cusEntitymanager = null;
		EntityTransaction entityTransaction = null;
		try {
			cusEntitymanager = entityManagerFactory.createEntityManager();
			entityTransaction = cusEntitymanager.getTransaction();
			entityTransaction.begin();
			int counter = 0;
			for (Make make : listMake) {
				if (counter > 0 && counter % batchSize == 0) {
					cusEntitymanager.flush();
					cusEntitymanager.clear();
					counter++;
				}

				cusEntitymanager.persist(make);
			}
			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (entityTransaction != null && entityTransaction.isActive())
				entityTransaction.rollback();
			throw e;
		} finally {
			if (cusEntitymanager != null) {
				cusEntitymanager.close();
			}
		}
		return listMake;
	}

	@Override
	public Make update() {

		return null;
	}

	@Override
	public List<Make> getAllMake() {
		return iMakeRepository.findAll();
	}

}
