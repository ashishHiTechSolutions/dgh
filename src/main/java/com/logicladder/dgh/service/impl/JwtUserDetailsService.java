/*
 * 
 */
package com.logicladder.dgh.service.impl;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.dto.UserTokenDto;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.entity.app.UserAuthData;
import com.logicladder.dgh.security.JwtUserFactory;
import com.logicladder.dgh.security.repository.IUserAuthDataRepository;
import com.logicladder.dgh.security.repository.IUserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private IUserAuthDataRepository iUserAuthDataRepository;

	@Autowired
	private IUserRepository iUserRepository;

	@Autowired
	Mapper mapper;

	@Autowired
	SecurityContextUserDetails securityContextUserDetails;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = this.iUserRepository.findByUserName(email);

		if (user == null) {
			throw new UsernameNotFoundException(String.format("Invalid UserName and Password"));
		} else {
			return JwtUserFactory.create(user);
		}
	}

	public UserTokenDto saveUpdateUserToken(UserTokenDto userTokenDto, Long userId) {

		// Assuming that device ID will be unique
		UserAuthData userAuthData = this.iUserAuthDataRepository.findByDeviceId(userTokenDto.getDeviceId());
		// if 1sT Time user
		if (userAuthData == null) {
			userAuthData = this.iUserAuthDataRepository.save(this.mapper.map(userTokenDto,
					UserAuthData.class));
		} else {
			userAuthData.setJwtToken(userTokenDto.getJwtToken());
			userAuthData.setLlAuthToken(userTokenDto.getLlAuthToken());
			this.iUserAuthDataRepository.save(userAuthData);
		}

		return this.mapper.map(userAuthData, UserTokenDto.class);

	}
}
