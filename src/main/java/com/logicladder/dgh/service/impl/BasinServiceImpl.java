/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dao.IBasinDao;
import com.logicladder.dgh.dto.BasinDto;
import com.logicladder.dgh.entity.app.Basin;
import com.logicladder.dgh.entity.app.City;
import com.logicladder.dgh.entity.app.State;
import com.logicladder.dgh.enums.EnumUtils.BasinType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.security.repository.IBasinRepository;
import com.logicladder.dgh.security.repository.ICityRepository;
import com.logicladder.dgh.security.repository.IStateRepository;
import com.logicladder.dgh.service.IBasinService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderEnumUtil.EntityTypeLogicLadder;
import com.logicladder.dgh.util.Utility;

@Service
@Transactional(rollbackFor = Throwable.class)
public class BasinServiceImpl extends AbstractGenricSevice implements IBasinService {

	private static Logger logger = LogManager.getLogger(City.class);

	@Autowired
	private IBasinRepository basinResp;

	@Autowired
	ICityRepository cityResp;

	@Autowired
	IBasinDao iBasinDao;

	@Autowired
	private ICityRepository iCityRepository;

	@Autowired
	private IStateRepository iStateRepository;

	@Autowired
	private IAsyncLogicLadderService iAsyncLogicLadderService;

	@Autowired
	private IBasinRepository iBasinRepository;

	@Autowired
	SecurityContextUserDetails securityContextUserDetails;

	@Override
	public List<Basin> getAllByStatus(EntityStatus status) {
		List<Basin> listBasin = this.basinResp.findByStatus(status);
		return CollectionUtils.isEmpty(listBasin) == true ? null : listBasin;
	}

	@Override
	public void createDefaultBasin(String authToken) {
		logger.info("creating Basins");
		String[] basins = Utility.getPreloadProperties().get("app.basin").toString().split(",");

		try {
			for (String basin : basins) {
				Basin entity = new Basin();
				try {

					String basinName = basin.split(":")[0];
					String stateName = basin.split(":")[1];
					Double area = (double) 0;
					if (basin.split(":").length > 2) {
						area = Double.valueOf(basin.split(":")[2]);
					}

//				City city = this.iCityRepository.findByStateName(stateName).iterator().next();
					State state = iStateRepository.findByName(stateName);
					entity.setName(basinName);
					entity.setState(state);
					entity.setCode(basin.substring(0, 3));
					entity.setCreatedBy(ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA);
					entity.setDescription(stateName + " is a basin");
					entity.setStatus(EntityStatus.ACTIVE);
					entity.setType(BasinType.BASIN);
					entity.setArea(area);

					entity.setCoreId(iAsyncLogicLadderService.createEntityRequest(
							new GenricEntityCreationRequestDto(Integer.valueOf(state.getCoreId().intValue()),
									EntityTypeLogicLadder.BASIN.getDescription(), entity.getName()), authToken)
							.getId());
					this.getHibernateSession().save(entity);
					logger.info("Basin Created : " + basinName);
				} catch (Exception e) {
					e.printStackTrace();
					logger.info(entity.getName());
				}
				logger.info("creating Basins");
			}
		} catch (Exception e) {
			logger.fatal(e.getMessage());
			throw e;
		}

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Basin create(@Valid BasinDto entityDto) {
		Basin basinEntity = this.mapper.map(entityDto, Basin.class);
		basinEntity = this.basinResp.save(basinEntity);
		return basinEntity;
	}

	@Override
	public boolean delete(BasinDto entityDto) {
		this.iBasinRepository.deleteById(entityDto.getId());
		return true;
	}

	@Override
	public Basin getById(Long id) {
		Optional<Basin> objOpt = this.basinResp.findById(id);
		return objOpt.orElseThrow(() -> new EntityNotFoundException(String.format(
				"Entity not found for Id [%s]", id)));
	}

	@Override
	public Basin update(@Valid BasinDto entityDto) {

		Basin basinEntity = this.mapper.map(entityDto, Basin.class);
		/*
		 * if(CollectionUtils.isEmpty(entityDto.getContractAreas())) {
		 * basinEntity.setContractAreas(iBasinRepository.findById(entityDto.getId()).get
		 * ().getContractAreas()); }
		 * 
		 * iBasinRepository.save(basinEntity);
		 */
		// TODO cahnged due to entity handling
		this.iBasinDao.updateBasinProperty(basinEntity);
		return basinEntity;
	}

	@Override
	public boolean deleteById(Long entityId) {
		this.iBasinRepository.deleteById(entityId);
		return true;
	}

	@Override
	public List<Basin> findByIdIn(Iterable<Long> ids) {
		return iBasinRepository.findAllById(ids);
	}
}
