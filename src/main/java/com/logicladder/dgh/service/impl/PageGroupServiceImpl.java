package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.PageGroup;
import com.logicladder.dgh.security.repository.IPageGroupRepository;
import com.logicladder.dgh.service.IPageGroupService;

@Service
@Transactional
public class PageGroupServiceImpl extends AbstractGenericServiceImpl<PageGroup, Long> implements
		IPageGroupService {

	private IPageGroupRepository iPageGroupRepository;

	@Autowired
	public PageGroupServiceImpl(IPageGroupRepository iPageGroupRepository, Mapper mapper,
			EntityManager entityManager) {
		super(iPageGroupRepository, mapper, entityManager);
	}

}
