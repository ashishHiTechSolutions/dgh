/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.entity.app.City;
import com.logicladder.dgh.entity.app.State;
import com.logicladder.dgh.security.repository.ICityRepository;
import com.logicladder.dgh.security.repository.IStateRepository;
import com.logicladder.dgh.service.ICityService;

@Service
@Transactional
public class CityServiceImpl extends AbstractGenericServiceImpl<City, Long> implements ICityService {

	private static Logger logger = LogManager.getLogger(City.class);

	private ICityRepository iCityRepository;

	private IStateRepository iStateRepository;

	public CityServiceImpl() {

	}

	@Autowired
	public CityServiceImpl(ICityRepository iCityRepository, Mapper mapper, EntityManager entityManager,
			IStateRepository iStateRepository) {
		super(iCityRepository, mapper, entityManager);
		this.iCityRepository = iCityRepository;
		this.iStateRepository = iStateRepository;
	}

	@Override
	public void createDefaultCities() {
		try {
			logger.info("Creating Default Cities");
			for (State state : this.iStateRepository.findAll()) {
				int i = 1;
				City city = new City();
				city.setCreatedBy(ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA);
				city.setName(state.getName() + " " + i);
				city.setState(state);
				this.iCityRepository.save(city);
				logger.info("State created :" + city.getName());
			}
			logger.info("Cities created");
		} catch (Exception e) {
			logger.fatal(e.getMessage());
			throw e;
		}
	}
}
