/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.dao.IContractAreaDao;
import com.logicladder.dgh.dto.ContractAreaDto;
import com.logicladder.dgh.entity.app.City;
import com.logicladder.dgh.entity.app.ContractArea;
import com.logicladder.dgh.entity.app.FacilityOperator;
import com.logicladder.dgh.entity.app.State;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.security.repository.IBasinRepository;
import com.logicladder.dgh.security.repository.ICityRepository;
import com.logicladder.dgh.security.repository.IContractAreaRepository;
import com.logicladder.dgh.security.repository.IFacilityOperatorRepository;
import com.logicladder.dgh.service.IContractAreaService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderEnumUtil.EntityTypeLogicLadder;
import com.logicladder.dgh.thirdparty.response.dto.Entity;

@Service
@Transactional
public class ContractAreaServiceImpl extends AbstractGenricSevice implements IContractAreaService {

	@Autowired
	IContractAreaRepository iContractAreaRepository;

	@Autowired
	IContractAreaDao iContractAreaDao;

	@Autowired
	private IAsyncLogicLadderService iAsyncLogicLadderService;

	@Autowired
	private IBasinRepository iBasinRepository;

	@Autowired
	SecurityContextUserDetails securityContextUserDetails;

	@Autowired
	private ICityRepository iCityRepository;

	@Autowired
	private IFacilityOperatorRepository iFacilityOperatorRepository;

	@Override
	public boolean deleteById(Long entityId) {
		try {
			Integer coreId = Integer.valueOf(iContractAreaRepository.findById(entityId).get().getCoreId()
					.intValue());
			iAsyncLogicLadderService.removeEntity(coreId);

			ContractArea contractArea = iContractAreaRepository.findById(entityId).get();

			contractArea.getFacilities().forEach(facility -> {
				List<FacilityOperator> facilityOperators = iFacilityOperatorRepository.findByFacilityId(
						facility.getId());

				facilityOperators.forEach(facilityOperator -> {
					iFacilityOperatorRepository.deleteById(facilityOperator.getId());
				});

			});
			iContractAreaRepository.deleteById(entityId);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<ContractArea> getAllByStatus(EntityStatus status) {

		List<ContractArea> listContractArea = (List<ContractArea>) this.iContractAreaRepository.findByStatus(
				status);
		return CollectionUtils.isEmpty(listContractArea) == true ? null : listContractArea;
	}

	@Override
	public ContractArea getById(Long id) {
		Optional<ContractArea> objOpt = this.iContractAreaRepository.findById(id);
		ContractArea contractArea = objOpt.orElseThrow(() -> new EntityNotFoundException(String.format(
				"Entity not found for Id [%s]", id)));
		contractArea.setBasinName(iBasinRepository.findById(contractArea.getBasinId()).get().getName());
		return contractArea;
	}

	@Override
	public ContractArea create(@Valid ContractAreaDto entityDto) {
		ContractArea contractArea = this.mapper.map(entityDto, ContractArea.class);
//		contractArea.setStatus(EntityStatus.ACTIVE);
		contractArea = iContractAreaRepository.save(contractArea);
		ContractAreaDto respObj = mapper.map(contractArea, ContractAreaDto.class);

		Integer parentId = Integer.valueOf(iBasinRepository.findById(entityDto.getBasinId()).get().getCoreId()
				.intValue());
		System.out.println("parentId=======" +parentId);
		Entity createEntityResponse = iAsyncLogicLadderService.createEntityRequest(
				new GenricEntityCreationRequestDto(parentId, EntityTypeLogicLadder.CONTRACT_AREA.getDescription(), respObj.getName()), null);

		contractArea.setCoreId(Long.valueOf(createEntityResponse.getId()));
		contractArea = this.iContractAreaRepository.save(contractArea);
		return contractArea;
	}

	@Override
	public boolean delete(ContractAreaDto entityDto) {
		try {
			Integer coreId = Integer.valueOf(iContractAreaRepository.findById(entityDto.getId()).get()
					.getCoreId().intValue());
			iAsyncLogicLadderService.removeEntity(coreId);
			iContractAreaRepository.deleteById(entityDto.getId());
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public ContractArea update(@Valid ContractAreaDto entityDto) {

		ContractArea contractAreaEntity = this.mapper.map(entityDto, ContractArea.class);

		Integer parentId = Integer.valueOf(iBasinRepository.findById(entityDto.getBasinId()).get().getCoreId()
				.intValue());
		iAsyncLogicLadderService.updateEntityRequest(new GenricEntityCreationRequestDto(parentId,
				EntityTypeLogicLadder.CONTRACT_AREA.getDescription(), entityDto.getName()), entityDto
						.getCoreId(), null);
		this.iContractAreaDao.updateContractAreaProperty(contractAreaEntity);
		ContractArea contractArea = this.iContractAreaRepository.findById(contractAreaEntity.getId()).get();
		if (!entityDto.getStatus().equals(contractArea.getStatus())) {
			contractArea.changeStatus(entityDto.getStatus());
			this.iContractAreaRepository.save(contractArea);
		}
		return contractArea;

	}

	@Override
	public List<City> retrieveCitiesForFacility(long id) {
		State state = iBasinRepository.findById(id).get().getState();
		List<City> cities = iCityRepository.findByStateId(state.getId());

		return cities;
	}

	@Override
	public List<ContractArea> findByIdIn(Iterable<Long> ids) {
		return iContractAreaRepository.findAllById(ids);
	}
}
