/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.dao.IFacilityDao;
import com.logicladder.dgh.dto.FacilityDto;
import com.logicladder.dgh.entity.app.Address;
import com.logicladder.dgh.entity.app.ContractArea;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.entity.app.FacilityOperator;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.security.repository.ICityRepository;
import com.logicladder.dgh.security.repository.IContractAreaRepository;
import com.logicladder.dgh.security.repository.IFacilityOperatorRepository;
import com.logicladder.dgh.security.repository.IFacilityRepository;
import com.logicladder.dgh.security.repository.IOperatorRepository;
import com.logicladder.dgh.service.IFacilityService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderEnumUtil.EntityTypeLogicLadder;
import com.logicladder.dgh.thirdparty.response.dto.Entity;
import com.logicladder.dgh.util.Utility;

@Service
@Transactional
public class FacilityServiceImpl extends AbstractGenricSevice implements IFacilityService {

	@Autowired
	private IAsyncLogicLadderService iAsyncLogicLadderService;

	@Autowired
	private IFacilityDao iFacilityDao;

	@Autowired
	private IFacilityOperatorRepository iFacilityOperatorRepository;

	@Autowired
	private IFacilityRepository iFacilityRepository;

	@Autowired
	private IOperatorRepository iOperatorRepository;

	@Autowired
	private IContractAreaRepository iContractAreaRepository;

	@Autowired
	private SecurityContextUserDetails securityContextUserDetails;

	@Autowired
	private ICityRepository iCityRepository;

	@Override
	public boolean delete(FacilityDto facilityDto) {

		return false;
	}

	@Override
	public List<Facility> getAllByStatus(EntityStatus status) {
		List<Facility> listEntity = this.iFacilityRepository.findByStatus(status);

		return CollectionUtils.isEmpty(listEntity) == true ? null : listEntity;
	}

	@Override
	public Facility getById(Long id) {

		Optional<Facility> objOpt = this.iFacilityRepository.findById(id);
		Facility facility = objOpt.orElseThrow(() -> new EntityNotFoundException(String.format(
				"Entity not found for Id [%s]", id)));
		facility.setContractAreaName(iContractAreaRepository.findById(facility.getContractAreaId()).get()
				.getName());
		return facility;
	}

	@Override
	public Facility update(FacilityDto entityDto) {

		Facility facilityEntity = this.mapper.map(entityDto, Facility.class);
		facilityEntity.setAddress(new Address(null, iCityRepository.findById(entityDto.getAddressDto()
				.getCityDto().getId()).get(), null));

		Integer parentId = Integer.valueOf(iContractAreaRepository.findById(entityDto.getContractAreaId())
				.get().getCoreId().intValue());
		iAsyncLogicLadderService.updateEntityRequest(new GenricEntityCreationRequestDto(parentId,
				EntityTypeLogicLadder.FACILITY.getDescription(), entityDto.getName()), facilityEntity
						.getCoreId(), null);
		// TODO why hitting core for contract area?
		iAsyncLogicLadderService.updateEntityRequest(new GenricEntityCreationRequestDto(parentId,
				EntityTypeLogicLadder.CONTRACT_AREA.getDescription(), entityDto.getName()), entityDto
						.getCoreId(), null);
		Facility dbEntity = iFacilityRepository.findById(entityDto.getId()).get();
		if (CollectionUtils.isNotEmpty(dbEntity.getWells()) && CollectionUtils.isNotEmpty(dbEntity
				.getTanks())) {
			facilityEntity.setTanks(dbEntity.getTanks());
			facilityEntity.setWells(dbEntity.getWells());
		}
		iFacilityRepository.save(facilityEntity);

		if (entityDto.getOperatorId() != null && entityDto.getOperatorId() > 0) {
			FacilityOperator facilityOperator = new FacilityOperator(Utility.createDateFromString(entityDto
					.getContractStartDate()), Utility.createDateFromString(entityDto.getContractEndDate()),
					facilityEntity, iOperatorRepository.findById(entityDto.getOperatorId()).get());
			facilityOperator.setId(entityDto.getFacilityOperatorId());
			facilityOperator = this.iFacilityOperatorRepository.save(facilityOperator);
			facilityEntity.setFacilityOperatorId(facilityOperator.getId());

		}

		if (!facilityEntity.getStatus().equals(entityDto.getStatus())) {
			facilityEntity.changeStatus(entityDto.getStatus());
		}
		iFacilityRepository.save(facilityEntity);

		return facilityEntity;
	}

	@Override
	public Facility create(@Valid FacilityDto entityDto) throws CustomException {

		Facility facility = this.mapper.map(entityDto, Facility.class);
//		facility.setStatus(EntityStatus.ACTIVE);
		facility.setAddress(new Address(null, iCityRepository.findById(entityDto.getAddressDto().getCityDto()
				.getId()).get(), null));
		ContractArea contractArea = iContractAreaRepository.findById(entityDto.getContractAreaId()).get();

		Integer parentId = 0;

		if (contractArea != null)
			parentId = Integer.valueOf(contractArea.getCoreId().intValue());
		else
			throw new CustomException(21, new Exception("Contract Area does not exist"));
		Entity entityResponse = this.iAsyncLogicLadderService.createEntityRequest(
				new GenricEntityCreationRequestDto(parentId, EntityTypeLogicLadder.FACILITY.getDescription(),
						entityDto.getName()), null);
		if (entityDto.getOperatorId() != null && entityDto.getOperatorId() > 0) {
			FacilityOperator facilityOperator = new FacilityOperator(Utility.createDateFromString(entityDto
					.getContractStartDate()), Utility.createDateFromString(entityDto.getContractEndDate()),
					facility, iOperatorRepository.findById(entityDto.getOperatorId()).get());
			facilityOperator = this.iFacilityOperatorRepository.save(facilityOperator);
			facility.setFacilityOperatorId(facilityOperator.getId());
		}

		facility.setCoreId(entityResponse.getId());
		facility = iFacilityRepository.save(facility);
		return facility;
	}

	@Override
	public boolean deleteById(Long entityId) {
		try {

			List<FacilityOperator> facilityOperators = iFacilityOperatorRepository.findByFacilityId(entityId);

			Integer coreId = Integer.valueOf(iFacilityRepository.findById(entityId).get().getCoreId()
					.intValue());

			if (null != coreId && coreId != 0) {
				iAsyncLogicLadderService.removeEntity(coreId);
			}

			facilityOperators.forEach(faciltyOperator -> {
				iFacilityOperatorRepository.deleteById(faciltyOperator.getId());
			});
			iFacilityRepository.deleteById(entityId);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public FacilityOperator getFacilityOperatorById(long id) {
		return iFacilityOperatorRepository.findById(id).get();
	}

	@Override
	public boolean deleteFacilityOperator(long id) {
		List<FacilityOperator> facilityOperators = iFacilityOperatorRepository.findByFacilityId(id);
		facilityOperators.forEach(item -> {
			iFacilityOperatorRepository.deleteById(item.getId());
		});
		return true;
	}

	@Override
	public List<Facility> findByIdIn(Iterable<Long> ids) {
		return iFacilityRepository.findAllById(ids);
	}
}
