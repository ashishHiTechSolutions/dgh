/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.dto.PageGroupDto;
import com.logicladder.dgh.dto.RoleDto;
import com.logicladder.dgh.entity.app.AccessRolePageGroup;
import com.logicladder.dgh.entity.app.PageGroup;
import com.logicladder.dgh.entity.app.Role;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.security.repository.IAccessRolePageGroupRepository;
import com.logicladder.dgh.service.IAccessRolePageGroupService;

@Service
@Transactional
public class AccessRolePageGroupServiceImpl extends AbstractGenericServiceImpl<AccessRolePageGroup, Long>
		implements IAccessRolePageGroupService {

	private IAccessRolePageGroupRepository iAccessRolePageGroupDao;

	private Mapper mapper;

	@Autowired
	protected SecurityContextUserDetails securityContextUserDetails;

	public AccessRolePageGroupServiceImpl() {

	}

	@Autowired
	public AccessRolePageGroupServiceImpl(IAccessRolePageGroupRepository iAccessRoleGroupDao, Mapper mapper,
			EntityManager entityManager) {
		super(iAccessRoleGroupDao, mapper, entityManager);
		this.iAccessRolePageGroupDao = iAccessRoleGroupDao;
		this.mapper = mapper;
	}

	@Override
	public AccessRolePageGroup findByRoleRoleName(String role) {
		AccessRolePageGroup group = iAccessRolePageGroupDao.findByRoleRoleName(role);
		return group;
	}

	@Override
	public boolean createNewAccessRoleGroup(RoleDto roleDto) throws CustomException {
		try {
			AccessRolePageGroup accessRolePageGroup = new AccessRolePageGroup();
			accessRolePageGroup.setRole(new Role(roleDto.getName(), securityContextUserDetails
					.getSecurityContextUserName()));
			Set<PageGroup> pageGroups = new HashSet<>();
			for (PageGroupDto pageGroup : roleDto.getPageGroupDto()) {
				PageGroup pageGroupEntity = mapper.map(pageGroup, PageGroup.class);
				pageGroups.add(pageGroupEntity);
			}
			accessRolePageGroup.setPageGroup(pageGroups);
			iAccessRolePageGroupDao.save(accessRolePageGroup);
			return true;
		} catch (Exception e) {
			throw new CustomException(ErrorCode.DATABASE_ERROR.getErrorCode(), e);
		}
	}
}
