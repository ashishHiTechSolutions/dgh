package com.logicladder.dgh.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.app.exception.CustomInValidRequestException;
import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.dto.OperatorDto;
import com.logicladder.dgh.entity.app.FacilityOperator;
import com.logicladder.dgh.entity.app.Operator;
import com.logicladder.dgh.entity.app.Role;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.repository.ICityRepository;
import com.logicladder.dgh.security.repository.IFacilityOperatorRepository;
import com.logicladder.dgh.security.repository.IOperatorRepository;
import com.logicladder.dgh.security.repository.IUserRepository;
import com.logicladder.dgh.service.IMailService;
import com.logicladder.dgh.service.IOperatorService;

@Service
@Transactional
@Validated
public class OperatorServiceImpl extends AbstractOperatorService implements IOperatorService {

	private IOperatorRepository iOperatorRepository;

	private IUserRepository iUSerRepository;

	private Mapper mapper;

	private SecurityContextUserDetails securityContextUserDetails;

	private IFacilityOperatorRepository iFacilityOperatorRepository;

	public OperatorServiceImpl() {
		super();
	}

	@Autowired
	public OperatorServiceImpl(IOperatorRepository iOperatorDao, ICityRepository iCityDao,
			IUserRepository iUSerDao, SecurityContextUserDetails securityContextUserDetails, Mapper mapper,
			EntityManager entityManager, IMailService iMailService) {
		super(iOperatorDao, iCityDao, mapper, entityManager, iMailService);
		this.iOperatorRepository = iOperatorDao;
		this.iUSerRepository = iUSerDao;
		this.securityContextUserDetails = securityContextUserDetails;
		this.mapper = mapper;
	}

	@Override
	public Operator changeRequestStatus(Long requestedOperatorId, ApprovalStatus status) {
		Operator operator = null;
		switch (status) {
		case ACTIVE:
			operator = this.getHibernateSession().load(Operator.class, requestedOperatorId);
			if (operator != null) {
				operator.setStatus(status);
				this.getHibernateSession().saveOrUpdate(operator);
				this.sendOperatorApprovedEmail(operator);

			}
			break;
		case PENDING:

			break;
		case SUSPENDED:

			break;
		case REJECTED:
			operator = this.getHibernateSession().load(Operator.class, requestedOperatorId);
			if (operator != null && (operator.getStatus().equals(status.ACTIVE))) {
				throw new CustomInValidRequestException(new Response<>(HttpStatus.BAD_REQUEST.value(), String
						.format("Invalid Request -- of Id %s", requestedOperatorId)));
			}
			iOperatorRepository.delete(operator);
			break;

		default:
			break;
		}
		return operator;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Operator createOperatorRequest(OperatorDto operatorDto, String username) throws CustomException {

		this.checkIfOperatorEmailValid(operatorDto.getEmail());
		Operator operator = this.mapToEntity(operatorDto, ApprovalStatus.ACTIVE);
		this.iOperatorRepository.save(operator);
		Role creatorUserRole = null;
		if (username != null) {
			creatorUserRole = iUSerRepository.findByUserName(username).getUserRole();
		}
		if (null != creatorUserRole && (creatorUserRole.getRoleName().equals("Super Admin") || creatorUserRole
				.getRoleName().equals("Admin"))) {
			this.sendOperatorCreatedNotificationEmail(operator);
		}

		return operator;

	}

	@Override
	public Operator findByEmail(String email) {
		return this.iOperatorRepository.findByEmail(email);
	}

	@Override
	public List<Operator> getRequestByStatus(String username, ApprovalStatus status) {
		List<Operator> operatorRequests = this.iOperatorRepository.findByStatus(status);
		return operatorRequests;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Operator updateOperator(OperatorDto operatorDto) {
		Operator operator = this.mapper.map(operatorDto, Operator.class);
		this.getHibernateSession().merge(operator);
		return operator;
	}

	@Override
	public boolean verifyOperatorOtp(Long operatorId, String emailOtp, String mobileOtp) {
		Optional<Operator> optEntity = iOperatorRepository.findById(operatorId);
		Operator objEnity = optEntity.orElseThrow(() -> new EntityNotFoundException(String.format(
				"Entity not found for Id [%s]", operatorId)));
		if (objEnity.getMobileOtp().equals(mobileOtp) && objEnity.getEmailOtp().equals(emailOtp)) {
			objEnity.setStatus(ApprovalStatus.PENDING);
			iOperatorRepository.saveAndFlush(objEnity);
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteById(long id) {
		List<FacilityOperator> facilityOperators = iFacilityOperatorRepository.findByOperatorId(id);
		facilityOperators.forEach(facilityOperator -> {
			iFacilityOperatorRepository.deleteById(facilityOperator.getId());
		});
		iOperatorRepository.deleteById(id);
		return true;
	}

}
