/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.EventLog;
import com.logicladder.dgh.security.repository.IEventLogRepository;

@Service
@Transactional
public class EventLogServiceImpl extends AbstractGenericServiceImpl<EventLog, Long> {

	private IEventLogRepository iEventLogDao;

	public EventLogServiceImpl() {

	}

	@Autowired
	public EventLogServiceImpl(IEventLogRepository iEventLogDao, Mapper mapper, EntityManager entityManager) {
		super(iEventLogDao, mapper, entityManager);
	}

	public List<EventLog> getAllEventLogsBetweenDate(Date from, Date to) {
		return null;

//				iEventLogDao.findByCreatedDateBetweenFromAndTo(from, to);
	}

}
