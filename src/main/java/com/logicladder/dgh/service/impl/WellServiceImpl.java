/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.dao.IWellDao;
import com.logicladder.dgh.dto.WellDto;
import com.logicladder.dgh.entity.app.Well;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.security.repository.IFacilityRepository;
import com.logicladder.dgh.security.repository.IWellRepository;
import com.logicladder.dgh.service.IWellService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderEnumUtil.EntityTypeLogicLadder;

@Service
@Transactional
public class WellServiceImpl extends AbstractGenricSevice implements IWellService {

	@Autowired
	private IWellDao iWellDao;

	@Autowired
	private IWellRepository iWellRepository;

	@Autowired
	private SecurityContextUserDetails securityContextUserDetails;

	@Autowired
	private IAsyncLogicLadderService iAsyncLogicLadderService;

	@Autowired
	private IFacilityRepository iFacilityRepository;

	@Override
	public Well create(WellDto entityDto) {

		Well entity = this.mapper.map(entityDto, Well.class);
//		entity.setStatus(EntityStatus.ACTIVE);
		Integer coreParentId = Integer.valueOf(iFacilityRepository.findById(entityDto.getFacilityId()).get()
				.getCoreId().intValue());

		entity.setCoreId(iAsyncLogicLadderService.createEntityRequest(new GenricEntityCreationRequestDto(
				coreParentId, EntityTypeLogicLadder.WELL.getDescription(), entityDto.getName()), null)
				.getId());
		entity = this.iWellRepository.save(entity);
		return entity;
	}

	@Override
	public boolean delete(WellDto entityDto) {
		iWellRepository.deleteById(entityDto.getId());
		return true;
	}

	@Override
	public boolean deleteById(Long entityId) {
		iWellRepository.deleteById(entityId);
		return true;
	}

	@Override
	public List<Well> getAllByStatus(EntityStatus status) {

		List<Well> listEnitiy = this.iWellRepository.findByStatus(status);
		return CollectionUtils.isEmpty(listEnitiy) == true ? null : listEnitiy;
	}

	@Override
	public Well getById(Long id) {

		Optional<Well> objOpt = this.iWellRepository.findById(id);
		objOpt.orElseThrow(() -> new EntityNotFoundException(String.format("Entity not found for Id [%s]",
				id)));
		Well well = objOpt.get();
		well.setFacilityName(iFacilityRepository.findById(well.getFacilityId()).get().getName());
		return well;
	}

	public Well getEntityById(Long id) {

		Optional<Well> objOpt = this.iWellRepository.findById(id);
		objOpt.orElseThrow(() -> new EntityNotFoundException(String.format("Entity not found for Id [%s]",
				id)));
		return objOpt.get();
	}

	@Override
	public Well update(WellDto entityDto) {

		Well wellEntity = this.mapper.map(entityDto, Well.class);
		Integer coreParentId = Integer.valueOf(iFacilityRepository.findById(entityDto.getFacilityId()).get()
				.getCoreId().intValue());

		wellEntity.setCoreId(iAsyncLogicLadderService.updateEntityRequest(new GenricEntityCreationRequestDto(
				coreParentId, EntityTypeLogicLadder.WELL.getDescription(), entityDto.getName()), entityDto
						.getCoreId(), null).getId());
		this.iWellDao.updateWellProperty(wellEntity);

		Well well = this.iWellRepository.findById(entityDto.getId()).get();
		if (!well.getStatus().equals(entityDto.getStatus())) {
			well.changeStatus(entityDto.getStatus());
			iWellRepository.save(well);
		}
		return well;
	}

	@Override
	public List<Well> findByIdIn(Iterable<Long> ids) {
		return iWellRepository.findAllById(ids);
	}

}
