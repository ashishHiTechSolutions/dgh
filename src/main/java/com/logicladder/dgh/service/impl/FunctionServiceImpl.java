/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.Function;
import com.logicladder.dgh.security.repository.IFunctionRepository;
import com.logicladder.dgh.service.IFunctionService;

@Service
@Transactional
public class FunctionServiceImpl extends AbstractGenericServiceImpl<Function, Long> implements
		IFunctionService {

	private IFunctionRepository iFunctionDao;

	public FunctionServiceImpl() {

	}

	@Autowired
	public FunctionServiceImpl(IFunctionRepository iFunctionDao, Mapper mapper, EntityManager entityManager) {
		super(iFunctionDao, mapper, entityManager);
	}
}
