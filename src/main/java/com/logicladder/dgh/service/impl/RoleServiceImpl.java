/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.Role;
import com.logicladder.dgh.security.repository.IRoleRepository;
import com.logicladder.dgh.service.IRoleService;
import com.logicladder.dgh.util.Utility;

@Service
@Transactional
public class RoleServiceImpl extends AbstractGenericServiceImpl<Role, Long> implements IRoleService {

	private IRoleRepository iRoleDao;

	public RoleServiceImpl() {

	}

	@Autowired
	public RoleServiceImpl(IRoleRepository iRoleDao, Mapper mapper, EntityManager entityManager) {
		super(iRoleDao, mapper, entityManager);
		this.iRoleDao = iRoleDao;
	}

	@Override
	public Role findByRoleName(String roleName) {
		List<Role> roles = this.iRoleDao.findByRoleName(roleName);
		return (roles != null) && !roles.isEmpty() ? roles.get(0) : null;
	}

	/*@Override
	public Set<PageFunction> getAllPageFunctionForRole(String roleName) {
		List<Role> roles = this.iRoleDao.findByRoleName(roleName);
		return (roles != null) && !roles.isEmpty() ? roles.get(0).getPageFunctions() : null;
	}*/

	@Override
	public boolean validateIfAccessPermitted(String userRole, String function, String page) {
		for (String noAuthRequiredPage : ((String) Utility.getActiveSpringProfileBasedConfig().get(
				"app.no.security.page")).split(",")) {
			if (null == page) {
				return true;
			} else if (page.equalsIgnoreCase(noAuthRequiredPage)) {
				return true;
			}
		}
		if (StringUtils.isBlank(userRole)) {
			return false;
		}
		List<Role> role = null;//TODO this.iRoleDao
//				.findByRoleNameAndPageFunctionsPagePageNameAndPageFunctionsFunctionFunctionName(userRole,
//						function, page);
		return (role != null) && !role.isEmpty() ? true : false;
	}
}
