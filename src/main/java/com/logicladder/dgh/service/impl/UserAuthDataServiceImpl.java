/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.entity.app.UserAuthData;
import com.logicladder.dgh.security.repository.IUserAuthDataRepository;
import com.logicladder.dgh.service.IUserAuthDataService;

@Service
@Transactional
public class UserAuthDataServiceImpl extends AbstractGenericServiceImpl<UserAuthData, Long> implements
		IUserAuthDataService {

	private IUserAuthDataRepository iUserAuthDataRepository;

	public UserAuthDataServiceImpl() {
	}

	@Autowired
	public UserAuthDataServiceImpl(IUserAuthDataRepository iUserAuthDataRepository, Mapper mapper,
			EntityManager entityManager) {
		super(iUserAuthDataRepository, mapper, entityManager);
		this.iUserAuthDataRepository = iUserAuthDataRepository;
	}

	@Override
	public UserAuthData findByDeviceId(String deviceId) {
		return this.iUserAuthDataRepository.findByDeviceId(deviceId);
	}

	@Override
	public UserAuthData findByJwtToken(String jwtToken) {
		return this.iUserAuthDataRepository.findByJwtToken(jwtToken);
	}

	@Override
	public UserAuthData findByOtpVerifiedAndUserUserName(Boolean otpVerified, String username) {
		return this.iUserAuthDataRepository.findByOtpVerifiedAndUserUserName(otpVerified, username);
	}

	@Override
	public UserAuthData findByTempJwtToken(String tempJwtToken) {
		return this.iUserAuthDataRepository.findByTempJwtToken(tempJwtToken);
	}

	@Override
	public String findLLAuthToken(String userName) {
		return iUserAuthDataRepository.findByUserUserName(userName).getLlAuthToken();
	}

	public UserAuthData findByUserUserName(String userName) {
		return this.iUserAuthDataRepository.findByUserUserName(userName);
	}

	@Override
	public Boolean isValidAuthToken(String authToken) {
		UserAuthData authData = this.iUserAuthDataRepository.findByJwtTokenOrTempJwtToken(authToken,
				authToken);
		return authData != null ? true : false;
	}
}
