/*
 * 
 */
package com.logicladder.dgh.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.FacilityOperator;
import com.logicladder.dgh.security.repository.IFacilityOperatorRepository;
import com.logicladder.dgh.service.IGenericService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class FacilityOperatorServiceImpl extends AbstractGenericServiceImpl<FacilityOperator, Long> implements
		IGenericService<FacilityOperator, Long> {

	@Autowired
	private IFacilityOperatorRepository iFacilityOperatorRepository;

}
