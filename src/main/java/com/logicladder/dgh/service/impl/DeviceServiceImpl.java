package com.logicladder.dgh.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.dao.IDeviceDao;
import com.logicladder.dgh.dto.DeviceDto;
import com.logicladder.dgh.entity.app.Device;
import com.logicladder.dgh.entity.app.DeviceParam;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.entity.app.Model;
import com.logicladder.dgh.entity.app.StdParam;
import com.logicladder.dgh.entity.app.Tank;
import com.logicladder.dgh.entity.app.Well;
import com.logicladder.dgh.enums.DeviceType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.logic.ladder.service.IDeviceLogicLadderService;
import com.logicladder.dgh.security.repository.IDeviceRepository;
import com.logicladder.dgh.security.repository.IFacilityRepository;
import com.logicladder.dgh.security.repository.IModelRepository;
import com.logicladder.dgh.security.repository.IStdParamRepository;
import com.logicladder.dgh.security.repository.ITankRepository;
import com.logicladder.dgh.security.repository.IWellRepository;
import com.logicladder.dgh.service.IDeviceService;
import com.logicladder.dgh.thirdparty.request.dto.CreateDeviceRequest;
import com.logicladder.dgh.thirdparty.request.dto.Location;
import com.logicladder.dgh.thirdparty.response.dto.DeviceSource;
import com.logicladder.dgh.thirdparty.response.dto.Param;

@Transactional
@Service
public class DeviceServiceImpl extends AbstractGenricSevice implements IDeviceService {

	@Autowired
	IDeviceRepository iDeviceRepository;

	@Autowired
	IDeviceLogicLadderService iDeviceLogicLadderService;

	@Autowired
	SecurityContextUserDetails securityContextUserDetails;

	@Autowired
	IModelRepository iModelRepository;

	@Autowired
	IFacilityRepository iFacilityRepository;

	@Autowired
	IStdParamRepository iStdParamRepository;

	@Autowired
	ITankRepository iTankRepository;

	@Autowired
	IWellRepository iWellRepository;

	@Autowired
	IDeviceDao iDeviceDao;

	@Override
	public Device create(@Valid DeviceDto entityDto) {

		return null;
	}

	@Override
	public boolean delete(DeviceDto entityDto) {

		return false;
	}

	@Override
	public boolean deleteById(Long entityId) {
		Optional<Device> deviceObj = iDeviceRepository.findById(entityId);
		deviceObj.orElseThrow(
				() -> new EntityNotFoundException(String.format("Entity not found for Id [%s]", entityId)));
		iDeviceRepository.deleteById(entityId);
		return true;
	}

	@Override
	public Device getById(Long id) {
		Optional<Device> entityDevice = iDeviceRepository.findById(id);
		return entityDevice
				.orElseThrow(() -> new EntityNotFoundException(String.format("Entity not found for Id [%s]", id)));
	}

	@Override
	public Device update(@Valid DeviceDto entityDto) {

		return null;
	}

	@Override
	public List<Device> getAllByStatus(EntityStatus status) {
		List<Device> listDevice = null;
		if (status != null) {
			listDevice = iDeviceRepository.findByStatus(status);
		} else {
			listDevice = iDeviceRepository.findAll();
		}
		return CollectionUtils.isEmpty(listDevice) == true ? null : listDevice;
	}

	// TODO
	@Override
	public long save(DeviceDto entityDto) {
		Device deviceObj = mapper.map(entityDto, Device.class);
		deviceObj.setStatus(EntityStatus.ACTIVE);
		Optional<Model> modelEntityOpt = iModelRepository.findById(entityDto.getModelId());
		Model modelobj = modelEntityOpt.orElseThrow(() -> new EntityNotFoundException(
				String.format("Entity not found of Model Id [%s]", entityDto.getModelId())));

		// setting model Obj Mandatory
		deviceObj.setModel(modelobj);

		Optional<Facility> facilityEntityOpt = iFacilityRepository.findById(entityDto.getFacilityId());
		Facility facilityobj = facilityEntityOpt.orElseThrow(() -> new EntityNotFoundException(
				String.format("Entity not found of Facility Id [%s]", entityDto.getFacilityId())));

		// setting Facility Obj Mandatory
		deviceObj.setFacility(facilityobj);

		if (CollectionUtils.isNotEmpty(deviceObj.getDeviceParam())) {
			for (DeviceParam entityParam : deviceObj.getDeviceParam()) {
				Optional<StdParam> deviceStdParam = iStdParamRepository.findById(entityParam.getStdParamId());
				StdParam stdParambj = deviceStdParam.orElseThrow(() -> new EntityNotFoundException(
						String.format("Entity not found of StdParam Id [%s]", entityParam.getStdParamId())));
				// setting StdParam Obj Mandatory
				entityParam.setStdParam(stdParambj);
			}
		}
		Optional<Tank> tankObj = null;
		Optional<Well> wellObj = null;
		if (entityDto.getTankId() != null) {
			tankObj = iTankRepository.findById(entityDto.getTankId());
			tankObj.orElseThrow(() -> new EntityNotFoundException(
					String.format("Entity not found of Tank Id [%s]", entityDto.getTankId())));
			deviceObj.setTank(tankObj.get());
		}
		if (entityDto.getWellId() != null) {
			wellObj = iWellRepository.findById(entityDto.getWellId());
			wellObj.orElseThrow(() -> new EntityNotFoundException(
					String.format("Entity not found of Well Id [%s]", entityDto.getWellId())));
			deviceObj.setWell(wellObj.get());
		}
		deviceObj = iDeviceRepository.save(deviceObj);
		// CreateDeviceRequest llobj=this.makeLogicLadderDeviceCreateReq(deviceObj);
		// com.logicladder.dgh.thirdparty.response.dto.Device deviceRespDto
		// =iDeviceLogicLadderService.createDevice( llobj);
		// deviceObj.setCoreId(Long.valueOf(deviceRespDto.getId()));
		return deviceObj.getId();
	}

	private CreateDeviceRequest makeLogicLadderDeviceCreateReq(Device deviceObj) {
		List<Param> params = new ArrayList<Param>();

		DeviceSource deviceSource = new DeviceSource();
		deviceSource.setDataFrequency(null);
		deviceSource.setId(null);
		deviceSource.setSensorId(null);
		deviceSource.setSerialNo(deviceObj.getSerialNumber());

		Location location = new Location();
		location.setId(String.valueOf(deviceObj.getFacility().getCoreId()));

		CreateDeviceRequest createObj = mapper.map(deviceObj, CreateDeviceRequest.class);
		createObj.setDeviceSource(deviceSource);
		createObj.setName(deviceObj.getName());
		createObj.setLocation(location);

		if (CollectionUtils.isNotEmpty(createObj.getParams())) {
			for (Param param : createObj.getParams()) {
				param.setId(null);
			}
		}
		return createObj;
	}

	// TODO
	@Override
	public long merge(@Valid DeviceDto entityDto) {

		Device deviceObj = mapper.map(entityDto, Device.class);
		// Just to verify if entity exits or not otherwise it will throw the exception
		// getHibernateSession().get(Device.class, entityDto.getId());
		Optional<Model> modelEntityOpt = iModelRepository.findById(entityDto.getModelId());
		Model modelobj = modelEntityOpt.orElseThrow(() -> new EntityNotFoundException(
				String.format("Entity not found of Model Id [%s]", entityDto.getModelId())));

		deviceObj.setModel(modelobj);

		Optional<Facility> facilityEntityOpt = iFacilityRepository.findById(entityDto.getFacilityId());
		Facility facilityobj = facilityEntityOpt.orElseThrow(() -> new EntityNotFoundException(
				String.format("Entity not found of Facility Id [%s]", entityDto.getFacilityId())));

		deviceObj.setFacility(facilityobj);

		if (CollectionUtils.isNotEmpty(deviceObj.getDeviceParam())) {
			for (DeviceParam entityParam : deviceObj.getDeviceParam()) {
				Optional<StdParam> deviceStdParam = iStdParamRepository.findById(entityParam.getStdParamId());
				StdParam stdParambj = deviceStdParam.orElseThrow(() -> new EntityNotFoundException(
						String.format("Entity not found of StdParam Id [%s]", entityParam.getStdParamId())));
				entityParam.setStdParam(stdParambj);
			}
		}
		Optional<Tank> tankObj = null;
		Optional<Well> wellObj = null;
		if (entityDto.getTankId() != null) {
			tankObj = iTankRepository.findById(entityDto.getTankId());
			tankObj.orElseThrow(() -> new EntityNotFoundException(
					String.format("Entity not found of Tank Id [%s]", entityDto.getTankId())));
			deviceObj.setTank(tankObj.get());
		}
		if (entityDto.getWellId() != null) {
			wellObj = iWellRepository.findById(entityDto.getWellId());
			wellObj.orElseThrow(() -> new EntityNotFoundException(
					String.format("Entity not found of Well Id [%s]", entityDto.getWellId())));
			deviceObj.setWell(wellObj.get());
		}

		getHibernateSession().merge(deviceObj);
		return entityDto.getId();
	}

	@Override
	public List<Device> findByIdIn(Iterable<Long> ids) {
		return iDeviceRepository.findAllById(ids);
	}

	@Override
	public List<Device> getAllByDeviceTypeAndFacilityId(DeviceType deviceType, Long facilityId) {
		return iDeviceDao.getAllByDeviceTypeAndFacilityId(deviceType, facilityId);
	}

	@Override
	public boolean toggleStatus(EntityStatus status, long id) {
		iDeviceDao.toogleStatus(status, id);
		return true;
	}

}
