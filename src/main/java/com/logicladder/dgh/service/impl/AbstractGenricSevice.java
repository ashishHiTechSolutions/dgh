/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.dozer.Mapper;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractGenricSevice {

	@Autowired
	EntityManagerFactory entityManagerFactory;
	
	@Autowired
	EntityManager entityManager;

	@Autowired
	Mapper mapper;

	public Session getHibernateSession() {
		return this.entityManager.unwrap(Session.class);
	}
}
