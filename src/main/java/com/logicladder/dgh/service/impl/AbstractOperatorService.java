/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;

import org.dozer.Mapper;

import com.logicladder.dgh.app.email.Email;
import com.logicladder.dgh.app.email.OperatorApprovedEmail;
import com.logicladder.dgh.app.email.OperatorCreatedEmail;
import com.logicladder.dgh.app.email.OperatorCreatedNotificationEmail;
import com.logicladder.dgh.app.email.OperatorVerificationEmail;
import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.dto.AddressDto;
import com.logicladder.dgh.dto.OperatorDto;
import com.logicladder.dgh.entity.app.Address;
import com.logicladder.dgh.entity.app.Operator;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.enums.EmailType;
import com.logicladder.dgh.security.repository.ICityRepository;
import com.logicladder.dgh.security.repository.IOperatorRepository;
import com.logicladder.dgh.service.IMailService;
import com.logicladder.dgh.service.IOperatorService;
import com.logicladder.dgh.util.Utility;

public abstract class AbstractOperatorService extends AbstractGenericServiceImpl<Operator, Long> implements
		IOperatorService {

	private EntityManager entityManager;

	private ICityRepository iCityDao;

	private IMailService iMailService;

	private IOperatorRepository iOperatorDao;

	private Mapper mapper;

	public AbstractOperatorService() {
	}

	public AbstractOperatorService(IOperatorRepository iOperatorDao, ICityRepository iCityDao, Mapper mapper,
			EntityManager entityManager, IMailService iMailService) {
		super(iOperatorDao, mapper, entityManager);
		this.iOperatorDao = iOperatorDao;
		this.iCityDao = iCityDao;
		this.mapper = mapper;
		this.entityManager = entityManager;
		this.iMailService = iMailService;
	}

	@Override
	public OperatorDto mapToDto(Operator operator) {
		OperatorDto operatorDto = this.mapper.map(operator, OperatorDto.class);
		AddressDto addressDto = this.mapper.map(operator.getAddress(), AddressDto.class);
		operatorDto.setAddressDto(addressDto);
		return operatorDto;
	}

	@Override
	public Operator mapToEntity(OperatorDto operatorDto, ApprovalStatus status) {
		Operator operator = this.mapper.map(operatorDto, Operator.class);
		operator.setAddress(new Address(operatorDto.getAddressDto().getAddressLine(), this.iCityDao.findById(
				operatorDto.getAddressDto().getCityDto().getId()).get(), operatorDto.getAddressDto()
						.getZip()));
		operator.getAddress().setId(operatorDto.getAddressDto().getId());
		operator.setStatus(status);
		return operator;
	}

	protected void sendOperatorApprovedEmail(Operator operator) {
		OperatorApprovedEmail email = (OperatorApprovedEmail) Email.createGenericEmail(
				EmailType.OPERATOR_APPROVED);
		email.setCompanyName(operator.getCompanyName());
		email.setOperatorName(operator.getName());
		email.setUsername(new String[] { operator.getEmail() });
		try {
			this.iMailService.sendEmail(email);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void sendOperatorCreatedEmail(Operator operator) {
		OperatorCreatedEmail email = (OperatorCreatedEmail) Email.createGenericEmail(
				EmailType.OPERATOR_CREATED);
		email.setCompanyName(operator.getCompanyName());
		email.setOperatorName(operator.getName());
		email.setUsername(new String[] { operator.getEmail() });
		try {
			this.iMailService.sendEmail(email);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void sendOperatorVerificationEmail(Operator operator, String emailOtp, String mobilOtp) {
		OperatorVerificationEmail email = (OperatorVerificationEmail) Email.createGenericEmail(
				EmailType.OPERATOR_VERIFICATION);
		email.setCompanyName(operator.getCompanyName());
		email.setOperatorName(operator.getName());
		email.setUsername(new String[] { operator.getEmail() });
		email.setEmailOtp(emailOtp);
		email.setMobileOtp(mobilOtp);
		try {
			this.iMailService.sendEmail(email);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void checkIfOperatorEmailValid(String email) throws CustomException {

		Operator operator = iOperatorDao.findByEmail(email);
		if (operator != null) {
			if (operator.getStatus().equals(ApprovalStatus.ACTIVE)) {
				throw new CustomException(19, new Exception("Email account is not unique"));
			} else {
				iOperatorDao.delete(operator);
			}
		}
	}

	protected void sendOperatorCreatedNotificationEmail(Operator operator) {
		OperatorCreatedNotificationEmail email = (OperatorCreatedNotificationEmail) Email.createGenericEmail(
				EmailType.OPERATOR_CREATED_NOTIFICATION_EMAIL);
		StringBuilder builder = new StringBuilder();
		builder.append(Utility.getActiveSpringProfileBasedConfig().get("application.url") + "/");
		builder.append(Utility.getActiveSpringProfileBasedConfig().get("application.context") + "/");
		builder.append("account/operator?id=" + operator.getId());
		builder.append("&email=" + operator.getEmail());
		builder.append("&emailOtp=" + operator.getEmailOtp());
		email.setVerifyOtpLink(builder.toString());
		email.setOperatorEmail(operator.getEmail());
		email.setUsername(new String[] { operator.getEmail() });
		try {
			this.iMailService.sendEmail(email);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
