package com.logicladder.dgh.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.dto.GroupDto;
import com.logicladder.dgh.entity.app.Group;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.GroupEntityType;
import com.logicladder.dgh.security.repository.IDeviceRepository;
import com.logicladder.dgh.security.repository.IFacilityRepository;
import com.logicladder.dgh.security.repository.IGroupRepository;
import com.logicladder.dgh.security.repository.ITankRepository;
import com.logicladder.dgh.security.repository.IUserRepository;
import com.logicladder.dgh.security.repository.IWellRepository;
import com.logicladder.dgh.service.IGroupService;

@Service
@Transactional
public class GroupServiceImpl extends AbstractGenricSevice implements IGroupService {

	@Autowired
	private IGroupRepository iGroupRepository;

	@Autowired
	private IFacilityRepository iFacilityRepository;

	@Autowired
	private ITankRepository iTankRepository;

	@Autowired
	private IWellRepository iWellRepository;

	@Autowired
	private IDeviceRepository iDeviceRepository;

	@Autowired
	private IUserRepository iUserRepository;

	@Override
	public Group create(@Valid GroupDto entityDto) throws CustomException {
		Group group = this.mapper.map(entityDto, Group.class);
		group = iGroupRepository.save(group);
		return group;
	}

	@Override
	public List<Group> getAllByStatus(EntityStatus status) {
		List<Group> listEntity = this.iGroupRepository.findByStatus(status);

		return CollectionUtils.isEmpty(listEntity) == true ? null : listEntity;
	}

	@Override
	public boolean delete(GroupDto GroupDto) {

		return false;
	}

	@Override
	public boolean deleteById(Long id) {
		try {

			iGroupRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Group getById(Long id) {

		Optional<Group> objOpt = this.iGroupRepository.findById(id);
		Group group = objOpt
				.orElseThrow(() -> new EntityNotFoundException(String.format("Entity not found for Id [%s]", id)));

		group = setGroupEntityAndUser(group);

		return group;
	}
	
	private Group setGroupEntityAndUser(Group group) {
		
		if (group.getEntityList() != null) {

			List<Long> ids = getIdListFromString(group.getEntityList());
			if (ids.size() > 0) {
				if (GroupEntityType.FACILITY == group.getType()) {
					group.setEntities(iFacilityRepository.findAllById(ids));
				} else if (GroupEntityType.TANK == group.getType()) {
					group.setEntities(iTankRepository.findAllById(ids));
				} else if (GroupEntityType.WELL == group.getType()) {
					group.setEntities(iWellRepository.findAllById(ids));
				} else if (GroupEntityType.DEVICE == group.getType()) {
					group.setEntities(iDeviceRepository.findAllById(ids));
				}
			}
		}

		if (group.getUserList() != null) {
			List<Long> ids = getIdListFromString(group.getUserList());
			if(ids.size() > 0) {
				group.setUsers(iUserRepository.findAllById(ids));
			}
		}
		
		return group;
	}

	private List<Long> getIdListFromString(String strIds) {
		List<Long> ids = new ArrayList<Long>();
		if (strIds != null && !"".equalsIgnoreCase(strIds)) {
			String[] strArray = strIds.split(",");
			for (int i = 0; i < strArray.length; i++) {
				try {
					if (!"".equalsIgnoreCase(strArray[i])) {
						ids.add(Long.parseLong(strArray[i]));
					}

				} catch (Exception e) {
					// TODO: Handle exception for formatting
				}
			}
		}
		return ids;
	}

	@Override
	public Group update(GroupDto groupDto) {
		Group group = this.mapper.map(groupDto, Group.class);
		iGroupRepository.save(group);
		group = setGroupEntityAndUser(group);
		return group;
	}

	@Override
	public List<Group> findByIdIn(Iterable<Long> ids) {
		return iGroupRepository.findAllById(ids);
	}

}
