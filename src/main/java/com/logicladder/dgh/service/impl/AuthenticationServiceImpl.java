/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.logicladder.dgh.app.email.Email;
import com.logicladder.dgh.app.email.ForgotPasswordEmail;
import com.logicladder.dgh.app.email.LoginEmail;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.entity.app.UserAuthData;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.enums.EmailType;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.enums.OTP_Type;
import com.logicladder.dgh.logic.ladder.service.impl.LogicLadderUserAPI;
import com.logicladder.dgh.response.LoginResponse;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.JwtTokenUtil;
import com.logicladder.dgh.security.JwtUser;
import com.logicladder.dgh.security.controller.CusAuthenticationException;
import com.logicladder.dgh.security.repository.IUserRepository;
import com.logicladder.dgh.service.IAuthenticationService;
import com.logicladder.dgh.service.IMailService;
import com.logicladder.dgh.service.IUserAuthDataService;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.util.Utility;

@Service
@Transactional
public class AuthenticationServiceImpl extends AbstractLogicLadderUserAPI implements IAuthenticationService {

	@Autowired
	private IMailService iMailService;

	@Autowired
	private IUserAuthDataService iUserAuthDataService;

	@Autowired
	private IUserRepository iUserDao;

	@Autowired
	private IUserService iUserService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

	@Autowired
	@Qualifier("jwtUserDetailsService")
	private UserDetailsService userDetailsService;

	@Autowired
	public AuthenticationServiceImpl(RestTemplate restTemplate) {
		super(restTemplate);
	}

	@Override
	public void checkIfLoginValid(User user) {
		if (user.getStatus().equals(ApprovalStatus.ACTIVE)) {
			if ((user != null) && (user.getFailedAttemptCount() < 2)) {
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.DAY_OF_YEAR, -11);
				Date dateAllowedForPassword = calendar.getTime();

				if ((user.getPasswordLastChangeDate() == null) || user.getPasswordLastChangeDate().after(
						dateAllowedForPassword)) {
					return;
				} else {
					throw new CusAuthenticationException(Response.of(ErrorCode.getEnumByErrorCode(17),
							"Password has expired", ApplicationConstants.ResponseMessage.FAILURE));
				}

			} else {
				throw new CusAuthenticationException(Response.of(ErrorCode.getEnumByErrorCode(18),
						"Too many failed attempts for this account",
						ApplicationConstants.ResponseMessage.FAILURE));
			}
		} else {
			throw new CusAuthenticationException(Response.of(ErrorCode.ACCOUNT_INACTIVE,
					"User is not active in system, please contact administrator.",
					ApplicationConstants.ResponseMessage.FAILURE));
		}
	}

	@Override
	public Response<?> forgotPassword(String emailId) throws InterruptedException {

		User user = this.iUserService.findByUserName(emailId);
		UserAuthData authData = this.iUserAuthDataService.findByUserUserName(emailId);

		if (user != null) {
			String jwtToken = this.jwtTokenUtil.generateToken(new JwtUser(user));
			String tempJwtToken = this.jwtTokenUtil.generateToken(new JwtUser(user), Boolean.FALSE);
			if (authData != null) {
				authData.setJwtToken(jwtToken);
				authData.setOtpVerified(Boolean.FALSE);
				authData.setUser(user);
				authData.setTempJwtToken(tempJwtToken);
			} else {
				authData = new UserAuthData(jwtToken, tempJwtToken, null, Boolean.FALSE, null, null, null,
						user);
			}
			this.iUserAuthDataService.saveOrUpdate(authData);
			this.sendForgotPasswordEmail(tempJwtToken, emailId, user.getName());
			return new Response(ApplicationConstants.ResponseMessage.SUCCESS,
					"Email sent to registered mail address", new HashMap<>());
		}

		throw new CusAuthenticationException(Response.of(ErrorCode.ACCOUNT_DOES_NOT_EXIST,
				"Account Does not exist", ApplicationConstants.ResponseMessage.FAILURE));
	}

	@Override
	public String loginUser(String username, String password, String deviceId, String ipAddress)
			throws CusAuthenticationException {

		LoginResponse loginResponse = null;

		final UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

		User user = this.iUserService.findByUserName(username);
		this.checkIfLoginValid(user);
		UserAuthData authData = this.iUserAuthDataService.findByUserUserName(username);
		loginResponse = LogicLadderUserAPI.login(username, password);
		String OTP = null;
		if ((loginResponse != null) && !loginResponse.getRedirectUrl().isEmpty()) {

			// TODO change this for active accounts.
			if (!username.equalsIgnoreCase("aman.sharma+dgh77@logicladder.com")) {
				OTP = Utility.generateOTP(user.getPhone(), OTP_Type.LOGIN);
			} else {
				OTP = "123456";
			}
			if (authData == null) {
				final String tempToken = this.jwtTokenUtil.generateToken((JwtUser) userDetails, false);
				final String token = this.jwtTokenUtil.generateToken((JwtUser) userDetails, true);
				String redirectUrl = loginResponse.getRedirectUrl();
				String authTokenLL = redirectUrl.substring(redirectUrl.lastIndexOf("=") + 1);
				authData = new UserAuthData(token, tempToken, authTokenLL, Boolean.FALSE, deviceId, ipAddress,
						OTP, user);
			} else {
				final String tempToken1 = this.jwtTokenUtil.generateToken((JwtUser) userDetails, false);
				String redirectUrl = loginResponse.getRedirectUrl();
				String authTokenLL = redirectUrl.substring(redirectUrl.lastIndexOf("=") + 1);
				authData = new UserAuthData(authData.getId(), authData.getJwtToken(), tempToken1, authTokenLL,
						Boolean.FALSE, deviceId, ipAddress, OTP, user);

				if (!this.jwtTokenUtil.getExpirationDateFromToken(authData.getJwtToken()).after(Calendar
						.getInstance().getTime())) {
					authData.setJwtToken(this.jwtTokenUtil.generateToken((JwtUser) userDetails, false));
				}
			}
		} else {
			user.setFailedAttemptCount(user.getFailedAttemptCount() + 1);
			this.iUserService.saveOrUpdate(user);
			throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
		}
		this.iUserAuthDataService.saveOrUpdate(authData);
		user.setFailedAttemptCount(0);
		this.iUserService.saveOrUpdate(user);
		this.sendOtpToUser(OTP, user);
		return authData.getTempJwtToken();

	}

	private void sendForgotPasswordEmail(String jwtToken, String emailId, String name)
			throws InterruptedException {
		ForgotPasswordEmail email = (ForgotPasswordEmail) Email.createGenericEmail(EmailType.FORGOT_PASSWORD);
		StringBuilder builder = new StringBuilder();

		builder.append(Utility.getActiveSpringProfileBasedConfig().get("application.url") + "/");
		builder.append(Utility.getActiveSpringProfileBasedConfig().get("application.context") + "/");
		builder.append("user/setpassword?localAuthToken=");
		builder.append(jwtToken);
		email.setPasswordLink(builder.toString());
		email.setName(name);
		email.setUsername(new String[] { emailId });

		try {
			this.iMailService.sendEmail(email);
		} catch (InterruptedException e) {
			this.logger.warn("System Error sending email to user");
			e.printStackTrace();
			throw e;
		}
	}

	private void sendOtpToUser(String OTP, User user) {
		LoginEmail email = (LoginEmail) Email.createGenericEmail(EmailType.LOGIN);
		email.setUsername(new String[] { user.getUserName() });
		email.setOtp(OTP);
		try {
			this.iMailService.sendEmail(email);
		} catch (InterruptedException e) {
			this.logger.warn("Error sending mail to user" + e.getMessage());
		}
	}

	@Override
	public Response<Object> verifyOtp(String otp, String username, String jwtToken)
			throws CusAuthenticationException {
		this.logger.info("OTP Validation");
		Map<Object, Object> responseData = new HashMap<>();

		User user = this.iUserService.findByUserName(username);
		UserAuthData userAuthData = this.iUserAuthDataService.findByUserUserName(username);
		if (userAuthData.getOtp().equalsIgnoreCase(otp)) {
			userAuthData.setOtpVerified(Boolean.TRUE);
//			userAuthData.setTempJwtToken("");
			this.iUserAuthDataService.saveOrUpdate(userAuthData);
			responseData.put("authToken", userAuthData.getJwtToken());
			responseData.put("user", user);
			Response<Object> response = new Response<>(ApplicationConstants.ResponseMessage.SUCCESS,
					"OTP Verified", responseData);
			return response;
		}
		throw new CusAuthenticationException(Response.of(ErrorCode.OTP_VERIFICATION, "Otp is not valid",
				ApplicationConstants.ResponseMessage.FAILURE));

	}
}
