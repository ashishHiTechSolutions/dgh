/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.Page;
import com.logicladder.dgh.security.repository.IPageRepository;
import com.logicladder.dgh.service.IPageService;

@Service
@Transactional
public class PageServiceImpl extends AbstractGenericServiceImpl<Page, Long> implements IPageService {

	private IPageRepository iPageDao;

	public PageServiceImpl() {

	}

	@Autowired
	public PageServiceImpl(IPageRepository iPageDao, Mapper mapper, EntityManager entityManager) {
		super(iPageDao, mapper, entityManager);
	}

}
