package com.logicladder.dgh.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.entity.app.Unit;
import com.logicladder.dgh.logic.ladder.service.IUnitLogicLadderService;
import com.logicladder.dgh.security.repository.IUnitRepository;
import com.logicladder.dgh.service.IUnitService;
import com.logicladder.dgh.thirdparty.response.dto.Units;

@Transactional
@Validated
@Service
public class UnitServiceImpl extends AbstractGenricSevice implements IUnitService {

	@Autowired
	IUnitLogicLadderService iUnitLogicLadderService;
	
	@Autowired
	IUnitRepository iUnitRepository;

	/*
	 * Should be Executed out of boundary Transaction. Since it would be bulk update
	 * and may take time
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@Override
	public List<Unit> save() {
		Units[] llUnitsResp = iUnitLogicLadderService.getUnits(null);
		List<Unit> listEntityUnits = new ArrayList<>();
		for (Units llunits : llUnitsResp) {

			Unit entityUnit = mapper.map(llunits, Unit.class);
			entityUnit.setId(null);
			entityUnit.setCoreId(Long.valueOf(llunits.getId()));
			listEntityUnits.add(entityUnit);

		}

		EntityManager cusEntitymanager = null;
		EntityTransaction entityTransaction = null;
		try {
			cusEntitymanager = entityManagerFactory.createEntityManager();
			entityTransaction = cusEntitymanager.getTransaction();
			entityTransaction.begin();
			int counter = 0;
			int batchSize = 25;
			for (Unit unitEntity : listEntityUnits) {
				if (counter > 0 && counter % batchSize == 0) {
					cusEntitymanager.flush();
					cusEntitymanager.clear();
					counter++;
				}

				cusEntitymanager.persist(unitEntity);
			}
			entityTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (entityTransaction != null && entityTransaction.isActive())
				entityTransaction.rollback();
			throw e;
		} finally {
			if (cusEntitymanager != null) {
				cusEntitymanager.close();
			}
		}
		return listEntityUnits;
	}

	@Override
	public List<Unit> getAllUnit() {
		return iUnitRepository.findAll();
	}

}
