/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.xml.ws.http.HTTPException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.logicladder.dgh.app.email.Email;
import com.logicladder.dgh.app.email.RegistrationAlertEmail;
import com.logicladder.dgh.app.email.RegistrationEmail;
import com.logicladder.dgh.app.email.UserCreatedEmail;
import com.logicladder.dgh.app.email.UserVerificationEmail;
import com.logicladder.dgh.dto.UserDto;
import com.logicladder.dgh.entity.app.Address;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.entity.app.Role;
import com.logicladder.dgh.entity.app.SuperEntity;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.entity.app.UserAuthData;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.enums.EmailType;
import com.logicladder.dgh.security.repository.IUserRepository;
import com.logicladder.dgh.service.ICityService;
import com.logicladder.dgh.service.ICountryService;
import com.logicladder.dgh.service.IFacilityService;
import com.logicladder.dgh.service.IMailService;
import com.logicladder.dgh.service.IOperatorService;
import com.logicladder.dgh.service.IRoleService;
import com.logicladder.dgh.service.IUserAuthDataService;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.util.Utility;

public abstract class AbstractUserServiceImpl extends AbstractGenericServiceImpl<User, Long> implements
		IUserService {

	private static final Logger logger = LogManager.getLogger(AbstractUserServiceImpl.class);

	private ICityService iCityService;

	private IMailService iMailService;

	private IOperatorService iOperatorService;

	private IRoleService iRoleService;

	protected IUserRepository iUserDao;

	private Mapper mapper;

	@Autowired
	private IFacilityService ifacilityService;

	public AbstractUserServiceImpl() {

	}

	public AbstractUserServiceImpl(IUserRepository iUserDao, IRoleService iRoleService,
			IMailService iMailService, IOperatorService iOperatorService,
			IUserAuthDataService iUserAuthDataService, ICityService iCityService,
			ICountryService iCountryService, Mapper mapper, EntityManager entityManager) {
		super(iUserDao, mapper, entityManager);
		this.iUserDao = iUserDao;
		this.iMailService = iMailService;
		this.iCityService = iCityService;
		this.iRoleService = iRoleService;
		this.iOperatorService = iOperatorService;
		this.mapper = mapper;
	}

	protected void changeUserRequestStatus(Long id, ApprovalStatus newStatus) {
		Optional<User> userOptional = this.iUserDao.findById(id);
		try {
			User user = userOptional.get();
			user.setStatus(ApprovalStatus.PENDING);
			this.iUserDao.saveAndFlush(user);
		} catch (NoSuchElementException e) {
			throw new HTTPException(500);
		}
	}

	protected Map<String, List<? extends SuperEntity>> createRegistrationData() {
		Map<String, List<? extends SuperEntity>> preloadData = new HashMap<>();
		List<Role> roles = this.iRoleService.getAll();
		roles = roles.parallelStream().filter(role -> !"Super Admin".equals(role.getRoleName())).collect(
				Collectors.toList());
		preloadData.put("userRole", roles);
		preloadData.put("contractors", this.iOperatorService.getAll());
		preloadData.put("cities", this.iCityService.getAll());
		return preloadData;
	}

	protected User createUserEntity(UserDto userDto, ApprovalStatus status) {
		User userEntity = this.mapToEntity(userDto);
		return userEntity;
	}

	@Override
	public UserDto mapToDto(User user) {

		UserDto userDto = this.mapper.map(user, UserDto.class);

		userDto.setAddressLine(user.getAddress().getAddressLine());
		userDto.setZip(user.getAddress().getZip());
		userDto.setCityId(user.getAddress().getCity().getId());
		userDto.setOperatorId(user.getOperator().getId());
		userDto.setUserRoleId(user.getUserRole().getId());
		return userDto;
	}

	@Override
	public User mapToEntity(UserDto userDto) {
		User userEntity = this.mapper.map(userDto, User.class);
		userEntity.setAddress(new Address(userDto.getAddressLine(), this.iCityService.get(userDto
				.getCityId()), userDto.getZip()));
		userEntity.setOperator(this.iOperatorService.get(userDto.getOperatorId()));
		userEntity.setUserRole(this.iRoleService.get(userDto.getUserRoleId()));
		userEntity.setFacilities(this.findFacilitiesById(userDto.getAccessibleEntities()));
		return userEntity;
	}

	protected Set<Facility> findFacilitiesById(String accessibleEntities) {
		Set<Facility> facilities = null;
		if (accessibleEntities != null && !accessibleEntities.isEmpty()) {
			Long[] ids = null;
			if (accessibleEntities.contains(",")) {
				ids = Arrays.stream(accessibleEntities.split(",")).mapToLong(Long::parseLong).boxed().toArray(
						Long[]::new);
			} else {
				ids = new Long[] { Long.valueOf(accessibleEntities) };
			}
			List<Long> facilityIds = Arrays.asList(ids);
			facilities = new HashSet<>(ifacilityService.findByIdIn(facilityIds));
		}
		return facilities;
	}

	protected void sendEmail(Email email) {
		try {
			this.iMailService.sendEmail(email);
		} catch (InterruptedException e) {
			AbstractUserServiceImpl.logger.warn("Error sending mail to user" + e.getMessage());
		}

	}

	protected void sendRegistrationAlertToAdmins(List<User> admins, String userName) {
		RegistrationAlertEmail email = (RegistrationAlertEmail) Email.createGenericEmail(
				EmailType.NEW_REGISTRATION_ALERT);
		email.setUsername(admins.stream().map(user -> user.getUserName()).collect(Collectors.toList())
				.toArray(new String[] {}));
		email.setUser(userName);
		this.sendEmail(email);
	}

	protected void sendRegistrationEmailToUser(String username, String authToken) {
		RegistrationEmail email = (RegistrationEmail) Email.createGenericEmail(EmailType.REGISTRATION);
		StringBuilder builder = new StringBuilder();

		builder.append(Utility.getActiveSpringProfileBasedConfig().get("application.url") + "/");
		builder.append(Utility.getActiveSpringProfileBasedConfig().get("application.context") + "/");
		builder.append("user/setpassword?localAuthToken=");
		builder.append(authToken);
		email.setPasswordLink(builder.toString());
		email.setUsername(new String[] { username });
		this.sendEmail(email);
	}

	protected void sendAccountCreationConfirmationToUser(User user, UserAuthData authData) {
		UserCreatedEmail email = (UserCreatedEmail) Email.createGenericEmail(EmailType.USER_CREATED);
		StringBuilder builder = new StringBuilder();
		builder.append(Utility.getActiveSpringProfileBasedConfig().get("application.url") + "/");
		builder.append(Utility.getActiveSpringProfileBasedConfig().get("application.context") + "/");
		builder.append("user/signup?emailOtp=");
		builder.append(user.getEmailOtp());
		builder.append("&authToken=");
		builder.append(authData.getTempJwtToken());
		email.setLink(builder.toString());
		email.setName(user.getName());
		email.setEmailOtp(user.getEmailOtp());
		email.setUsername(new String[] { user.getUserName() });
		this.sendEmail(email);
	}

	protected void sendAccountVerificationConfirmationToUser(User user) {

		UserVerificationEmail email = (UserVerificationEmail) Email.createGenericEmail(
				EmailType.USER_VERIFICATION_EMAIL);
		email.setName(user.getName());
		email.setUsername(new String[] { user.getEmail() });
		this.sendEmail(email);
	}
}
