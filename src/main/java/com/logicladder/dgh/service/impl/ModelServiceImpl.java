package com.logicladder.dgh.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.Make;
import com.logicladder.dgh.entity.app.Model;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.logic.ladder.service.IMakeLogicLadderService;
import com.logicladder.dgh.logic.ladder.service.IModelLogicLadderService;
import com.logicladder.dgh.security.repository.IMakeRepository;
import com.logicladder.dgh.security.repository.IModelRepository;
import com.logicladder.dgh.service.IModelService;

@Transactional
@Service
public class ModelServiceImpl extends AbstractModelcSevice implements IModelService {

	@Autowired
	IModelLogicLadderService iModelLogicLadderService;

	@Autowired
	IMakeLogicLadderService iMakeLogicLadderService;

	@Autowired
	IMakeRepository iMakeRepository;

	@Autowired
	IModelRepository iModelRepository;

	/*
	 * Should be Executed out of Transaction boundary. Since it would be bulk update
	 * and may take time
	 * 
	 * Mapping between Dozer entity and model is being defined in @link
	 * ModelMappings.xml File.
	 * 
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@Override
	public List<Model> save() {
		// Make[] llRespMake = iMakeLogicLadderService.getAllDeviceMake();
		// TODO Need to move to Propertied Files
		int batchSize = 25;
		List<Model> listModelEntity = new ArrayList<>();

		EntityManager cusEntitymanager = null;
		EntityTransaction entityTransaction = null;
		try {
			cusEntitymanager = entityManagerFactory.createEntityManager();
			entityTransaction = cusEntitymanager.getTransaction();
			entityTransaction.begin();
			List<com.logicladder.dgh.entity.app.Make> makeEntityList = iMakeRepository.findAll();

			for (Make entityMake : makeEntityList) {
				com.logicladder.dgh.thirdparty.response.dto.Model[] llModelresp = iModelLogicLadderService
						.getMakeModel(Long.valueOf(entityMake.getCoreId().longValue()));
				for (com.logicladder.dgh.thirdparty.response.dto.Model llmodel : llModelresp) {

					Model modelEntity = mapper.map(llmodel, Model.class);
					modelEntity.setMake(entityMake);
					// this.mapParamsToEntity(llmodel.getParams(), modelEntity);
					listModelEntity.add(modelEntity);
				}
			}
			int counter = 0;
			for (Model modelEntity : listModelEntity) {
				if (counter > 0 && counter % batchSize == 0) {
					cusEntitymanager.flush();
					cusEntitymanager.clear();
					counter++;
				}

				cusEntitymanager.persist(modelEntity);
			}
			entityTransaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
			if (entityTransaction != null && entityTransaction.isActive())
				entityTransaction.rollback();
			throw e;
		} finally {
			if (cusEntitymanager != null) {
				cusEntitymanager.close();
			}
		}

		return listModelEntity;
	}

	@Override
	public Model update() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Model> getAllModel() {
		return iModelRepository.getAllByStatus(EntityStatus.ACTIVE);
	}

	@Override
	public List<Model> getAllByMakeId(Long makeId) {
		return iModelRepository.getByMakeId(makeId);
	}

}
