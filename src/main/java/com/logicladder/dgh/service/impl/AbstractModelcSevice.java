package com.logicladder.dgh.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.logicladder.dgh.entity.app.Model;
import com.logicladder.dgh.entity.app.StdParam;
import com.logicladder.dgh.thirdparty.response.dto.Param;

public abstract class AbstractModelcSevice extends AbstractGenricSevice {

	
	/**
	 * Map params to entity.
	 * 
	 * written in case of ant explicit mapping is needed
	 *
	 * @param paramsList the params list
	 * @param modelEntity the model entity
	 */
	protected void mapParamsToEntity(List<Param> paramsList, Model modelEntity) {

		if (CollectionUtils.isEmpty(paramsList)) {
			return;
		}
		List<com.logicladder.dgh.entity.app.Param> paramEntityList = new ArrayList<>();

		for (Param llparamResp : paramsList) {
			com.logicladder.dgh.entity.app.Param paramEntity = mapper.map(llparamResp,
					com.logicladder.dgh.entity.app.Param.class);
		
			if (llparamResp.getStdParam() != null) {
				StdParam stdParamEntity = mapper.map(llparamResp.getStdParam(), StdParam.class);
				paramEntity.setStdParam(stdParamEntity);
			} else {
				paramEntity.setStdParam(null);
			}
			paramEntityList.add(paramEntity);
		}
		modelEntity.setParams(paramEntityList);

	}
}
