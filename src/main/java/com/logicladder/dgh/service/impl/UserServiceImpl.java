/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.xml.ws.http.HTTPException;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.logicladder.dgh.app.email.Email;
import com.logicladder.dgh.app.email.PasswordSetEmail;
import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.UserDto;
import com.logicladder.dgh.dto.UserPasswordChangeDto;
import com.logicladder.dgh.entity.app.Role;
import com.logicladder.dgh.entity.app.SuperEntity;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.entity.app.UserAuthData;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.enums.EmailType;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.enums.OTP_Type;
import com.logicladder.dgh.logic.ladder.service.impl.LogicLadderUserAPI;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.JwtTokenUtil;
import com.logicladder.dgh.security.JwtUserFactory;
import com.logicladder.dgh.security.controller.CusAuthenticationException;
import com.logicladder.dgh.security.repository.IRoleRepository;
import com.logicladder.dgh.security.repository.IUserRepository;
import com.logicladder.dgh.service.ICityService;
import com.logicladder.dgh.service.ICountryService;
import com.logicladder.dgh.service.IMailService;
import com.logicladder.dgh.service.IOperatorService;
import com.logicladder.dgh.service.IRoleService;
import com.logicladder.dgh.service.IUserAuthDataService;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.thirdparty.request.dto.UserPatchRequest;
import com.logicladder.dgh.util.Utility;

@Service
@Transactional
public class UserServiceImpl extends AbstractUserServiceImpl implements IUserService {

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	private IRoleRepository iRoleDao;

	private IUserAuthDataService iUserAuthDataService;

	private IUserRepository iUserDao;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	public UserServiceImpl() {
		super();
	}

	@Autowired
	public UserServiceImpl(IUserRepository iUserDao, IRoleService iRoleService, IMailService iMailService,
			IOperatorService iOperatorService, IUserAuthDataService iUserAuthDataService,
			ICityService iCityService, ICountryService iCountryService, Mapper mapper,
			EntityManager entityManager) {
		super(iUserDao, iRoleService, iMailService, iOperatorService, iUserAuthDataService, iCityService,
				iCountryService, mapper, entityManager);
		this.iUserDao = iUserDao;
		this.iUserAuthDataService = iUserAuthDataService;
	}

	@Override
	public User changePassword(UserPasswordChangeDto userPasswordChangeDto) {
		if (userPasswordChangeDto.getPassword().equals(userPasswordChangeDto.getConfirmPassword())) {
			User user = this.iUserDao.findByUserName(userPasswordChangeDto.getUserName());
			UserAuthData authData = this.iUserAuthDataService.findByUserUserName(userPasswordChangeDto
					.getUserName());
			LogicLadderUserAPI.resetUserPassword(userPasswordChangeDto, authData.getLlAuthToken());
			user.setPasswordLastChangeDate(new Date());
			this.iUserDao.save(user);
			return user;
		}
		return null;
	}

	@Override
	public Response<?> changeRequestStatus(String username, Long requestedUserId, ApprovalStatus status,
			String jwtToken, String comments, String accessibleEntities) throws CustomException {
		User user = this.iUserDao.findById(requestedUserId).orElse(null);
		if (user != null) {
			user.setStatus(status);
			user.setComment(comments);

			if (StringUtils.isNotBlank(accessibleEntities) || (user.getFacilities() != null && user
					.getFacilities().isEmpty())) {
				if (StringUtils.isNotBlank(accessibleEntities))
					user.setFacilities(this.findFacilitiesById(accessibleEntities));

				try {
					UserServiceImpl.logger.info("Before changeRequestStatus.LogicLadderUserAPI.createUser ");
					try {
						UserAuthData authData = this.iUserAuthDataService.findByUserUserName(username);
						UserServiceImpl.logger.info(
								"After changeRequestStatus.iUserAuthDataService.findByUserUserName "
										+ authData.getLlAuthToken());
						Long coreUserId = LogicLadderUserAPI.createUser(authData.getLlAuthToken(), user)
								.getId();
						UserServiceImpl.logger.info("After changeRequestStatus.LogicLadderUserAPI.createUser "
								+ coreUserId);
						user.setCoreUserId(coreUserId);
					} catch (Exception ex) {
						UserServiceImpl.logger.error("Error creating user at LL end :: " + ex.getMessage());
						throw new CusAuthenticationException(Response.of(
								ErrorCode.LOGIC_LADDER_AUTHENTICATION, ex.getMessage(),
								ApplicationConstants.ResponseMessage.FAILURE));
					}

					this.iUserDao.save(user);
					final String token = this.jwtTokenUtil.generateToken(JwtUserFactory.create(user));
					final String tempToken = this.jwtTokenUtil.generateToken(JwtUserFactory.create(user),
							Boolean.FALSE);
					UserAuthData authData = iUserAuthDataService.findByUserUserName(user.getUserName());
					if (authData != null) {
						authData = new UserAuthData(authData.getId(), token, tempToken, "", false, "", "", "",
								user);
					} else {
						authData = new UserAuthData(token, tempToken, "", false, "", "", "", user);
					}
					iUserAuthDataService.saveOrUpdate(authData);

					this.sendRegistrationEmailToUser(user.getUserName(), authData.getTempJwtToken());
				} catch (HTTPException e) {
					e.printStackTrace();
					throw e;
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
			} else {
				throw new CustomException(ErrorCode.Entities_NOT_ASSIGNED_TO_USER.getErrorCode(),
						new Exception("facilities not assigned to user"));
			}
		} else {
			throw new CusAuthenticationException(Response.of(ErrorCode.ACCOUNT_DOES_NOT_EXIST,
					"invalid user ID", ApplicationConstants.ResponseMessage.FAILURE));
		}
		Response<?> response = new Response<>(ApplicationConstants.ResponseMessage.SUCCESS,
				"account created in core system", new HashMap<>());
		return response;
	}

	@Override
	public User findByUserName(String username) {
		return this.iUserDao.findByUserName(username);
	}

	@Override
	public Response<?> getAllAccessModuleForRole(String username) {
		UserServiceImpl.logger.info("OTP Validation");

		Map<Object, Object> responseData = new HashMap<>();

		Role role = this.iUserDao.findByUserName(username).getUserRole();
		responseData.put("responseData", role);
		Response<?> response = new Response<>("Role Auth Retrieved",
				ApplicationConstants.ResponseMessage.SUCCESS, responseData);

		return response;
	}

	@Override
	public Response<?> getApplicationData(String username) {
		User user = this.iUserDao.findByUserName(username);
		Map<String, Object> responseData = new HashMap<>();
		if (user != null) {
			responseData.put("user", user);
		}
		responseData.put("status", ApprovalStatus.values());
		responseData.put("roles", this.iRoleDao.findAll());
		return new Response<>(ApplicationConstants.ResponseMessage.SUCCESS, "application data retrieved",
				responseData);
	}

	@Override
	public Response<?> getRegistrationPreloadData() {

		Map<String, List<? extends SuperEntity>> preloadData = this.createRegistrationData();

		Response<?> response = new Response<>();
		response.setResponseData(preloadData);
		response.setStatus("success");
		response.setMessage("Data Retrieval Successfull");
		return response;
	}

	@Override
	public Response<?> getRequestByStatus(String username, ApprovalStatus status) {
		Response<?> response = new Response<>();
		List<User> pendingRequests = this.iUserDao.findByStatus(status);
		Map<Object, Object> responseData = new HashMap<>();
		response.setResponseData(responseData);
		responseData.put("pendingRequests", (pendingRequests != null) && !pendingRequests.isEmpty()
				? pendingRequests
				: new ArrayList<>());
		response.setMessage(ApplicationConstants.ResponseMessage.SUCCESS);
		response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
		return response;
	}

	@Override
	public Response<?> getUserData(String username) {
		User user = this.iUserDao.findByUserName(username);
		if (user != null) {
			Map<String, User> responseData = new HashMap<>();
			responseData.put("userData", user);
			return new Response<>(ApplicationConstants.ResponseMessage.SUCCESS, "User Data Retrieval Success",
					responseData);
		}

		return null;
	}

	@Override
	public List<User> getUserListByStatus(ApprovalStatus[] status) {
		List<User> users = new ArrayList<>();
		for (ApprovalStatus userStatus : status) {
			users.addAll(this.iUserDao.findByStatus(userStatus));
		}
		return users;
	}

	@Override
	public Response<?> logOutUser(String username) {
		UserAuthData authData = this.iUserAuthDataService.findByUserUserName(username);

		if (null != authData) {
			this.iUserAuthDataService.remove(authData);
			return Response.of(200, "User Logged off from all devices",
					ApplicationConstants.ResponseMessage.SUCCESS, new HashMap<>());
		}
		return Response.of(ErrorCode.UNABLE_TO_FIND_AUTHENTICATION_DATA, "error logging off user",
				ApplicationConstants.ResponseMessage.FAILURE);
	}

	@Override
	public Response<?> patchUserWithNewData(UserPatchRequest[] userPatchRequests, long id, String jwtToken) {
		User user = this.iUserDao.findById(id).get();

		if ((userPatchRequests != null) && (userPatchRequests.length > 0)) {
			if ("status".equals(userPatchRequests[0].getAtt())) {
				try {
					com.logicladder.dgh.thirdparty.response.dto.User userResponse = LogicLadderUserAPI
							.setPatch(user, userPatchRequests, jwtToken);
					user = this.iUserDao.findByUserName(userResponse.getUserName());
					user.setStatus(ApprovalStatus.valueOf(userPatchRequests[0].getValue()));

					return new Response<>(ApplicationConstants.ResponseMessage.SUCCESS, "User Data Updated",
							new HashMap<>());

				} catch (HttpClientErrorException httpError) {
					UserServiceImpl.logger.error("Error from core system" + httpError);
				} catch (Exception e) {
					UserServiceImpl.logger.error("Error in middle layer" + e);
				}
				throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
			}

			// TODO only maps status as of now. will be capable of mapping all attributes.
		}
		return new Response<>(ApplicationConstants.ResponseMessage.SUCCESS, "No Data To Update",
				new HashMap<>());
	}

	@Override
	public Response<?> resendActivationLink(String username, Long userId) {
		User user = this.iUserDao.findById(userId).orElse(null);
		if (user != null) {

			UserAuthData userAuthData = this.iUserAuthDataService.findByUserUserName(user.getUserName());
			String tempToken = null;
			String OTP = Utility.generateOTP(user.getPhone(), OTP_Type.LOGIN);
			tempToken = this.jwtTokenUtil.generateToken(JwtUserFactory.create(user), false);
			final String token = this.jwtTokenUtil.generateToken(JwtUserFactory.create(user), true);
			if (userAuthData != null) {
				userAuthData = new UserAuthData(userAuthData.getId(), token, tempToken, null, false, null,
						null, OTP, user);
			} else {
				userAuthData = new UserAuthData(token, tempToken, null, false, null, null, OTP, user);
			}
			this.iUserAuthDataService.saveOrUpdate(userAuthData);
			this.sendRegistrationEmailToUser(user.getUserName(), tempToken);
			Response<?> response = new Response<>(ApplicationConstants.ResponseMessage.SUCCESS,
					"Email sent successfully", new HashMap<>());
			return response;
		}
		return Response.of(ErrorCode.ACCOUNT_DOES_NOT_EXIST, "Email not sent",
				ApplicationConstants.ResponseMessage.FAILURE);

	}

	@Override
	public User updatePassword(String username, String newPassword, String tempJwtToken) {
		try {
			UserAuthData authData = this.iUserAuthDataService.findByTempJwtToken(tempJwtToken);
			ApprovalStatus userStatus = iUserDao.findByUserName(username).getStatus();
			Calendar cal = Calendar.getInstance();
			cal.setTime(authData.getCreatedDate());
			cal.add(Calendar.DAY_OF_YEAR, (userStatus.equals(ApprovalStatus.ACTIVE) || userStatus.equals(
					ApprovalStatus.AWAITED)) ? 1 : 2);
			if (cal.getTime().after(new Date())) {
				if (!authData.isPasswordSet()) {
					PasswordSetEmail email = (PasswordSetEmail) Email.createGenericEmail(
							EmailType.PASSWORD_IS_SET);
					email.setUsername(new String[] { username });
					User user = this.iUserDao.findByUserName(username);
					user.setStatus(ApprovalStatus.ACTIVE);
					LogicLadderUserAPI.setPassword(user, newPassword, newPassword);
					user.setPasswordLastChangeDate(new Date());

					authData.setPasswordSet(true);
					this.iUserAuthDataService.saveOrUpdate(authData);
					this.iUserDao.save(user);
					this.sendEmail(email);
					return user;
				}
			}
			return null;
		} catch (Exception e) {
			logger.fatal(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public User updateUser(UserDto userDto, String username) {
		User user = this.createUserEntity(userDto, userDto.getStatus());
		this.iUserDao.save(user);
		String authToken = this.iUserAuthDataService.findByUserUserName(username).getLlAuthToken();
		LogicLadderUserAPI.createUser(authToken, user);
		return user;
	}

	@Override
	public String userRegistrationRequest(UserDto userDto, String userName, String jwtToken) {
		User user = null;
		ApprovalStatus status = ApprovalStatus.PENDING_OTP;
		String roleName = null;
		if ((userName != null) && !userName.isEmpty()) {
			roleName = this.iUserDao.findByUserName(userName).getUserRole().getRoleName();
		}
		if ((roleName != null) && (roleName.equalsIgnoreCase("super admin") || roleName.equalsIgnoreCase(
				"Admin"))) {
			status = ApprovalStatus.AWAITED;
		}
		user = this.createUserEntity(userDto, status);
		UserAuthData authData = null;
		if (status.equals(ApprovalStatus.PENDING_OTP)) {
			String mobileOtp = Utility.generateOTP(userDto.getPhone(), OTP_Type.REGISTRATION);
			String emailOtp = Utility.generateRandomOTP();
			user.setEmailOtp(emailOtp);
			user.setMobileOtp(mobileOtp);
			this.iUserDao.save(user);
			final String token = this.jwtTokenUtil.generateToken(JwtUserFactory.create(user));
			final String tempToken = this.jwtTokenUtil.generateToken(JwtUserFactory.create(user),
					Boolean.FALSE);
			authData = new UserAuthData(token, tempToken, "", false, "", "", "", user);
			authData.setPasswordSet(false);
			this.getHibernateSession().saveOrUpdate(authData);

			this.iUserDao.save(user);
			this.sendAccountCreationConfirmationToUser(user, authData);
		} else {
			Long coreUserId = LogicLadderUserAPI.createUser(this.iUserAuthDataService.findByJwtToken(jwtToken)
					.getLlAuthToken(), user).getId();
			user.setCoreUserId(coreUserId);
			this.iUserDao.save(user);

			final String token = this.jwtTokenUtil.generateToken(JwtUserFactory.create(user));
			final String tempToken = this.jwtTokenUtil.generateToken(JwtUserFactory.create(user),
					Boolean.FALSE);
			authData = new UserAuthData(token, tempToken, "", false, "", "", "", user);
			authData.setPasswordSet(false);
			this.getHibernateSession().saveOrUpdate(authData);
			this.sendRegistrationEmailToUser(user.getUserName(), tempToken);
		}

		return authData.getTempJwtToken();
	}

	@Override
	public Boolean verifyOtp(String emailOtp, String mobileOtp, String username) {
		User user = iUserDao.findByUserName(username);
		if (user.getEmailOtp().equals(emailOtp) && user.getMobileOtp().equals(mobileOtp)) {
			user.setStatus(ApprovalStatus.PENDING);
			user.setMobileOtp("");
			user.setEmailOtp("");

			iUserDao.save(user);
			this.sendAccountVerificationConfirmationToUser(user);
			List<User> admins = this.iUserDao.findByUserRoleRoleNameAndStatus("Super Admin",
					ApprovalStatus.ACTIVE);
			admins.addAll(this.iUserDao.findByUserRoleRoleNameAndStatus("Admin", ApprovalStatus.ACTIVE));
			this.sendRegistrationAlertToAdmins(admins, user.getName());
			return true;
		}

		return false;
	}

	@Override
	public boolean resendOtp(String username) {
		try {
			User user = iUserDao.findByUserName(username);
			UserAuthData authData = iUserAuthDataService.findByUserUserName(username);
			if (user.getStatus().equals(ApprovalStatus.ACTIVE)) {
				String otp = Utility.generateOTP(user.getPhone(), OTP_Type.LOGIN);
				authData.setOtp(otp);
				iUserAuthDataService.saveOrUpdate(authData);
			} else {
				String emailOtp = Utility.generateRandomOTP();
				String mobileOtp = Utility.generateOTP(user.getPhone(), OTP_Type.REGISTRATION);
				user.setMobileOtp(mobileOtp);
				user.setEmailOtp(emailOtp);
				iUserDao.save(user);
				this.sendAccountCreationConfirmationToUser(user, authData);
			}

			return true;
		} catch (Exception e) {
			throw e;
		}
	}
}
