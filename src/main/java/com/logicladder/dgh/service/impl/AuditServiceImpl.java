package com.logicladder.dgh.service.impl;

import com.logicladder.dgh.dto.AuditLogDto;
import com.logicladder.dgh.dto.AuditResponseDto;
import com.logicladder.dgh.entity.app.AuditLog;
import com.logicladder.dgh.security.repository.IAuditRepository;
import com.logicladder.dgh.service.IAuditService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.stream.Collectors;

@Service
public class AuditServiceImpl implements IAuditService {

    @Autowired
    IAuditRepository auditRepository;

    @Override
    public AuditResponseDto getAuditLogs(Date startDate, Date endDate, String entity, int page, int size) {
        AuditResponseDto auditResponseDto = new AuditResponseDto();
        Page<AuditLog> auditLogs = null;
        Pageable pageable = PageRequest.of(page, size, new Sort(Sort.Direction.DESC, "changedOn", "changedAt"));
        if (null != startDate && null != endDate && StringUtils.isNotEmpty(entity)) {
            auditLogs = auditRepository.findByEntityAndChangedOnBetween(entity, startDate, endDate, pageable);
        } else {
            auditLogs = auditRepository.findAll(pageable);
        }
        auditResponseDto.setPage(page);
        auditResponseDto.setSize(size);
        auditResponseDto.setTotal(auditLogs.getTotalPages());
        auditResponseDto.setTotalElements(auditLogs.getTotalElements());
        auditResponseDto.setAuditLogs(auditLogs.getContent().stream().map(auditLog -> transformToAuditLogDto(auditLog)).collect(Collectors.toList()));
        return auditResponseDto;
    }

    private AuditLogDto transformToAuditLogDto(AuditLog auditLog) {
        AuditLogDto auditLogDto = new AuditLogDto();
        BeanUtils.copyProperties(auditLog, auditLogDto);
        if (null != auditLog.getUser()) {
            auditLogDto.setUserId(auditLog.getUser().getId());
            auditLogDto.setUserName(auditLog.getUser().getUserName());
            auditLogDto.setEmail(auditLog.getUser().getEmail());
            if (null != auditLog.getUser().getUserRole())
                auditLogDto.setUserType(auditLog.getUser().getUserRole().getRoleName());
        }
        return auditLogDto;
    }
}
