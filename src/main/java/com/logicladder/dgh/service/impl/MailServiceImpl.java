/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.logicladder.dgh.app.email.Email;
import com.logicladder.dgh.service.IMailService;
import com.logicladder.dgh.util.Utility;

import freemarker.template.Configuration;
import freemarker.template.Template;

@Service
public class MailServiceImpl implements IMailService {

	private static final Integer DO_NOT_SEND_EMAIL = 0;

	private static final String DUMMY_EMAIL_CONTENT = "This is a dummy Email.";

	private static Logger logger = LogManager.getLogger(MailServiceImpl.class);

	private static final Integer SEND_AS_DUMMY_EMAIL = 1;

	private static final Integer SEND_OFFICIAL_EMAIL = 2;

	@Autowired
	Configuration freemarkerConfiguration;

	@Autowired
	JavaMailSender mailSender;

	private Integer checkIfMailTobeSent() {
		Properties prop = Utility.getActiveSpringProfileBasedConfig();

		Boolean dummyEmailFlag = Boolean.valueOf(prop.getProperty("mark.email.as.dummy"));
		Boolean sendEmailFlag = Boolean.valueOf(prop.getProperty("send.email.flag"));

		return sendEmailFlag ? (dummyEmailFlag ? MailServiceImpl.SEND_AS_DUMMY_EMAIL
				: MailServiceImpl.SEND_OFFICIAL_EMAIL) : MailServiceImpl.DO_NOT_SEND_EMAIL;
	}

	public String geFreeMarkerTemplateContent(Map<String, Email> model) {
		try {
			File file = new File(this.getClass().getResource("/templates").toURI());
			this.freemarkerConfiguration.setDirectoryForTemplateLoading(file);
			Template t = this.freemarkerConfiguration.getTemplate(model.values().iterator().next()
					.getEmailLocation());
			return FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
		} catch (Exception e) {
			MailServiceImpl.logger.error("Exception occured while processing fmtemplate:" + e.getMessage());
		}
		return "";
	}

	private MimeMessagePreparator getMessagePreparator(final Email email) {

		return mimeMessage -> {
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,
					MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

			helper.setSubject(email.getSubject());
			helper.setFrom(email.getFrom());
			helper.setTo(email.getUsername());
			if (MailServiceImpl.this.checkIfMailTobeSent() == MailServiceImpl.SEND_AS_DUMMY_EMAIL) {
				email.setAdditionalContent(MailServiceImpl.DUMMY_EMAIL_CONTENT);
			}

			Map<String, Email> model = new HashMap<>();
			model.put("email", email);

			String text = MailServiceImpl.this.geFreeMarkerTemplateContent(model);
			MailServiceImpl.logger.info("Template content : " + text);

			// use the true flag to indicate you need a multipart message
			helper.setText(text, true);

			/*
			 * Add attachment in cases needed. helper.addAttachment("attachmentSample.png",
			 * new ClassPathResource("test.png"));
			 */

		};
	}

	@Async
	@Override
	public void sendEmail(Email email) throws InterruptedException {
		if (email.getUsername().length > 0) {

			MailServiceImpl.logger.info("Control received at sendEmail with email content as : " + email);
			MimeMessagePreparator preparator = this.getMessagePreparator(email);
			try {
				if ((this.checkIfMailTobeSent() == MailServiceImpl.SEND_OFFICIAL_EMAIL) || (this
						.checkIfMailTobeSent() == MailServiceImpl.SEND_AS_DUMMY_EMAIL)) {
					MailServiceImpl.logger.info("Sending email now");
					this.mailSender.send(preparator);
					MailServiceImpl.logger.info("Email has been sent ");
				} else {
					MailServiceImpl.logger.info("Email not sent based on application settings" + email);
				}
			} catch (MailException ex) {
				ex.printStackTrace();
				MailServiceImpl.logger.error(email.getEmailType() + " Email not sent due to " + ex
						.getMessage());
			} catch (Exception ex) {
				ex.printStackTrace();
				MailServiceImpl.logger.error(email.getEmailType() + " Email not sent due to " + ex
						.getMessage());
			}

		} else {
			MailServiceImpl.logger.error(email.getEmailType()
					+ " Email not sent due to since recipient list is empty");
		}

	}
}