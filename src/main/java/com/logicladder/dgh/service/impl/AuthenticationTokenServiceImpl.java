/*
 * 
 */
package com.logicladder.dgh.service.impl;

import javax.persistence.EntityManager;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logicladder.dgh.entity.app.JwtToken;
import com.logicladder.dgh.security.repository.IAuthenticationTokenRepository;

@Service
@Transactional
public class AuthenticationTokenServiceImpl extends AbstractGenericServiceImpl<JwtToken, Long> {

	private IAuthenticationTokenRepository iAuthDao;

	public AuthenticationTokenServiceImpl() {
	}

	@Autowired
	public AuthenticationTokenServiceImpl(IAuthenticationTokenRepository iAuthDao, Mapper mapper,
			EntityManager entityManager) {
		super(iAuthDao, mapper, entityManager);
	}

}
