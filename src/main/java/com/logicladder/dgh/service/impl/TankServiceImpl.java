/*
 * 
 */
package com.logicladder.dgh.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.dao.ITankDao;
import com.logicladder.dgh.dto.TankDto;
import com.logicladder.dgh.entity.app.Tank;
import com.logicladder.dgh.entity.app.Well;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.security.repository.IFacilityRepository;
import com.logicladder.dgh.security.repository.ITankRepository;
import com.logicladder.dgh.security.repository.IWellRepository;
import com.logicladder.dgh.service.ITankService;
import com.logicladder.dgh.thirdparty.request.dto.GenricEntityCreationRequestDto;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderEnumUtil.EntityTypeLogicLadder;
import com.logicladder.dgh.util.Utility;

@Service
@Transactional
public class TankServiceImpl extends AbstractGenricSevice implements ITankService {

	@Autowired
	private ITankDao iTankDao;

	@Autowired
	private ITankRepository iTankRepository;

	@Autowired
	private IWellRepository iWellRepository;

	@Autowired
	private SecurityContextUserDetails securityContextUserDetails;

	@Autowired
	private IAsyncLogicLadderService iAsyncLogicLadderService;

	@Autowired
	private IFacilityRepository iFacilityRepository;

	@Override
	public Tank create(TankDto entityDto) {
		Tank objTank = this.mapper.map(entityDto, Tank.class);

		Set<Well> wells = new HashSet<>();
		if (entityDto.getWellIds() != null) {
			for (Long wellId : entityDto.getWellIds()) {
				Optional<Well> obj = this.iWellRepository.findById(wellId);
				obj.orElseThrow(() -> new EntityNotFoundException(String.format("Well not found for Id [%s]",
						wellId)));
				wells.add(obj.get());
			}
		}
		objTank.setWells(wells);

		if (entityDto.getLastCalibration() != null)
			objTank.setLastCalibrationDate(Utility.createDateFromString(entityDto.getLastCalibration()));
		Integer parentCoreId = Integer.valueOf(iFacilityRepository.findById(entityDto.getFacilityId()).get()
				.getCoreId().intValue());

		objTank.setCoreId(iAsyncLogicLadderService.createEntityRequest(new GenricEntityCreationRequestDto(
				parentCoreId, EntityTypeLogicLadder.TANK.getDescription(), entityDto.getName()), null)
				.getId());
		objTank = this.iTankRepository.save(objTank);
		return objTank;
	}

	@Override
	public boolean delete(TankDto entityDto) {
		iTankRepository.deleteById(entityDto.getId());
		return true;
	}

	@Override
	public boolean deleteById(Long entityId) {
		iTankRepository.deleteById(entityId);
		return true;
	}

	@Override
	public List<Tank> getAllByStatus(EntityStatus status) {
		List<Tank> listEnitiy = this.iTankRepository.findByStatus(status);
		return CollectionUtils.isEmpty(listEnitiy) == true ? null : listEnitiy;
	}

	@Override
	public Tank getById(Long id) {

		Optional<Tank> objOpt = this.iTankRepository.findById(id);
		objOpt.orElseThrow(() -> new EntityNotFoundException(String.format("Entity not found for Id [%s]",
				id)));
		Tank tank = objOpt.get();
		tank.setFacilityName(iFacilityRepository.findById(tank.getFacilityId()).get().getName());
		return tank;
	}

	@Override
	public Tank update(TankDto entityDto) {

		Tank tankEntity = this.mapper.map(entityDto, Tank.class);
		tankEntity.setLastCalibrationDate(Utility.createDateFromString(entityDto.getLastCalibration()));
		Integer parentCoreId = Integer.valueOf(iFacilityRepository.findById(entityDto.getFacilityId()).get()
				.getCoreId().intValue());

		Set<Well> wells = new HashSet<>();
		if (entityDto.getWellIds() != null && entityDto.getWellIds().length > 0) {
			for (Long wellId : entityDto.getWellIds()) {
				Optional<Well> obj = this.iWellRepository.findById(wellId);
				obj.orElseThrow(() -> new EntityNotFoundException(String.format("Well not found for Id [%s]",
						wellId)));
				wells.add(obj.get());
			}
			tankEntity.setWells(wells);
		}

		iAsyncLogicLadderService.updateEntityRequest(new GenricEntityCreationRequestDto(parentCoreId,
				EntityTypeLogicLadder.TANK.getDescription(), entityDto.getName()), tankEntity.getCoreId(),
				null);
		Tank dbEntity = iTankRepository.findById(entityDto.getId()).get();
		if (!entityDto.getStatus().equals(dbEntity.getStatus())) {
			tankEntity.changeStatus(entityDto.getStatus());
		}
		this.iTankRepository.save(tankEntity);
		//	this.iTankDao.updateTankProperty(tankEntity);
		return tankEntity;
	}

	@Override
	public List<Tank> findByIdIn(Iterable<Long> ids) {
		return iTankRepository.findAllById(ids);
	}

}
