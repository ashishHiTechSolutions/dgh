/*
 * 
 */
package com.logicladder.dgh.service.impl;
/*
 * package com.logicladder.dgh.service;
 *
 * import java.util.List;
 *
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.beans.factory.annotation.Qualifier; import
 * org.springframework.stereotype.Service; import
 * org.springframework.transaction.annotation.Transactional;
 *
 * import com.logicladder.dgh.dao.IGenericDao; import
 * com.logicladder.dgh.dao.IRolePageFunctionDao; import
 * com.logicladder.dgh.entity.app.PageFunction; import
 * com.logicladder.dgh.entity.app.RolePageFunction; import
 * com.logicladder.dgh.entity.app.Role;
 *
 * @Service public class RolePageFunctionServiceImpl extends
 * GenericServiceImpl<RolePageFunction, Long> implements
 * IRolePageFunctionService {
 *
 * private IRolePageFunctionDao iRolePageFunctionDao;
 *
 * public RolePageFunctionServiceImpl() {
 *
 * }
 *
 * @Autowired public RolePageFunctionServiceImpl(
 *
 * @Qualifier("rolePageFunctionDaoImpl") IGenericDao<RolePageFunction, Long>
 * genericDao) { super(genericDao); this.iRolePageFunctionDao =
 * (IRolePageFunctionDao) genericDao; }
 *
 * @Override public List<PageFunction> getAllPageFunctionForRole(String role) {
 * return iRolePageFunctionDao.getAllPageFunctionForRole(role); }
 *
 * @Override public List<PageFunction> getAllPageFunctionForRole(Role role) {
 * return iRolePageFunctionDao.getAllPageFunctionForRole(role); }
 *
 * @Override public boolean validateIfAccessPermitted(String userRole, String
 * function, String page) { return
 * iRolePageFunctionDao.validateIfAccessPermitted(userRole, function, page); }
 *
 * }
 */