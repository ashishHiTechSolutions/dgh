package com.logicladder.dgh.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.dao.ITariffDao;
import com.logicladder.dgh.dto.TariffDto;
import com.logicladder.dgh.entity.app.Tariff;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.security.repository.IDeviceRepository;
import com.logicladder.dgh.security.repository.ITariffRepository;
import com.logicladder.dgh.service.ITariffService;

@Service
@Transactional
public class TariffServiceImpl extends AbstractGenricSevice implements ITariffService {

	@Autowired
	private ITariffRepository iTariffRepository;
	
	@Autowired
	private ITariffDao iTariffDao;

	@Autowired
	private IDeviceRepository iDeviceRepository;
	
	@Override
	public Tariff create(@Valid TariffDto entityDto) throws CustomException {
		Tariff tariff = this.mapper.map(entityDto, Tariff.class);

		// validation
		tariff.setDevice(iDeviceRepository.findById(entityDto.getDeviceId()).get());
		tariff = iTariffRepository.save(tariff);
		return tariff;
	}

	@Override
	public List<Tariff> getAllByStatus(EntityStatus status) {
		List<Tariff> listEntity = this.iTariffRepository.findByStatus(status);

		return CollectionUtils.isEmpty(listEntity) == true ? null : listEntity;
	}

	@Override
	public boolean delete(TariffDto facilityDto) {

		return false;
	}

	@Override
	public boolean deleteById(Long entityId) {
		try {

			iTariffRepository.deleteById(entityId);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Tariff getById(Long id) {

		Optional<Tariff> objOpt = this.iTariffRepository.findById(id);
		Tariff tariff = objOpt
				.orElseThrow(() -> new EntityNotFoundException(String.format("Entity not found for Id [%s]", id)));
		tariff.setDeviceName(iDeviceRepository.findById(tariff.getDevice().getId()).get().getName());
		return tariff;
	}

	@Override
	public Tariff update(TariffDto entityDto) {
		Tariff tariff = this.mapper.map(entityDto, Tariff.class);
		tariff.setDevice(iDeviceRepository.findById(entityDto.getDeviceId()).get());
		iTariffRepository.save(tariff);
		return tariff;
	}
	
	@Override
	public List<Tariff> findByIdIn(Iterable<Long> ids) {
		return iTariffRepository.findAllById(ids);
	}

}
