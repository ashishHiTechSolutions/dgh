/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.Role;

public interface IRoleService extends IGenericService<Role, Long> {

	Role findByRoleName(String string);

	/*Set<PageFunction> getAllPageFunctionForRole(String roleName);*/

	boolean validateIfAccessPermitted(String userRole, String page, String function);
}
