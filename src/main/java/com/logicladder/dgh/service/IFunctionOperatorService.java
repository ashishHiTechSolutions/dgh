/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.FacilityOperator;

public interface IFunctionOperatorService extends IGenericService<FacilityOperator, Long> {

}
