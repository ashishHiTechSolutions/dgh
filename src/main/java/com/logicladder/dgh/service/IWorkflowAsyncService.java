/*
 * 
 */
package com.logicladder.dgh.service;

import java.util.concurrent.CompletableFuture;

public interface IWorkflowAsyncService {

	CompletableFuture<?> lookForMovie() throws InterruptedException;
}
