/*
 * 
 */
package com.logicladder.dgh.service;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.dto.TankDto;
import com.logicladder.dgh.entity.app.Tank;

@Validated
public interface ITankService extends IGenricService<TankDto, Tank> {

}
