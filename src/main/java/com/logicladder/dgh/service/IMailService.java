/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.app.email.Email;

public interface IMailService {

	void sendEmail(final Email email) throws InterruptedException;

}
