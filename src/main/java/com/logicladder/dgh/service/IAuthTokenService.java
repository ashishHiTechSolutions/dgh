/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.JwtToken;

public interface IAuthTokenService extends IGenericService<JwtToken, Long> {

}
