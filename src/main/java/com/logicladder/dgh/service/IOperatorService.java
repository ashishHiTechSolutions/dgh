package com.logicladder.dgh.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.dto.OperatorDto;
import com.logicladder.dgh.entity.app.Operator;
import com.logicladder.dgh.enums.ApprovalStatus;

public interface IOperatorService extends IGenericService<Operator, Long> {

	Operator changeRequestStatus( @NotNull Long requestedOperatorId, ApprovalStatus status);

	Operator createOperatorRequest(@Valid OperatorDto operatorDto, String username) throws CustomException;

	Operator findByEmail(String email);

	List<Operator> getRequestByStatus(String username, ApprovalStatus status);

	OperatorDto mapToDto(Operator saveOrUpdate);

	Operator mapToEntity(OperatorDto operatorDto, ApprovalStatus status);

	Operator updateOperator(OperatorDto operatorDto);

	boolean verifyOperatorOtp(@NotNull Long operatorId, @NotEmpty String emailOtp, @NotEmpty String mobileOtp);

	boolean deleteById(long id);
}
