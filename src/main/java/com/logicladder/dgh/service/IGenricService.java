/*
 * 
 */
package com.logicladder.dgh.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.dto.SuperDto;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.entity.app.SuperEntity;
import com.logicladder.dgh.entity.app.Well;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

import lombok.NonNull;

public interface IGenricService<T extends SuperDto, V extends SuperEntity> {

	public V create(@Valid T entityDto) throws CustomException;

	public boolean delete(T entityDto);

	public boolean deleteById(@NonNull Long entityId);

	public V getById(@NonNull Long id);

	public V update(@Valid T entityDto);

	public List<V> getAllByStatus(EntityStatus status);

	public List<V> findByIdIn(Iterable<Long> ids);
}
