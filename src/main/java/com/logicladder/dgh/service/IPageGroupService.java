package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.PageGroup;

public interface IPageGroupService extends IGenericService<PageGroup, Long> {

}
