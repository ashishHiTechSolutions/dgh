/*
 * 
 */
package com.logicladder.dgh.service;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.dto.WellDto;
import com.logicladder.dgh.entity.app.Well;

@Validated
public interface IWellService extends IGenricService<WellDto, Well> {

}
