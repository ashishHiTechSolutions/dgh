/*
 * 
 */
package com.logicladder.dgh.service;

import java.util.Collection;
import java.util.Set;

import com.logicladder.dgh.dto.FacilityDto;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.entity.app.FacilityOperator;

public interface IFacilityService extends IGenricService<FacilityDto, Facility> {

	FacilityOperator getFacilityOperatorById(long id);

	boolean deleteFacilityOperator(long id);
}
