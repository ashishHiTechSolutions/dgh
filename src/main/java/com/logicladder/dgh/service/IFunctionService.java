/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.Function;

public interface IFunctionService extends IGenericService<Function, Long> {

}
