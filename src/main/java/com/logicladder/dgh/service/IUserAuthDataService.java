/*
 * 
 */
package com.logicladder.dgh.service;

import javax.validation.constraints.NotEmpty;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.entity.app.UserAuthData;

@Validated
public interface IUserAuthDataService extends IGenericService<UserAuthData, Long> {

	UserAuthData findByDeviceId(String deviceId);

	UserAuthData findByJwtToken(String jwtToken);

	UserAuthData findByOtpVerifiedAndUserUserName(Boolean false1, String username);

	UserAuthData findByTempJwtToken(String tempJwtToken);

	UserAuthData findByUserUserName(String userName);

	String findLLAuthToken(@NotEmpty String userName);

	Boolean isValidAuthToken(String authToken);
}
