package com.logicladder.dgh.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.dto.DeviceDto;
import com.logicladder.dgh.entity.app.Device;
import com.logicladder.dgh.enums.DeviceType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Validated
public interface IDeviceService extends IGenricService<DeviceDto, Device> {

	public long save(@Valid DeviceDto entityDto);

	public long merge(@Valid DeviceDto entityDto);

	public List<Device> getAllByDeviceTypeAndFacilityId(DeviceType deviceType, Long facilityId);

	public boolean toggleStatus(EntityStatus status, long id);
}
