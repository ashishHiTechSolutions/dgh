/*
 * 
 */
package com.logicladder.dgh.service;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.dto.UserDto;
import com.logicladder.dgh.dto.UserPasswordChangeDto;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.thirdparty.request.dto.UserPatchRequest;

@Validated
public interface IUserService extends IGenericService<User, Long> {

	User changePassword(UserPasswordChangeDto userPasswordChangeDto);

	Response<?> changeRequestStatus(String username, Long requestedUserId, ApprovalStatus status, String jwtToken, String comments, String accessibleEntities) throws CustomException;

	User findByUserName(String username);

	Response<?> getAllAccessModuleForRole(String username);

	Response<?> getApplicationData(String username);

	Response<?> getRegistrationPreloadData();

	Response<?> getRequestByStatus(String username, ApprovalStatus status);

	Response<?> getUserData(String username);

	List<User> getUserListByStatus(ApprovalStatus[] status);

	Response<?> logOutUser(String username);

	UserDto mapToDto(User user);

	User mapToEntity(UserDto userDto);

	Response<?> patchUserWithNewData(UserPatchRequest[] userPatchRequests, long id, String jwtToken);

	Response<?> resendActivationLink(String username, Long userId);

	User updatePassword(String username, String password, String jwtToken);

	User updateUser(UserDto userDto, String username);

	String userRegistrationRequest(UserDto userDto, String username, String jwtToken);

	Boolean verifyOtp(String emailOtp, String mobileOtp, String username);

	boolean resendOtp(String username);

}
