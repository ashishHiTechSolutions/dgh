/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.entity.app.Page;

public interface IPageService extends IGenericService<Page, Long> {

}
