/*
 * 
 */
package com.logicladder.dgh.service;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.dto.RoleDto;
import com.logicladder.dgh.entity.app.AccessRolePageGroup;

public interface IAccessRolePageGroupService extends IGenericService<AccessRolePageGroup, Long> {

	AccessRolePageGroup findByRoleRoleName(String role);

	boolean createNewAccessRoleGroup(RoleDto roleDto) throws CustomException;

}
