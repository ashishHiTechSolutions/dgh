/*
 * 
 */
package com.logicladder.dgh.service;

import java.util.Date;
import java.util.List;

import com.logicladder.dgh.entity.app.EventLog;

public interface IEventLogService extends IGenericService<EventLog, Long> {

	List<EventLog> getAllEventLogsBetweenDate(Date from, Date to);
}
