package com.logicladder.dgh.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.service.IMakeService;
import com.logicladder.dgh.service.IModelService;
import com.logicladder.dgh.service.IUnitService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/model")
public class ModelController {

	private static Logger logger = LogManager.getLogger(ModelController.class);

	@Autowired
	IMakeService iMakeService;

	@Autowired
	IModelService iModelService;

	@Autowired
	IUnitService iUnitService;

	@PostMapping(path = "make/create")
	public ResponseEntity<?> invokeMakeCreation(HttpServletRequest request) {
		try {
			logger.info("Application Control received at BasinController.getBasinById");

			iMakeService.save();
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Fetch SuccessFull", "", null));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/create")
	public ResponseEntity<?> invokeMakeModelCreation(HttpServletRequest request) {
		try {
			logger.info("Application Control received at BasinController.getBasinById");

			iModelService.save();
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Fetch SuccessFull", "", null));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/unit/create")
	public ResponseEntity<?> invokeUnitCreation(HttpServletRequest request) {
		try {
			logger.info("Application Control received at BasinController.getBasinById");

			iUnitService.save();
			// return ResponseEntity.ok("Successfull");
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Fetch SuccessFull", "", null));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getAllByMakeId/{makeId}")
	public ResponseEntity<?> getAllByMakeId(@PathVariable("makeId") Long makeId, HttpServletRequest request) {
		try {
			// return ResponseEntity.ok(new Response<>(iModelService.getAllByMakeId(makeId),
			// "Fetch SuccessFull"));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Fetch SuccessFull", "models", iModelService
							.getAllByMakeId(makeId)));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getAll")
	public ResponseEntity<?> getAllModel(HttpServletRequest request) {
		try {
			// return ResponseEntity.ok(new Response<>(iModelService.getAllModel(), "Fetch
			// SuccessFull"));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Fetch SuccessFull", "models", iModelService
							.getAllModel()));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getAllMake")
	public ResponseEntity<?> getAllMake(HttpServletRequest request) {
		try {
			// return ResponseEntity.ok(new Response<>(iMakeService.getAllMake(), "Fetch
			// SuccessFull"));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Fetch SuccessFull", "makes", iMakeService
							.getAllMake()));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getAllUnit")
	public ResponseEntity<?> getAllUnit(HttpServletRequest request) {
		try {
			// return ResponseEntity.ok(new Response<>(iUnitService.getAllUnit(), "Fetch
			// SuccessFull"));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, String.format("Fetch SuccessFull"), "units",
					iUnitService.getAllUnit()));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

}
