package com.logicladder.dgh.controller.reports;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;


public class ReportsFilter {
	
	@Getter
	@Setter
	
	private Map<String, Object> filters;


}
