package com.logicladder.dgh.controller.reports.config;

import java.util.List;

import com.logicladder.dgh.service.IGenericService;

public interface IEndpointApiConfigService extends IGenericService<EndpointApiConfigDto, EndpointApiConfig> {
	
	EndpointApiConfigDto getByName(String name);

	List<EndpointApiConfigDto> getAll();

	EndpointApiConfig create(EndpointApiConfigDto endpointApiConfigDto);	

}
