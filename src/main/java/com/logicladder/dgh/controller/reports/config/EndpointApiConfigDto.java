package com.logicladder.dgh.controller.reports.config;

import javax.validation.constraints.NotNull;

import com.logicladder.dgh.dto.SuperDto;

import lombok.ToString;

@ToString
public class EndpointApiConfigDto extends SuperDto{
	
	@NotNull
	private String name;

	private String address;
	
	private String user;

	private String credential;
	
	private String credentialType;

	private String description;

	private String tokenAPI;

	private String tokenAPIProperty;
	
	private String tokenAPIHeader;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCredential() {
		return credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}

	public String getCredentialType() {
		return credentialType;
	}

	public void setCredentialType(String credentialType) {
		this.credentialType = credentialType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTokenAPI() {
		return tokenAPI;
	}

	public void setTokenAPI(String tokenAPI) {
		this.tokenAPI = tokenAPI;
	}

	public String getTokenAPIProperty() {
		return tokenAPIProperty;
	}

	public void setTokenAPIProperty(String tokenAPIProperty) {
		this.tokenAPIProperty = tokenAPIProperty;
	}

	public String getTokenAPIHeader() {
		return tokenAPIHeader;
	}

	public void setTokenAPIHeader(String tokenAPIHeader) {
		this.tokenAPIHeader = tokenAPIHeader;
	}

	
}
