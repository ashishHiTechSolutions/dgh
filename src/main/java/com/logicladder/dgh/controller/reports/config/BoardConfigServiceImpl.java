package com.logicladder.dgh.controller.reports.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.service.impl.AbstractGenericServiceImpl;

import io.jsonwebtoken.lang.Arrays;


@Service
@Transactional
public class BoardConfigServiceImpl extends AbstractGenericServiceImpl<BoardConfigDto,BoardConfig>  implements IBoardConfigService {

	@Autowired
	IBoardConfigDao iBoardConfigDao;
		
	@Autowired
	private SecurityContextUserDetails securityContextUserDetails;
	
	@Autowired
	Mapper mapper;

	public BoardConfig create(@Valid BoardConfigDto boardConfigDto) {
		BoardConfig previousBoard = iBoardConfigDao.findByTitle(boardConfigDto.getTitle());
		
		BoardConfig board = this.mapper.map(boardConfigDto, BoardConfig.class);
		if(previousBoard != null) {
			previousBoard.setAccess(board.getAccess());
			previousBoard.setBoard(board.getBoard());
			iBoardConfigDao.update(previousBoard);
			return previousBoard;
		}
		else {
			board = iBoardConfigDao.create(board);
			return board;
		}
	}


	
	public boolean delete(BoardConfigDto entityDto) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public BoardConfigDto getByTitle(String title) {
		BoardConfig board = this.iBoardConfigDao.findByTitle(title);
		if(board !=null) {
			return this.mapper.map(board, BoardConfigDto.class);
		}
		else {
			return null;
		}
	}

	@Override
	public List<BoardConfigDto> getAll() {
		List<BoardConfig> allBoards = iBoardConfigDao.getAll();
		ArrayList<BoardConfigDto> boards = new ArrayList<BoardConfigDto>();
		
		for(int i=0;i<allBoards.size();i++) {
			boards.add(this.mapper.map(allBoards.get(i), BoardConfigDto.class));
		}
			
				
		return boards;
	}
}
