package com.logicladder.dgh.controller.reports.config;

import java.util.List;

import com.logicladder.dgh.service.IGenericService;


public interface IBoardConfigService extends IGenericService<BoardConfigDto, BoardConfig> {

	BoardConfigDto getByTitle(String title);

	List<BoardConfigDto> getAll();

	BoardConfig create(BoardConfigDto boardConfigDto);

}
