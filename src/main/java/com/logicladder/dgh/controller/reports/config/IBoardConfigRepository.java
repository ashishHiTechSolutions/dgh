package com.logicladder.dgh.controller.reports.config;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBoardConfigRepository extends JpaRepository<BoardConfig, Long> {

}
