package com.logicladder.dgh.controller.reports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.apache.http.protocol.RequestDate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.RequestEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.controller.reports.config.BoardConfigController;
import com.logicladder.dgh.entity.app.ContractArea;
import com.logicladder.dgh.entity.app.SuperEntity;
import com.logicladder.dgh.entity.app.Well;
import com.logicladder.dgh.enums.WellStatus;
import com.logicladder.dgh.thirdparty.response.dto.ResponseData;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/reports/gobal")
public class GlobalReportsController {
	private static Logger logger = LogManager.getLogger(GlobalReportsController.class);

	@PersistenceContext
	public EntityManager em;
	
	
	@RequestMapping(value = "/count/contract-areas", method = RequestMethod.POST)
	public ResponseEntity<?> countTotalContractArea(@RequestBody String requestBody) {
		HashMap<String, Object> reportData = new HashMap<String, Object>();
		ResponseData response = new ResponseData();
		Long contractareaCount;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData =====" + filterData);

			logger.info("request body=====" + requestBody);

			Query query = em.createQuery("select count(*) from ContractArea");
			contractareaCount = (Long) query.getSingleResult();
			reportData.put("name", "");
			reportData.put("total", contractareaCount);
			reportData.put("image", "");
			reportData.put("color", "Black");

			response.setStatus("success");
			response.setResponseData(reportData);
		} catch (Exception e) {
			e.printStackTrace();

			logger.info("Error in retriving data for cause :  " + e);
		}
		return ResponseEntity.ok().body(response);
	}

	@RequestMapping(value = "/count/fields", method = RequestMethod.POST)
	public ResponseEntity<?> countFields(@RequestBody String requestBody) {
		HashMap<String, Object> reportData = new HashMap<String, Object>();
		ResponseData response = new ResponseData();
		// boolean flag = false;
		Long totalno_offields;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData =====" + filterData);

			logger.info("request body=====" + requestBody);

			Query query = em.createQuery("select count(*) from Facility");
			totalno_offields = (Long) query.getSingleResult();
			reportData.put("name", "");
			reportData.put("total", totalno_offields);
			reportData.put("image", "");
			reportData.put("color", "Black");
			response.setStatus("success");
			response.setResponseData(reportData);
		} catch (Exception e) {
			e.printStackTrace();

			logger.info("Error in retriving data for cause :  " + e);
		}
		return ResponseEntity.ok().body(response);

	}

	@RequestMapping(value = "/count/wells", method = RequestMethod.POST)
	public ResponseEntity<?> countWells(@RequestBody String requestBody) {
		HashMap<String, Object> reportData = new HashMap<String, Object>();
		ResponseData response = new ResponseData();
		
		// boolean flag = false;
		Long totalno_ofwells;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData =====" + filterData);
			logger.info("request body=====" + requestBody);
			
			String filterStatus = (String) filterData.get("status");
			logger.info("filterData =====" + filterStatus);
			if( filterStatus == null ){
				
				// Request is asking for all wells;
				Query query = em.createQuery("select count(*) from Well");
				totalno_ofwells = (Long) query.getSingleResult();
				reportData.put("name", "");
				reportData.put("total", totalno_ofwells);
				reportData.put("image", "");
				reportData.put("color", "Black");	
				logger.info("status null =====" + filterStatus);
			}
			else if ( filterStatus.equalsIgnoreCase("Producing Wells") ){
				
				Query query = em.createQuery("select count(*) from Well where wellStatus IN ('Artificial Lift','Self Lift','Under WO')");
				totalno_ofwells = (Long) query.getSingleResult();
				reportData.put("name", "");
				reportData.put("total", totalno_ofwells);
				reportData.put("image", "");
				reportData.put("color", "Black");
				logger.info("Producing Wells =====" + filterStatus);
			}
			else if( filterStatus.equalsIgnoreCase("Non Producing Wells") ){
				
				Query query = em.createQuery("select count(*) from Well where wellStatus IN ('Abandoned After Completion','Shut in Isolation','Stopped Producing','Suspended','Under Testing')");
				totalno_ofwells = (Long) query.getSingleResult();
				reportData.put("name", "");
				reportData.put("total", totalno_ofwells);
				reportData.put("image", "");
				reportData.put("color", "Black");
				logger.info("Non Producing Wells =====" + filterStatus);
			}			
			else{
				response.setErrorCode(500);
				response.setMessage("Null pointer Exception");
			}
			
			response.setStatus("success");
			response.setResponseData(reportData);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error in retriving data for cause :  " + e);
		}
		return ResponseEntity.ok().body(response);
	}

	@RequestMapping(value = "/count/operators", method = RequestMethod.POST)
	public ResponseEntity<?> countOperators(@RequestBody String requestBody) {
		
		HashMap<String, Object> reportData = new HashMap<String, Object>();
		ResponseData response = new ResponseData();
		Long totalnoofoprator;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData =====" + filterData);

			logger.info("request body=====" + requestBody);

			Query query = em.createQuery("select count(*) from Operator");
			totalnoofoprator = (Long) query.getSingleResult();
			reportData.put("name", "");
			reportData.put("total", totalnoofoprator);
			reportData.put("image", "");
			reportData.put("color", "Black");

			response.setStatus("success");
			response.setResponseData(reportData);
		} catch (Exception e) {
			e.printStackTrace();

			logger.info("Error in retriving data for cause :  " + e);
		}
		return ResponseEntity.ok().body(response);
		
	}
	
	@RequestMapping(value = "/productiondata/contract-areas", method = RequestMethod.POST)
	public ResponseEntity<?> countProductiondataContractArea(@RequestBody String requestBody){
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData =====" + filterData);

			logger.info("request body=====" + requestBody);

			Query query = em.createQuery("select ca from ContractArea ca");
			ArrayList contracts =(ArrayList) query.getResultList();
			ProductionData[] forSorting = new ProductionData[contracts.size()];
			
			for (int i = 0; i < contracts.size(); i++) {
				ContractArea contract = (ContractArea) contracts.get(i);
				Long coreid = contract.getCoreId();
				
				double oilProduced = getProductionContractArea(coreid); 
			    forSorting[i] = new ProductionData(contract.getId(),contract.getCode(),oilProduced);
			}
			
			Arrays.sort(forSorting, new ProductionDataComparator());

			
			return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
						"ContractArea Oil Production", "data",forSorting));
		} catch (Exception e) {
			e.printStackTrace();

			logger.info("Error in retriving data for cause :  " + e);
		}
		return ResponseEntity.ok().body(Utility.createFailure(20001, "Issue in fetching production data"));
	}	
	
	private double getProductionContractArea(Long coreid){
		
		return coreid;
		
	}

}
