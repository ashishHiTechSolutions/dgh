package com.logicladder.dgh.controller.reports.jasper;

import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import com.mysql.cj.api.jdbc.Statement;

import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Transparency;

public class AbstractReportGenerator {

	private static final String DGH_REPORT_WATERMARK = "DGH RTMS";

	protected DynamicReportBuilder getBaseReportBuilder() {

		// DynamicReportBuilder drb = new DynamicReportBuilder();
		// drb.setMargins(30, 20, 30, 15); //define the margin space for each
		// side (top, bottom, left and right)
		// drb.setDetailHeight(15); //defines the height for each record of the
		// report

		Style detailStyle = new Style();
		Style headerStyle = new Style();

		headerStyle.setBackgroundColor(new Color(230, 230, 230));
		headerStyle.setBorderBottom(Border.THIN());
		headerStyle.getBorderBottom().setColor(Color.black);
		headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
		headerStyle.setTransparency(Transparency.OPAQUE);

		Style titleStyle = new Style();
		titleStyle.setHorizontalAlign(HorizontalAlign.CENTER);
		titleStyle.setFont(Font.ARIAL_BIG_BOLD);

		Style subtitleStyle = new Style();

		DynamicReportBuilder drb = new DynamicReportBuilder();
		drb.setMargins(30, 20, 30, 15); // define the margin space for each side
										// (top, bottom, left and right)
		drb.setDetailHeight(15); // defines the height for each record of the
									// report
		drb.setDefaultStyles(titleStyle, subtitleStyle, headerStyle, detailStyle);

		drb.setTemplateFile("templates/DGHReportTemplate.jrxml");
		drb.addWatermark(DGH_REPORT_WATERMARK);
		drb.setUseFullPageWidth(true);

		return drb;
	}

	public Connection getConnection() {
		Connection con = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dghNew", "root", "Xtreme@123");
			// here sonoo is database name, root is username and password

		} catch (Exception e) {
			System.out.println(e);
		}

		return con;
	}
}
