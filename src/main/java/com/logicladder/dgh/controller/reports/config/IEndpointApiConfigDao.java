package com.logicladder.dgh.controller.reports.config;

import java.util.List;

import com.logicladder.dgh.dao.IGenricDao;

public interface IEndpointApiConfigDao extends IGenricDao<EndpointApiConfig> {
	void delete(EndpointApiConfig endpointApiConfig);
	
	EndpointApiConfig findByName(String name);

	List<EndpointApiConfig> getAll();
}
