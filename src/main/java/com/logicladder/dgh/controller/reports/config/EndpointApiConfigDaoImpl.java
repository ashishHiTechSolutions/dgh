package com.logicladder.dgh.controller.reports.config;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.Valid;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import com.logicladder.dgh.dao.impl.AbstractGenricDaoImpl;

@Repository
public class EndpointApiConfigDaoImpl extends AbstractGenricDaoImpl<EndpointApiConfig> implements IEndpointApiConfigDao {


	private EntityManager entityManager;

	public EndpointApiConfigDaoImpl(EntityManager entityManager) {
		super(entityManager);
		// TODO Auto-generated constructor stub
		this.entityManager = entityManager;
	}

	@Override
	public EndpointApiConfig findByName(String name) {
		Query query = entityManager.createQuery("select e from EndpointApiConfig e where e.name = :NAME");
		query.setParameter("NAME", name);
		
		List result = query.getResultList();
		if (result.size() > 0) {
			EndpointApiConfig endpoint = (EndpointApiConfig) result.get(0);
			return endpoint;
		}
		return null;
	}

	@Override
	public void delete(EndpointApiConfig endpointApiConfig) {
		this.entityManager.unwrap(Session.class).remove(endpointApiConfig);
	}

	@Override
	public List<EndpointApiConfig> getAll() {
		Query query = entityManager.createQuery("select ep from EndpointApiConfig ep");
		List result = query.getResultList();
		return result;
	}
	
	@Override
	public EndpointApiConfig create(@Valid EndpointApiConfig endpointApi) {
	   Object saveResult =	entityManager.unwrap(Session.class).save(endpointApi);
	   System.out.println("EndpointApiConfigDaoImpl.create() " + saveResult);
	   return endpointApi;
	}	
}
