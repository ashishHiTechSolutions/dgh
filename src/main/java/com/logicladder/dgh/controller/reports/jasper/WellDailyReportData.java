package com.logicladder.dgh.controller.reports.jasper;

import lombok.Getter;
import lombok.Setter;

public class WellDailyReportData {
	@Getter
	@Setter
	private int serialNo;
	@Getter
	@Setter
	private int wellId;
	@Getter
	@Setter
	private String contractAreaName;
	@Getter
	@Setter
	private String fieldName;
	@Getter
	@Setter
	private String wellName;
	@Getter
	@Setter
	private String opretorName;
}
