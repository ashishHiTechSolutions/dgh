package com.logicladder.dgh.controller.reports.config;

import java.util.List;

import com.logicladder.dgh.dao.IGenricDao;

public interface IBoardConfigDao extends IGenricDao<BoardConfig> {
	void delete(BoardConfig boardConfig);
	
	BoardConfig findByTitle(String title);

	List<BoardConfig> getAll();
}
