package com.logicladder.dgh.controller.reports.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/reports/config")
public class BoardConfigController {

	private static Logger logger = LogManager.getLogger(BoardConfigController.class);
	
	@Autowired
	IBoardConfigService boardConfigService;

	@Autowired
	IEndpointApiConfigService endpointApiConfigService;
	
	@PostMapping(path = "/board")
	public ResponseEntity<?> createBoardConfig(HttpServletRequest request,
			@RequestBody BoardConfigDto boardConfigDto)
	{
		logger.info("Application Control received at BoardConfigController.saveBoardConfig");
//		if( boardConfigDto.getId() !=null) {
//			throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]",boardConfigDto.getId()));
//		}
		
		BoardConfig board = this.boardConfigService.create(boardConfigDto);
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"Board Saved Successfully", "board",board));		
	}
	
	
	@GetMapping(path = "/board/{title}")
	public ResponseEntity<?> getBoardByTitle(HttpServletRequest request, @PathVariable String title ){
		logger.info("Application Control received at BoardConfigController.getBoardByTitle");

		BoardConfigDto board = this.boardConfigService.getByTitle(title);
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"Board Saved Successfully", "board",board));
	}
	
	@GetMapping(path = "/board")
	public ResponseEntity<?> getBoards(HttpServletRequest request){
		logger.info("Application Control received at BoardConfigController.allboards");

		List<BoardConfigDto> allBoards = boardConfigService.getAll();
	 	
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"All Boards ", "boards",allBoards));
		
	}
	
	
	@PostMapping(path = "/endpoint")
	public ResponseEntity<?> createEndpointApiConfig(HttpServletRequest request,
			@RequestBody EndpointApiConfigDto endpointApiConfigDto)
	{
		logger.info("Application Control received at BoardConfigController.saveEndpointConfig");
//		if( endpointApiConfigDto.getId() !=null) {
//			throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]",endpointApiConfigDto.getId()));
//		}
		
		EndpointApiConfig endpoint = this.endpointApiConfigService.create(endpointApiConfigDto);
		
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"Endpoints Saved Successfully", "endpoints",endpoint));		
	}
	
	/*public ResponseEntity<?> createEndpointApiConfig(HttpServletRequest request,
			@RequestBody EndpointApiConfigDto[] endpointApiConfigDto)
	{
		logger.info("Application Control received at BoardConfigController.saveEndpointConfig");
//		if( endpointApiConfigDto.getId() !=null) {
//			throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]",endpointApiConfigDto.getId()));
//		}
		ArrayList<EndpointApiConfig> saveEndpoints = new ArrayList<EndpointApiConfig>();
		for (int i = 0; i < endpointApiConfigDto.length; i++) {
			EndpointApiConfig endpoint = this.endpointApiConfigService.create(endpointApiConfigDto[i]);
			saveEndpoints.add(endpoint);
		}
		
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"Endpoints Saved Successfully", "endpoints",saveEndpoints));		
	}*/

	
	@GetMapping(path = "/endpoint/{name}")
	public ResponseEntity<?> getEndpointByName(HttpServletRequest request, @PathVariable String name ){
		logger.info("Application Control received at BoardConfigController.getEndpointByName");

		EndpointApiConfigDto endpoint = this.endpointApiConfigService.getByName(name);
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"Endpoint retrieved successfully", "endpoint",endpoint));
	}
	
	@GetMapping(path = "/endpoint")
	public ResponseEntity<?> getEndpoints(HttpServletRequest request){
		logger.info("Application Control received at BoardConfigController.allendpoints");

		List<EndpointApiConfigDto> endpoints = endpointApiConfigService.getAll();
	 	
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"All Endpoints ", "endpoints",endpoints));
		
	}
	
}
