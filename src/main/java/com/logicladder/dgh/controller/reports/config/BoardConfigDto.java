package com.logicladder.dgh.controller.reports.config;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.logicladder.dgh.dto.SuperDto;

import lombok.ToString;

@ToString
public class BoardConfigDto extends SuperDto{
	
	@NotEmpty
	@Size(max = 80)
	private String title;

	private String access;
	
	private String board;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}
}
