package com.logicladder.dgh.controller.reports.jasper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.CustomExpression;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class WellReports extends AbstractReportGenerator {

	@PersistenceContext
	public EntityManager em;

	public Object generateDailyWellReport(String wellId, DataSource dataSource, Map<String, Object> filter,
			String format) {
		DynamicReportBuilder drb = getBaseReportBuilder();
		DynamicReport dr = drb.build();
		Object result = null;
		drb.setTitle("Contract Area Wise Daily Well Parameter Report");

		AbstractColumn numbers = ColumnBuilder.getInstance().setCustomExpression(getExpression()).build();
		drb.addColumn(numbers).setTitle("S.No");
		// AbstractColumn columnSn = ColumnBuilder.getNew()
		// .setColumnProperty("serialNo", String.class.getName())
		// .setTitle("S.No").setWidth(15)
		// .build();
		AbstractColumn columnContractArea = ColumnBuilder.getNew()
				.setColumnProperty("contract_name", String.class.getName()).setTitle("Contract Area").setWidth(10)
				.build();

		AbstractColumn operator = ColumnBuilder.getNew().setColumnProperty("operator_name", String.class.getName())
				.setTitle("Operator").setWidth(10).build();

		AbstractColumn field = ColumnBuilder.getNew().setColumnProperty("field_name", String.class.getName())
				.setTitle("Field").setWidth(10).build();

		AbstractColumn well = ColumnBuilder.getNew().setColumnProperty("well_name", String.class.getName())
				.setTitle("Well").setWidth(10).build();

		drb.addColumn(numbers);
		drb.addColumn(columnContractArea);
		drb.addColumn(operator);
		drb.addColumn(field);
		drb.addColumn(well);

		ResultSet rs = null;
		Connection con = null;
		try {
			con = getConnection();// dataSource.getConnection();
			Statement stmt = con.createStatement();
			if (!wellId.equals("allWell")) {
				rs = stmt.executeQuery("select * from well_daily_report where well_id=" + wellId);

			} else {
				rs = stmt.executeQuery("select * from well_daily_report");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			JasperReport jr = DynamicJasperHelper.generateJasperReport(dr, new ClassicLayoutManager(), null, "dgh");
			JasperPrint jp = JasperFillManager.fillReport(jr, null, new JRResultSetDataSource(rs));
			//Unique file name 
			
			if (format.equals("pdf")) {
				ReportExporter.exportReport(jp, System.getProperty("user.dir") + "/target/reports/sample.pdf");

			}
			if (format.equals("xls")) {
				
			}
			if (format.equals("html")) {
				ReportExporter.exportReportHtml(jp, System.getProperty("user.dir") + "/target/reports/sample.html");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public void generateWeeklyWellReport() {

	}

	public void generateMonthlyWellReport() {

	}

	private CustomExpression getExpression() {
		return new CustomExpression() {

			/**
			* 
			*/
			private static final long serialVersionUID = 1L;

			public Object evaluate(Map fields, Map variables, Map parameters) {
				return variables.get("REPORT_COUNT");
			}

			public String getClassName() {
				return Integer.class.getName();
			}
		};
	}
}
