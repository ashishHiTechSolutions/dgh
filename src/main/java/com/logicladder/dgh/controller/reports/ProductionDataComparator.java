package com.logicladder.dgh.controller.reports;

import java.util.Comparator;

public class ProductionDataComparator implements Comparator<ProductionData> {
	
	
	@Override
	public int compare(ProductionData productionData1, ProductionData productionData2) {
		// TODO Auto-generated method stub
		double prodVal1 = productionData1.getProductionValue();
		double prodVal2 = productionData2.getProductionValue();
		
		if(prodVal1 < prodVal2){
			return -1;	
		}
		else if(prodVal1 == prodVal2){
			return 0;	
		}
		else {
			return 1;	
		}
	}
}
