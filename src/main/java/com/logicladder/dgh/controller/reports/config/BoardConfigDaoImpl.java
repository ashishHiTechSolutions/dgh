package com.logicladder.dgh.controller.reports.config;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.Valid;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.logicladder.dgh.dao.impl.AbstractGenricDaoImpl;

@Repository
public class BoardConfigDaoImpl extends AbstractGenricDaoImpl<BoardConfig> implements IBoardConfigDao {

	private EntityManager entityManager;
	
	@Autowired
	public BoardConfigDaoImpl(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	@Override
	public void delete(BoardConfig boardConfig) {
		this.entityManager.unwrap(Session.class).delete(boardConfig);
	}

	@Override
	public BoardConfig findByTitle(String title) {
		Query query = entityManager.createQuery("select b from BoardConfig b where b.title = :TITLE");
		query.setParameter("TITLE", title);
		
		List result = query.getResultList();
		if (result.size() > 0) {
			BoardConfig board = (BoardConfig) result.get(0);
			return board;
		}
		return null;
	}
	
	@Override
	public BoardConfig create(@Valid BoardConfig board) {
	   Object saveResult =	entityManager.unwrap(Session.class).save(board);
	   System.out.println("BoardConfigDaoImpl.create() " + saveResult);
	   return board;
	}

	@Override
	public List<BoardConfig> getAll() {
		Query query = entityManager.createQuery("select b from BoardConfig b");
		List result = query.getResultList();
		return result;
	}
}
