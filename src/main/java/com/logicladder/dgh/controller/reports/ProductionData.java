package com.logicladder.dgh.controller.reports;

import lombok.ToString;

@ToString
public class ProductionData {
	private Long entityid;
	private double productionValue;
	private String label;
	
	public Long getEntityid() {
		return entityid;
	}

	public void setEntityid(Long entityid) {
		this.entityid = entityid;
	}

	public double getProductionValue() {
		return productionValue;
	}

	public void setProductionValue(double productionValue) {
		this.productionValue = productionValue;
	}
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	ProductionData(Long entityid,String label, double productionValue){
		this.entityid = entityid;
		this.label = label;
		this.productionValue =  productionValue;
	}

	
	

	
		
}
