package com.logicladder.dgh.controller.reports;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.ResponseData;
import com.logicladder.dgh.logic.ladder.service.impl.AbstractLogicLadderServiceImpl;
//import com.logicladder.dgh.logic.ladder.service.impl.AbstractLogicLadderServiceImpl;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/reports/contract-area")
public class ContractAreaReportsController extends AbstractLogicLadderServiceImpl{
	
	private static final Logger logger = Logger.getLogger(ContractAreaReportsController.class);
	
	@PersistenceContext
	public EntityManager em;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${jwt.header}")
	private String tokenHeader;

	
	
	
	@RequestMapping(value = "/count/fields", method = RequestMethod.POST)	
	public  ResponseEntity<?> countFields(@RequestBody String requestBody) {
		logger.info("request body====="+requestBody);
		HashMap<String, Object> reportData = new HashMap<String, Object>(); 
		Long facilityCount;
		ResponseData response= new ResponseData();
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData ====="+filterData);
			if(!filterData.isEmpty()) {
			Long contractareaid = Long.parseLong(filterData.get("contractareaid").toString()) ;
			logger.info("contractareaid ====="+contractareaid);
			Query query = em.createQuery("select count(*) from Facility where contractAreaId = "+contractareaid);
			facilityCount = (Long)query.getSingleResult();
			reportData.put("name", "");
			reportData.put("total", facilityCount);
			reportData.put("image", "");
			reportData.put("color", "Black");	
			}			
			else {
				response.setErrorCode(500);
				response.setMessage("Error in retriving data for cause Null");
			}
			
			response.setStatus("success");
			response.setResponseData(reportData);
				
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error("Error in retriving data for cause :  " + e);			
		}
		
		return ResponseEntity.ok().body(response);
		
	}
	
	@RequestMapping(value = "/count/wells", method = RequestMethod.POST)	
	public  ResponseEntity<?> countWells(@RequestBody String requestBody) {
		logger.info("request body====="+requestBody);
		HashMap<String, Object> reportData = new HashMap<String, Object>(); 
		Long wellCount;
		ResponseData response= new ResponseData();
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData ====="+filterData);
			if(!filterData.isEmpty()) {
			Long contractareaid = Long.parseLong(filterData.get("contractareaid").toString()) ;
			logger.info("contractareaid ====="+contractareaid);
			Query query = em.createQuery("select count(*) from Facility f inner join Well w on f.id = w.facilityId where f.contractAreaId = "+contractareaid);
			wellCount = (Long)query.getSingleResult();
			reportData.put("name", "");
			reportData.put("total", wellCount);
			reportData.put("image", "");
			reportData.put("color", "Black");	
			}
			
			else {
				response.setErrorCode(500);
				response.setMessage("Error in retriving data for cause Null");
			}
			
			response.setStatus("success");
			response.setResponseData(reportData);
				
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error("Error in retriving data for cause :  " + e);			
		}
		
		return ResponseEntity.ok().body(response);
		
	}
	
	@RequestMapping(value = "/count/tanks", method = RequestMethod.POST)	
	public  ResponseEntity<?> countTanks(@RequestBody String requestBody) {
		logger.info("request body====="+requestBody);
		HashMap<String, Object> reportData = new HashMap<String, Object>(); 
		Long tankCount;
		ResponseData response= new ResponseData();
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData ====="+filterData);
			if(!filterData.isEmpty()) {
			Long contractareaid = Long.parseLong(filterData.get("contractareaid").toString()) ;
			logger.info("contractareaid ====="+contractareaid);
			Query query = em.createQuery("select count(*) from Facility f inner join Tank t on f.id = t.facilityId where f.contractAreaId = "+contractareaid);
			tankCount = (Long)query.getSingleResult();
			reportData.put("name", "");
			reportData.put("total", tankCount);
			reportData.put("image", "");
			reportData.put("color", "Black");	
			}
			
			else {
				response.setErrorCode(500);
				response.setMessage("Error in retriving data for cause Null");
			}
			
			response.setStatus("success");
			response.setResponseData(reportData);
				
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error("Error in retriving data for cause :  " + e);			
		}
		
		return ResponseEntity.ok().body(response);
		
	}
	@RequestMapping(value = "/count/alerts", method = RequestMethod.POST)	
	public  ResponseEntity<?> countAlerts(@RequestBody String requestBody, HttpServletRequest httpRequest) {
		System.out.println("httpRequest===============>"+httpRequest);
		String llAuthToken = httpRequest.getHeader("LL-AUTH-TOKEN");
		logger.info("request body====="+requestBody);
		
		HashMap<String, Object> reportData = new HashMap<String, Object>(); 
		ResponseData response= new ResponseData();
		ContractAreaReportsController contractAreaReports= new ContractAreaReportsController();
		try {
			
			
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData ====="+filterData);
			Long contractareaid = Long.parseLong(filterData.get("contractareaid").toString()) ;
			logger.info("contractareaid ====="+contractareaid);
			Query query = em.createQuery("select coreId from ContractArea where id =" +contractareaid);
			Long coreId = (Long)query.getSingleResult();
			System.out.println("coreId=========" +coreId);
			Map<String, String> start_EndDate = contractAreaReports.getStartAndEndDate();
			String startDate = start_EndDate.get("startDate");
			String endDate = start_EndDate.get("endDate");
			
			int alertCount = getAlerCount(coreId,startDate,endDate,llAuthToken);
			reportData.put("name", "");
			reportData.put("total", alertCount);
			reportData.put("image", "");
			reportData.put("color", "Black");
			
			response.setStatus("success");
			response.setResponseData(reportData);
			
				
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error("Error in retriving data for cause :  " + e);			
		}
		
		return ResponseEntity.ok().body(response);
		
	}
	
	
	
	
	
	public int getAlerCount(Long coreId, String startDate, String endDate, String llAuthToken) {

		StringBuffer url = new StringBuffer(ApplicationConstants.TP_BASE_URL).append(
				ApplicationConstants.LogicLadderAPI.URL_ALERTS).append("/count?considerDescendants=true&endDate="+endDate+"&startDate="+startDate+"&status=ACTIVE&nodeId=" + coreId);
		//HttpEntity<String> entity = new HttpEntity<>("parameters", this.getRequestHeaders());
		System.out.println("url=========" + url);
		ResponseEntity<String> tpUserResponse = null;
		try {
			
            HttpEntity entity = new HttpEntity(this.getRequestHeaders(llAuthToken));
			System.out.println("Header=========" + entity);
			
			tpUserResponse = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, String.class);
			System.out.println("getAlerCount :: " + tpUserResponse);
			logger.info("getAlerCount :: " + tpUserResponse);
			System.out.println("getAlerCount :: " + tpUserResponse);
		} catch (HttpClientErrorException httpError) {
			logger.error("Error from core system" + httpError);
			System.out.println("Error from core system" + httpError);
		} catch (Exception e) {
			logger.error("Error in middle layer" + e);
			e.printStackTrace(System.out);
		}
		return 0;
	}
	
	private Map<String, String> getStartAndEndDate() {

		Map<String, String> startAndEndDate = new HashMap<String, String>();

		try {
			Calendar aCalendar = Calendar.getInstance();
			aCalendar.add(Calendar.DATE, -1);
			Date startdate = aCalendar.getTime();
            Date enddate = new Date();
			

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = sdf.format(startdate);
			String endDate = sdf.format(enddate);
			System.out.println("report start date======" + startDate + "endDate======" + endDate);
			logger.info("report start date======" + startDate + "endDate======" + endDate);
			startAndEndDate.put("startDate", startDate);
			startAndEndDate.put("endDate", endDate);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return startAndEndDate;
	}
	
	/*@RequestMapping(value = "/production/oil", method = RequestMethod.POST)	
	public  ResponseEntity<?> productionOil(@RequestBody String requestBody, HttpServletRequest httpRequest) {
		System.out.println("httpRequest===============>"+httpRequest);
		
		logger.info("request body====="+requestBody);
		HashMap<String, Object> reportData = new HashMap<String, Object>(); 
		ResponseData response= new ResponseData();
		ContractAreaReportsController contractAreaReports= new ContractAreaReportsController();
		try {
			
			String llAuthToken = null;	
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData ====="+filterData);
			Long contractareaid = Long.parseLong(filterData.get("contractareaid").toString()) ;
			logger.info("contractareaid ====="+contractareaid);
			Query query = em.createQuery("select coreId from ContractArea where id =" +contractareaid);
			Long coreId = (Long)query.getSingleResult();
			System.out.println("coreId=========" +coreId);
			Map<String, String> start_EndDate = contractAreaReports.getStartAndEndDate();
			String startDate = start_EndDate.get("startDate");
			String endDate = start_EndDate.get("endDate");
			
			//int alertCount = getAlerCount(coreId,startDate,endDate,llAuthToken);
			reportData.put("name", "");
			//reportData.put("total", alertCount);
			reportData.put("image", "");
			reportData.put("color", "Black");
			
			response.setStatus("success");
			response.setResponseData(reportData);
			
				
		} catch (Exception e) {
			
			e.printStackTrace();
			logger.error("Error in retriving data for cause :  " + e);			
		}
		
		return ResponseEntity.ok().body(response);
		
	}*/
	
	
	


}
