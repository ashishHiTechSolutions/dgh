package com.logicladder.dgh.controller.reports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.entity.app.ContractArea;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.thirdparty.response.dto.ResponseData;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/reports/operator")
public class OperatorReportsController {
	private static Logger logger = LogManager.getLogger(GlobalReportsController.class);

	@PersistenceContext
	public EntityManager em;
	
	
	@RequestMapping(value = "/count/fields", method = RequestMethod.POST)
	public ResponseEntity<?> countFields(@RequestBody String requestBody) {
		HashMap<String, Object> reportData = new HashMap<String, Object>();
		ResponseData response = new ResponseData();
		// boolean flag = false;
		Long totalno_offields;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData =====" + filterData);
			logger.info("request body=====" + requestBody);
			Long facilityOperatorId = Long.parseLong(filterData.get("facilityOperatorId").toString()) ;
			System.out.println("facilityOperatorId ====="+facilityOperatorId);
			Query query = em.createQuery("select count(*) from Facility where facilityOperatorId="+facilityOperatorId);
			totalno_offields = (Long) query.getSingleResult();
			reportData.put("name", "");
			reportData.put("total", totalno_offields);
			reportData.put("image", "");
			reportData.put("color", "Black");
			response.setStatus("success");
			response.setResponseData(reportData);
		} catch (Exception e) {
			e.printStackTrace();

			logger.info("Error in retriving data for cause :  " + e);
		}
		return ResponseEntity.ok().body(response);

		
	}
	
	@RequestMapping(value = "/count/wells", method = RequestMethod.POST)
	public ResponseEntity<?> countWells(@RequestBody String requestBody) {
		HashMap<String, Object> reportData = new HashMap<String, Object>();
		ResponseData response = new ResponseData();
	
		Long totalno_ofwells;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData =====" + filterData);
			logger.info("request body=====" + requestBody);
			Long facilityOperatorId = Long.parseLong(filterData.get("facilityOperatorId").toString()) ;
			System.out.println("facilityOperatorId ====="+facilityOperatorId);
			
			String filterStatus = (String) filterData.get("status");
			logger.info("filterData =====" + filterStatus);
			if( filterStatus == null ){
				
				// Request is asking for all wells;
				Query query = em.createQuery("select count(*) from Facility f inner join Well w on f.id=w.facilityId where f.facilityOperatorId="+facilityOperatorId);
				totalno_ofwells = (Long) query.getSingleResult();
				reportData.put("name", "");
				reportData.put("total", totalno_ofwells);
				reportData.put("image", "");
				reportData.put("color", "Black");	
				logger.info("status null =====" + filterStatus);
			}
			else if ( filterStatus.equalsIgnoreCase("Producing Wells") ){
				
				Query query = em.createQuery("select count(*) from Facility f inner join Well w on f.id=w.facilityId where w.wellStatus IN ('Artificial Lift','Self Lift','Under WO') and f.facilityOperatorId="+facilityOperatorId);
				totalno_ofwells = (Long) query.getSingleResult();
				reportData.put("name", "");
				reportData.put("total", totalno_ofwells);
				reportData.put("image", "");
				reportData.put("color", "Black");
				logger.info("Producing Wells =====" + filterStatus);
			}
			else if( filterStatus.equalsIgnoreCase("Non Producing Wells") ){
				Query query = em.createQuery("select count(*) from Facility f inner join Well w on f.id=w.facilityId where  w.wellStatus IN ('Abandoned After Completion','Shut in Isolation','Stopped Producing','Suspended','Under Testing') and f.facilityOperatorId="+facilityOperatorId);
				totalno_ofwells = (Long) query.getSingleResult();
				reportData.put("name", "");
				reportData.put("total", totalno_ofwells);
				reportData.put("image", "");
				reportData.put("color", "Black");	
				logger.info("Non Producing Wells =====" + filterStatus);
			}			
			else{
				response.setErrorCode(500);
				response.setMessage("Null pointer Exception");
			}
			
			response.setStatus("success");
			response.setResponseData(reportData);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error in retriving data for cause :  " + e);
		}
		return ResponseEntity.ok().body(response);
	}
		

	@RequestMapping(value = "/productiondata/fields", method = RequestMethod.POST)
	public ResponseEntity<?> countProductiondataContractArea(@RequestBody String requestBody){
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			ReportsFilter filterObj = mapper.readValue(requestBody, ReportsFilter.class);
			Map<String, Object> filterData = filterObj.getFilters();
			logger.info("filterData =====" + filterData);

			logger.info("request body=====" + requestBody);
			Long facilityOperatorId = Long.parseLong(filterData.get("facilityOperatorId").toString()) ;
			System.out.println("facilityOperatorId ====="+facilityOperatorId);
			
			Query query = em.createQuery("select f from Facility f where f.facilityOperatorId="+facilityOperatorId);
			ArrayList fields =(ArrayList) query.getResultList();
			ProductionData[] forSorting = new ProductionData[fields.size()];
			
			for (int i = 0; i < fields.size(); i++) {
				Facility field = (Facility) fields.get(i);
				Long coreid = field.getCoreId();
				double oilProduced = getProductionFields(coreid); 
			    forSorting[i] = new ProductionData(field.getId(),field.getCode(),oilProduced);
			}
			
			Arrays.sort(forSorting, new ProductionDataComparator());

			return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
						"Fields Oil Production", "data",forSorting));
		} catch (Exception e) {
			e.printStackTrace();

			logger.info("Error in retriving data for cause :  " + e);
		}
		return ResponseEntity.ok().body(Utility.createFailure(20001, "Issue in fetching production data"));
	}	
	
	
	private double getProductionFields(Long coreid){
		return coreid;
		
	}
	
}
	
	
	
	

