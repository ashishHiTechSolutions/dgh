/*
 * 
 */
package com.logicladder.dgh.controller.reports.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import com.logicladder.dgh.entity.app.SuperEntity;

@Entity
@Table(name = "BoardConfig")
public class BoardConfig extends SuperEntity {
	
	@Column (length = 80 )
	private String title;

	@Column (length = 255)
	private String access;
	
	@Column (name = "board")
	@Lob
	private String board;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAccess() {
		return access;
	}

	public void setAccess(String access) {
		this.access = access;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}
	
}
