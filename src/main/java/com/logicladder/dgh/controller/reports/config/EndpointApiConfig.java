/*
 * 
 */
package com.logicladder.dgh.controller.reports.config;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import com.logicladder.dgh.entity.app.SuperEntity;

@Entity
@Table(name = "EndpointApiConfig")
public class EndpointApiConfig extends SuperEntity {
	
	@Column (length = 80 )
	private String name;

	@Column (length = 255)
	private String address;
	
	@Column (length = 100)
	private String user;

	@Column (length = 100)
	private String credential;
	
	@Column (length = 50)
	private String credentialType;

	@Column (length = 100)
	private String description;

	@Column (length = 80)
	private String tokenAPI;

	@Column (length = 50)
	private String tokenAPIProperty;
	
	@Column (length = 100)
	private String tokenAPIHeader;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCredential() {
		return credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}

	public String getCredentialType() {
		return credentialType;
	}

	public void setCredentialType(String credentialType) {
		this.credentialType = credentialType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTokenAPI() {
		return tokenAPI;
	}

	public void setTokenAPI(String tokenAPI) {
		this.tokenAPI = tokenAPI;
	}

	public String getTokenAPIProperty() {
		return tokenAPIProperty;
	}

	public void setTokenAPIProperty(String tokenAPIProperty) {
		this.tokenAPIProperty = tokenAPIProperty;
	}

	public String getTokenAPIHeader() {
		return tokenAPIHeader;
	}

	public void setTokenAPIHeader(String tokenAPIHeader) {
		this.tokenAPIHeader = tokenAPIHeader;
	}

}
