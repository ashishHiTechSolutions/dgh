package com.logicladder.dgh.controller.reports.config;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.logicladder.dgh.service.impl.AbstractGenericServiceImpl;

@Service
@Transactional
public class EndpointApiConfigServiceImpl extends AbstractGenericServiceImpl<EndpointApiConfigDto, EndpointApiConfig>
		implements IEndpointApiConfigService {
	
	@Autowired
	IEndpointApiConfigDao iEndpointApiConfigDao;
	
	@Autowired
	Mapper mapper;

	@Override
	public EndpointApiConfigDto getByName(String name) {
		EndpointApiConfig endpoint = iEndpointApiConfigDao.findByName(name);
		if (endpoint !=null) {
			return mapper.map(endpoint, EndpointApiConfigDto.class);
		}
		else {
			return null;
		}		
	}

	
	public EndpointApiConfig create(@Valid EndpointApiConfigDto endpointApiConfigDto) {
		EndpointApiConfig previousEndpoint = iEndpointApiConfigDao.findByName(endpointApiConfigDto.getName());
		
		EndpointApiConfig endpoint = this.mapper.map(endpointApiConfigDto, EndpointApiConfig.class);
		if(previousEndpoint != null) {
			previousEndpoint.setAddress(endpoint.getAddress());
			iEndpointApiConfigDao.update(previousEndpoint);
			return previousEndpoint;
		}
		else {
			endpoint = iEndpointApiConfigDao.create(endpoint);
			return endpoint;
		}
	}
	
	@Override
	public List<EndpointApiConfigDto> getAll() {
		List<EndpointApiConfig> allEndpoints = iEndpointApiConfigDao.getAll();
		ArrayList<EndpointApiConfigDto> endpoints = new ArrayList<EndpointApiConfigDto>();
		
		for(int i=0;i<allEndpoints.size();i++) {
			endpoints.add(this.mapper.map(allEndpoints.get(i), EndpointApiConfigDto.class));
		}
			
				
		return endpoints;

	
	}
}
