package com.logicladder.dgh.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.security.JwtTokenUtil;
import com.logicladder.dgh.service.IUserAuthDataService;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.thirdparty.request.dto.AlertRequest;
import com.logicladder.dgh.thirdparty.request.dto.Entity;
import com.logicladder.dgh.thirdparty.request.dto.LLCreateAlertRuleRequest;
import com.logicladder.dgh.thirdparty.request.dto.UserList;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.AlertRule;
import com.logicladder.dgh.thirdparty.response.dto.ResponseData;
import com.logicladder.dgh.thirdparty.response.dto.TPEntity;
import com.logicladder.dgh.thirdparty.response.dto.TriggeredAlert;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/alert")
public class AlertController {

	private static final Logger logger = Logger.getLogger(AlertController.class);

	@Autowired
	private IUserService iUserService;

	@Autowired
	private IUserAuthDataService iUserAuthDataService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private IAsyncLogicLadderService iAsyncLogicLadderService;

	@Value("${jwt.header}")
	private String tokenHeader;

	@PostMapping(path = "/create")
	public ResponseEntity<?> createAlert(@RequestBody AlertRequest alertRequest,
			HttpServletRequest httprequest) {
		try {
			logger.info("Request to process received at AlertController.createAlert");

			String username = Utility.getUsernameFromAuthToken(httprequest, this.tokenHeader,
					this.jwtTokenUtil);

			String jwtToken = Utility.getJwtTokenFromRequest(httprequest, this.tokenHeader);

			String llAuthToken = this.iUserAuthDataService.findByJwtToken(jwtToken).getLlAuthToken();

			logger.info("getAlertList() llAuthToken :: " + llAuthToken);

			if (StringUtils.isNotBlank(username)) { // Validate the request is from a genuine user

				LLCreateAlertRuleRequest request = new LLCreateAlertRuleRequest();

				User user = this.iUserService.get(alertRequest.getUserId());
				Long coreUserId = user.getCoreUserId();
				UserList userList = new UserList(coreUserId);
				List<UserList> list = new ArrayList<UserList>();
				list.add(userList);
				request.setUserList((list));

				Long entityId = alertRequest.getEntityId();
				Entity entities = new Entity(entityId);
				List<Entity> listEntites = new ArrayList<Entity>();
				listEntites.add(entities);
				request.setEntities(listEntites);

				String lhs = "{\"metric\":{\"dependsOn\":[\"" + alertRequest.getMetric() + "\"],\"expr\":\""
						+ alertRequest.getMetric() + "\"},\"dArg\":\"instant\",\"units\":{\"" + alertRequest
								.getMetric() + "\":\"" + alertRequest.getUnit() + "\"}}";

				String rhs = "{\"metric\":{\"expr\":\"" + alertRequest.getVal() + "\"}}";
				request.setLhs(lhs);
				request.setRhs(rhs);
				request.setIsRHSMetric(false);
				request.setOnEveryOccurence(true);
				request.setOnFirstOccurence(false);
				request.setSendSMS(true);
				request.setSendEmail(true);
				request.setName(alertRequest.getName());
				request.setPriority(alertRequest.getPriority());
				request.setCondition(alertRequest.getCondition());
				request.setDay("2");
				request.setHour("2");
				request.setMin("20");
				request.setPhone(alertRequest.getPhone());
				request.setEmail(alertRequest.getEmail());
				request.setVal(alertRequest.getVal());
				request.setApplicableBetween(alertRequest.getFrom() + "-" + alertRequest.getTo());
				request.setStatus("ENABLED");
				request.setType(alertRequest.getType());

				boolean success = true;
				try {
					String str = iAsyncLogicLadderService.createAlertRule(request);
				} catch (HttpClientErrorException httpError) {
					logger.error("Error from core system" + httpError);
					success = false;
				} catch (HttpServerErrorException httpError) {
					logger.error("Error from core system" + httpError);
					success = false;
				} catch (Exception e) {
					success = false;
					logger.error("Error in middle layer" + e);

				}

				if (!success) {
					return ResponseEntity.ok().body(Utility.createFailure(10001,
							"Some problem occurred. Please try again."));
				}

				logger.info("Request to process received at AlertController.createAlert alertRequest :: "
						+ alertRequest.toString());
				ResponseData response = new ResponseData();
				response = new ResponseData();
				response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
				response.setMessage(ApplicationConstants.ResponseMessage.ALERT_RULE_SUCCESS);

				return ResponseEntity.ok().body(response);

			}

			return ResponseEntity.ok().body(Utility.createFailure(10001, "Authentication Failure"));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/list")
	public ResponseEntity<?> getAlertList(HttpServletRequest httprequest) {
		try {
			logger.info("Request to process received at AlertController.getAlertList");

			ResponseData response = new ResponseData();

			String username = Utility.getUsernameFromAuthToken(httprequest, this.tokenHeader,
					this.jwtTokenUtil);

			String jwtToken = Utility.getJwtTokenFromRequest(httprequest, this.tokenHeader);

			String llAuthToken = this.iUserAuthDataService.findByJwtToken(jwtToken).getLlAuthToken();

			if (StringUtils.isNotBlank(username)) { // Validate the request is from a genuine user
				logger.info("getAlertList() llAuthToken :: " + llAuthToken);

				try {

					AlertRule[] alertArr = null;

					Map<String, AlertRule[]> preloadData = new HashMap<>();

					alertArr = iAsyncLogicLadderService.getUserAlerts();
					if (alertArr == null || alertArr.length == 0) {
						return ResponseEntity.ok().body(Utility.getResponseForObject(
								ApplicationConstants.ResponseMessage.SUCCESS, "No alerts found for user",
								"alerts", false));
					}

					preloadData.put("alerts", alertArr);

					response = new ResponseData(preloadData);
					response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
					response.setMessage(ApplicationConstants.ResponseMessage.ALERT_RULE_FETCH_SUCCESS);

					logger.info("getAlertList() Response :: " + alertArr);

					return ResponseEntity.ok().body(response);

				} catch (HttpClientErrorException httpError) {
					logger.error("Error in LL APIs" + httpError);
				} catch (Exception e) {
					logger.error("Error in middle layer" + e);
				}
			}
			return ResponseEntity.ok().body(Utility.createFailure(10001, "Authentication Failure"));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/triggered")
	public ResponseEntity<?> getTriggeredAlertList(HttpServletRequest httprequest) {
		try {
			logger.info("Request to process received at AlertController.getTriggeredAlertList");

			ResponseData response = new ResponseData();

			String username = Utility.getUsernameFromAuthToken(httprequest, this.tokenHeader,
					this.jwtTokenUtil);

			String jwtToken = Utility.getJwtTokenFromRequest(httprequest, this.tokenHeader);

			String llAuthToken = this.iUserAuthDataService.findByJwtToken(jwtToken).getLlAuthToken();

			if (StringUtils.isNotBlank(username)) { // Validate the request is from a genuine user
				logger.info("getTriggeredAlertList() llAuthToken :: " + llAuthToken);

				try {
					TriggeredAlert[] alertArr = null;

					Map<String, TriggeredAlert[]> preloadData = new HashMap<>();
					int pageNum = 1; // TODO - remove hardcoded pageNumber

					// TODO: Replace with API Call

					alertArr = iAsyncLogicLadderService.getTriggeredAlerts(pageNum);

					if (alertArr == null || alertArr.length == 0) {
						return ResponseEntity.ok().body(Utility.createFailure(10011,
								"No Alerts Triggered for User"));
					}

					preloadData.put("alerts", alertArr);

					response = new ResponseData(preloadData);
					response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
					response.setMessage(ApplicationConstants.ResponseMessage.TRIGGERED_ALERT_FETCH_SUCCESS);

					logger.info("getTriggeredAlertList() Response :: " + alertArr);

					return ResponseEntity.ok().body(response);

				} catch (HttpClientErrorException httpError) {
					logger.error("Error in LL APIs" + httpError);
				} catch (Exception e) {
					logger.error("Error in middle layer" + e);
				}
			}
			return ResponseEntity.ok().body(Utility.createFailure(10001, "Authentication Failure"));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/facilities")
	public ResponseEntity<?> getFacilityList(HttpServletRequest httprequest) {
		try {
			String username = Utility.getUsernameFromAuthToken(httprequest, this.tokenHeader,
					this.jwtTokenUtil);

			String jwtToken = Utility.getJwtTokenFromRequest(httprequest, this.tokenHeader);

			String llAuthToken = this.iUserAuthDataService.findByJwtToken(jwtToken).getLlAuthToken();

			logger.info("Request to process received at AlertController.getFacilityList llAuthToken :: "
					+ llAuthToken);

			TPEntity[] tpEntities = iAsyncLogicLadderService.getEntities(0L, false);

			List<TPEntity> facilityEntity = new ArrayList<TPEntity>();
			if (tpEntities != null && tpEntities.length > 0) {
				for (int i = 0; i < tpEntities.length; i++) {
					for (TPEntity child1 : tpEntities[i].getChildren()) {
						makeEntityInMapView(facilityEntity, child1);
						if (child1.getChildren() != null && child1.getChildren().size() > 0) {
							for (TPEntity child2 : child1.getChildren()) {
								makeEntityInMapView(facilityEntity, child2);
								if (child2.getChildren() != null && child2.getChildren().size() > 0) {
									for (TPEntity child3 : child2.getChildren()) {
										makeEntityInMapView(facilityEntity, child3);
										if (child3.getChildren() != null && child3.getChildren().size() > 0) {
											for (TPEntity child4 : child3.getChildren()) {
												makeEntityInMapView(facilityEntity, child4);
												if (child4.getChildren() != null && child4.getChildren()
														.size() > 0) {
													for (TPEntity child5 : child4.getChildren()) {
														makeEntityInMapView(facilityEntity, child5);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			ResponseData response = new ResponseData();
			Map<String, List<TPEntity>> preloadData = new HashMap<>();
			preloadData.put("entities", facilityEntity);

			response = new ResponseData(preloadData);
			response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
			response.setMessage(ApplicationConstants.ResponseMessage.FACILITY_LIST_FETCH_SUCCESS);

			logger.info("Request to process received at AlertController.getFacilityList response :: "
					+ response);

			return ResponseEntity.ok().body(response);
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	public void makeEntityInMapView(List<TPEntity> facilityEntity, TPEntity child) {

		if (ApplicationConstants.EntityType.FACILITY.equalsIgnoreCase(child.getType())) {
			facilityEntity.add(child);
		}
	}

	@GetMapping(path = "/input")
	public ResponseEntity<?> getAlertInputList(HttpServletRequest httprequest) {
		try {
			logger.info("Request to process received at AlertController.getAlertInputList");

			ResponseData response = new ResponseData();

			String username = Utility.getUsernameFromAuthToken(httprequest, this.tokenHeader,
					this.jwtTokenUtil);

			String jwtToken = Utility.getJwtTokenFromRequest(httprequest, this.tokenHeader);

			String llAuthToken = this.iUserAuthDataService.findByJwtToken(jwtToken).getLlAuthToken();

			if (StringUtils.isNotBlank(username)) { // Validate the request is from a genuine user
				logger.info("getAlertInputList() llAuthToken :: " + llAuthToken);

				try {
					AlertRule[] alertArr = null;

					Map<String, AlertRule[]> preloadData = new HashMap<>();

					String jsonString = "[{\"id\":46191,\"alertType\":\"Low Hydrant Pressure\",\"startTime\":\"2017-09-14 17:24:56\",\"endTime\":null,\"description\":\"Pressure 0.24 kg/cm² is less than 3 kg/cm²\",\"entity\":{\"id\":469,\"name\":\"ND10-PVR Vasant Kunj Ambiance\",\"type\":\"facility\",\"description\":null,\"locatedDevices\":null,\"shifts\":null,\"escalationLevels\":null,\"budgetAllocation\":null,\"billingCycles\":null,\"timezone\":null,\"path\":null,\"leftValue\":0,\"rightValue\":0,\"rootValue\":0,\"properties\":[]},\"status\":\"ACTIVE\",\"currentValue\":0.236049,\"ruleId\":50,\"isCommunicated\":0,\"min\":3,\"max\":null,\"isSystem\":0,\"type\":null},{\"id\":46191,\"alertType\":\"Low Hydrant Pressure\",\"startTime\":\"2017-07-12 17:24:56\",\"endTime\":null,\"description\":\"Pressure 0.24 kg/cm² is less than 3 kg/cm²\",\"entity\":{\"id\":469,\"name\":\"ND10-PVR Vasant Kunj Ambiance\",\"type\":\"facility\",\"description\":null,\"locatedDevices\":null,\"shifts\":null,\"escalationLevels\":null,\"budgetAllocation\":null,\"billingCycles\":null,\"timezone\":null,\"path\":null,\"leftValue\":0,\"rightValue\":0,\"rootValue\":0,\"properties\":[]},\"status\":\"ACTIVE\",\"currentValue\":0.236049,\"ruleId\":50,\"isCommunicated\":0,\"min\":3,\"max\":null,\"isSystem\":0,\"type\":null},{\"id\":46191,\"alertType\":\"Low Hydrant Pressure\",\"startTime\":\"2018-07-05 17:24:56\",\"endTime\":null,\"description\":\"Pressure 0.24 kg/cm² is less than 3 kg/cm²\",\"entity\":{\"id\":469,\"name\":\"ND10-PVR Vasant Kunj Ambiance\",\"type\":\"facility\",\"description\":null,\"locatedDevices\":null,\"shifts\":null,\"escalationLevels\":null,\"budgetAllocation\":null,\"billingCycles\":null,\"timezone\":null,\"path\":null,\"leftValue\":0,\"rightValue\":0,\"rootValue\":0,\"properties\":[]},\"status\":\"ACTIVE\",\"currentValue\":0.236049,\"ruleId\":50,\"isCommunicated\":0,\"min\":3,\"max\":null,\"isSystem\":0,\"type\":null}]";
					try {
						alertArr = (AlertRule[]) Utility.convertStringToAlertRuleArray(jsonString);
					} catch (Exception e) {
						e.printStackTrace();
					}

					if (alertArr == null || alertArr.length == 0) {
						return ResponseEntity.ok().body(Utility.createFailure(10011,
								"No Data Found for User"));
					}

					preloadData.put("alertInputs", alertArr);

					response = new ResponseData(preloadData);
					response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);

					logger.info("getAlertInputList() Response :: " + alertArr);

					return ResponseEntity.ok().body(response);

				} catch (HttpClientErrorException httpError) {
					logger.error("Error in LL APIs" + httpError);
				} catch (Exception e) {
					logger.error("Error in middle layer" + e);
				}
			}
			return ResponseEntity.ok().body(Utility.createFailure(10001, "Authentication Failure"));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/metrics")
	public ResponseEntity<?> getMetrics(@RequestBody String entityId, HttpServletRequest httprequest) {
		try {
			logger.info("Request to process received at AlertController.getMetrics");

			ResponseData response = new ResponseData();

			String username = Utility.getUsernameFromAuthToken(httprequest, this.tokenHeader,
					this.jwtTokenUtil);

			String jwtToken = Utility.getJwtTokenFromRequest(httprequest, this.tokenHeader);

			String llAuthToken = this.iUserAuthDataService.findByJwtToken(jwtToken).getLlAuthToken();

			if (StringUtils.isNotBlank(username)) { // Validate the request is from a genuine user
				logger.info("getMetrics() entityId :: " + entityId);

				try {
					AlertMetric[] alertArr = null;

					Map<String, AlertMetric[]> preloadData = new HashMap<>();

					// TODO: Replace with API Call

					alertArr = iAsyncLogicLadderService.getMetrics(entityId);

					if (alertArr == null || alertArr.length == 0) {
						return ResponseEntity.ok().body(Utility.createFailure(10011,
								"No Metrics found for User"));
					}

					preloadData.put("metrics", alertArr);

					response = new ResponseData(preloadData);
					response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
					response.setMessage(ApplicationConstants.ResponseMessage.METRIC_DATA_FETCH_SUCCESS);

					logger.info("getMetrics() Response :: " + alertArr);

					return ResponseEntity.ok().body(response);

				} catch (HttpClientErrorException httpError) {
					logger.error("Error in LL APIs" + httpError);
				} catch (Exception e) {
					logger.error("Error in middle layer" + e);
				}
			}
			return ResponseEntity.ok().body(Utility.createFailure(10001, "Authentication Failure"));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

}
