/*
 * 
 */
package com.logicladder.dgh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.WellDto;
import com.logicladder.dgh.entity.app.Well;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.service.IWellService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/well")
public class WellController {

	private static Logger logger = LogManager.getLogger(WellController.class);

	@Autowired
	IWellService wellService;

	@PostMapping(path = "/create")
	public ResponseEntity<?> createWell(HttpServletRequest request, @RequestBody WellDto wellDto)
			throws CustomException {
		try {
			if (wellDto.getId() != null) {
				throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]",
						wellDto.getId()));
			}

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Well Created Successfully", "well", Utility
							.processForSerialization(this.wellService.create(wellDto))));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "deleteById")
	public ResponseEntity<?> deleteWellById(@RequestParam long id, HttpServletRequest reques) {
		try {
			boolean resp = this.wellService.deleteById(id);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Well Deleted Succesfully", "deleted",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "getById/{id}")
	public ResponseEntity<?> getWellById(@PathVariable("id") long id, HttpServletRequest reques) {
		try {
			Well resp = this.wellService.getById(id);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Well Fetched Succesfully", "well", Utility
							.processForSerialization(resp)));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> updateWell(@PathVariable("id") long id, @RequestBody WellDto wellDto,
			HttpServletRequest reques) {

		try {
			wellDto.setId(id);
			Well resp = this.wellService.update(wellDto);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Well updated Successfully", "well", Utility
							.processForSerialization(resp)));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/getAll")
	public ResponseEntity<?> getAll(HttpServletRequest request, @RequestParam EntityStatus status) {
		try {
			List<Well> resp = this.wellService.getAllByStatus(status);
			if (resp != null) {
				resp.forEach(well -> {
					well = Utility.processForSerialization(well);
				});
			} else {
				resp = new ArrayList<>();
			}
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Wells Fetched Succesfully ", "wells",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
}
