
package com.logicladder.dgh.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.OperatorDto;
import com.logicladder.dgh.entity.app.Operator;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.JwtTokenUtil;
import com.logicladder.dgh.service.IOperatorService;
import com.logicladder.dgh.util.Utility;

/**
 * The Class OperatorController.
 * 
 * There Would be Aop which will log Methods execution time and all . So No need
 * to Log Manually Start and End
 */
@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/operator")
public class OperatorController {

	@Autowired
	private IOperatorService iOperatorService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	Logger logger;

	@Value("${jwt.header}")
	private String tokenHeader;

	@PostMapping("/changeRequestStatus/{operatorId}")
	public ResponseEntity<?> changeRequestStatus(
			@PathVariable(value = "operatorId", required = true) Long operatorId,
			@RequestParam(value = "status", required = true) ApprovalStatus status,
			HttpServletRequest request) {
		this.logger.info("Request to process received at UserController.changeRequestStatus");

		Operator operator = this.iOperatorService.changeRequestStatus(operatorId, status);
		if (operator == null) {
			throw new IllegalArgumentException(String.format("Invalid Request -  %s", status));
		}
		Map<String, Object> respobj = new HashMap<>();
		respobj.put("isUpdated", operator != null ? true : false);
		return ResponseEntity.ok().body(new Response<>(respobj, "ApprovalStatus Altered Succssfully"));
	}

	@DeleteMapping(path = "deleteById")
	public ResponseEntity<?> deleteOperatorById(@RequestParam long id, HttpServletRequest reques) {
		this.logger.info("Request to process received at OperatorController.deleteOperatorById");

		boolean resp = this.iOperatorService.deleteById(id);
		return ResponseEntity.ok(new Response<>(resp, String.format("Data Deletd Succesfully of Id [%s]",
				id)));
	}

	@RequestMapping(path = "/getAll", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<?> getAllOperator(HttpServletRequest request) {
		return ResponseEntity.ok().body(Utility.getResponseForObject(
				ApplicationConstants.ResponseMessage.SUCCESS, "Retreival Successfull", "operators",
				this.iOperatorService.getAll()));
	}

	@GetMapping(path = "getById/{id}")
	public ResponseEntity<?> getOperatorById(@PathVariable("id") long id, HttpServletRequest reques) {
		this.logger.info("Request to process received at OperatorController.getOperatorById");

		Operator resp = this.iOperatorService.get(id);
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("operator", resp);
		return ResponseEntity.ok().body(new Response<>(ApplicationConstants.ResponseMessage.SUCCESS,
				"operator data retreived", responseData));
	}

	@PostMapping("/getRequestsByType")
	public ResponseEntity<?> getRequestsByType(HttpServletRequest request,
			@RequestParam ApprovalStatus status) {
		this.logger.info("Request to process received at UserController.getRequestsByType");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username)) {
			Map<Object, Object> responseData = new HashMap<>();
			responseData.put("pendingOperatorRequests", this.iOperatorService.getRequestByStatus(username,
					status));
			Response<?> response = new Response<>(ApplicationConstants.ResponseMessage.SUCCESS,
					"data retreival success", responseData);

			return ResponseEntity.ok().body(response);
		}
		throw new HttpServerErrorException(HttpStatus.UNAUTHORIZED);
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> updateOperator(@PathVariable("id") long id, @RequestBody OperatorDto operatorDto,
			HttpServletRequest reques) {
		this.logger.info("Request to process received at OperatorController.updateOperator");

		operatorDto.setId(id);
		Operator operator = this.iOperatorService.saveOrUpdate(this.iOperatorService.mapToEntity(operatorDto,
				operatorDto.getStatus()));
		return ResponseEntity.ok(new Response<>(operator, String.format("Data updated Successfully", operator
				.getId())));
	}

	@GetMapping(path = "verifyEmail/{email}")
	public ResponseEntity<?> verifyEmailIdExits(@PathVariable("email") String email,
			HttpServletRequest reques) {

		Operator resp = iOperatorService.findByEmail(email);
		boolean status = resp == null ? false : true;
		Map<String, Boolean> respMap = new HashMap<>();
		respMap.put("isExits", status);
		return ResponseEntity.ok().body(new Response<>(respMap, "Successfull"));
	}

	@PostMapping(path = "/registrationRequest")
	public ResponseEntity<?> createOperator(HttpServletRequest request, @RequestBody OperatorDto operatorDto)
			throws CustomException {
		this.logger.info("Request to process received at OperatorController.createOperator");

		if (operatorDto.getId() != null) {
			throw new IllegalArgumentException(String.format("Request can not contain the Id [%s]",
					operatorDto.getId()));
		}

		String username = Utility.getUsernameFromAuthToken(request, tokenHeader, jwtTokenUtil);

		if (StringUtils.isNotBlank(username)) {
			Operator operator = this.iOperatorService.createOperatorRequest(operatorDto, username);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Operator Created successfully", "operator",
					operator));
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Authentication is required to view this resource"));

	}

}
