package com.logicladder.dgh.controller;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.AuditResponseDto;
import com.logicladder.dgh.service.IAuditService;
import com.logicladder.dgh.util.Utility;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/audit")
public class AuditLogController {

    @Autowired
    Logger logger;

    @Autowired
    IAuditService auditService;

    @GetMapping(path = "/get")
    public ResponseEntity<?> getAll(@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "startDate",required = false) Date startDate,
                                    @DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam(value = "endDate",required = false) Date endDate,
                                    @RequestParam(value = "entity",required = false) String entity,
                                    @RequestParam(value = "page",required = false,defaultValue = "0") int page,
                                    @RequestParam(value = "size",required = false,defaultValue = "10") int size) {

        AuditResponseDto auditResponseDto = auditService.getAuditLogs(startDate,endDate,entity,page,size);
        return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
                "AuditLogs Fetched Successfully ", "auditLogs", auditResponseDto));
    }
}
