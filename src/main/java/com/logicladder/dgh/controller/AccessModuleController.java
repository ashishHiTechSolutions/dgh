package com.logicladder.dgh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.RoleDto;
import com.logicladder.dgh.entity.app.AccessRolePageGroup;
import com.logicladder.dgh.entity.app.PageGroup;
import com.logicladder.dgh.enums.EnumUtils;
import com.logicladder.dgh.service.IAccessRolePageGroupService;
import com.logicladder.dgh.service.IPageGroupService;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/access")
public class AccessModuleController {

	@Autowired
	private IAccessRolePageGroupService iAccessRolePageGroupService;

	@Autowired
	private IUserService iUserService;

	@Autowired
	private IPageGroupService iPageGroupService;

	private static Logger logger = LogManager.getLogger(AccessModuleController.class);
	
	@PostMapping(path = "/getAccessForRole")
	public ResponseEntity<?> getAccessByRole(HttpServletRequest httprequest, @RequestParam String role)
			throws CustomException {
		try {
			if (StringUtils.isBlank(role)) {
				throw new CustomException(EnumUtils.ErrorCode.ROLE_REQUIRED.getErrorCode(), new Exception(
						"Role is Required"));
			}
			AccessRolePageGroup accessRolePageGroup = iAccessRolePageGroupService.findByRoleRoleName(role);
			return ResponseEntity.ok().body(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Access Retrieved successfully",
					"accessGroup", accessRolePageGroup));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/getAccessForUser")
	public ResponseEntity<?> getAccessByUsername(HttpServletRequest httprequest,
			@RequestParam String username) throws CustomException {
		try {
			if (StringUtils.isBlank(username)) {
				throw new CustomException(EnumUtils.ErrorCode.ACCOUNT_DOES_NOT_EXIST.getErrorCode(),
						new Exception("Role is Required"));
			}

			AccessRolePageGroup accessRolePageGroup = iAccessRolePageGroupService.findByRoleRoleName(
					iUserService.findByUserName(username).getUserRole().getRoleName());
			return ResponseEntity.ok().body(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Access Retrieved successfully",
					"accessGroup", accessRolePageGroup));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getAllPageGroups")
	public ResponseEntity<?> getAllPages(HttpServletRequest httprequest) throws CustomException {
		List<PageGroup> pageGroups = iPageGroupService.getAll();
		try {
			return ResponseEntity.ok().body(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "All Pages Retrieved successfully",
					"pageGroups", pageGroups));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/createAccessRole")
	public ResponseEntity<?> createNewAccessRole(HttpServletRequest httprequest, @RequestBody RoleDto roleDto)
			throws CustomException {
		try {
			boolean status = iAccessRolePageGroupService.createNewAccessRoleGroup(roleDto);

			return ResponseEntity.ok().body(Utility.getResponseForObject(status
					? ApplicationConstants.ResponseMessage.SUCCESS
					: ApplicationConstants.ResponseMessage.FAILURE, "Role Creation status", "isCreated",
					status));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
}
