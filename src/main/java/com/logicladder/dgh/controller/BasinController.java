/*
 * 
 */
package com.logicladder.dgh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.BasinDto;
import com.logicladder.dgh.entity.app.Basin;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.service.IBasinService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/basin")
public class BasinController {

	private static Logger logger = LogManager.getLogger(BasinController.class);

	@Autowired
	IBasinService iBasinService;

	@PostMapping(path = "/create")
	public ResponseEntity<?> createBasin(HttpServletRequest request, @RequestBody BasinDto basinDto)
			throws CustomException {
		try {
			logger.info("Application Control received at BasinController.createBasin");

			if (basinDto.getId() != null) {
				throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]",
						basinDto.getId()));
			}
			Basin basin = Utility.processForSerialization(this.iBasinService.create(basinDto));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Basin Created Successfully", "basin",
					basin));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@DeleteMapping(path = "/deleteById")
	public ResponseEntity<?> deleteBasinById(@RequestBody long id, HttpServletRequest request) {
		try {
			logger.info("Application Control received at BasinController.deleteBasinById");

			boolean resp = this.iBasinService.deleteById(id);

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, String.format(
							"Data Deletd Succesfully of Id [%s]", id), "basin", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/getAll")
	public ResponseEntity<?> getAll(HttpServletRequest request, @RequestParam EntityStatus status) {
		try {
			logger.info("Application Control received at BasinController.getAll");

			List<Basin> resp = this.iBasinService.getAllByStatus(status);
			if (resp != null) {
				resp.forEach(basin -> {
					basin = Utility.processForSerialization(basin);
				});
			} else {
				resp = new ArrayList<>();
			}
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Data Fetched Succesfully", "basin", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getById/{id}")
	public ResponseEntity<?> getBasinById(@PathVariable("id") long id, HttpServletRequest request) {
		try {
			logger.info("Application Control received at BasinController.getBasinById");

			Basin resp = Utility.processForSerialization(this.iBasinService.getById(id));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, String.format(
							"Data Fetched Succesfully of Id [%s]", resp.getId()), "basin", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> updateBasin(@PathVariable("id") long id, @RequestBody BasinDto basinDto,
			HttpServletRequest request) {
		try {
			logger.info("Application Control received at BasinController.updateBasin");

			basinDto.setId(id);
			Basin resp = Utility.processForSerialization(this.iBasinService.update(basinDto));

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, String.format(
							"Data updated Successfully of Id [%s]", resp.getId()), "basin", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

}
