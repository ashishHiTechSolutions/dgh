/*
 * 
 */
package com.logicladder.dgh.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.UserDto;
import com.logicladder.dgh.dto.UserPasswordChangeDto;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.JwtTokenUtil;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.thirdparty.request.dto.UserPatchRequest;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH)
public class UserController {

	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
	private IUserService iUserService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;

	@PostMapping("/user/verifyOtp")
	public ResponseEntity<?> verifyOtp(@RequestParam String emailOtp, @RequestParam String mobileOtp,
			HttpServletRequest request) throws CustomException {
		UserController.logger.info("Request to process received at UserController.verifyOtp");

		if (StringUtils.isNotBlank(emailOtp) && StringUtils.isNotBlank(mobileOtp)) {
			try {
				Boolean verifyOtp = this.iUserService.verifyOtp(emailOtp, mobileOtp, Utility
						.getUsernameFromAuthToken(request, tokenHeader, jwtTokenUtil));
				if (verifyOtp)
					return ResponseEntity.ok().body(Utility.getResponseForObject(
							ApplicationConstants.ResponseMessage.SUCCESS, "OTP Verification Successfull", "otpVerified",
							true));
				return ResponseEntity.ok().body(Utility.getResponseForObject(
						ApplicationConstants.ResponseMessage.FAILURE, "OTP Verification Failed", "otpVerified", false));
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}

		return null;
	}

	@PostMapping("/user/changeRequestStatus")
	public ResponseEntity<?> changeRequestStatus(HttpServletRequest request, @RequestParam Long requestedUserId,
			@RequestParam ApprovalStatus status, @RequestParam String comments, @RequestParam String accessibleEntities) throws CustomException {
		UserController.logger.info("Request to process received at UserController.changeRequestStatus");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username)) {
			try {
				return ResponseEntity.ok().body(this.iUserService.changeRequestStatus(username,
						requestedUserId, status, Utility.getJwtTokenFromRequest(request, this.tokenHeader), comments, accessibleEntities));
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Invalid Authentication Token"));
	}

	@RequestMapping(path = "/user/list", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<?> getAllUsers(HttpServletRequest request) throws CustomException {
		UserController.logger.info("Request to process received at UserController.getAllUsers");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username)) {
			try {
				Response<?> response = new Response<>();

				List<User> allUsers = this.iUserService.getUserListByStatus(new ApprovalStatus[] {
						ApprovalStatus.ACTIVE, ApprovalStatus.SUSPENDED });
				if ((allUsers != null) && !allUsers.isEmpty()) {
					return ResponseEntity.ok().body(Utility.getResponseForObject(
							ApplicationConstants.ResponseMessage.SUCCESS,
							ApplicationConstants.ResponseMessage.SUCCESS, "users", allUsers));
				} else {
					response.setMessage("No user found");
					response.setStatus(ApplicationConstants.ResponseMessage.FAILURE);
				}
				return ResponseEntity.ok().body(response);
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Invalid Authentication Token"));
	}

	@PostMapping("user/getApplicationData")
	public ResponseEntity<?> getApplicationData(HttpServletRequest request) throws CustomException {
		UserController.logger.info(
				"Request to process received at AuthenticationRestController.getAllAccessModuleForRole");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);

		if (StringUtils.isNoneBlank(username)) {
			try {
				return ResponseEntity.ok().body(this.iUserService.getApplicationData(username));
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Invalid Authentication Token"));
	}

	@PostMapping("/user/getRequestsByType")
	public ResponseEntity<?> getRequestsByType(HttpServletRequest request,
			@RequestParam ApprovalStatus status) throws CustomException {
		UserController.logger.info("Request to process received at UserController.getRequestsByType");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username)) {
			try {
				return ResponseEntity.ok().body(this.iUserService.getRequestByStatus(username, status));
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Invalid Authentication Token"));
	}

	@RequestMapping(path = "/user/userData", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<?> getUserData(HttpServletRequest request) throws CustomException {
		UserController.logger.info("Request to process received at UserController.getUserData");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username)) {
			try {
				Response<?> response = this.iUserService.getUserData(username);
				if (response != null) {
					return ResponseEntity.ok().body(response);
				}
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Invalid Authentication Token"));
	}

	@RequestMapping(path = "/user/getUserDetails/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<?> getUserDetails(HttpServletRequest request, @PathVariable("id") long id)
			throws CustomException {
		UserController.logger.info(
				"Request to process received at AuthenticationRestController.getUserDetails");

		Map<String, Object> responseData = new HashMap<>();
		try {
			responseData.put("user", this.iUserService.get(id));
			return ResponseEntity.ok(new Response<>(ApplicationConstants.ResponseMessage.SUCCESS,
					"User Data Retrieved", responseData));
		} catch (Exception e) {
			throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
		}

	}

	@RequestMapping(path = "/logout", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<?> logoutOfAllDevices(HttpServletRequest request) throws CustomException {
		UserController.logger.info(
				"Request to process received at AuthenticationRestController.logoutOfAllDevices");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);

		if (StringUtils.isNoneBlank(username)) {
			try {
				return ResponseEntity.ok().body(this.iUserService.logOutUser(username));
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Invalid Authentication Token"));
	}

	@PatchMapping("/user/{id}")
	public ResponseEntity<?> patchUserWithNewData(HttpServletRequest request,
			@RequestBody UserPatchRequest[] userPatchRequests, @PathVariable("id") long id) {
		UserController.logger.info("Request to process received at UserController.patchUserWithNewData");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username)) {
			Response<?> response = this.iUserService.patchUserWithNewData(userPatchRequests, id,
					this.tokenHeader);

			if (response != null) {
				return ResponseEntity.ok().body(response);
			}
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);

		}
		throw new HttpServerErrorException(HttpStatus.UNAUTHORIZED);
	}

	@PostMapping("/user/resendActivation")
	public ResponseEntity<?> resendActivationLink(HttpServletRequest request, @RequestParam Long userId)
			throws CustomException {
		UserController.logger.info("Request to process received at UserController.resendActivationLink");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username) && StringUtils.isNoneBlank(username)) {
			try {
				Response<?> response = this.iUserService.resendActivationLink(username, userId);

				return ResponseEntity.ok().body(response);
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Invalid Authentication Token"));
	}

	@PostMapping(path = "/user/changePassword")
	public ResponseEntity<?> resetUserPassword(HttpServletRequest request,
			@RequestBody UserPasswordChangeDto userPasswordChangeDto) {
		UserController.logger.info("Request to process received at UserController.resetUserPassword");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username)) {
			User user = this.iUserService.changePassword(userPasswordChangeDto);
			if (user != null) {
				return ResponseEntity.ok().body(Utility.getResponseForObject(
						ApplicationConstants.ResponseMessage.SUCCESS,
						ApplicationConstants.ResponseMessage.SUCCESS, "user", user));
			}
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		throw new HttpServerErrorException(HttpStatus.UNAUTHORIZED);
	}

	@PostMapping(path = "/user/setPassword")
	public ResponseEntity<?> setUserPassword(HttpServletRequest request, @RequestParam String password)
			throws CustomException {
		UserController.logger.info("Request to process received at UserController.setUserPassword");

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		if (StringUtils.isNotBlank(username) && Utility.isPasswordValid(password)) {
			try {
				User user = this.iUserService.updatePassword(username, password, Utility
						.getJwtTokenFromRequest(request, this.tokenHeader));
				if (user != null) {
					return ResponseEntity.ok().body(Utility.getResponseForObject(
							ApplicationConstants.ResponseMessage.SUCCESS, "password successfully set",
							"responseData", user));
				} else {
					return ResponseEntity.ok().body(Response.of(ErrorCode.PASSWORD_HAS_BEEN_SET,
							"Password has already been set for this user",
							ApplicationConstants.ResponseMessage.FAILURE));
				}
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Password Is not valid as per password policy"));

	}

	@PutMapping(path = "/user/update")
	public ResponseEntity<?> updateUser(HttpServletRequest request, @RequestBody UserDto userDto)
			throws CustomException {
		UserController.logger.info("Request to process received at UserController.updateUser");
		if (userDto.getId() == null) {
			throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]", userDto
					.getId()));
		}
		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		Map<String, Object> responseData = new HashMap<>();
		if (StringUtils.isNotBlank(username)) {
			try {
				responseData.put("user", this.iUserService.updateUser(userDto, username));
				return ResponseEntity.ok().body(new Response<>(ApplicationConstants.ResponseMessage.SUCCESS,
						"user data updated", responseData));
			} catch (Exception e) {
				throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), e);
			}
		}
		throw new CustomException(ErrorCode.AUTHENTICATION.getErrorCode(), new Exception(
				"Invalid Authentication Token"));
	}

	@PostMapping(path = "/user/validatePassword")
	public ResponseEntity<?> registration(@RequestParam String password) {

		return ResponseEntity.ok().body(Utility.getResponseForObject(
				ApplicationConstants.ResponseMessage.SUCCESS, "password validated", "ispasswordValid", true));
	}

	@GetMapping(path = "/user/resendOtp")
	public ResponseEntity<?> resendOtp(HttpServletRequest request) {
		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
		iUserService.resendOtp(username);

		return ResponseEntity.ok().body(Utility.getResponseForObject(
				ApplicationConstants.ResponseMessage.SUCCESS, "OTP Sent", "OTP Sent", true));
	}
}