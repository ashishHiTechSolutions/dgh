package com.logicladder.dgh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.TariffDto;
import com.logicladder.dgh.entity.app.Tariff;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.service.ITariffService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/tariff")
public class TariffController {

	private static Logger logger = LogManager.getLogger(TariffController.class);

	@Autowired
	private ITariffService tariffSevice;

	@PostMapping(path = "/create")
	public ResponseEntity<?> createTariff(HttpServletRequest request, @RequestBody TariffDto tariffDto)
			throws CustomException {
		try {
			// Tariff tariff =
			// Utility.processForSerialization(this.tariffSevice.create(tariffDto));
			if (tariffDto.getId() != null) {
				throw new IllegalArgumentException(String.format("Request should not contain the Id [%s]",
						tariffDto.getId()));
			}

			Tariff tariff = this.tariffSevice.create(tariffDto);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Tariff Created Successfully",
					"tarfiffCreate", tariff));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "deleteById")
	public ResponseEntity<?> deleteTariffById(@RequestParam long id, HttpServletRequest request) {
		try {
			logger.info("Application Control received at TariffController.deleteTariffById");

			boolean resp = false;
			resp = this.tariffSevice.deleteById(id);

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Tariff Deleted Succesfully", "deleted",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getAll")
	public ResponseEntity<?> getAll(HttpServletRequest request, @RequestParam EntityStatus status) {
		try {
			logger.info("Application Control received at TariffController.getAll");

			List<Tariff> resp = this.tariffSevice.getAllByStatus(status);

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Facilities Fetched Succesfully ",
					"tariffs", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "getById/{id}")
	public ResponseEntity<?> getTariffById(@PathVariable("id") long id, HttpServletRequest request) {
		try {
			logger.info("Application Control received at FacilityController.getFacilityById");
			Tariff tariff = this.tariffSevice.getById(id);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Tariff Fetched Succesfully", "tariff",
					tariff));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> updateFacility(@PathVariable("id") long id, @RequestBody TariffDto tariffDto,
			HttpServletRequest request) {
		try {
			logger.info("Application Control received at FacilityController.updateFacility");

			Tariff resp = this.tariffSevice.update(tariffDto);
			tariffDto.setId(id);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Facility updated Successfully", "facility",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

}