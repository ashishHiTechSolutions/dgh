package com.logicladder.dgh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.FacilityDto;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.entity.app.FacilityOperator;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.service.IFacilityService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/facility")
public class FacilityController {

	private static Logger logger = LogManager.getLogger(FacilityController.class);

	@Autowired
	private IFacilityService facilitySevice;

	@PostMapping(path = "/create")
	public ResponseEntity<?> createFacility(HttpServletRequest request, @RequestBody FacilityDto facilityDto)
			throws CustomException {
		logger.info("Application Control received at FacilityController.createFacility");
		try {
			if (facilityDto.getId() != null) {
				throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]",
						facilityDto.getId()));
			}

			Facility facility = Utility.processForSerialization(this.facilitySevice.create(facilityDto));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Facility Created Successfully",
					"facilityCreate", facility));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "deleteById")
	public ResponseEntity<?> deleteFacilityById(@RequestParam long id, HttpServletRequest request) {
		logger.info("Application Control received at FacilityController.deleteFacilityById");
		try {
			boolean resp = false;
			resp = this.facilitySevice.deleteById(id);

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Facility Deleted Succesfully", "deleted",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/getAll")
	public ResponseEntity<?> getAll(HttpServletRequest request, @RequestParam EntityStatus status) {
		logger.info("Application Control received at FacilityController.getAll");
		try {
			List<Facility> resp = this.facilitySevice.getAllByStatus(status);

			if (resp != null) {
				resp.forEach(facility -> {
					facility = Utility.processForSerialization(facility);
				});
			} else {
				resp = new ArrayList<>();
			}

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Facilities Fetched Succesfully ",
					"facilities", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "getById/{id}")
	public ResponseEntity<?> getFacilityById(@PathVariable("id") long id, HttpServletRequest request) {
		logger.info("Application Control received at FacilityController.getFacilityById");
		try {
			Facility resp = Utility.processForSerialization(this.facilitySevice.getById(id));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Facility Fetched Succesfully", "facility",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> updateFacility(@PathVariable("id") long id, @RequestBody FacilityDto facilityDto,
			HttpServletRequest request) {
		logger.info("Application Control received at FacilityController.updateFacility");
		try {
			facilityDto.setId(id);
			Facility resp = Utility.processForSerialization(this.facilitySevice.update(facilityDto));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Facility updated Successfully", "facility",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "getByFacilityOperatorId/{id}")
	public ResponseEntity<?> getFacilityOperatorById(@PathVariable("id") long id,
			HttpServletRequest request) {
		logger.info("Application Control received at FacilityController.getFacilityOperatorById");
		try {
			FacilityOperator resp = Utility.processForSerialization(this.facilitySevice
					.getFacilityOperatorById(id));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "FacilitYOperator Fetched Succesfully",
					"facilityOperator", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
}
