package com.logicladder.dgh.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.logic.ladder.service.IAsyncLogicLadderService;
import com.logicladder.dgh.security.JwtTokenUtil;
import com.logicladder.dgh.service.IUserAuthDataService;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.thirdparty.request.dto.AlertRequest;
import com.logicladder.dgh.thirdparty.request.dto.Entity;
import com.logicladder.dgh.thirdparty.request.dto.LLCreateAlertRuleRequest;
import com.logicladder.dgh.thirdparty.request.dto.UserList;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.AlertRule;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequest;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequestData;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequestMetricData;
import com.logicladder.dgh.thirdparty.response.dto.MetricData;
import com.logicladder.dgh.thirdparty.response.dto.ResponseData;
import com.logicladder.dgh.thirdparty.response.dto.TPEntity;
import com.logicladder.dgh.thirdparty.response.dto.TriggeredAlert;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/changeRequest")
public class ChangeRequestController {

	private static final Logger logger = LogManager.getLogger(ChangeRequestController.class);

	@Autowired
	private IUserService iUserService;

	@Autowired
	private IUserAuthDataService iUserAuthDataService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private IAsyncLogicLadderService iAsyncLogicLadderService;

	@Value("${jwt.header}")
	private String tokenHeader;

	@PostMapping(path = "/edit")
	public ResponseEntity<?> editDataRequest(@RequestBody ChangeRequest changeRequest,
			HttpServletRequest httprequest) {
		ChangeRequestController.logger.info("Request to process received at ChangeRequestController.createAlert");

		String resp = this.iAsyncLogicLadderService.editDataRequest(changeRequest);
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
						"Status Succesfully Updated", "updated", resp));
	}
	
	@PostMapping(path = "/delete")
	public ResponseEntity<?> deleteDataRequest(@RequestBody ChangeRequest changeRequest,
			HttpServletRequest httprequest) {
		ChangeRequestController.logger.info("Request to process received at ChangeRequestController.createAlert");

		String resp = this.iAsyncLogicLadderService.deleteDataRequest(changeRequest);
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
						"Status Succesfully Updated", "updated", resp));
	}


	@GetMapping(path = "/list")
	public ResponseEntity<?> getChangeRequestList(HttpServletRequest httprequest) {
		ChangeRequestController.logger.info("Request to process received at ChangeRequestController.getChangeRequestList");

		ResponseData response = new ResponseData();

		String username = Utility.getUsernameFromAuthToken(httprequest, this.tokenHeader, this.jwtTokenUtil);

		String jwtToken = Utility.getJwtTokenFromRequest(httprequest, this.tokenHeader);

		if (StringUtils.isNotBlank(username)) { // Validate the request is from a genuine user

			try {

				ChangeRequest[] changeRequestArr = null;

				Map<String, ChangeRequest[]> preloadData = new HashMap<>();

				changeRequestArr = iAsyncLogicLadderService.getChangeRequests();
				if (changeRequestArr == null || changeRequestArr.length == 0) {
					return ResponseEntity.ok().body(Utility.getResponseForObject(
							ApplicationConstants.ResponseMessage.SUCCESS, "No change requests found",
							"changeRequests", false));
				}

				preloadData.put("changeRequests", changeRequestArr);

				response = new ResponseData(preloadData);
				response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
				response.setMessage(ApplicationConstants.ResponseMessage.CHANGE_REQUEST_FETCH_SUCCESS);

				logger.info("getChangeRequestList() Response :: " + changeRequestArr);

				return ResponseEntity.ok().body(response);

			} catch (HttpClientErrorException httpError) {
				logger.error("Error in LL APIs" + httpError);
			} catch (Exception e) {
				logger.error("Error in middle layer" + e);
			}
		}
		return ResponseEntity.ok().body(Utility.createFailure(10001, "Authentication Failure"));
	}
	
	
	@GetMapping(path = "/getById/{id}")
	public ResponseEntity<?> getChangeRequest(@PathVariable long id, HttpServletRequest httprequest) {
		ChangeRequestController.logger.info("Request to process received at ChangeRequestController.getChangeRequest");

		ResponseData response = new ResponseData();

		String username = Utility.getUsernameFromAuthToken(httprequest, this.tokenHeader, this.jwtTokenUtil);

		String jwtToken = Utility.getJwtTokenFromRequest(httprequest, this.tokenHeader);

		if (StringUtils.isNotBlank(username)) { // Validate the request is from a genuine user

			try {

				ChangeRequest changeRequest = null;

				Map<String, ChangeRequest> preloadData = new HashMap<>();

				changeRequest = iAsyncLogicLadderService.getChangeRequest(id);
				if (changeRequest == null) {
					return ResponseEntity.ok().body(Utility.getResponseForObject(
							ApplicationConstants.ResponseMessage.SUCCESS, "No change request found",
							"changeRequest", false));
				}

				preloadData.put("changeRequest", changeRequest);

				response = new ResponseData(preloadData);
				response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
				response.setMessage(ApplicationConstants.ResponseMessage.CR_FETCH_SUCCESS);

				logger.info("getChangeRequest() Response :: " + changeRequest);

				return ResponseEntity.ok().body(response);

			} catch (HttpClientErrorException httpError) {
				logger.error("Error in LL APIs" + httpError);
			} catch (Exception e) {
				logger.error("Error in middle layer" + e);
			}
		}
		return ResponseEntity.ok().body(Utility.createFailure(10001, "Authentication Failure"));
	}

	@PostMapping(path = "/updateStatus/{id}/{status}")
	public ResponseEntity<?> updateStatus(@PathVariable(value = "id", required = true) Long id,
			@PathVariable(value = "status", required = true) String status, HttpServletRequest request) {

		boolean resp = true;
		//boolean resp = this.iAsyncLogicLadderService.updateStatus(id, status);
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"Status Succesfully Updated", "updated", resp));
	}
	
	@GetMapping(path = "/metricData")
	public ResponseEntity<?> getMetricData(@RequestParam String entityId, @RequestParam String metricName, @RequestParam String fromDate, @RequestParam String toDate, HttpServletRequest request) {
		ChangeRequestController.logger.info("Request to process received at AlertController.getMetricData");

		ResponseData response = new ResponseData();

		String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);

		String jwtToken = Utility.getJwtTokenFromRequest(request, this.tokenHeader);

		if (StringUtils.isNotBlank(username)) { // Validate the request is from a genuine user

			try {
				ChangeRequestMetricData metricDataArr = null;

				Map<String, ChangeRequestMetricData> preloadData = new HashMap<>();

				// TODO
				metricDataArr = iAsyncLogicLadderService.getMetricData(entityId, metricName, fromDate, toDate);

				if (metricDataArr == null) {
					return ResponseEntity.ok().body(Utility.createFailure(10011,
							"No Metrics found for User"));
				}

				preloadData.put("metricsData", metricDataArr);

				response = new ResponseData(preloadData);
				response.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
				response.setMessage(ApplicationConstants.ResponseMessage.PARAM_DATA_FETCH_SUCCESS);

				logger.info("getMetricData() Response :: " + metricDataArr);

				return ResponseEntity.ok().body(response);

			} catch (HttpClientErrorException httpError) {
				logger.error("Error in LL APIs" + httpError);
			} catch (Exception e) {
				logger.error("Error in middle layer" + e);
			}
		}
		return ResponseEntity.ok().body(Utility.createFailure(10001, "Authentication Failure"));
	}

}
