/*
 * 
 */
package com.logicladder.dgh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.ContractAreaDto;
import com.logicladder.dgh.entity.app.City;
import com.logicladder.dgh.entity.app.ContractArea;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.service.IContractAreaService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/contractArea")
public class ContractAreaController {

	@Autowired
	IContractAreaService contractAreaService;

	private static Logger logger = LogManager.getLogger(ContractAreaController.class);

	@PostMapping(path = "/create")
	public ResponseEntity<?> createContractArea(HttpServletRequest request,
			@RequestBody ContractAreaDto contractAreaDto) throws CustomException {
		if (contractAreaDto.getId() != null) {
			throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]",
					contractAreaDto.getId()));
		}
		try {
			ContractArea contractArea = Utility.processForSerialization(this.contractAreaService.create(
					contractAreaDto));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Contract Area Created Successfully",
					"contractArea", contractArea));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/deleteById")
	public ResponseEntity<?> deleteContractAreaById(@RequestParam long id, HttpServletRequest reques) {

		boolean resp = this.contractAreaService.deleteById(id);
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"Contract Area Deleted Succesfully", "deleted", resp));
	}

	@PostMapping(path = "/getAll")
	public ResponseEntity<?> getAll(HttpServletRequest reques, @RequestParam EntityStatus status) {

		try {
			List<ContractArea> resp = this.contractAreaService.getAllByStatus(status);

			if (resp != null) {
				resp.forEach(contractArea -> {
					contractArea = Utility.processForSerialization(contractArea);
				});
			} else {
				resp = new ArrayList<>();
			}
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Contract Area Fetched Succesfully ",
					"contractAreas", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getById/{id}")
	public ResponseEntity<?> getContractAreaById(@PathVariable("id") long id, HttpServletRequest reques) {

		try {
			ContractArea resp = Utility.processForSerialization(this.contractAreaService.getById(id));

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, String.format(
							"Data Fetched Succesfully of Id [%s]", resp.getId()), "contractArea", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> updateContractArea(@PathVariable("id") long id,
			@RequestBody ContractAreaDto contractAreaDto, HttpServletRequest reques) {

		try {
			contractAreaDto.setId(id);
			ContractArea resp = Utility.processForSerialization(this.contractAreaService.update(
					contractAreaDto));
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Contract Area updated Successfully",
					"contractArea", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getCitiesForBasin/{id}")
	public ResponseEntity<?> getCitiesForFacility(@PathVariable("id") long id, HttpServletRequest request) {
		try {
			List<City> cities = this.contractAreaService.retrieveCitiesForFacility(id);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Cities retreived successfully", "cities",
					cities));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
}
