package com.logicladder.dgh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.DeviceDto;
import com.logicladder.dgh.entity.app.Device;
import com.logicladder.dgh.enums.DeviceType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.service.IDeviceService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/device")
public class DeviceController {

	@Autowired
	IDeviceService iDeviceService;

	private static Logger logger = LogManager.getLogger(DeviceController.class);

	@PostMapping(path = "/create")
	public ResponseEntity<?> createDevice(HttpServletRequest request, @RequestBody DeviceDto deviceDto) {
		if (deviceDto.getId() != null) {
			throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]", deviceDto
					.getId()));
		}
		try {
			long resp = iDeviceService.save(deviceDto);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Device Created Successfully", "Device",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> updateDevice(@PathVariable("id") long id, @RequestBody DeviceDto deviceDto,
			HttpServletRequest reques) {
		try {
			deviceDto.setId(id);
			long resp = this.iDeviceService.merge(deviceDto);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Device updated Successfully", "Device",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getById/{id}")
	public ResponseEntity<?> getDeviceById(@PathVariable("id") long id, HttpServletRequest reques) {
		try {
			Device resp = iDeviceService.getById(id);

			Utility.processForSerialization(resp);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, String.format(
							"Data Fetched Succesfully of Id [%s]", resp.getId()), "Device", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "/getAllByDeviceTypeAndFacilityId")
	public ResponseEntity<?> getAllByDeviceTypeAndFacilityId(HttpServletRequest reques,
			@RequestParam(required = false) DeviceType deviceType,
			@RequestParam(required = false) Long facilityId) {

		List<Device> resp = iDeviceService.getAllByDeviceTypeAndFacilityId(deviceType, facilityId);
		for (Device device : resp) {
			Utility.processForSerialization(device);
		}
		if (resp == null) {
			resp = new ArrayList<>();
		}
		return ResponseEntity.ok(Utility.getResponseForObject(ApplicationConstants.ResponseMessage.SUCCESS,
				"Device Fetched Succesfully ", "device", resp));
	}

	@GetMapping(path = "/getAll")
	public ResponseEntity<?> getAll(HttpServletRequest reques,
			@RequestParam(required = false) EntityStatus status) {
		try {
			List<Device> resp = iDeviceService.getAllByStatus(status);
			if (CollectionUtils.isNotEmpty(resp)) {
				for (Device device : resp) {
					Utility.processForSerialization(device);
				}
			}
			if (resp == null) {
				resp = new ArrayList<>();
			}
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Device Fetched Succesfully ", "device",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@DeleteMapping(path = "/deleteById/{id}")
	public ResponseEntity<?> deleteDeviceById(@PathVariable long id, HttpServletRequest request) {
		try {
			boolean resp = this.iDeviceService.deleteById(id);

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, String.format(
							"Data Deletd Succesfully of Id [%s]", id), "device", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PutMapping(path = "/toggleStatus/{id}")
	public ResponseEntity<?> toggleStatus(@PathVariable long id, @RequestParam EntityStatus status,
			HttpServletRequest request) {
		try {
			boolean resp = this.iDeviceService.toggleStatus(status, id);

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, String.format(
							"Status updated Succesfully of Id [%s]", id), "device", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

}
