package com.logicladder.dgh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.GroupDto;
import com.logicladder.dgh.entity.app.Group;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.service.IGroupService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/group")
public class GroupController {
	
	private static Logger logger = LogManager.getLogger(TariffController.class);
	
	@Autowired
	private IGroupService groupSevice;
	
	@PostMapping(path = "/create")
	public ResponseEntity<?> createGroup(HttpServletRequest request, @RequestBody GroupDto groupDto)
			throws CustomException {
		
		logger.info("Application Control received at GroupController.createGroup");
		
		try {
			if (groupDto.getId() != null) {
				throw new IllegalArgumentException(String.format("Request should not contain the Id [%s]",
						groupDto.getId()));
			}

			Group group = this.groupSevice.create(groupDto);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Group Created Successfully",
					"groupCreate", group));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
	
	@PostMapping(path = "deleteById")
	public ResponseEntity<?> deleteGroupById(@RequestParam long id, HttpServletRequest request) {
		try {
			logger.info("Application Control received at GroupController.deleteGroupById");

			boolean resp = false;
			resp = this.groupSevice.deleteById(id);

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Group Deleted Succesfully", "groupDeleted",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
	
	@PostMapping(path = "/getAll")
	public ResponseEntity<?> getAll(HttpServletRequest request, @RequestParam EntityStatus status) {
		try {
			logger.info("Application Control received at GroupController.getAll");

			List<Group> resp = this.groupSevice.getAllByStatus(status);

			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Groups Fetched Succesfully ",
					"groups", resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "getById/{id}")
	public ResponseEntity<?> getGroupById(@PathVariable("id") long id, HttpServletRequest request) {
		try {
			logger.info("Application Control received at GroupController.getGroupById");
			Group group = this.groupSevice.getById(id);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Group Fetched Succesfully", "group",
					group));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
	
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> updateGroup(@PathVariable("id") long id, @RequestBody GroupDto groupDto,
			HttpServletRequest request) {
		try {
			logger.info("Application Control received at GroupController.updateGroup");
			
			groupDto.setId(id);
			Group resp = this.groupSevice.update(groupDto);
			
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Facility updated Successfully", "facility",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
}
