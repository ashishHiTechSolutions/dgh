/*
 * 
 */
package com.logicladder.dgh.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.TankDto;
import com.logicladder.dgh.entity.app.Tank;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.service.ITankService;
import com.logicladder.dgh.util.Utility;

@RestController
@RequestMapping(ApplicationConstants.API_BASEPATH + "/tank")
public class TankController {

	private static Logger logger = LogManager.getLogger(TankController.class);

	@Autowired
	ITankService tankService;

	@PostMapping(path = "/create")
	public ResponseEntity<?> create(HttpServletRequest request, @RequestBody TankDto dto)
			throws CustomException {
		if (dto.getId() != null) {
			throw new IllegalArgumentException(String.format("Request does not contain the Id [%s]", dto
					.getId()));
		}
		try {
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Tank Created Successfully", "tank", Utility
							.processForSerialization(this.tankService.create(dto))));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "deleteById")
	public ResponseEntity<?> deleteById(@RequestParam long id, HttpServletRequest request)
			throws CustomException {

		try {
			boolean resp = this.tankService.deleteById(id);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Tank Deleted Succesfully", "deleted",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping(path = "/getAll")
	public ResponseEntity<?> getAll(HttpServletRequest request, @RequestParam EntityStatus status)
			throws CustomException {

		try {
			List<Tank> resp = this.tankService.getAllByStatus(status);
			if (resp != null) {
				resp.forEach(tank -> {
					tank = Utility.processForSerialization(tank);
				});
			} else

			{
				resp = new ArrayList<>();
			}
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Tank Fetched Succesfully ", "tanks",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@GetMapping(path = "getById/{id}")
	public ResponseEntity<?> getById(@PathVariable("id") long id, HttpServletRequest request)
			throws CustomException {
		try {
			Tank resp = this.tankService.getById(id);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Tank Fetched Succesfully", "tank", Utility
							.processForSerialization(resp)));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}

	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<?> update(@PathVariable("id") long id, @RequestBody TankDto tankDto,
			HttpServletRequest request) throws CustomException {
		try {
			tankDto.setId(id);
			Tank resp = this.tankService.update(tankDto);
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Tank updated Successfully", "tank", Utility
							.processForSerialization(resp)));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}
}
