/*
 * 
 */
package com.logicladder.dgh.components;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

@Component
public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {

	private final Logger log = LogManager.getLogger(RequestResponseLoggingInterceptor.class);

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body,
			ClientHttpRequestExecution execution) throws IOException {
		this.logRequest(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		this.logResponse(response);
		return response;
	}

	private void logRequest(HttpRequest request, byte[] body) throws IOException {
		// if (log.isDebugEnabled()) {
		this.log.info(
				"===========================request begin================================================");
		this.log.info("URI         : {}", request.getURI());
		this.log.info("Method      : {}", request.getMethod());
		this.log.info("Headers     : {}", request.getHeaders());
		this.log.info("Request body: {}", new String(body, "UTF-8"));
		this.log.info(
				"==========================request end================================================");
		// }
	}

	private void logResponse(ClientHttpResponse response) throws IOException {
		// if (log.isDebugEnabled()) {
		this.log.info("============================response begin==========================================");
		this.log.info("Status code  : {}", response.getStatusCode());
		this.log.info("Status text  : {}", response.getStatusText());
		this.log.info("Headers      : {}", response.getHeaders());
		this.log.info("Response body: {}", StreamUtils.copyToString(response.getBody(), Charset
				.defaultCharset()));
		this.log.info("=======================response end=================================================");
		// }
	}
}