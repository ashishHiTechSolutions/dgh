/*
 * 
 */
package com.logicladder.dgh.components;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.logicladder.dgh.entity.app.AccessRolePageGroup;
import com.logicladder.dgh.entity.app.Address;
import com.logicladder.dgh.entity.app.Operator;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.ApprovalStatus;
import com.logicladder.dgh.logic.ladder.service.impl.LogicLadderUserAPI;
import com.logicladder.dgh.response.LoginResponse;
import com.logicladder.dgh.service.IAccessRolePageGroupService;
import com.logicladder.dgh.service.IBasinService;
import com.logicladder.dgh.service.ICityService;
import com.logicladder.dgh.service.IOperatorService;
import com.logicladder.dgh.service.IRoleService;
import com.logicladder.dgh.service.IStateService;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.util.Utility;

@Component
public class WebApplicationConfigLoader implements InitializingBean {

	@Autowired
	private ICityService iCityService;

	@Autowired
	private IRoleService iRoleService;

	@Autowired
	private IStateService iStateService;

	@Autowired
	private IBasinService iBasinService;

	@Autowired
	private IUserService iUserService;
	
	@Autowired
	private IOperatorService iOperatorService;

	@Autowired
	private IAccessRolePageGroupService iAccessRolePageGroupService;

	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String hbDDLType;

	Logger logger = LogManager.getLogger(WebApplicationConfigLoader.class);

	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info("Web application config loader loaded with hbDDLType as :" + hbDDLType);
		if (hbDDLType.equalsIgnoreCase("create")) {
			try {
				List<AccessRolePageGroup> accessGroupDatas = Utility.getYamlProperties();
				
				createInCoreSystem();
				iAccessRolePageGroupService.saveAll(accessGroupDatas);
				createOperator();
				createUser();
			} catch (Exception e) {
			    logger.warn(e.getMessage()); 
				e.printStackTrace();
			}

		}
	}

	private void createInCoreSystem() {
		if (Boolean.parseBoolean((String) Utility.getActiveSpringProfileBasedConfig().get(
				"db.create.in.core"))) {
		LoginResponse loginResponse = LogicLadderUserAPI.login("abhishek.agarwal+dgh2@logicladder.com",
				"Ll@dgh2018");
		String redirectUrl = loginResponse.getRedirectUrl();
		String authTokenLL = redirectUrl.substring(redirectUrl.lastIndexOf("=") + 1);
		iStateService.createDefaultStates(authTokenLL);
		iCityService.createDefaultCities();
		iBasinService.createDefaultBasin(authTokenLL);
		}
	}


	@SuppressWarnings("unused")
	private void createUser() {
		User user = new User();
		user.setUserName("abhishek.agarwal+dgh2@logicladder.com");
		user.setEmail("abhishek.agarwal+dgh2@logicladder.com");
		user.setUserRole(this.iRoleService.findByRoleName("Super Admin"));
		// user.setOperator(this.iOperatorService.get(1L));
		user.setStatus(ApprovalStatus.ACTIVE);
		user.setName("Abhishek Agarwal");
		user.setCoreUserId(592L);
		user.setPhone("8512081177");
		this.iUserService.saveOrUpdate(user);
	}

	private Properties getLoggableProperties() {
		Properties prop = new Properties();
		InputStream input;
		input = this.getClass().getClassLoader().getResourceAsStream(
				"applicationProperties/authProperties.properties");
		try {
			prop.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	private void createOperator() {
		for (String operatorName : ((String) this.getLoggableProperties().get("app.operator")).split(",")) {
			int i = 1;
			long license = 1l;
			Address address = new Address("temp Address Line", this.iCityService.getAll().get((i * 5) - 1),
					"zipCode");
			Operator operator = new Operator(operatorName, operatorName, operatorName, operatorName,
					operatorName, operatorName, address, license, ApprovalStatus.ACTIVE);
			i++;
			license++;
			this.iOperatorService.saveOrUpdate(operator);
		}
	}

}
