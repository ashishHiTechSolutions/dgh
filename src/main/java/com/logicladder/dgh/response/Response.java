/*
 * 
 */
package com.logicladder.dgh.response;

import java.util.Date;
import java.util.Map;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;

@JsonInclude(Include.NON_NULL)
public class Response<T> extends GenericResponse {

	public static Response<?> of(ErrorCode errorCode, final String message, String status) {
		return new Response<>(errorCode.getErrorCode(), message, status);
	}

	public static Response<?> of(Integer responseCode, String message, String status,
			Map<? extends Object, ? extends Object> responseData) {
		return new Response<>(responseCode, status, message, responseData);
	}

	private T data;

	public Map<? extends Object, ? extends Object> responseData;

	private Date timeStamp;

	public Response() {
		super();
	}

	public Response(Integer errorCode, String message) {
		this.setErrorCode(errorCode);
		this.setMessage(message);
		this.setStatus(ApplicationConstants.ResponseMessage.FAILURE);
		this.timeStamp = new Date();
	}

	public Response(Integer errorCode, String message, String status) {
		this.setErrorCode(errorCode);
		this.setMessage(message);
		this.setStatus(status);
		this.timeStamp = new Date();
	}

	public Response(Integer responseCode, String status, String message,
			Map<? extends Object, ? extends Object> responseData) {
		this.setErrorCode(responseCode);
		this.setMessage(message);
		this.setStatus(status);
		this.responseData = responseData;
		this.timeStamp = new Date();
	}

	public Response(String status, String message, Map<? extends Object, ? extends Object> responseData) {
		this.setStatus(status);
		this.setMessage(message);
		this.setResponseData(responseData);
		this.responseData = responseData;
		this.timeStamp = new Date();
	}

	public Response(T data, String message) {
		this.setErrorCode(HttpStatus.OK.value());
		this.setMessage(message);
		this.setStatus(ApplicationConstants.ResponseMessage.SUCCESS);
		this.data = data;
		this.timeStamp = new Date();
	}

	public T getData() {
		return this.data;
	}

	public Map<? extends Object, ? extends Object> getResponseData() {
		return this.responseData;
	}

	public Date getTimeStamp() {
		return this.timeStamp;
	}

	public void setData(T data) {
		this.data = data;
	}

	public void setResponseData(Map<? extends Object, ? extends Object> responseData) {
		this.responseData = responseData;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
}
