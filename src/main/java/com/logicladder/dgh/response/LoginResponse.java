/*
 * 
 */
package com.logicladder.dgh.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {

	@SuppressWarnings("unused")
	private final static long serialVersionUID = 2021671971182819071L;

	private String redirectUrl;

	public LoginResponse() {
	}

	public LoginResponse(String redirectUrl) {
		super();
		this.redirectUrl = redirectUrl;

	}

	public String getRedirectUrl() {
		return this.redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

}