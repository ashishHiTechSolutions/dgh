/*
 * 
 */
package com.logicladder.dgh.response;

public class ValidationErrorResponse {

	private String field;
	private String message;

	public ValidationErrorResponse(String field, String message) {
		super();
		this.field = field;
		this.message = message;
	}

	public String getField() {
		return this.field;
	}

	public String getMessage() {
		return this.message;
	}

	public void setField(String field) {
		this.field = field;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
