/*
 * 
 */
package com.logicladder.dgh.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class GenericResponse {
	@SuppressWarnings("unused")
	private final static long serialVersionUID = 2021671971182819071L;

	private Integer errorCode;
	private String message;

	private String status;

	public GenericResponse() {

	}

	public GenericResponse(Integer errorCode, String message) {
		super();
		this.errorCode = errorCode;
		this.status = "failure";
		this.message = message;
	}

	public GenericResponse(String message) {
		super();
		this.status = "success";
		this.message = message;
	}

	public Integer getErrorCode() {
		return this.errorCode;
	}

	public String getMessage() {
		return this.message;
	}

	public String getStatus() {
		return this.status;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
