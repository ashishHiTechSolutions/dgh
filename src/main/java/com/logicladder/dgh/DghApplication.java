/*
 * 
 */
package com.logicladder.dgh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication()
@ComponentScan(basePackages = { "com.logicladder.dgh" })
@EnableJpaRepositories(enableDefaultTransactions = false, basePackages = {
		"com.logicladder.dgh.security.repository", "com.logicladder.dgh.repository","com.logicladder.dgh.controller.reports.config" })
@EntityScan(basePackages = { "com.logicladder.dgh.entity.app","com.logicladder.dgh.controller.reports.config" })

@EnableAspectJAutoProxy
@EnableAsync
@EnableAutoConfiguration
@Configuration()
@EnableTransactionManagement
public class DghApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(DghApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(DghApplication.class);
	}

}
