/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

@MappedSuperclass
public class SuperEntity {

	@JsonProperty("createdBy")
	private String createdBy;

	@JsonProperty("createdDate")
	@CreationTimestamp
	private @Temporal(TemporalType.TIMESTAMP) Date createdDate;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonProperty("lastModifiedOn")
	@UpdateTimestamp
	private Date lastModifiedOn;

	@JsonProperty("modifiedBy")
	private String modifiedBy;
	
	@JsonProperty("isDeleted")
	private boolean isDeleted = false;

	public String getCreatedBy() {
		return this.createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public Long getId() {
		return this.id;
	}

	public Date getLastModifiedOn() {
		return this.lastModifiedOn;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
