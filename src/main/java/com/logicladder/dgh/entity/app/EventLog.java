/*
 * 
 */
package com.logicladder.dgh.entity.app;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.logicladder.dgh.enums.EventCompletionStatus;
import com.logicladder.dgh.enums.EventSeverity;
import com.logicladder.dgh.enums.EventType;

import lombok.ToString;

@Entity
@Table(name = "Event_Log")
@ToString
public class EventLog extends SuperEntity {

	private EventCompletionStatus eventCompletionStatus;

	private String eventDescription;

	private EventType eventType;

	// pending discussion
	private EventSeverity severity;

	public EventCompletionStatus getEventCompletionStatus() {
		return this.eventCompletionStatus;
	}

	public String getEventDescription() {
		return this.eventDescription;
	}

	public EventType getEventType() {
		return this.eventType;
	}

	public EventSeverity getSeverity() {
		return this.severity;
	}

	public void setEventCompletionStatus(EventCompletionStatus eventCompletionStatus) {
		this.eventCompletionStatus = eventCompletionStatus;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public void setSeverity(EventSeverity severity) {
		this.severity = severity;
	}

}
