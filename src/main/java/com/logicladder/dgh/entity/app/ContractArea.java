/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.logicladder.dgh.enums.EnumUtils.ContractAreaType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.ProducingStatus;

@Entity
@SQLDelete(sql = "update contract_area set is_deleted = true where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted <> true")
public class ContractArea extends SuperEntity {

	@Column(length = 60)
	private String name;

	@Column
	private Integer areaCovered;

	@Column(length = 100)
	private String code;

	@Enumerated(EnumType.STRING)
	private com.logicladder.dgh.enums.ContractAreaType areaType;

	@Enumerated(EnumType.STRING)
	private ProducingStatus producingStatus;

	@Enumerated(EnumType.STRING)
	private EntityStatus status;

	@Column(name = "basin_id", nullable = false)
	private Long basinId;

	private Long coreId;

	@OneToMany(orphanRemoval = false, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "contractAreaId")
	private Set<Facility> facilities = new HashSet<>();

	@Enumerated(EnumType.STRING)
	private ContractAreaType type;

	@Transient
	private String basinName;

	public Integer getAreaCovered() {
		return this.areaCovered;
	}

	public Long getBasinId() {
		return this.basinId;
	}

	public String getCode() {
		return this.code;
	}

	public Long getCoreId() {
		return this.coreId;
	}

	public Set<Facility> getFacilities() {
		return this.facilities;
	}

	public String getName() {
		return this.name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public ContractAreaType getType() {
		return this.type;
	}

	public void setAreaCovered(Integer areaCovered) {
		this.areaCovered = areaCovered;
	}

	public void setBasinId(Long basinId) {
		this.basinId = basinId;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setFacilities(Set<Facility> facilities) {
		this.facilities = facilities;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setType(ContractAreaType type) {
		this.type = type;
	}

	public ProducingStatus getProducingStatus() {
		return producingStatus;
	}

	public void setProducingStatus(ProducingStatus producingStatus) {
		this.producingStatus = producingStatus;
	}

	public String getBasinName() {
		return basinName;
	}

	public void setBasinName(String basinName) {
		this.basinName = basinName;
	}

	public com.logicladder.dgh.enums.ContractAreaType getAreaType() {
		return areaType;
	}

	public void setAreaType(com.logicladder.dgh.enums.ContractAreaType areaType) {
		this.areaType = areaType;
	}

	public void changeStatus(EntityStatus entityStatus) {
		for (Facility facility : this.facilities) {
			facility.setStatus(entityStatus);
			for (Well well : facility.getWells()) {
				well.setStatus(entityStatus);
				for (Device device : well.getDevices()) {
					device.setStatus(entityStatus);
				}
			}
			for (Tank tank : facility.getTanks()) {
				tank.setStatus(entityStatus);
				for (Device device : tank.getDevices()) {
					device.setStatus(entityStatus);
				}
			}
			for (Device device : facility.getDevices()) {
				device.setStatus(entityStatus);
			}
		}
	}
}
