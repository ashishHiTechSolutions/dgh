package com.logicladder.dgh.entity.app;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Unit extends SuperEntity {

	@Column(unique = true)
	private Long coreId;
	private String name;
	private String type;
	private Integer conversionFactor;
	private Integer conversionConstant;
	private Boolean si;
	private Boolean considerMolWt;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getConversionFactor() {
		return conversionFactor;
	}
	public void setConversionFactor(Integer conversionFactor) {
		this.conversionFactor = conversionFactor;
	}
	public Integer getConversionConstant() {
		return conversionConstant;
	}
	public void setConversionConstant(Integer conversionConstant) {
		this.conversionConstant = conversionConstant;
	}
	public Boolean getSi() {
		return si;
	}
	public void setSi(Boolean si) {
		this.si = si;
	}
	public Boolean getConsiderMolWt() {
		return considerMolWt;
	}
	public void setConsiderMolWt(Boolean considerMolWt) {
		this.considerMolWt = considerMolWt;
	}
	public Long getCoreId() {
		return coreId;
	}
	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}
	
	
}
