package com.logicladder.dgh.entity.app;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * The Class Make. 
 * make Sure for Dozer mapper to work  variables name should match
 * with {@link com.logicladder.dgh.thirdparty.response.dto.Make}
 */
@Entity
public class Make extends SuperEntity {

	private String name;

	@Column(unique=true)
	private Long coreId;

	public Long getCoreId() {
		return coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
