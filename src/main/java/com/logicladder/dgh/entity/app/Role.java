/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Role extends SuperEntity {

	@Column(unique = true)
	private String roleName;

	public Role() {

	}

	public Role(String roleName, String createdBy) {
		this.roleName = roleName;
		this.setCreatedBy(createdBy);
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
