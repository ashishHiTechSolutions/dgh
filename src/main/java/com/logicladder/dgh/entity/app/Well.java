/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logicladder.dgh.enums.WellProductionType;
import com.logicladder.dgh.enums.WellStatus;
import com.logicladder.dgh.enums.WellType;

@Entity
@SQLDelete(sql = "update well set is_deleted = true  where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted <> true ")
public class Well extends SuperEntity {

	@Column(length = 60)
	private String name;

	@Enumerated(EnumType.STRING)
	private WellType wellType;

	@Enumerated(EnumType.STRING)
	private WellProductionType wellProductionType;

	@Enumerated(EnumType.STRING)
	private WellStatus wellStatus;

	@Enumerated(EnumType.STRING)
	private EntityStatus status;

	private Long coreId;

	@Column(name = "facility_id", nullable = true)
	private Long facilityId;

	@ManyToMany(mappedBy = "wells", cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER)
	private Set<Tank> tanks = new HashSet<>();

	@JsonIgnore
	@OneToMany(mappedBy = "well")
	private Set<Device> devices = new HashSet<>();

	@Transient
	private String facilityName;

	public Long getCoreId() {
		return this.coreId;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public String getName() {
		return this.name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public Set<Tank> getTanks() {
		return this.tanks;
	}

	public WellProductionType getWellProductionType() {
		return this.wellProductionType;
	}

	public WellStatus getWellStatus() {
		return this.wellStatus;
	}

	public WellType getWellType() {
		return this.wellType;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setTanks(Set<Tank> tanks) {
		this.tanks = tanks;
	}

	public void setWellProductionType(WellProductionType wellProductionType) {
		this.wellProductionType = wellProductionType;
	}

	public void setWellStatus(WellStatus wellStatus) {
		this.wellStatus = wellStatus;
	}

	public void setWellType(WellType wellType) {
		this.wellType = wellType;
	}

	public Set<Device> getDevices() {
		return devices;
	}

	public void setDevices(Set<Device> devices) {
		this.devices = devices;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public void changeStatus(EntityStatus entityStatus) {
		for (Device device : this.getDevices()) {
			device.setStatus(entityStatus);
		}
	}

}
