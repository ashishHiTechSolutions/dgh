package com.logicladder.dgh.entity.app;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Entity
public class Tariff extends SuperEntity {

	@Column(length = 60)
	String name;
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER, optional = false, targetEntity = Device.class)
	@JoinColumn(name = "device_Id")
	private Device device;

	@Temporal(TemporalType.DATE)
	private Date tariffStartDate;

	@Temporal(TemporalType.DATE)
	private Date tariffEndDate;

	@Enumerated(EnumType.STRING)
	private EntityStatus status;

	private Double rate;

	private Integer waterCutPercentage;
	
	@Transient
	private String deviceName;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Date getTariffStartDate() {
		return tariffStartDate;
	}

	public void setTariffStartDate(Date tariffStartDate) {
		this.tariffStartDate = tariffStartDate;
	}

	public Date getTariffEndDate() {
		return tariffEndDate;
	}

	public void setTariffEndDate(Date tariffEndDate) {
		this.tariffEndDate = tariffEndDate;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Integer getWaterCutPercentage() {
		return waterCutPercentage;
	}

	public void setWaterCutPercentage(Integer waterCutPercentage) {
		this.waterCutPercentage = waterCutPercentage;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	

}
