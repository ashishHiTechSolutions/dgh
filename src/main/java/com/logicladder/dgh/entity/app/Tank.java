/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Entity
@SQLDelete(sql = "update tank set is_deleted = true  where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted <> true ")
public class Tank extends SuperEntity {

	@Column(length = 60)
	private String name;

	private Double height;

	private Double diameter;

	private Double capacity;

	@Enumerated(EnumType.STRING)
	private EntityStatus status;

	private Long coreId;

	@Column(name = "facility_id", nullable = true)
	private Long facilityId;

	@Temporal(TemporalType.DATE)
	private Date lastCalibrationDate;

	private String tagNumber;

	@JsonIgnore
	@OneToMany(mappedBy = "tank")
	private Set<Device> devices = new HashSet<>();

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinTable(joinColumns = { @JoinColumn(name = "tankId") }, inverseJoinColumns = {
			@JoinColumn(name = "wellId") })
	private Set<Well> wells = new HashSet<>();

	@Transient
	private String facilityName;

	public Long getCoreId() {
		return this.coreId;
	}

	public Double getDiameter() {
		return this.diameter;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public Double getHeight() {
		return this.height;
	}

	public Date getLastCalibrationDate() {
		return this.lastCalibrationDate;
	}

	public String getName() {
		return this.name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public Set<Well> getWells() {
		return this.wells;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setDiameter(Double diameter) {
		this.diameter = diameter;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public void setLastCalibrationDate(Date lastCalibrationDate) {
		this.lastCalibrationDate = lastCalibrationDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setWells(Set<Well> wells) {
		this.wells = wells;
	}

	public Double getCapacity() {
		return capacity;
	}

	public void setCapacity(Double capacity) {
		this.capacity = capacity;
	}

	public Set<Device> getDevices() {
		return devices;
	}

	public void setDevices(Set<Device> devices) {
		this.devices = devices;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getTagNumber() {
		return tagNumber;
	}

	public void setTagNumber(String tagNumber) {
		this.tagNumber = tagNumber;
	}

	public void changeStatus(EntityStatus entityStatus) {
		for (Device device : this.getDevices()) {
			device.setStatus(entityStatus);
		}
	}
}
