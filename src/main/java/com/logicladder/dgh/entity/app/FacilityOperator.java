/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@SQLDelete(sql = "update facility_operator set is_deleted = true  where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted <> true ")
public class FacilityOperator extends SuperEntity {

	public FacilityOperator() {
		super();
	}

	public FacilityOperator(Date contractStartDate, Date contractEndDate, Facility facility,
			Operator operator) {
		super();
		this.contractStartDate = contractStartDate;
		this.contractEndDate = contractEndDate;
		this.operator = operator;
		this.facility = facility;
	}

	@Temporal(TemporalType.DATE)
	private Date contractEndDate;

	@Temporal(TemporalType.DATE)
	private Date contractStartDate;

	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER, targetEntity = Facility.class)
	@JoinColumn(nullable = true)
	private Facility facility;

	@OneToOne(cascade = { CascadeType.DETACH,
			CascadeType.REFRESH }, fetch = FetchType.EAGER, targetEntity = Operator.class)
	@JoinColumn(nullable = true)
	private Operator operator;

	public Date getContractEndDate() {
		return this.contractEndDate;
	}

	public Date getContractStartDate() {
		return this.contractStartDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}
}
