package com.logicladder.dgh.entity.app;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
public class AccessRolePageGroup extends SuperEntity {

	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER, targetEntity = Role.class)
	@JoinColumn
	private Role role;

	@ManyToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinTable(joinColumns = {
			@JoinColumn(unique = false) }, inverseJoinColumns = { @JoinColumn(unique = false) })
	private Set<PageGroup> pageGroup = new HashSet<>();

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Set<PageGroup> getPageGroup() {
		return pageGroup;
	}

	public void setPageGroup(Set<PageGroup> pageGroup) {
		this.pageGroup = pageGroup;
	}
}
