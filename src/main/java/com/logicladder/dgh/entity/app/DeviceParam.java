package com.logicladder.dgh.entity.app;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class DeviceParam extends SuperEntity {

	/*
	 * private Object cPCBUnit; 
	 * private Object max; 
	 * private Object min;
	 * private Object persisted; 
	 * private Object resetValue; 
	 * private Object sPCBUnit;
	 */

	@ManyToOne(optional = false)
	@JoinColumn(name = "stdParamId", nullable = false)
	private StdParam stdParam;

	@Column(name = "stdParamId", insertable = false, updatable = false, nullable = false)
	private Long stdParamId;

	private String unit;

	private String uSERUnit;

	public StdParam getStdParam() {
		return stdParam;
	}

	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}

	public Long getStdParamId() {
		return stdParamId;
	}

	public void setStdParamId(Long stdParamId) {
		this.stdParamId = stdParamId;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getuSERUnit() {
		return uSERUnit;
	}

	public void setuSERUnit(String uSERUnit) {
		this.uSERUnit = uSERUnit;
	}

}
