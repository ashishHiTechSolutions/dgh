package com.logicladder.dgh.entity.app;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class StdParam extends SuperEntity {

/*	private Object alertLabel;
	private Object defaultStdUnit;
	private Object molWt;
	private Object paramMetric;
	private List<Object> virtualStdParam = null;
*/
	@Column(unique=true)
	private Long coreId;
	private Boolean isAlertEnabled;
	private Boolean isConversion;
	private Boolean isCounter;
	private String label;
	private String mtype;
	private String name;
	private String paramKey;
	private String stdUnit;
	private String type;
	
	public Boolean getIsAlertEnabled() {
		return isAlertEnabled;
	}
	public void setIsAlertEnabled(Boolean isAlertEnabled) {
		this.isAlertEnabled = isAlertEnabled;
	}
	public Boolean getIsConversion() {
		return isConversion;
	}
	public void setIsConversion(Boolean isConversion) {
		this.isConversion = isConversion;
	}
	public Boolean getIsCounter() {
		return isCounter;
	}
	public void setIsCounter(Boolean isCounter) {
		this.isCounter = isCounter;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getMtype() {
		return mtype;
	}
	public void setMtype(String mtype) {
		this.mtype = mtype;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParamKey() {
		return paramKey;
	}
	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}
	public String getStdUnit() {
		return stdUnit;
	}
	public void setStdUnit(String stdUnit) {
		this.stdUnit = stdUnit;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getCoreId() {
		return coreId;
	}
	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}


}
