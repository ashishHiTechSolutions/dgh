package com.logicladder.dgh.entity.app;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Type extends SuperEntity {

	private String name;
	@Column(unique = true)
	private Long coreId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCoreId() {
		return coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}
}
