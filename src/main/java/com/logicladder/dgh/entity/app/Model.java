package com.logicladder.dgh.entity.app;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

/**
 * Static Model Data From Logic ladder is going to be Stored at our end
 *
 */
@Entity
public class Model extends SuperEntity {

	private String name;

	@Column(unique = true)
	private Long coreId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "make_id")
	private Make make;

	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "model_id")
	private List<Param> params;

	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "type_id")
	private Type type;

	@Enumerated(EnumType.STRING)
	private EntityStatus status = EntityStatus.ACTIVE;
	// private Object registerDef;
	// private List<Object> registers = null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Make getMake() {
		return make;
	}

	public void setMake(Make make) {
		this.make = make;
	}

	public List<Param> getParams() {
		return params;
	}

	public void setParams(List<Param> params) {
		this.params = params;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Long getCoreId() {
		return coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

}
