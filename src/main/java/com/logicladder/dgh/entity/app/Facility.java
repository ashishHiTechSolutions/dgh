/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.ProducingStatus;

@Entity
@SQLDelete(sql = "update facility set is_deleted = true where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted <> true ")
public class Facility extends SuperEntity {

	@Column(length = 60)
	private String name;

	private Long coreId;

	private Integer fieldArea;

	@Column(length = 100)
	private String code;

	@Enumerated(EnumType.STRING)
	private ProducingStatus fieldStatus;

	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST })
	@JoinColumn(name = "address_id")
	private Address address;

	@Enumerated(EnumType.STRING)
	private EntityStatus status;

	@Column(name = "contractAreaId")
	private Long contractAreaId;

	@OneToMany(fetch = FetchType.EAGER, targetEntity = Tank.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "facility_id", nullable = true)
	private Set<Tank> tanks = new HashSet<>();

	private Double targetProduction;

	@OneToMany(fetch = FetchType.EAGER, targetEntity = Well.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "facility_id", nullable = true)
	private Set<Well> wells = new HashSet<>();

	@OneToMany(fetch = FetchType.EAGER, targetEntity = Device.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "facility_id")
	private Set<Device> devices = new HashSet<>();

	private Long facilityOperatorId;

	@Transient
	private String contractAreaName;

	public Address getAddress() {
		return this.address;
	}

	public String getCode() {
		return this.code;
	}

	public Long getContractAreaId() {
		return this.contractAreaId;
	}

	public Long getCoreId() {
		return this.coreId;
	}

	public Integer getFieldArea() {
		return this.fieldArea;
	}

	public String getName() {
		return this.name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public Set<Tank> getTanks() {
		return this.tanks;
	}

	public Double getTargetProduction() {
		return this.targetProduction;
	}

	public Set<Well> getWells() {
		return this.wells;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setContractAreaId(Long contractAreaId) {
		this.contractAreaId = contractAreaId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setFieldArea(Integer fieldArea) {
		this.fieldArea = fieldArea;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setTanks(Set<Tank> tanks) {
		this.tanks = tanks;
	}

	public void setTargetProduction(Double targetProduction) {
		this.targetProduction = targetProduction;
	}

	public void setWells(Set<Well> wells) {
		this.wells = wells;
	}

	public Long getFacilityOperatorId() {
		return facilityOperatorId;
	}

	public void setFacilityOperatorId(Long facilityOperatorId) {
		this.facilityOperatorId = facilityOperatorId;
	}

	public ProducingStatus getFieldStatus() {
		return fieldStatus;
	}

	public void setFieldStatus(ProducingStatus fieldStatus) {
		this.fieldStatus = fieldStatus;
	}

	public Set<Device> getDevices() {
		return devices;
	}

	public void setDevices(Set<Device> devices) {
		this.devices = devices;
	}

	public String getContractAreaName() {
		return contractAreaName;
	}

	public void setContractAreaName(String contractAreaName) {
		this.contractAreaName = contractAreaName;
	}

	public void changeStatus(EntityStatus entityStatus) {
		for (Well well : this.getWells()) {
			well.setStatus(entityStatus);
			for (Device device : well.getDevices()) {
				device.setStatus(entityStatus);
			}
		}
		for (Tank tank : this.getTanks()) {
			tank.setStatus(entityStatus);
			for (Device device : tank.getDevices()) {
				device.setStatus(entityStatus);
			}
		}
		for (Device device : this.getDevices()) {
			device.setStatus(entityStatus);
		}
	}

}
