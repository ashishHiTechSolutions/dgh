package com.logicladder.dgh.entity.app;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity
public class Param extends SuperEntity {
	
	/*private Object cPCBUnit;
	private Object max;
	private Object min;
	private Object persisted;
	private Object resetValue;
	private Object sPCBUnit;*/
	
	@Column(unique=true)
	private Long coreId;
	
	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name ="stdParamId",nullable=false)
	private StdParam stdParam;
	
	@Column(name ="stdParamId",insertable = false, updatable = false)
	private Long stdParamId;

	private String unit;
	
	private String uSERUnit;
	
	public Long getCoreId() {
		return coreId;
	}
	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}
	public StdParam getStdParam() {
		return stdParam;
	}
	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getuSERUnit() {
		return uSERUnit;
	}
	public void setuSERUnit(String uSERUnit) {
		this.uSERUnit = uSERUnit;
	}


	
	
}
