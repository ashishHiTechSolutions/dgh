/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.logicladder.dgh.enums.EnumUtils.BasinType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Entity
@SQLDelete(sql = "update basin set is_deleted=true where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted<>true")
public class Basin extends SuperEntity {

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "state_Id", unique = false, nullable = false)
	private State state;

	@Column(length = 100)
	private String code;

	@OneToMany(orphanRemoval = false, fetch = FetchType.EAGER, cascade = { CascadeType.DETACH,
			CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name = "basin_id")
	private Set<ContractArea> contractAreas;

	private Long coreId;

	private String description;

	// devices and shifts are also additional attributes is coming as Array in Resp
	// from logic ladder side

	@Column(length = 100, nullable = false, unique = true)
	private String name;

	// TODO Values of Enum should get saved
	@Enumerated(EnumType.STRING)
	private EntityStatus status;

	// TODO Need to confirmed type of basin from logic ladder side
	@Enumerated(EnumType.STRING)
	private BasinType type;

	private Double area;

	public String getCode() {
		return this.code;
	}

	public Set<ContractArea> getContractAreas() {
		return this.contractAreas;
	}

	public Long getCoreId() {
		return this.coreId;
	}

	public String getDescription() {
		return this.description;
	}

	public String getName() {
		return this.name;
	}

	public EntityStatus getStatus() {
		return this.status;
	}

	public BasinType getType() {
		return this.type;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setContractAreas(Set<ContractArea> contractAreas) {
		this.contractAreas = contractAreas;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public void setType(BasinType type) {
		this.type = type;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

}
