/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import org.springframework.transaction.annotation.Transactional;

@Entity
public class Page extends SuperEntity {

	private String pageName;

	@ManyToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinTable(joinColumns = { @JoinColumn(unique = false) }, inverseJoinColumns = {
			@JoinColumn(unique = false) })
	private Set<Function> functions = new HashSet<>();

	@Transient
	private String groupName;

	public Page() {
		super();
	}

	public Page(String pageName, String createdBy) {
		this.pageName = pageName;
		this.setCreatedBy(createdBy);
	}

	public String getPageName() {
		return this.pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public Set<Function> getFunctions() {
		return functions;
	}

	public void setFunctions(Set<Function> functions) {
		this.functions = functions;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
