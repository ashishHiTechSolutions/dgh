package com.logicladder.dgh.entity.app;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.logicladder.dgh.enums.ApprovalStatus;

@Entity
public class Operator extends SuperEntity {

	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH,
			CascadeType.PERSIST }, fetch = FetchType.EAGER)
	@JoinColumn(name = "address_Id")
	private Address address;

	@Column(name = "address_Id", updatable = false, insertable = false)
	private Long addressId;

	@Column(length = 100)
	private String companyName;

	private Long coreId;

	@Column(length = 100)
	private String designation;

	private String email;

	@Column(length = 60)
	private String name;

	@Column(length = 50)
	private String operatorLicense;

	private String phone;

	@Enumerated(EnumType.STRING)
	private ApprovalStatus status;

	@JsonIgnore
	@Column(length = 10)
	private String emailOtp;

	@JsonIgnore
	@Column(length = 10)
	private String mobileOtp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonProperty("otpCreatedOn")
	private Date otpCreatedOn;

	public Operator() {

	}

	public Operator(String name, String designation, String companyName, String operatorLicense, String email,
			String mobileNumber, Address address, Long coreSystemOperatorId, ApprovalStatus status) {
		this.setName(name);
		this.designation = designation;
		this.companyName = companyName;
		this.operatorLicense = operatorLicense;
		this.email = email;
		this.setPhone(mobileNumber);
		this.address = address;
		this.coreId = coreSystemOperatorId;
		this.status = status;
	}

	public Address getAddress() {
		return this.address;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getDesignation() {
		return this.designation;
	}

	public String getEmail() {
		return this.email;
	}

	public String getName() {
		return this.name;
	}

	public String getOperatorLicense() {
		return this.operatorLicense;
	}

	public String getPhone() {
		return this.phone;
	}

	public ApprovalStatus getStatus() {
		return this.status;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOperatorLicense(String operatorLicense) {
		this.operatorLicense = operatorLicense;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setStatus(ApprovalStatus status) {
		this.status = status;
	}

	public Long getCoreId() {
		return coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public String getEmailOtp() {
		return emailOtp;
	}

	public void setEmailOtp(String emailOtp) {
		this.emailOtp = emailOtp;
	}

	public String getMobileOtp() {
		return mobileOtp;
	}

	public void setMobileOtp(String mobileOtp) {
		this.mobileOtp = mobileOtp;
	}

	public Date getOtpCreatedOn() {
		return otpCreatedOn;
	}

	public void setOtpCreatedOn(Date otpCreatedOn) {
		this.otpCreatedOn = otpCreatedOn;
	}

}
