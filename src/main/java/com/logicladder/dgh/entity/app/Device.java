package com.logicladder.dgh.entity.app;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logicladder.dgh.enums.DeviceType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Entity
@SQLDelete(sql = "update device set is_deleted = true  where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted <> true ")
public class Device extends SuperEntity {

	@Column(length = 60)
	private String name;

	private Long coreId;

	@Enumerated(EnumType.STRING)
	private DeviceType deviceType;

	@Column(name = "model_id", insertable = false, updatable = false)
	private Long modelId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "model_id", nullable = false)
	private Model model;

	private String serialNumber;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "facility_id", nullable = false)
	private Facility facility;

	@Column(name = "facility_id", insertable = false, updatable = false)
	private Long facilityId;

	@ManyToOne()
	@JoinColumn(name = "well_id")
	private Well well;

	@ManyToOne()
	@JoinColumn(name = "tank_id")
	private Tank tank;

	// Will save the User custom Unit Data Saved While Saving and Will link to
	// standard stdParam
	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinColumn(name = "device_id")
	private List<DeviceParam> deviceParam;

	@Enumerated(EnumType.STRING)
	private EntityStatus status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public Long getCoreId() {
		return coreId;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public Long getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public Well getWell() {
		return well;
	}

	public void setWell(Well well) {
		this.well = well;
	}

	public Long getModelId() {
		return modelId;
	}

	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}

	public List<DeviceParam> getDeviceParam() {
		return deviceParam;
	}

	public void setDeviceParam(List<DeviceParam> deviceParam) {
		this.deviceParam = deviceParam;
	}

	public Tank getTank() {
		return tank;
	}

	public void setTank(Tank tank) {
		this.tank = tank;
	}
}
