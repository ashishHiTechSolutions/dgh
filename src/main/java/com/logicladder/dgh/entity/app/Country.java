/*
 * 
 */
package com.logicladder.dgh.entity.app;

import javax.persistence.Entity;

@Entity
public class Country extends SuperEntity {

	private String name;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}