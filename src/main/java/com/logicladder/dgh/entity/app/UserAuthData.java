/*
 * 
 */
package com.logicladder.dgh.entity.app;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name = "user_token")
public class UserAuthData extends SuperEntity {

	private String deviceId;

	private String ipAddress;

	@Column(nullable = false, length = 1000)
	private String jwtToken;

	private String llAuthToken;

	private String otp;

	private boolean otpVerified;

	private boolean passwordSet = false;

	@Column(nullable = false, length = 1000)
	private String tempJwtToken;

	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.REFRESH }, fetch = FetchType.EAGER, targetEntity = User.class, optional = false)
	@JoinColumn(name = "user_id")
	private User user;

	public UserAuthData() {
		super();
	}

	public UserAuthData(Long id, String jwtToken, String tempJwtToken, String llAuthToken,
			Boolean otpVerified, String deviceId, String ipAddress, String otp, User user) {
		this.setId(id);
		this.jwtToken = jwtToken;
		this.tempJwtToken = tempJwtToken;
		this.otp = otp;
		this.llAuthToken = llAuthToken;
		this.otpVerified = otpVerified;
		this.deviceId = deviceId;
		this.ipAddress = ipAddress;
		this.setUser(user);
	}

	public UserAuthData(String jwtToken, String tempJwtToken, String llAuthToken, boolean otpVerified,
			String deviceId, String ipAddress, String otp, User user) {
		this.jwtToken = jwtToken;
		this.tempJwtToken = tempJwtToken;
		this.otp = otp;
		this.llAuthToken = llAuthToken;
		this.otpVerified = otpVerified;
		this.deviceId = deviceId;
		this.ipAddress = ipAddress;
		this.setUser(user);
	}

	public String getDeviceId() {
		return this.deviceId;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public String getJwtToken() {
		return this.jwtToken;
	}

	public String getLlAuthToken() {
		return this.llAuthToken;
	}

	public String getOtp() {
		return this.otp;
	}

	public String getTempJwtToken() {
		return this.tempJwtToken;
	}

	public User getUser() {
		return this.user;
	}

	public boolean isOtpVerified() {
		return this.otpVerified;
	}

	public boolean isPasswordSet() {
		return this.passwordSet;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	public void setLlAuthToken(String llAuthToken) {
		this.llAuthToken = llAuthToken;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public void setOtpVerified(boolean otpVerified) {
		this.otpVerified = otpVerified;
	}

	public void setPasswordSet(boolean passwordSet) {
		this.passwordSet = passwordSet;
	}

	public void setTempJwtToken(String tempJwtToken) {
		this.tempJwtToken = tempJwtToken;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
