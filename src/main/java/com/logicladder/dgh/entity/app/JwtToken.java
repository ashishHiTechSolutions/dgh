/*
 * 
 */
package com.logicladder.dgh.entity.app;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "Auth_Token")
public class JwtToken extends SuperEntity {

	@Column(nullable = false, unique = true, updatable = false)
	private String authToken;

	public String getAuthToken() {
		return this.authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

}
