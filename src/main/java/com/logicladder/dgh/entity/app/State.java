/*
 * 
 */
package com.logicladder.dgh.entity.app;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class State extends SuperEntity {

	private Integer areaCovered;

	private String code;

	private Long coreId;

	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER, orphanRemoval = false)
	@JoinColumn(name = "country_id")
	private Country country;

	private Double latitude;

	private Double longitude;

	@Column(length = 60)
	private String name;

	private Double zoomLevel;

	public Integer getAreaCovered() {
		return this.areaCovered;
	}

	public String getCode() {
		return this.code;
	}

	public Long getCoreId() {
		return this.coreId;
	}

	public Country getCountry() {
		return this.country;
	}

	public Double getLatitude() {
		return this.latitude;
	}

	public Double getLongitude() {
		return this.longitude;
	}

	public String getName() {
		return this.name;
	}

	public Double getZoomLevel() {
		return this.zoomLevel;
	}

	public void setAreaCovered(Integer areaCovered) {
		this.areaCovered = areaCovered;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCoreId(Long coreId) {
		this.coreId = coreId;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setZoomLevel(Double zoomLevel) {
		this.zoomLevel = zoomLevel;
	}

}
