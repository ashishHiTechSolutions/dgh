/*
 * 
 */
package com.logicladder.dgh.entity.app;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address extends SuperEntity {

	private String addressLine;

	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "city_Id")
	private City city;

	@Column(name = "city_Id", updatable = false, insertable = false)
	private Long cityId;

	private String zip;

	public Address() {

	}

	public Address(String addressLine, City city, String zip) {
		this.addressLine = addressLine;
		this.city = city;
		this.zip = zip;
	}

	public String getAddressLine() {
		return this.addressLine;
	}

	public City getCity() {
		return this.city;
	}

	public String getZip() {
		return this.zip;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

}