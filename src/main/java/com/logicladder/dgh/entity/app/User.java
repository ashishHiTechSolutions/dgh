/*
 * 
 */
package com.logicladder.dgh.entity.app;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logicladder.dgh.enums.ApprovalStatus;

@Entity
@SQLDelete(sql = "update user set is_deleted=true where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_deleted<>true")
public class User extends SuperEntity {

	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST })
	@JoinColumn(name = "address_id")
	private Address address;

	private Long coreUserId;

	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	@Column(unique = true)
	private String email;

	private long failedAttemptCount;

	private String name;

	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.DETACH,
			CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "operator_Id")
	private Operator operator;

	private Date passwordLastChangeDate;

	private String phone;

	@Enumerated(EnumType.STRING)
	private ApprovalStatus status;

	@Column(unique = true)
	private String userName;

	@OneToOne(cascade = { CascadeType.MERGE, CascadeType.DETACH,
			CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "role_Id")
	private Role userRole;

	@Column(length = 1000)
	private String comment;

	@ManyToMany(cascade = { CascadeType.DETACH, CascadeType.MERGE,
			CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinTable(joinColumns = { @JoinColumn(name = "user_id", unique = false) }, inverseJoinColumns = {
			@JoinColumn(name = "facility_id", unique = false) }, name = "user_facility")
	private Set<Facility> facilities = new HashSet<>();
	
	@JsonIgnore
	private String mobileOtp;
	
	@JsonIgnore
	private String emailOtp;

	public Address getAddress() {
		return this.address;
	}

	public Long getCoreUserId() {
		return this.coreUserId;
	}

	public Date getDeletedDate() {
		return this.deletedDate;
	}

	public String getEmail() {
		return this.email;
	}

	public long getFailedAttemptCount() {
		return this.failedAttemptCount;
	}

	public String getName() {
		return this.name;
	}

	public Operator getOperator() {
		return this.operator;
	}

	public Date getPasswordLastChangeDate() {
		return this.passwordLastChangeDate;
	}

	public String getPhone() {
		return this.phone;
	}

	public ApprovalStatus getStatus() {
		return this.status;
	}

	public String getUserName() {
		return this.userName;
	}

	public Role getUserRole() {
		return this.userRole;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setCoreUserId(Long coreUserId) {
		this.coreUserId = coreUserId;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFailedAttemptCount(long failedAttemptCount) {
		this.failedAttemptCount = failedAttemptCount;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public void setPasswordLastChangeDate(Date passwordLastChangeDate) {
		this.passwordLastChangeDate = passwordLastChangeDate;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setStatus(ApprovalStatus status) {
		this.status = status;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Set<Facility> getFacilities() {
		return facilities;
	}

	public void setFacilities(Set<Facility> facilities) {
		this.facilities = facilities;
	}

	public String getMobileOtp() {
		return mobileOtp;
	}

	public void setMobileOtp(String mobileOtp) {
		this.mobileOtp = mobileOtp;
	}

	public String getEmailOtp() {
		return emailOtp;
	}

	public void setEmailOtp(String emailOtp) {
		this.emailOtp = emailOtp;
	}
}
