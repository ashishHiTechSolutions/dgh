package com.logicladder.dgh.entity.app;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Where;

import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.GroupEntityType;

@Entity
@Where(clause = "is_deleted <> true ")
@Table (name="Group_")
public class Group extends SuperEntity {

	@Column(length = 60)
	private String name;

	@Column(columnDefinition = "TEXT")
	private String userList;

	@Column(columnDefinition = "TEXT")
	private String entityList;

	@Enumerated(EnumType.STRING)
	private EntityStatus status;

	@Enumerated(EnumType.STRING)
	private GroupEntityType type;

	@Transient
	private List<?> entities;

	@Transient
	private List<User> users;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserList() {
		return userList;
	}

	public void setUserList(String userList) {
		this.userList = userList;
	}

	public String getEntityList() {
		return entityList;
	}

	public void setEntityList(String entityList) {
		this.entityList = entityList;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public List<?> getEntities() {
		return entities;
	}

	public void setEntities(List<?> entities) {
		this.entities = entities;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public GroupEntityType getType() {
		return type;
	}

	public void setType(GroupEntityType type) {
		this.type = type;
	}
}
