/*
 * 
 */
package com.logicladder.dgh.entity.app;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class City extends SuperEntity {

	private String name;

	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, fetch = FetchType.EAGER, optional = false, targetEntity = State.class)
	@JoinColumn(name = "state_Id")
	private State state;

	public String getName() {
		return this.name;
	}

	public State getState() {
		return this.state;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setState(State state) {
		this.state = state;
	}

}
