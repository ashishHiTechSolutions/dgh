/*
 * 
 */
package com.logicladder.dgh.entity.app;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.ToString;

@Entity
@ToString
public class Function extends SuperEntity {

	private String functionCode;

	private String functionName;

	public Function() {
		super();
	}

	public Function(String functionName, String functionCode, String createdBy) {
		super();
		this.functionName = functionName;
		this.functionCode = functionCode;
		this.setCreatedBy(createdBy);
	}

	public String getFunctionCode() {
		return this.functionCode;
	}

	public String getFunctionName() {
		return this.functionName;
	}

	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

}
