/*
 * 
 */
package com.logicladder.dgh.app.email;

import com.logicladder.dgh.enums.EmailType;
import com.logicladder.dgh.util.Utility;

import lombok.ToString;

@ToString
public abstract class Email {

	public static Email createGenericEmail(EmailType emailType) {
		Email email = null;
		switch (emailType) {
		case REGISTRATION:
			email = new RegistrationEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.registration_email_subject"));
			break;
		case LOGIN:
			email = new LoginEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.login_email_subject"));
			break;
		case ALERTS:
//			email = new AlertEmail();
			break;
		case REMINDER:
//			email = new ReminderEmail();
			break;
		case PASSWORD_IS_SET:
			email = new PasswordSetEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.pass_is_set_subject"));
			break;
		case SET_PASSWORD_ALERT:
			email = new RegistrationEmail();

			break;
		case NEW_REGISTRATION_ALERT:
			email = new RegistrationAlertEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.new_registration_alert_subject"));
			break;
		case FORGOT_PASSWORD:
			email = new ForgotPasswordEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.forgot_password_subject"));
			break;
		case OPERATOR_APPROVED:
			email = new OperatorApprovedEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.operator_approved_subject"));
			break;
		case OPERATOR_CREATED:
			email = new OperatorCreatedEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.operator_created_subject"));
			break;
		case OPERATOR_VERIFICATION:
			email = new OperatorVerificationEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.operator_verification_subject"));
			break;	
		case USER_CREATED:
			email = new UserCreatedEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.user_created"));
			break;
		case OPERATOR_CREATED_NOTIFICATION_EMAIL:
			email = new OperatorCreatedNotificationEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.operator_created_notification_email"));
			break;
		case USER_CREATION_REJECTED_EMAIL:
			email = new UserCreationRejectedEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.user_creation_rejected_email"));
			break;
		case USER_VERIFICATION_EMAIL:
			email = new UserVerificationEmail();
			email.setSubject((String) Utility.getApplicationDefaultProperties().get(
					"constant.email.config.user_creation_verified_email"));
		}

		email.setAdditionalContent(Boolean.parseBoolean((String) Utility.getApplicationDefaultProperties()
				.get("config.mail.dummy")) ? (String) Utility.getApplicationDefaultProperties().get(
						"constant.email.config.dummy_data") : "");
		email.setEmailType(emailType);
		email.setFrom((String) Utility.getApplicationDefaultProperties().get("config.mail.username"));
		return email;

	}

	private String additionalContent;

	private String emailLocation;

	private EmailType emailType;

	private String from;

	private String subject;

	private String[] username;

	public String getAdditionalContent() {
		return this.additionalContent;
	}

	public String getEmailLocation() {
		return this.emailLocation;
	}

	public EmailType getEmailType() {
		return this.emailType;
	}

	public String getFrom() {
		return this.from;
	}

	public String getSubject() {
		return this.subject;
	}

	public String[] getUsername() {
		return this.username;
	}

	public void setAdditionalContent(String additionalContent) {
		this.additionalContent = additionalContent;
	}

	protected void setEmailLocation(String emailLocation) {
		this.emailLocation = emailLocation;
	}

	public void setEmailType(EmailType emailType) {
		this.emailType = emailType;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setUsername(String[] username) {
		this.username = username;
	}
}
