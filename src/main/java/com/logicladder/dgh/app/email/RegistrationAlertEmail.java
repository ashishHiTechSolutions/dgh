/*
 * 
 */
package com.logicladder.dgh.app.email;

public class RegistrationAlertEmail extends Email {

	private String user;

	public RegistrationAlertEmail() {
		super();
		this.setEmailLocation("registrationAlertEmail.ftl");
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}
