/*
 * 
 */
package com.logicladder.dgh.app.email;

public class OperatorApprovedEmail extends Email {

	private String companyName;

	private String operatorName;

	public OperatorApprovedEmail() {
		super();
		this.setEmailLocation("operatorApprovedEmail.ftl");
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getOperatorName() {
		return this.operatorName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

}
