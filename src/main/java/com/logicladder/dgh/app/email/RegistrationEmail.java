/*
 * 
 */
package com.logicladder.dgh.app.email;

import lombok.ToString;

@ToString
public class RegistrationEmail extends Email {

	private String passwordLink;

	public RegistrationEmail() {
		super();
		this.setEmailLocation("registrationEmail.ftl");
	}

	public String getPasswordLink() {
		return this.passwordLink;
	}

	public void setPasswordLink(String passwordLink) {
		this.passwordLink = passwordLink;
	}

}
