package com.logicladder.dgh.app.email;

import com.logicladder.dgh.app.email.Email;

public class UserCreationRejectedEmail extends Email {

	public UserCreationRejectedEmail() {
		super();
		this.setEmailLocation("userCreationRejectedEmail.ftl");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String name;
}
