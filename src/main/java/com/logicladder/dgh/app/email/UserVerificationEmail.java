package com.logicladder.dgh.app.email;

public class UserVerificationEmail extends Email {

	public UserVerificationEmail() {
		super();
		this.setEmailLocation("userCreationVerified.ftl");
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String name;
	
}
