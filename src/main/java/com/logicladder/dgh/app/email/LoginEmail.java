/*
 * 
 */
package com.logicladder.dgh.app.email;

import lombok.ToString;

@ToString
public class LoginEmail extends Email {

	private String otp;

	public LoginEmail() {
		super();
		this.setEmailLocation("loginEmail.ftl");
	}

	public String getOtp() {
		return this.otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
}
