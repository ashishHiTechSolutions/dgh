/*
 * 
 */
package com.logicladder.dgh.app.email;

public class OperatorCreatedEmail extends Email {

	private String companyName;

	private String operatorName;

	public OperatorCreatedEmail() {
		super();
		this.setEmailLocation("operatorCreatedEmail.ftl");
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getOperatorName() {
		return this.operatorName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
}
