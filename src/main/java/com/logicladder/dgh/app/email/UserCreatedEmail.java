package com.logicladder.dgh.app.email;

public class UserCreatedEmail extends Email {

	public UserCreatedEmail() {
		super();
		this.setEmailLocation("userCreatedEmail.ftl");
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getEmailOtp() {
		return emailOtp;
	}

	public void setEmailOtp(String emailOtp) {
		this.emailOtp = emailOtp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String link;
	
	private String emailOtp;
	
	private String name;
}
