/*
 * 
 */
package com.logicladder.dgh.app.email;

public class ForgotPasswordEmail extends Email {

	private String passwordLink;
	
	private String name;

	public ForgotPasswordEmail() {
		super();
		this.setEmailLocation("forgotPasswordEmail.ftl");
	}

	public String getPasswordLink() {
		return this.passwordLink;
	}

	public void setPasswordLink(String passwordLink) {
		this.passwordLink = passwordLink;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
