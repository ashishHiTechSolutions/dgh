package com.logicladder.dgh.app.email;

public class OperatorCreatedNotificationEmail extends Email {

	public OperatorCreatedNotificationEmail() {
		super();
		this.setEmailLocation("operatorCreatedNotificationEmail.ftl");
	}
	
	private String operatorEmail;
	
	private String verifyOtpLink;

	public String getOperatorEmail() {
		return operatorEmail;
	}

	public void setOperatorEmail(String operatorEmail) {
		this.operatorEmail = operatorEmail;
	}

	public String getVerifyOtpLink() {
		return verifyOtpLink;
	}

	public void setVerifyOtpLink(String verifyOtpLink) {
		this.verifyOtpLink = verifyOtpLink;
	}

}
