package com.logicladder.dgh.app.email;

public class OperatorVerificationEmail extends Email {

	private String companyName;

	private String operatorName;

	private String emailOtp;

	private String mobileOtp;

	public OperatorVerificationEmail() {
		super();
		this.setEmailLocation("operatorVerificationEmail.ftl");
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getEmailOtp() {
		return emailOtp;
	}

	public void setEmailOtp(String emailOtp) {
		this.emailOtp = emailOtp;
	}

	public String getMobileOtp() {
		return mobileOtp;
	}

	public void setMobileOtp(String mobileOtp) {
		this.mobileOtp = mobileOtp;
	}
}
