package com.logicladder.dgh.app.exception;

import com.logicladder.dgh.response.Response;

public class CustomInValidRequestException extends RuntimeException {

	private Response resp;

	public CustomInValidRequestException(Response resp) {
		super();
		this.resp=resp;

	}

	public CustomInValidRequestException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public CustomInValidRequestException(String message, Throwable cause, Response resp) {
		super(message, cause);
		this.resp=resp;

	}

	public CustomInValidRequestException(String message, Response resp) {
		super(message);
		this.resp=resp;

	}

	public CustomInValidRequestException(Throwable cause) {
		super(cause);

	}

	public Response getResp() {
		return resp;
	}

	public void setResp(Response resp) {
		this.resp = resp;
	}

}
