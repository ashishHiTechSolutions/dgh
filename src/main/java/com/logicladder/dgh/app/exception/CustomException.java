/*
 * 
 */
package com.logicladder.dgh.app.exception;

public class CustomException extends Throwable {

	private static final long serialVersionUID = 6798854463776212842L;

	private Integer errorCode;

	private Throwable exception;

	public CustomException() {
		super();
	}

	public CustomException(Integer errorCode, Throwable e) {
		this.errorCode = errorCode;
		this.exception = e;
	}

	public Integer getErrorCode() {
		return this.errorCode;
	}

	public Throwable getException() {
		return this.exception;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "Error Code :" + this.errorCode + " Error Recieved is : " + this.exception.getMessage();
	}
}
