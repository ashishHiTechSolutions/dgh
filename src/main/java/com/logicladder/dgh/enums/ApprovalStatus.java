/*
 * 
 */
package com.logicladder.dgh.enums;

public enum ApprovalStatus {

	ACTIVE("Active User"), AWAITED("Awaiting Password"), SUSPENDED("User Account Suspended"), PENDING(
			"Pending Approval"), PENDING_OTP("Pending OTP Verification"), REJECTED("User Rejected");

	private ApprovalStatus(String description) {
		this.description = description;
	}

	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
