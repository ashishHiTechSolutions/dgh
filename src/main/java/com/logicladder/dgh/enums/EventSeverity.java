/*
 * 
 */
package com.logicladder.dgh.enums;

public enum EventSeverity {

	HIGH, LOW, MODERATE, VERY_HIGH, VERY_LOW;
}
