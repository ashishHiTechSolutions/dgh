/*
 * 
 */
package com.logicladder.dgh.enums;

import lombok.Getter;
import lombok.Setter;

public enum EventType {

	CREATE("Create Operation"), DELETE("Delete Operation"), READ("Read Operation"), UPDATE(
			"Update Operation");

	@Getter
	@Setter
	private String description;

	private EventType(String description) {
		this.description = description;
	}

}
