/*
 * 
 */
package com.logicladder.dgh.enums;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonValue;

public class EnumUtils {

	public enum BasinType {
		BASIN;
	}

	public enum ProducingStatus {

		NON_PRODUCING("Non Producing"), PRODUCING("Producing");

		private String description;

		private ProducingStatus(String description) {
			this.description = description;
		}

		public String getDescription() {
			return this.description;
		}
	}

	public enum ContractAreaType {
		CONTRACTAREA;
	}

	public enum EntityStatus {

		ACTIVE("Active"), INACTIVE("Inactive");

		private String description;

		private EntityStatus(String description) {
			this.setDescription(description);
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}
	
	public enum GroupEntityType {

		FACILITY("Facility"), TANK("Tank"), WELL("Well"), DEVICE("Device");

		private String description;

		private GroupEntityType(String description) {
			this.setDescription(description);
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
	}

	public enum ErrorCode {

		GLOBAL(2), AUTHENTICATION(10), JWT_TOKEN_EXPIRED(11), VALIDATION(12), OTP_VERIFICATION(13),
		LOGIC_LADDER_AUTHENTICATION(14), ACCOUNT_DOES_NOT_EXIST(15), UNABLE_TO_FIND_AUTHENTICATION_DATA(16),
		PASSWORD_EXPIRED(17), TOO_MANY_FAILED_ATTEMPTS(18), EMAIL_NOT_UNIQUE(19), LOGIC_LADDER_ERROR(20),
		ACCOUNT_INACTIVE(21), CONTRACT_AREA_NOT_PRESENT(22), ROLE_REQUIRED(23), PASSWORD_HAS_BEEN_SET(24),
		DATABASE_ERROR(25), ENTITY_DOES_NOT_EXITS(405), SERVER_ERROR(26), Entities_NOT_ASSIGNED_TO_USER(27);

		private static final Map<Integer, ErrorCode> MY_MAP = new HashMap<>();

		static {
			for (ErrorCode myEnum : ErrorCode.values()) {
				ErrorCode.MY_MAP.put(myEnum.getErrorCode(), myEnum);
			}
		}

		public static ErrorCode getEnumByErrorCode(int value) {
			return ErrorCode.MY_MAP.get(value);
		}

		private int errorCode;

		private ErrorCode(int errorCode) {
			this.errorCode = errorCode;
		}

		@JsonValue
		public int getErrorCode() {
			return this.errorCode;
		}
	}

	public enum AuditAction {
		UPDATE, DELETE;
	}

}
