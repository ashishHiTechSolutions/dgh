package com.logicladder.dgh.enums;

public enum DeviceType {

	FRTU("Remote Terminal Unit"), CTM("Custody Transfer Meter");

	private String description;

	private DeviceType(String description) {
		this.setDescription(description);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
