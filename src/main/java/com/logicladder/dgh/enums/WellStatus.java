/*
 * 
 */
package com.logicladder.dgh.enums;

public enum WellStatus {
	ABANDONED("Abandoned After Completion"), ARTIFICIAL_LIFT("Artificial Lift"), SELF_LIFT("Self Lift"),
	SHUT_IN_ISOLATION("Shut in Isolation"), SICK("Stopped Producing"), SUSPENDED("Suspended"), UNDER_TESTING(
			"Under Testing"), UNDER_WO("Under WO");

	private String description;

	private WellStatus(String description) {
		this.setDescription(description);
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
