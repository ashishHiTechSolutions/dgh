/*
 * 
 */
package com.logicladder.dgh.enums;

public enum WellType {
	GAS_WELL("Gas Well"), OIL_AND_GAS_WELL("Oil and Gas Well"), OIL_WELL("Oil Well");

	private String description;

	private WellType(String description) {
		this.setDescription(description);
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
