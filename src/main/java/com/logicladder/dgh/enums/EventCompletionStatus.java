/*
 * 
 */
package com.logicladder.dgh.enums;

import lombok.Getter;
import lombok.Setter;

public enum EventCompletionStatus {
	ERROR("Event Failed due to Logical/Data error"), FAILURE("Event Failed due to system error"), SUCCESS(
			"EVENT Completed Successfully");

	@Setter
	@Getter
	private String description;

	private EventCompletionStatus(String description) {
		this.description = description;
	}
}
