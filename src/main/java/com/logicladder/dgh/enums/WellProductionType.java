/*
 * 
 */
package com.logicladder.dgh.enums;

public enum WellProductionType {
	FLOWING_WELLS("Flowing Wells"), NON_FLOWING_WELLS("Non-Flowing Wells");

	private String description;

	private WellProductionType(String description) {
		this.setDescription(description);
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
