package com.logicladder.dgh.enums;

public enum ContractAreaType {

	ONSHORE("Onshore"), OFFSHORE("Offshore");

	private String description;

	private ContractAreaType(String description) {
		this.setDescription(description);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
