/*
 * 
 */
package com.logicladder.dgh.enums;

public enum OperationCode {

	CREATE, DELETE, READ, UPDATE;

}
