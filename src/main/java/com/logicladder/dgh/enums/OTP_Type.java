package com.logicladder.dgh.enums;

public enum OTP_Type {

	LOGIN("Login OTP"), REGISTRATION("Registration OTP");
	
	private String description;
	
	private OTP_Type(String description) {
		this.setDescription(description);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
