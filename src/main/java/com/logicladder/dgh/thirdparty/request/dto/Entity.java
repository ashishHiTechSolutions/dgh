package com.logicladder.dgh.thirdparty.request.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id" })
public class Entity implements Serializable {

	@JsonProperty("id")
	private Long id;
	private final static long serialVersionUID = 2867035978675632291L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Entity() {
	}

	/**
	 * 
	 * @param id
	 */
	public Entity(Long id) {
		super();
		this.id = id;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

}
