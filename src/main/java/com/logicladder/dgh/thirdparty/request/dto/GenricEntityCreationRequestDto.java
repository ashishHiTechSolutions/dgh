package com.logicladder.dgh.thirdparty.request.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenricEntityCreationRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer parentId;
	private String type;
	private String name;

	public GenricEntityCreationRequestDto() {
		super();
	}

	public GenricEntityCreationRequestDto(Integer parentId, String type, String name) {
		this.parentId = parentId;
		this.type = type;
		this.name = name;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
