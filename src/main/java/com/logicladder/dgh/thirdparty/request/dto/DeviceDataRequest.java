package com.logicladder.dgh.thirdparty.request.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceDataRequest {
	
	@Getter
	@Setter
	@JsonProperty("metric")
	private String metric;
	
	@Getter
	@Setter
	@JsonProperty("dims")
	private List<String> dims;
	
	@Getter
	@Setter
	@JsonProperty("startDate")
	private String startDate;
	
	@Getter
	@Setter
	@JsonProperty("endDate")
	private String endDate;
	
	@Getter
	@Setter
	@JsonProperty("format")
	private String format = "numeric:#.#";

	public DeviceDataRequest(){
		//no-op
	}
	
	public DeviceDataRequest(String metric, List<String> dims, String startDate, String endDate) {
		super();
		this.metric = metric;
		this.dims = dims;
		this.startDate = startDate;
		this.endDate = endDate;
	}

}
