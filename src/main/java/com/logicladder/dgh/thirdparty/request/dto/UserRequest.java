/*
 * 
 */
package com.logicladder.dgh.thirdparty.request.dto;

import java.io.Serializable;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.logicladder.dgh.entity.app.User;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "userName", "name", "phone", "address", "type", "role", "entity", "operatorId" })
@JsonIgnoreProperties({ "status", "city", "state", "country", "zip", "home", "homePage", "tenantId",
		"homeUrl", "domain", "createdDate", "deletedDate", "email", "accessibleEntities", "type" })
public class UserRequest implements Serializable {

	private final static long serialVersionUID = 2470632927908256975L;

	@JsonProperty("address")
	private String address;
	@JsonProperty("entity")
	private String entity;
	@Getter
	@Setter
	@JsonProperty("id")
	private Long id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("operatorId")
	private Long operatorId;
	@JsonProperty("phone")
	private String phone;
	@JsonProperty("role")
	private String role;
	
	@JsonProperty("tenantId")
	private Long tenantId;

	@JsonProperty("userEntity")
	private Integer[] userEntity;

	@JsonProperty("userName")
	private String userName;

	@JsonProperty("address")
	public String getAddress() {
		return this.address;
	}

	@JsonProperty("entity")
	public String getEntity() {
		return this.entity;
	}

	@JsonProperty("name")
	public String getName() {
		return this.name;
	}

	@JsonProperty("operatorId")
	public Long getOperatorId() {
		return this.operatorId;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return this.phone;
	}

	@JsonProperty("role")
	public String getRole() {
		return this.role;
	}

	@JsonProperty("userEntity")
	public Integer[] getUserEntity() {
		return this.userEntity;
	}

	@JsonProperty("userName")
	public String getUserName() {
		return this.userName;
	}

	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("entity")
	public void setEntity(String entity) {
		this.entity = entity;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("operatorId")
	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	@JsonProperty("phone")
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty("role")
	public void setRole(String role) {
		this.role = role;
	}

	@JsonProperty("userEntity")
	public void setUserEntity(Integer[] userEntity) {
		this.userEntity = userEntity;
	}

	@JsonProperty("userName")
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void userMapper(User user) {
		if (user.getCoreUserId() != null) {
			this.id = user.getCoreUserId();
			this.tenantId = 2334L;
		}
		this.userName = user.getUserName();
		this.name = user.getName();

		String entity = user.getFacilities().stream().map(facility -> facility.getCoreId()).collect(Collectors
				.toList()).toString();
		entity = entity.replace("[", "").replace("]", "").replaceAll(" ", "");
		this.entity = entity;
		this.role = "user";
		this.phone = user.getPhone();
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
}
