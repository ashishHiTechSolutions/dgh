/*
 * 
 */
package com.logicladder.dgh.thirdparty.request.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "op", "att", "value" })

public class UserPatchRequest {

	private final static long serialVersionUID = -2153307745391035067L;
	@JsonProperty("att")
	private String att;
	@JsonProperty("op")
	private String op;
	@JsonProperty("value")
	private String value;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public UserPatchRequest() {
	}

	/**
	 *
	 * @param op
	 * @param value
	 * @param att
	 */
	public UserPatchRequest(String op, String att, String value) {
		super();
		this.op = op;
		this.att = att;
		this.value = value;
	}

	@JsonProperty("att")
	public String getAtt() {
		return this.att;
	}

	@JsonProperty("op")
	public String getOp() {
		return this.op;
	}

	@JsonProperty("value")
	public String getValue() {
		return this.value;
	}

	@JsonProperty("att")
	public void setAtt(String att) {
		this.att = att;
	}

	@JsonProperty("op")
	public void setOp(String op) {
		this.op = op;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

}
