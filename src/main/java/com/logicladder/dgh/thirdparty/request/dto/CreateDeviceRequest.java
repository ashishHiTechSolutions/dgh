package com.logicladder.dgh.thirdparty.request.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.logicladder.dgh.thirdparty.response.dto.DeviceSource;
import com.logicladder.dgh.thirdparty.response.dto.Model;
import com.logicladder.dgh.thirdparty.response.dto.Param;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "params", "name", "deviceSource", "model", "location" })
public class CreateDeviceRequest implements Serializable {

	@JsonProperty("params")
	private List<Param> params = new ArrayList<Param>();
	@JsonProperty("name")
	private String name;
	@JsonProperty("deviceSource")
	private DeviceSource deviceSource;
	@JsonProperty("model")
	private Model model;
	@JsonProperty("location")
	private Location location;
	private final static long serialVersionUID = 2513026462235784224L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public CreateDeviceRequest() {
	}

	/**
	 * 
	 * @param model
	 * @param location
	 * @param name
	 * @param deviceSource
	 * @param params
	 */
	public CreateDeviceRequest(List<Param> params, String name, DeviceSource deviceSource, Model model,
			Location location) {
		super();
		this.params = params;
		this.name = name;
		this.deviceSource = deviceSource;
		this.model = model;
		this.location = location;
	}

	@JsonProperty("params")
	public List<Param> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<Param> params) {
		this.params = params;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("deviceSource")
	public DeviceSource getDeviceSource() {
		return deviceSource;
	}

	@JsonProperty("deviceSource")
	public void setDeviceSource(DeviceSource deviceSource) {
		this.deviceSource = deviceSource;
	}

	@JsonProperty("model")
	public Model getModel() {
		return model;
	}

	@JsonProperty("model")
	public void setModel(Model model) {
		this.model = model;
	}

	@JsonProperty("location")
	public Location getLocation() {
		return location;
	}

	@JsonProperty("location")
	public void setLocation(Location location) {
		this.location = location;
	}

}