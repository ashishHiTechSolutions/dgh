/*
 * 
 */
package com.logicladder.dgh.thirdparty.request.dto;

public class LogicLadderEnumUtil {

	public enum EntityTypeLogicLadder {

		BASIN("Basin"), CONTRACT_AREA("Contract Area"), CTM("CTM"), FACILITY("facility"), FIELD("Field"),
		OPERATOR("Operator"), STATE("State"), TANK("Tank"), TENANT("tenant"), WELL("Well"), WELL_DETAILS(
				"Well Detail"), ZONE("Zone");

		private String description;

		private EntityTypeLogicLadder(String description) {
			this.description = description;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return this.description;
		}
	}
}
