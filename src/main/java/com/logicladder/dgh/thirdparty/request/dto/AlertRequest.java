package com.logicladder.dgh.thirdparty.request.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "userId", "entityId", "metric","name", "val",
	"priority", "condition", "unit", "from", "to" , "min", "max", "email", "type", "phone"})
public class AlertRequest {

		@Getter
		@Setter
		@JsonProperty("id")
		private long id;
		
		@Getter
		@Setter
		@JsonProperty("userId")
		private long userId;
		
		@Getter
		@Setter
		@JsonProperty("entityId")
		private long entityId;
		
		@Getter
		@Setter
		@JsonProperty("metric")
		private String metric;
		
		@Getter
		@Setter
		@JsonProperty("name")
		private String name;

		@Getter
		@Setter
		@JsonProperty("val")
		private String val;
		
		@Getter
		@Setter
		@JsonProperty("priority")
		private String priority;
		
		@Getter
		@Setter
		@JsonProperty("unit")
		private String unit;
		
		@Getter
		@Setter
		@JsonProperty("condition")
		private String condition;
		
		@Getter
		@Setter
		@JsonProperty("from")
		private String from;

		@Getter
		@Setter
		@JsonProperty("to")
		private String to;

		@Getter
		@Setter
		@JsonProperty("description")
		private String description;

		@Getter
		@Setter
		@JsonProperty("status")
		private String status;
		
		@Getter
		@Setter
		@JsonProperty("email")
		private String email;
		
		@Getter
		@Setter
		@JsonProperty("phone")
		private String phone;

		@Getter
		@Setter
		@JsonProperty("currentValue")
		private String currentValue;

		@Getter
		@Setter
		@JsonProperty("ruleId")
		private String ruleId;

		@Getter
		@Setter
		@JsonProperty("isCommunicated")
		private String isCommunicated;

		@Getter
		@Setter
		@JsonProperty("min")
		private String min;

		@Getter
		@Setter
		@JsonProperty("max")
		private String max;

		@Getter
		@Setter
		@JsonProperty("isSystem")
		private String isSystem;

		@Getter
		@Setter
		@JsonProperty("type")
		private String type;

}

