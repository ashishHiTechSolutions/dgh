package com.logicladder.dgh.thirdparty.request.dto;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "userList", "entities", "lhs", "rhs", "isRHSMetric", "onEveryOccurence", "onFirstOccurence",
		"sendSMS", "sendEmail", "name", "priority", "condition", "day", "hour", "min", "phone", "email", "val",
		"applicableBetween", "status", "type" })
public class LLCreateAlertRuleRequest implements Serializable {

	@JsonProperty("userList")
	private List<UserList> userList = null;
	@JsonProperty("entities")
	private List<Entity> entities = null;
	@JsonProperty("lhs")
	private String lhs;
	@JsonProperty("rhs")
	private String rhs;
	@JsonProperty("isRHSMetric")
	private Boolean isRHSMetric;
	@JsonProperty("onEveryOccurence")
	private Boolean onEveryOccurence;
	@JsonProperty("onFirstOccurence")
	private Boolean onFirstOccurence;
	@JsonProperty("sendSMS")
	private Boolean sendSMS;
	@JsonProperty("sendEmail")
	private Boolean sendEmail;
	@JsonProperty("name")
	private String name;
	@JsonProperty("priority")
	private String priority;
	@JsonProperty("condition")
	private String condition;
	@JsonProperty("day")
	private String day;
	@JsonProperty("hour")
	private String hour;
	@JsonProperty("min")
	private String min;
	@JsonProperty("phone")
	private String phone;
	@JsonProperty("email")
	private String email;
	@JsonProperty("val")
	private String val;
	@JsonProperty("applicableBetween")
	private String applicableBetween;
	@JsonProperty("status")
	private String status;
	@JsonProperty("type")
	private String type;
	private final static long serialVersionUID = -2442265756729357761L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public LLCreateAlertRuleRequest() {
	}

	/**
	 * 
	 * @param val
	 * @param min
	 * @param phone
	 * @param onFirstOccurence
	 * @param applicableBetween
	 * @param isRHSMetric
	 * @param status
	 * @param condition
	 * @param onEveryOccurence
	 * @param sendSMS
	 * @param userList
	 * @param hour
	 * @param type
	 * @param entities
	 * @param lhs
	 * @param email
	 * @param priority
	 * @param name
	 * @param rhs
	 * @param sendEmail
	 * @param day
	 */
	public LLCreateAlertRuleRequest(List<UserList> userList, List<Entity> entities, String lhs, String rhs,
			Boolean isRHSMetric, Boolean onEveryOccurence, Boolean onFirstOccurence, Boolean sendSMS, Boolean sendEmail,
			String name, String priority, String condition, String day, String hour, String min, String phone,
			String email, String val, String applicableBetween, String status, String type) {
		super();
		this.userList = userList;
		this.entities = entities;
		this.lhs = lhs;
		this.rhs = rhs;
		this.isRHSMetric = isRHSMetric;
		this.onEveryOccurence = onEveryOccurence;
		this.onFirstOccurence = onFirstOccurence;
		this.sendSMS = sendSMS;
		this.sendEmail = sendEmail;
		this.name = name;
		this.priority = priority;
		this.condition = condition;
		this.day = day;
		this.hour = hour;
		this.min = min;
		this.phone = phone;
		this.email = email;
		this.val = val;
		this.applicableBetween = applicableBetween;
		this.status = status;
		this.type = type;
	}

	@JsonProperty("userList")
	public List<UserList> getUserList() {
		return userList;
	}

	@JsonProperty("userList")
	public void setUserList(List<UserList> userList) {
		this.userList = userList;
	}

	@JsonProperty("entities")
	public List<Entity> getEntities() {
		return entities;
	}

	@JsonProperty("entities")
	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}

	@JsonProperty("lhs")
	public String getLhs() {
		return lhs;
	}

	@JsonProperty("lhs")
	public void setLhs(String lhs) {
		this.lhs = lhs;
	}

	@JsonProperty("rhs")
	public String getRhs() {
		return rhs;
	}

	@JsonProperty("rhs")
	public void setRhs(String rhs) {
		this.rhs = rhs;
	}

	@JsonProperty("isRHSMetric")
	public Boolean getIsRHSMetric() {
		return isRHSMetric;
	}

	@JsonProperty("isRHSMetric")
	public void setIsRHSMetric(Boolean isRHSMetric) {
		this.isRHSMetric = isRHSMetric;
	}

	@JsonProperty("onEveryOccurence")
	public Boolean getOnEveryOccurence() {
		return onEveryOccurence;
	}

	@JsonProperty("onEveryOccurence")
	public void setOnEveryOccurence(Boolean onEveryOccurence) {
		this.onEveryOccurence = onEveryOccurence;
	}

	@JsonProperty("onFirstOccurence")
	public Boolean getOnFirstOccurence() {
		return onFirstOccurence;
	}

	@JsonProperty("onFirstOccurence")
	public void setOnFirstOccurence(Boolean onFirstOccurence) {
		this.onFirstOccurence = onFirstOccurence;
	}

	@JsonProperty("sendSMS")
	public Boolean getSendSMS() {
		return sendSMS;
	}

	@JsonProperty("sendSMS")
	public void setSendSMS(Boolean sendSMS) {
		this.sendSMS = sendSMS;
	}

	@JsonProperty("sendEmail")
	public Boolean getSendEmail() {
		return sendEmail;
	}

	@JsonProperty("sendEmail")
	public void setSendEmail(Boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("priority")
	public String getPriority() {
		return priority;
	}

	@JsonProperty("priority")
	public void setPriority(String priority) {
		this.priority = priority;
	}

	@JsonProperty("condition")
	public String getCondition() {
		return condition;
	}

	@JsonProperty("condition")
	public void setCondition(String condition) {
		this.condition = condition;
	}

	@JsonProperty("day")
	public String getDay() {
		return day;
	}

	@JsonProperty("day")
	public void setDay(String day) {
		this.day = day;
	}

	@JsonProperty("hour")
	public String getHour() {
		return hour;
	}

	@JsonProperty("hour")
	public void setHour(String hour) {
		this.hour = hour;
	}

	@JsonProperty("min")
	public String getMin() {
		return min;
	}

	@JsonProperty("min")
	public void setMin(String min) {
		this.min = min;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}

	@JsonProperty("phone")
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("val")
	public String getVal() {
		return val;
	}

	@JsonProperty("val")
	public void setVal(String val) {
		this.val = val;
	}

	@JsonProperty("applicableBetween")
	public String getApplicableBetween() {
		return applicableBetween;
	}

	@JsonProperty("applicableBetween")
	public void setApplicableBetween(String applicableBetween) {
		this.applicableBetween = applicableBetween;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

}