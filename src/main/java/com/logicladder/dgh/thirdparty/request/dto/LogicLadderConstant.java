/*
 * 
 */
package com.logicladder.dgh.thirdparty.request.dto;

public class LogicLadderConstant {

	public static final String LL_AUTH_TOKEN = "constant.logic.ladder.constants.ll_auth_token_header";

	// All Apis Urls Should be define with suffix API
	public static final String LL_BASE_URL = "constant.logic.ladder.constants.tp_base_url";

	// All Apis Urls Should be define with suffix API
	public static final String LL_BASE_URL_API = "constant.logic.ladder.constants.tp_base_url";

	public static final String LL_USERS_URL_API = "constant.logic.ladder.constants.url_users";

	public static final String LL_BASIN_CREATE_URL_API = "logic.ladder.api.url.basin.create";
	public static final String LL_BASIN_UPDATE_URL_API = "logic.ladder.api.url.basin.update";
	public static final String LL_BASIN_DELETE_URL_API = "logic.ladder.api.url.basin.delet";

	public static final String LL_GENRIC_ENITY_CREATE_URL_API = "logic.ladder.api.url.genric.entity.create";

	public final static String LL_MAKE_ENITY_GET_URL = "logic.ladder.api.url.make.entity.get";
	public final static String LL_MODEL_ENITY_GET_URL = "logic.ladder.api.url.model.entity.get";

	public final static String LL_UNIT_ENITY_GET_URL = "logic.ladder.api.url.unit.entity.get";

	public final static String LL_DEVICE_ENITY_GET_OR_LOCATE_URL = "logic.ladder.api.url.device.entity.getOrLocateDevice";
	public final static String LL_DEVICE_ENITY_CREATE_URL = "logic.ladder.api.url.device.entity.create";
	public final static String LL_DEVICE_ENITY_DELETE_URL = "logic.ladder.api.url.device.entity.delete";
	public final static String LL_FULL_ENTITITES = "logic.ladder.api.url.full.entities";
	public final static String LL_ENTITY_PROPS = "logic.ladder.api.url.full.entity.props";

}
