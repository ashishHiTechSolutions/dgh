package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "tenantId", "name", "type", "description", "timezone", "leftValue", "rightValue",
		"rootValue", "shifts", "properties" })
public class ChangeRequestEntity {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("tenantId")
	private Integer tenantId;
	@JsonProperty("name")
	private String name;
	@JsonProperty("type")
	private String type;
	@JsonProperty("description")
	private Object description;
	@JsonProperty("timezone")
	private Object timezone;
	@JsonProperty("leftValue")
	private Integer leftValue;
	@JsonProperty("rightValue")
	private Integer rightValue;
	@JsonProperty("rootValue")
	private Integer rootValue;
	@JsonProperty("shifts")
	private Object shifts;
	@JsonProperty("properties")
	private List<Object> properties = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("tenantId")
	public Integer getTenantId() {
		return tenantId;
	}

	@JsonProperty("tenantId")
	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("description")
	public Object getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(Object description) {
		this.description = description;
	}

	@JsonProperty("timezone")
	public Object getTimezone() {
		return timezone;
	}

	@JsonProperty("timezone")
	public void setTimezone(Object timezone) {
		this.timezone = timezone;
	}

	@JsonProperty("leftValue")
	public Integer getLeftValue() {
		return leftValue;
	}

	@JsonProperty("leftValue")
	public void setLeftValue(Integer leftValue) {
		this.leftValue = leftValue;
	}

	@JsonProperty("rightValue")
	public Integer getRightValue() {
		return rightValue;
	}

	@JsonProperty("rightValue")
	public void setRightValue(Integer rightValue) {
		this.rightValue = rightValue;
	}

	@JsonProperty("rootValue")
	public Integer getRootValue() {
		return rootValue;
	}

	@JsonProperty("rootValue")
	public void setRootValue(Integer rootValue) {
		this.rootValue = rootValue;
	}

	@JsonProperty("shifts")
	public Object getShifts() {
		return shifts;
	}

	@JsonProperty("shifts")
	public void setShifts(Object shifts) {
		this.shifts = shifts;
	}

	@JsonProperty("properties")
	public List<Object> getProperties() {
		return properties;
	}

	@JsonProperty("properties")
	public void setProperties(List<Object> properties) {
		this.properties = properties;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}