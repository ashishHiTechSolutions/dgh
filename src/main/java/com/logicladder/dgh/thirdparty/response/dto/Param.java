/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "stdParam", "unit", "min", "max", "createdDate", "resetValue", "persisted",
		"USERUnit", "CPCBUnit", "SPCBUnit" })
public class Param implements Serializable {

	private final static long serialVersionUID = -606644690963055726L;
	@JsonProperty("CPCBUnit")
	private Object cPCBUnit;
	@JsonProperty("createdDate")
	private Object createdDate;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("max")
	private Object max;
	@JsonProperty("min")
	private Object min;
	@JsonProperty("persisted")
	private Object persisted;
	@JsonProperty("resetValue")
	private Object resetValue;
	@JsonProperty("SPCBUnit")
	private Object sPCBUnit;
	@JsonProperty("stdParam")
	private StdParam stdParam;
	@JsonProperty("unit")
	private String unit;
	@JsonProperty("USERUnit")
	private String uSERUnit;

	public Param() {
	}

	@JsonProperty("CPCBUnit")
	public Object getCPCBUnit() {
		return this.cPCBUnit;
	}

	@JsonProperty("createdDate")
	public Object getCreatedDate() {
		return this.createdDate;
	}

	@JsonProperty("id")
	public Integer getId() {
		return this.id;
	}

	@JsonProperty("max")
	public Object getMax() {
		return this.max;
	}

	@JsonProperty("min")
	public Object getMin() {
		return this.min;
	}

	@JsonProperty("persisted")
	public Object getPersisted() {
		return this.persisted;
	}

	@JsonProperty("resetValue")
	public Object getResetValue() {
		return this.resetValue;
	}

	@JsonProperty("SPCBUnit")
	public Object getSPCBUnit() {
		return this.sPCBUnit;
	}

	@JsonProperty("stdParam")
	public StdParam getStdParam() {
		return this.stdParam;
	}

	@JsonProperty("unit")
	public String getUnit() {
		return this.unit;
	}

	@JsonProperty("USERUnit")
	public String getUSERUnit() {
		return this.uSERUnit;
	}

	@JsonProperty("CPCBUnit")
	public void setCPCBUnit(Object cPCBUnit) {
		this.cPCBUnit = cPCBUnit;
	}

	@JsonProperty("createdDate")
	public void setCreatedDate(Object createdDate) {
		this.createdDate = createdDate;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("max")
	public void setMax(Object max) {
		this.max = max;
	}

	@JsonProperty("min")
	public void setMin(Object min) {
		this.min = min;
	}

	@JsonProperty("persisted")
	public void setPersisted(Object persisted) {
		this.persisted = persisted;
	}

	@JsonProperty("resetValue")
	public void setResetValue(Object resetValue) {
		this.resetValue = resetValue;
	}

	@JsonProperty("SPCBUnit")
	public void setSPCBUnit(Object sPCBUnit) {
		this.sPCBUnit = sPCBUnit;
	}

	@JsonProperty("stdParam")
	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}

	@JsonProperty("unit")
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@JsonProperty("USERUnit")
	public void setUSERUnit(String uSERUnit) {
		this.uSERUnit = uSERUnit;
	}

}