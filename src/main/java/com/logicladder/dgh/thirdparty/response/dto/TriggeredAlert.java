package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "alertType", "startTime", "endTime", "description", "entity", "status", "currentValue",
		"ruleId", "isCommunicated", "min", "max", "isSystem", "type" })
public class TriggeredAlert implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("alertType")
	private String alertType;
	@JsonProperty("startTime")
	private String startTime;
	@JsonProperty("endTime")
	private Object endTime;
	@JsonProperty("description")
	private String description;
	@JsonProperty("entity")
	private TPEntity entity;
	@JsonProperty("status")
	private String status;
	@JsonProperty("currentValue")
	private Double currentValue;
	@JsonProperty("ruleId")
	private Integer ruleId;
	@JsonProperty("isCommunicated")
	private Integer isCommunicated;
	@JsonProperty("min")
	private Integer min;
	@JsonProperty("max")
	private Object max;
	@JsonProperty("isSystem")
	private Integer isSystem;
	@JsonProperty("type")
	private Object type;
	private final static long serialVersionUID = -7285401115225504267L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public TriggeredAlert() {
	}

	/**
	 * 
	 * @param min
	 * @param max
	 * @param status
	 * @param entity
	 * @param alertType
	 * @param type
	 * @param endTime
	 * @param currentValue
	 * @param isCommunicated
	 * @param isSystem
	 * @param id
	 * @param startTime
	 * @param description
	 * @param ruleId
	 */
	public TriggeredAlert(Integer id, String alertType, String startTime, Object endTime, String description,
			TPEntity entity, String status, Double currentValue, Integer ruleId, Integer isCommunicated, Integer min,
			Object max, Integer isSystem, Object type) {
		super();
		this.id = id;
		this.alertType = alertType;
		this.startTime = startTime;
		this.endTime = endTime;
		this.description = description;
		this.entity = entity;
		this.status = status;
		this.currentValue = currentValue;
		this.ruleId = ruleId;
		this.isCommunicated = isCommunicated;
		this.min = min;
		this.max = max;
		this.isSystem = isSystem;
		this.type = type;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("alertType")
	public String getAlertType() {
		return alertType;
	}

	@JsonProperty("alertType")
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	@JsonProperty("startTime")
	public String getStartTime() {
		return startTime;
	}

	@JsonProperty("startTime")
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@JsonProperty("endTime")
	public Object getEndTime() {
		return endTime;
	}

	@JsonProperty("endTime")
	public void setEndTime(Object endTime) {
		this.endTime = endTime;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("entity")
	public TPEntity getEntity() {
		return entity;
	}

	@JsonProperty("entity")
	public void setEntity(TPEntity entity) {
		this.entity = entity;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("currentValue")
	public Double getCurrentValue() {
		return currentValue;
	}

	@JsonProperty("currentValue")
	public void setCurrentValue(Double currentValue) {
		this.currentValue = currentValue;
	}

	@JsonProperty("ruleId")
	public Integer getRuleId() {
		return ruleId;
	}

	@JsonProperty("ruleId")
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	@JsonProperty("isCommunicated")
	public Integer getIsCommunicated() {
		return isCommunicated;
	}

	@JsonProperty("isCommunicated")
	public void setIsCommunicated(Integer isCommunicated) {
		this.isCommunicated = isCommunicated;
	}

	@JsonProperty("min")
	public Integer getMin() {
		return min;
	}

	@JsonProperty("min")
	public void setMin(Integer min) {
		this.min = min;
	}

	@JsonProperty("max")
	public Object getMax() {
		return max;
	}

	@JsonProperty("max")
	public void setMax(Object max) {
		this.max = max;
	}

	@JsonProperty("isSystem")
	public Integer getIsSystem() {
		return isSystem;
	}

	@JsonProperty("isSystem")
	public void setIsSystem(Integer isSystem) {
		this.isSystem = isSystem;
	}

	@JsonProperty("type")
	public Object getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(Object type) {
		this.type = type;
	}

}
