package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"metricParamId",
"virtualParamId",
"metricLabel",
"metricName",
"metricUnit",
"metricDownsampleAgg",
"metricAgg",
"dataPrecision",
"pickFirstOccurence",
"iscounter",
"counterCode",
"format",
"dataEntity",
"dataKeyAttr",
"functionalParam",
"dependsOnMetric",
"paramMetric",
"sqlMetric",
"isSqlMetric",
"units"
})
public class AlertMetric {

@JsonProperty("id")
private Integer id;
@JsonProperty("metricParamId")
private MetricParam metricParamId;
@JsonProperty("virtualParamId")
private Object virtualParamId;
@JsonProperty("metricLabel")
private String metricLabel;
@JsonProperty("metricName")
private String metricName;
@JsonProperty("metricUnit")
private String metricUnit;
@JsonProperty("metricDownsampleAgg")
private String metricDownsampleAgg;
@JsonProperty("metricAgg")
private String metricAgg;
@JsonProperty("dataPrecision")
private Integer dataPrecision;
@JsonProperty("pickFirstOccurence")
private Boolean pickFirstOccurence;
@JsonProperty("iscounter")
private Object iscounter;
@JsonProperty("counterCode")
private Object counterCode;
@JsonProperty("format")
private Object format;
@JsonProperty("dataEntity")
private Object dataEntity;
@JsonProperty("dataKeyAttr")
private Object dataKeyAttr;
@JsonProperty("functionalParam")
private Object functionalParam;
@JsonProperty("dependsOnMetric")
private List<Object> dependsOnMetric = null;
@JsonProperty("paramMetric")
private Object paramMetric;
@JsonProperty("sqlMetric")
private Boolean sqlMetric;
@JsonProperty("isSqlMetric")
private Boolean isSqlMetric;
@JsonProperty("units")
private List<String> units = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("metricParamId")
public MetricParam getMetricParamId() {
return metricParamId;
}

@JsonProperty("metricParamId")
public void setMetricParamId(MetricParam metricParamId) {
this.metricParamId = metricParamId;
}

@JsonProperty("virtualParamId")
public Object getVirtualParamId() {
return virtualParamId;
}

@JsonProperty("virtualParamId")
public void setVirtualParamId(Object virtualParamId) {
this.virtualParamId = virtualParamId;
}

@JsonProperty("metricLabel")
public String getMetricLabel() {
return metricLabel;
}

@JsonProperty("metricLabel")
public void setMetricLabel(String metricLabel) {
this.metricLabel = metricLabel;
}

@JsonProperty("metricName")
public String getMetricName() {
return metricName;
}

@JsonProperty("metricName")
public void setMetricName(String metricName) {
this.metricName = metricName;
}

@JsonProperty("metricUnit")
public String getMetricUnit() {
return metricUnit;
}

@JsonProperty("metricUnit")
public void setMetricUnit(String metricUnit) {
this.metricUnit = metricUnit;
}

@JsonProperty("metricDownsampleAgg")
public String getMetricDownsampleAgg() {
return metricDownsampleAgg;
}

@JsonProperty("metricDownsampleAgg")
public void setMetricDownsampleAgg(String metricDownsampleAgg) {
this.metricDownsampleAgg = metricDownsampleAgg;
}

@JsonProperty("metricAgg")
public String getMetricAgg() {
return metricAgg;
}

@JsonProperty("metricAgg")
public void setMetricAgg(String metricAgg) {
this.metricAgg = metricAgg;
}

@JsonProperty("dataPrecision")
public Integer getDataPrecision() {
return dataPrecision;
}

@JsonProperty("dataPrecision")
public void setDataPrecision(Integer dataPrecision) {
this.dataPrecision = dataPrecision;
}

@JsonProperty("pickFirstOccurence")
public Boolean getPickFirstOccurence() {
return pickFirstOccurence;
}

@JsonProperty("pickFirstOccurence")
public void setPickFirstOccurence(Boolean pickFirstOccurence) {
this.pickFirstOccurence = pickFirstOccurence;
}

@JsonProperty("iscounter")
public Object getIscounter() {
return iscounter;
}

@JsonProperty("iscounter")
public void setIscounter(Object iscounter) {
this.iscounter = iscounter;
}

@JsonProperty("counterCode")
public Object getCounterCode() {
return counterCode;
}

@JsonProperty("counterCode")
public void setCounterCode(Object counterCode) {
this.counterCode = counterCode;
}

@JsonProperty("format")
public Object getFormat() {
return format;
}

@JsonProperty("format")
public void setFormat(Object format) {
this.format = format;
}

@JsonProperty("dataEntity")
public Object getDataEntity() {
return dataEntity;
}

@JsonProperty("dataEntity")
public void setDataEntity(Object dataEntity) {
this.dataEntity = dataEntity;
}

@JsonProperty("dataKeyAttr")
public Object getDataKeyAttr() {
return dataKeyAttr;
}

@JsonProperty("dataKeyAttr")
public void setDataKeyAttr(Object dataKeyAttr) {
this.dataKeyAttr = dataKeyAttr;
}

@JsonProperty("functionalParam")
public Object getFunctionalParam() {
return functionalParam;
}

@JsonProperty("functionalParam")
public void setFunctionalParam(Object functionalParam) {
this.functionalParam = functionalParam;
}

@JsonProperty("dependsOnMetric")
public List<Object> getDependsOnMetric() {
return dependsOnMetric;
}

@JsonProperty("dependsOnMetric")
public void setDependsOnMetric(List<Object> dependsOnMetric) {
this.dependsOnMetric = dependsOnMetric;
}

@JsonProperty("paramMetric")
public Object getParamMetric() {
return paramMetric;
}

@JsonProperty("paramMetric")
public void setParamMetric(Object paramMetric) {
this.paramMetric = paramMetric;
}

@JsonProperty("sqlMetric")
public Boolean getSqlMetric() {
return sqlMetric;
}

@JsonProperty("sqlMetric")
public void setSqlMetric(Boolean sqlMetric) {
this.sqlMetric = sqlMetric;
}

@JsonProperty("isSqlMetric")
public Boolean getIsSqlMetric() {
return isSqlMetric;
}

@JsonProperty("isSqlMetric")
public void setIsSqlMetric(Boolean isSqlMetric) {
this.isSqlMetric = isSqlMetric;
}

@JsonProperty("units")
public List<String> getUnits() {
return units;
}

@JsonProperty("units")
public void setUnits(List<String> units) {
this.units = units;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}