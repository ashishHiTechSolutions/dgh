package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

public class ResponseData {

	private Integer errorCode;

	private String status;
	private String message;
	
	public ResponseData() {
		
	}
	
	public ResponseData(String message) {
		super();
		this.status = "success";
		this.message = message;
	}
	
		
	public ResponseData(Integer errorCode, String message) {
		super();
		this.errorCode = errorCode;
		this.status = "failure";
		this.message = message;
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	@Getter
	@Setter
	private Map<? extends Object, ? extends Object> responseData = new HashMap<>();
	
	@Getter
	@Setter
	private Map<Object, Object> responseDataMore = new HashMap<>();


	public ResponseData(Map<? extends Object, ? extends Object> responseData) {
		this.responseData = responseData;
	}
	
	private final static long serialVersionUID = 12387126376518475L;
}
