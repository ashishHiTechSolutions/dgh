package com.logicladder.dgh.thirdparty.response.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.logicladder.dgh.util.Utility;

import lombok.Getter;
import lombok.Setter;

public class TPTank {

	@Getter
	@Setter
	@JsonProperty("name")
	private String name;

	@Getter
	@Setter
	@JsonProperty("id")
	private Long id;

	@Getter
	@Setter
	@JsonProperty("description")
	private String description;

	@Getter
	@Setter
	@JsonProperty("devices")
	private List<Device> devices = null;

	@Getter
	@Setter
	@JsonProperty("type")
	private String type;

	@Getter
	@Setter
	@JsonProperty("locatedDevices")
	private List<Device> locatedDevices = null;

	@Getter
	@Setter
	@JsonProperty("tag")
	private String tag;

	@Getter
	@Setter
	@JsonProperty("height")
	private double height;

	@Getter
	@Setter
	@JsonProperty("percentHeight")
	private double percentHeight;

	@Getter
	@Setter
	@JsonProperty("diameter")
	private double diameter;

	@Getter
	@Setter
	@JsonProperty("volume")
	private double volume;

	@Getter
	@Setter
	@JsonProperty("lastCalibration")
	private String lastCalibration;

	/**
	 * Parent Id
	 */
	@Getter
	@Setter
	@JsonProperty("entityId")
	private String entityId;
	
	@Getter
	@Setter
	@JsonProperty("fieldName")
	private String fieldName;

	public TPTank(TPEntity entity) {
		super();
		this.id = entity.getId();
		this.name = entity.getName();
		this.type = entity.getType();
		this.devices = entity.getDevices();
		this.locatedDevices = entity.getLocatedDevices();

		for (Property property : entity.getProperties()) {

			if (property.getId() == 126) {
				this.lastCalibration = property.getValue();
			}
			if (property.getId() == 125) {
				if (Utility.isValidString(property.getValue())) {
					this.percentHeight = Double.parseDouble(property.getValue());
				}
			}

			if (property.getId() == 124) {
				if (Utility.isValidString(property.getValue())) {
					this.percentHeight = Double.parseDouble(property.getValue());
				}
			}
			if (property.getId() == 123) {
				if (Utility.isValidString(property.getValue())) {
					this.diameter = Double.parseDouble(property.getValue());
				}
			}
			if (property.getId() == 122) {
				if (Utility.isValidString(property.getValue())) {
					this.height = Double.parseDouble(property.getValue());
				}
			}
			if (property.getId() == 121) {
				this.tag = property.getValue();
			}

		}

	}

	public TPTank() {
		super();
	}

	public TPTank(String name, Long id, String description, List<Device> devices, String type,
			List<Device> locatedDevices, String tag, double height, double percentHeight, double diameter,
			double volume, String lastCalibration, String entityId, String fieldName) {
		
		super();
		this.name = name;
		this.id = id;
		this.description = description;
		this.devices = devices;
		this.type = type;
		this.locatedDevices = locatedDevices;
		this.tag = tag;
		this.height = height;
		this.percentHeight = percentHeight;
		this.diameter = diameter;
		this.volume = volume;
		this.lastCalibration = lastCalibration;
		this.entityId = entityId;
		this.fieldName = fieldName;
	}

}
