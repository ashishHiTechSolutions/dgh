/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "make", "params", "name", "registers", "registerDef", "type" })
public class Model implements Serializable {

	private final static long serialVersionUID = 2647654739117258698L;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("make")
	private Make make;
	@JsonProperty("name")
	private String name;
	@JsonProperty("params")
	private List<Param> params;
	@JsonProperty("registerDef")
	private Object registerDef;
	@JsonProperty("registers")
	private List<Object> registers;
	@JsonProperty("type")
	private Type type;

	public Model() {
	}

	@JsonProperty("id")
	public Integer getId() {
		return this.id;
	}

	@JsonProperty("make")
	public Make getMake() {
		return this.make;
	}

	@JsonProperty("name")
	public String getName() {
		return this.name;
	}

	@JsonProperty("params")
	public List<Param> getParams() {
		return this.params;
	}

	@JsonProperty("registerDef")
	public Object getRegisterDef() {
		return this.registerDef;
	}

	@JsonProperty("registers")
	public List<Object> getRegisters() {
		return this.registers;
	}

	@JsonProperty("type")
	public Type getType() {
		return this.type;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("make")
	public void setMake(Make make) {
		this.make = make;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("params")
	public void setParams(List<Param> params) {
		this.params = params;
	}

	@JsonProperty("registerDef")
	public void setRegisterDef(Object registerDef) {
		this.registerDef = registerDef;
	}

	@JsonProperty("registers")
	public void setRegisters(List<Object> registers) {
		this.registers = registers;
	}

	@JsonProperty("type")
	public void setType(Type type) {
		this.type = type;
	}

}