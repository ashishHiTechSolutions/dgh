package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"dependsOn",
"expr"
})
public class Metric {

@JsonProperty("dependsOn")
private List<String> dependsOn = null;
@JsonProperty("expr")
private String expr;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("dependsOn")
public List<String> getDependsOn() {
return dependsOn;
}

@JsonProperty("dependsOn")
public void setDependsOn(List<String> dependsOn) {
this.dependsOn = dependsOn;
}

@JsonProperty("expr")
public String getExpr() {
return expr;
}

@JsonProperty("expr")
public void setExpr(String expr) {
this.expr = expr;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}