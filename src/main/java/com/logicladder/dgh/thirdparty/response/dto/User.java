/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "userName", "name", "status", "phone", "address", "city", "state", "country",
		"zip", "home", "type", "homePage", "tenantId", "homeUrl", "role", "domain", "createdDate",
		"deletedDate", "email", "accessibleEntities", "entity" })
public class User {
	private final static long serialVersionUID = -2275287977209220486L;
	@JsonProperty("accessibleEntities")
	private Object accessibleEntities;
	@JsonProperty("address")
	private Object address;
	@JsonProperty("city")
	private Object city;
	@JsonProperty("country")
	private Object country;
	@JsonProperty("createdDate")
	private Object createdDate;
	@JsonProperty("deletedDate")
	private Object deletedDate;
	@JsonProperty("domain")
	private String domain;
	@JsonProperty("email")
	private String email;
	@JsonProperty("entity")
	private List<Entity> entity = null;
	@JsonProperty("home")
	private Object home;
	@JsonProperty("homePage")
	private Object homePage;
	@JsonProperty("homeUrl")
	private Object homeUrl;
	@JsonProperty("id")
	private Long id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("phone")
	private String phone;
	@JsonProperty("role")
	private String role;
	@JsonProperty("state")
	private Object state;
	@JsonProperty("status")
	private String status;
	@JsonProperty("tenantId")
	private Integer tenantId;
	@JsonProperty("type")
	private String type;
	@JsonProperty("userName")
	private String userName;

	@JsonProperty("zip")
	private Object zip;

	public User() {
	}

	public User(Long id, String userName, String name, String status, String phone, Object address,
			Object city, Object state, Object country, Object zip, Object home, String type, Object homePage,
			Integer tenantId, Object homeUrl, String role, String domain, Object createdDate,
			Object deletedDate, String email, Object accessibleEntities, List<Entity> entity) {
		super();
		this.id = id;
		this.userName = userName;
		this.name = name;
		this.status = status;
		this.phone = phone;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zip = zip;
		this.home = home;
		this.type = type;
		this.homePage = homePage;
		this.tenantId = tenantId;
		this.homeUrl = homeUrl;
		this.role = role;
		this.domain = domain;
		this.createdDate = createdDate;
		this.deletedDate = deletedDate;
		this.email = email;
		this.accessibleEntities = accessibleEntities;
		this.entity = entity;
	}

	@JsonProperty("accessibleEntities")
	public Object getAccessibleEntities() {
		return this.accessibleEntities;
	}

	@JsonProperty("address")
	public Object getAddress() {
		return this.address;
	}

	@JsonProperty("city")
	public Object getCity() {
		return this.city;
	}

	@JsonProperty("country")
	public Object getCountry() {
		return this.country;
	}

	@JsonProperty("createdDate")
	public Object getCreatedDate() {
		return this.createdDate;
	}

	@JsonProperty("deletedDate")
	public Object getDeletedDate() {
		return this.deletedDate;
	}

	@JsonProperty("domain")
	public String getDomain() {
		return this.domain;
	}

	@JsonProperty("email")
	public String getEmail() {
		return this.email;
	}

	@JsonProperty("entity")
	public List<Entity> getEntity() {
		return this.entity;
	}

	@JsonProperty("home")
	public Object getHome() {
		return this.home;
	}

	@JsonProperty("homePage")
	public Object getHomePage() {
		return this.homePage;
	}

	@JsonProperty("homeUrl")
	public Object getHomeUrl() {
		return this.homeUrl;
	}

	@JsonProperty("id")
	public Long getId() {
		return this.id;
	}

	@JsonProperty("name")
	public String getName() {
		return this.name;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return this.phone;
	}

	@JsonProperty("role")
	public String getRole() {
		return this.role;
	}

	@JsonProperty("state")
	public Object getState() {
		return this.state;
	}

	@JsonProperty("status")
	public String getStatus() {
		return this.status;
	}

	@JsonProperty("tenantId")
	public Integer getTenantId() {
		return this.tenantId;
	}

	@JsonProperty("type")
	public String getType() {
		return this.type;
	}

	@JsonProperty("userName")
	public String getUserName() {
		return this.userName;
	}

	@JsonProperty("zip")
	public Object getZip() {
		return this.zip;
	}

	@JsonProperty("accessibleEntities")
	public void setAccessibleEntities(Object accessibleEntities) {
		this.accessibleEntities = accessibleEntities;
	}

	@JsonProperty("address")
	public void setAddress(Object address) {
		this.address = address;
	}

	@JsonProperty("city")
	public void setCity(Object city) {
		this.city = city;
	}

	@JsonProperty("country")
	public void setCountry(Object country) {
		this.country = country;
	}

	@JsonProperty("createdDate")
	public void setCreatedDate(Object createdDate) {
		this.createdDate = createdDate;
	}

	@JsonProperty("deletedDate")
	public void setDeletedDate(Object deletedDate) {
		this.deletedDate = deletedDate;
	}

	@JsonProperty("domain")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("entity")
	public void setEntity(List<Entity> entity) {
		this.entity = entity;
	}

	@JsonProperty("home")
	public void setHome(Object home) {
		this.home = home;
	}

	@JsonProperty("homePage")
	public void setHomePage(Object homePage) {
		this.homePage = homePage;
	}

	@JsonProperty("homeUrl")
	public void setHomeUrl(Object homeUrl) {
		this.homeUrl = homeUrl;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("phone")
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonProperty("role")
	public void setRole(String role) {
		this.role = role;
	}

	@JsonProperty("state")
	public void setState(Object state) {
		this.state = state;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("tenantId")
	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("userName")
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonProperty("zip")
	public void setZip(Object zip) {
		this.zip = zip;
	}

}
