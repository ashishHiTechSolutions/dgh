/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "unit", "value", "name", "id" })
public class Property implements Serializable {

	private final static long serialVersionUID = -5919751170007839201L;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<>();
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("unit")
	private Object unit;
	@JsonProperty("value")
	private String value;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Property() {
	}

	/**
	 *
	 * @param id
	 * @param unit
	 * @param name
	 * @param value
	 */
	public Property(Object unit, String value, String name, Integer id) {
		super();
		this.unit = unit;
		this.value = value;
		this.name = name;
		this.id = id;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonProperty("id")
	public Integer getId() {
		return this.id;
	}

	@JsonProperty("name")
	public String getName() {
		return this.name;
	}

	@JsonProperty("unit")
	public Object getUnit() {
		return this.unit;
	}

	@JsonProperty("value")
	public String getValue() {
		return this.value;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("unit")
	public void setUnit(Object unit) {
		this.unit = unit;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

}