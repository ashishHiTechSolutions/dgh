/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "type", "name", "paramKey", "stdUnit", "alertLabel", "label", "paramMetric",
		"molWt", "defaultStdUnit", "virtualStdParam", "mtype", "isCounter", "isConversion",
		"isAlertEnabled" })
public class StdParam implements Serializable {

	private final static long serialVersionUID = -4312537373375423570L;
	@JsonProperty("alertLabel")
	private Object alertLabel;
	@JsonProperty("defaultStdUnit")
	private Object defaultStdUnit;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("isAlertEnabled")
	private Boolean isAlertEnabled;
	@JsonProperty("isConversion")
	private Boolean isConversion;
	@JsonProperty("isCounter")
	private Boolean isCounter;
	@JsonProperty("label")
	private String label;
	@JsonProperty("molWt")
	private Object molWt;
	@JsonProperty("mtype")
	private String mtype;
	@JsonProperty("name")
	private String name;
	@JsonProperty("paramKey")
	private String paramKey;
	@JsonProperty("paramMetric")
	private Object paramMetric;
	@JsonProperty("stdUnit")
	private String stdUnit;
	@JsonProperty("type")
	private String type;
	@JsonProperty("virtualStdParam")
	private List<Object> virtualStdParam = null;

	public StdParam() {
	}

	@JsonProperty("alertLabel")
	public Object getAlertLabel() {
		return this.alertLabel;
	}

	@JsonProperty("defaultStdUnit")
	public Object getDefaultStdUnit() {
		return this.defaultStdUnit;
	}

	@JsonProperty("id")
	public Integer getId() {
		return this.id;
	}

	@JsonProperty("isAlertEnabled")
	public Boolean getIsAlertEnabled() {
		return this.isAlertEnabled;
	}

	@JsonProperty("isConversion")
	public Boolean getIsConversion() {
		return this.isConversion;
	}

	@JsonProperty("isCounter")
	public Boolean getIsCounter() {
		return this.isCounter;
	}

	@JsonProperty("label")
	public String getLabel() {
		return this.label;
	}

	@JsonProperty("molWt")
	public Object getMolWt() {
		return this.molWt;
	}

	@JsonProperty("mtype")
	public String getMtype() {
		return this.mtype;
	}

	@JsonProperty("name")
	public String getName() {
		return this.name;
	}

	@JsonProperty("paramKey")
	public String getParamKey() {
		return this.paramKey;
	}

	@JsonProperty("paramMetric")
	public Object getParamMetric() {
		return this.paramMetric;
	}

	@JsonProperty("stdUnit")
	public String getStdUnit() {
		return this.stdUnit;
	}

	@JsonProperty("type")
	public String getType() {
		return this.type;
	}

	@JsonProperty("virtualStdParam")
	public List<Object> getVirtualStdParam() {
		return this.virtualStdParam;
	}

	@JsonProperty("alertLabel")
	public void setAlertLabel(Object alertLabel) {
		this.alertLabel = alertLabel;
	}

	@JsonProperty("defaultStdUnit")
	public void setDefaultStdUnit(Object defaultStdUnit) {
		this.defaultStdUnit = defaultStdUnit;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("isAlertEnabled")
	public void setIsAlertEnabled(Boolean isAlertEnabled) {
		this.isAlertEnabled = isAlertEnabled;
	}

	@JsonProperty("isConversion")
	public void setIsConversion(Boolean isConversion) {
		this.isConversion = isConversion;
	}

	@JsonProperty("isCounter")
	public void setIsCounter(Boolean isCounter) {
		this.isCounter = isCounter;
	}

	@JsonProperty("label")
	public void setLabel(String label) {
		this.label = label;
	}

	@JsonProperty("molWt")
	public void setMolWt(Object molWt) {
		this.molWt = molWt;
	}

	@JsonProperty("mtype")
	public void setMtype(String mtype) {
		this.mtype = mtype;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("paramKey")
	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}

	@JsonProperty("paramMetric")
	public void setParamMetric(Object paramMetric) {
		this.paramMetric = paramMetric;
	}

	@JsonProperty("stdUnit")
	public void setStdUnit(String stdUnit) {
		this.stdUnit = stdUnit;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("virtualStdParam")
	public void setVirtualStdParam(List<Object> virtualStdParam) {
		this.virtualStdParam = virtualStdParam;
	}

}