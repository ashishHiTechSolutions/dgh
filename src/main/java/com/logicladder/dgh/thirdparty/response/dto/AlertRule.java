package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.logicladder.dgh.thirdparty.request.dto.UserRequest;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"name",
"entities",
"lhs",
"rhs",
"status",
"onEveryOccurence",
"day",
"hour",
"min",
"description",
"priority",
"sendEmail",
"sendSMS",
"applicableBetween",
"considerDescendants",
"evaluationCycle",
"onFirstOccurence",
"type",
"alertFrequency",
"userList",
"site",
"phone",
"email",
"alertLabel",
"unit",
"tenantId",
"createdDate",
"condition",
"isRHSMetric"
})
public class AlertRule {

@JsonProperty("id")
private Integer id;
@JsonProperty("name")
private String name;
@JsonProperty("entities")
private List<TPEntity> entities = null;
@JsonProperty("lhs")
private Lhs lhs;
@JsonProperty("rhs")
private Rhs rhs;
@JsonProperty("status")
private String status;
@JsonProperty("onEveryOccurence")
private Boolean onEveryOccurence;
@JsonProperty("day")
private Integer day;
@JsonProperty("hour")
private Integer hour;
@JsonProperty("min")
private Integer min;
@JsonProperty("description")
private String description;
@JsonProperty("priority")
private Integer priority;
@JsonProperty("sendEmail")
private Boolean sendEmail;
@JsonProperty("sendSMS")
private Boolean sendSMS;
@JsonProperty("applicableBetween")
private String applicableBetween;
@JsonProperty("considerDescendants")
private Object considerDescendants;
@JsonProperty("evaluationCycle")
private Object evaluationCycle;
@JsonProperty("onFirstOccurence")
private Boolean onFirstOccurence;
@JsonProperty("type")
private String type;
@JsonProperty("alertFrequency")
private Object alertFrequency;
@JsonProperty("userList")
private List<UserRequest> userList = null;
@JsonProperty("site")
private Object site;
@JsonProperty("phone")
private String phone;
@JsonProperty("email")
private String email;
@JsonProperty("alertLabel")
private Object alertLabel;
@JsonProperty("unit")
private Object unit;
@JsonProperty("tenantId")
private Integer tenantId;
@JsonProperty("createdDate")
private Long createdDate;
@JsonProperty("condition")
private String condition;
@JsonProperty("isRHSMetric")
private Boolean isRHSMetric;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("entities")
public List<TPEntity> getEntities() {
return entities;
}

@JsonProperty("entities")
public void setEntities(List<TPEntity> entities) {
this.entities = entities;
}

@JsonProperty("lhs")
public Lhs getLhs() {
return lhs;
}

@JsonProperty("lhs")
public void setLhs(Lhs lhs) {
this.lhs = lhs;
}

@JsonProperty("rhs")
public Rhs getRhs() {
return rhs;
}

@JsonProperty("rhs")
public void setRhs(Rhs rhs) {
this.rhs = rhs;
}

@JsonProperty("status")
public String getStatus() {
return status;
}

@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}

@JsonProperty("onEveryOccurence")
public Boolean getOnEveryOccurence() {
return onEveryOccurence;
}

@JsonProperty("onEveryOccurence")
public void setOnEveryOccurence(Boolean onEveryOccurence) {
this.onEveryOccurence = onEveryOccurence;
}

@JsonProperty("day")
public Integer getDay() {
return day;
}

@JsonProperty("day")
public void setDay(Integer day) {
this.day = day;
}

@JsonProperty("hour")
public Integer getHour() {
return hour;
}

@JsonProperty("hour")
public void setHour(Integer hour) {
this.hour = hour;
}

@JsonProperty("min")
public Integer getMin() {
return min;
}

@JsonProperty("min")
public void setMin(Integer min) {
this.min = min;
}

@JsonProperty("description")
public String getDescription() {
return description;
}

@JsonProperty("description")
public void setDescription(String description) {
this.description = description;
}

@JsonProperty("priority")
public Integer getPriority() {
return priority;
}

@JsonProperty("priority")
public void setPriority(Integer priority) {
this.priority = priority;
}

@JsonProperty("sendEmail")
public Boolean getSendEmail() {
return sendEmail;
}

@JsonProperty("sendEmail")
public void setSendEmail(Boolean sendEmail) {
this.sendEmail = sendEmail;
}

@JsonProperty("sendSMS")
public Boolean getSendSMS() {
return sendSMS;
}

@JsonProperty("sendSMS")
public void setSendSMS(Boolean sendSMS) {
this.sendSMS = sendSMS;
}

@JsonProperty("applicableBetween")
public String getApplicableBetween() {
return applicableBetween;
}

@JsonProperty("applicableBetween")
public void setApplicableBetween(String applicableBetween) {
this.applicableBetween = applicableBetween;
}

@JsonProperty("considerDescendants")
public Object getConsiderDescendants() {
return considerDescendants;
}

@JsonProperty("considerDescendants")
public void setConsiderDescendants(Object considerDescendants) {
this.considerDescendants = considerDescendants;
}

@JsonProperty("evaluationCycle")
public Object getEvaluationCycle() {
return evaluationCycle;
}

@JsonProperty("evaluationCycle")
public void setEvaluationCycle(Object evaluationCycle) {
this.evaluationCycle = evaluationCycle;
}

@JsonProperty("onFirstOccurence")
public Boolean getOnFirstOccurence() {
return onFirstOccurence;
}

@JsonProperty("onFirstOccurence")
public void setOnFirstOccurence(Boolean onFirstOccurence) {
this.onFirstOccurence = onFirstOccurence;
}

@JsonProperty("type")
public String getType() {
return type;
}

@JsonProperty("type")
public void setType(String type) {
this.type = type;
}

@JsonProperty("alertFrequency")
public Object getAlertFrequency() {
return alertFrequency;
}

@JsonProperty("alertFrequency")
public void setAlertFrequency(Object alertFrequency) {
this.alertFrequency = alertFrequency;
}

@JsonProperty("userList")
public List<UserRequest> getUserList() {
return userList;
}

@JsonProperty("userList")
public void setUserList(List<UserRequest> userList) {
this.userList = userList;
}

@JsonProperty("site")
public Object getSite() {
return site;
}

@JsonProperty("site")
public void setSite(Object site) {
this.site = site;
}

@JsonProperty("phone")
public String getPhone() {
return phone;
}

@JsonProperty("phone")
public void setPhone(String phone) {
this.phone = phone;
}

@JsonProperty("email")
public String getEmail() {
return email;
}

@JsonProperty("email")
public void setEmail(String email) {
this.email = email;
}

@JsonProperty("alertLabel")
public Object getAlertLabel() {
return alertLabel;
}

@JsonProperty("alertLabel")
public void setAlertLabel(Object alertLabel) {
this.alertLabel = alertLabel;
}

@JsonProperty("unit")
public Object getUnit() {
return unit;
}

@JsonProperty("unit")
public void setUnit(Object unit) {
this.unit = unit;
}

@JsonProperty("tenantId")
public Integer getTenantId() {
return tenantId;
}

@JsonProperty("tenantId")
public void setTenantId(Integer tenantId) {
this.tenantId = tenantId;
}

@JsonProperty("createdDate")
public Long getCreatedDate() {
return createdDate;
}

@JsonProperty("createdDate")
public void setCreatedDate(Long createdDate) {
this.createdDate = createdDate;
}

@JsonProperty("condition")
public String getCondition() {
return condition;
}

@JsonProperty("condition")
public void setCondition(String condition) {
this.condition = condition;
}

@JsonProperty("isRHSMetric")
public Boolean getIsRHSMetric() {
return isRHSMetric;
}

@JsonProperty("isRHSMetric")
public void setIsRHSMetric(Boolean isRHSMetric) {
this.isRHSMetric = isRHSMetric;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
