package com.logicladder.dgh.thirdparty.response.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.logicladder.dgh.util.Utility;

import lombok.Getter;
import lombok.Setter;

public class TPWell {

	@Getter
	@Setter
	@JsonProperty("name")
	private String name;

	@Getter
	@Setter
	@JsonProperty("id")
	private Long id;

	@Getter
	@Setter
	@JsonProperty("description")
	private String description;

	@Getter
	@Setter
	@JsonProperty("devices")
	private List<Device> devices = null;

	@Getter
	@Setter
	@JsonProperty("type")
	private String type;

	@Getter
	@Setter
	@JsonProperty("locatedDevices")
	private List<Device> locatedDevices = null;

	@Getter
	@Setter
	@JsonProperty("code")
	private String code;

	@Getter
	@Setter
	@JsonProperty("latitude")
	private double latitude;

	@Getter
	@Setter
	@JsonProperty("longitude")
	private double longitude;

	@Getter
	@Setter
	@JsonProperty("status")
	private String status;

	@Getter
	@Setter
	@JsonProperty("elevation")
	private String elevation;

	@Getter
	@Setter
	@JsonProperty("fieldName")
	private String fieldName;

	@Getter
	@Setter
	@JsonProperty("licenceNumber")
	private String licenseNumber;

	@Getter
	@Setter
	@JsonProperty("welltype")
	private String welltype;

	@Getter
	@Setter
	@JsonProperty("tag")
	private String tag;
	
	/**
	 * Parent Id
	 */
	@Getter
	@Setter
	@JsonProperty("entityId")
	private String entityId;

	public TPWell(TPEntity entity) {

		super();
		this.id = entity.getId();
		this.name = entity.getName();
		this.type = entity.getType();
		this.devices = entity.getDevices();
		this.locatedDevices = entity.getLocatedDevices();

		for (Property property : entity.getProperties()) {
			if (property.getId() == 51) {
				this.tag = property.getValue();
			}

			if (property.getId() == 54) {
				this.welltype = property.getValue();
			}

			if (property.getId() == 55) {
				this.licenseNumber = property.getValue();
			}

			if (property.getId() == 56) {
				this.fieldName = property.getValue();
			}

			if (property.getId() == 57) {
				this.elevation = property.getValue();
			}

			if (property.getId() == 58) {
				if (Utility.isValidString(property.getValue())) {
					this.latitude = Double.parseDouble(property.getValue());
				}
			}
			if (property.getId() == 59) {
				if (Utility.isValidString(property.getValue())) {
					this.longitude = Double.parseDouble(property.getValue());
				}
			}
			if (property.getId() == 60) {

				this.status = property.getValue();

			}
		}

	}

	public TPWell(String name, Long id, String description, List<Device> devices, String type,
			List<Device> locatedDevices, String code, double latitude, double longitude, String status,
			String elevation, String fieldName, String licenseNumber, String welltype, String tag, String entityId) {
		super();
		this.name = name;
		this.id = id;
		this.description = description;
		this.devices = devices;
		this.type = type;
		this.locatedDevices = locatedDevices;
		this.code = code;
		this.latitude = latitude;
		this.longitude = longitude;
		this.status = status;
		this.elevation = elevation;
		this.fieldName = fieldName;
		this.licenseNumber = licenseNumber;
		this.welltype = welltype;
		this.tag = tag;
		this.entityId = entityId;
	}
	
	public TPWell() {
		super();
	}

	
	
}
