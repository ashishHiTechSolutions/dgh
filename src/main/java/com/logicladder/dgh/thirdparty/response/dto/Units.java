package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "type", "conversionFactor", "conversionConstant", "si", "considerMolWt" })
@JsonIgnoreProperties({"dghTubeHeadPressure" , "engAmbientTemp"})
public class Units implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("type")
	private String type;
	@JsonProperty("conversionFactor")
	private Integer conversionFactor;
	@JsonProperty("conversionConstant")
	private Integer conversionConstant;
	@JsonProperty("si")
	private Boolean si;
	@JsonProperty("considerMolWt")
	private Boolean considerMolWt;
	
	private final static long serialVersionUID = 6132897369398309587L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Units() {
	}

	/**
	 * 
	 * @param id
	 * @param conversionFactor
	 * @param name
	 * @param considerMolWt
	 * @param type
	 * @param conversionConstant
	 * @param si
	 */
	public Units(Integer id, String name, String type, Integer conversionFactor, Integer conversionConstant, Boolean si,
			Boolean considerMolWt) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.conversionFactor = conversionFactor;
		this.conversionConstant = conversionConstant;
		this.si = si;
		this.considerMolWt = considerMolWt;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("conversionFactor")
	public Integer getConversionFactor() {
		return conversionFactor;
	}

	@JsonProperty("conversionFactor")
	public void setConversionFactor(Integer conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	@JsonProperty("conversionConstant")
	public Integer getConversionConstant() {
		return conversionConstant;
	}

	@JsonProperty("conversionConstant")
	public void setConversionConstant(Integer conversionConstant) {
		this.conversionConstant = conversionConstant;
	}

	@JsonProperty("si")
	public Boolean getSi() {
		return si;
	}

	@JsonProperty("si")
	public void setSi(Boolean si) {
		this.si = si;
	}

	@JsonProperty("considerMolWt")
	public Boolean getConsiderMolWt() {
		return considerMolWt;
	}

	@JsonProperty("considerMolWt")
	public void setConsiderMolWt(Boolean considerMolWt) {
		this.considerMolWt = considerMolWt;
	}

}