package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.util.Utility;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "children", "name", "id", "description", "devices", "type", "properties", "parentId", "aHD",
		"locatedDevices" })
@JsonIgnoreProperties({ "shifts", "escalationLevels", "budgetAllocation", "billingCycles", "timezone", "path",
		"leftValue", "rightValue", "rootValue", "entityIdFromTable" , "userEntities"})
public class TPEntity implements Serializable {

	@JsonProperty("children")
	private List<TPEntity> children = null;
	@JsonProperty("name")
	private String name;

	@JsonProperty("id")
	private Long id;

	@JsonProperty("description")
	private String description;

	@JsonProperty("devices")
	private List<Device> devices = null;

	@JsonProperty("type")
	private String type;

	@JsonProperty("properties")
	private List<Property> properties = null;

	@JsonProperty("parentId")
	private String parentId;

	@JsonProperty("aHD")
	private Boolean aHD;

	@Getter
	@Setter
	@JsonProperty("locatedDevices")
	private List<Device> locatedDevices = null;

	private final static long serialVersionUID = 5834811061384945427L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public TPEntity() {
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param type
	 */
	public TPEntity(String name, Long id, String type) {
		super();
		this.name = name;
		this.id = id;
		this.type = type;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param children
	 * @param type
	 */
	public TPEntity(List<TPEntity> children, String name, Long id, String type) {
		super();
		this.children = children;
		this.name = name;
		this.id = id;
		this.type = type;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param children
	 * @param type
	 * @param description
	 */
	public TPEntity(List<TPEntity> children, String name, Long id, String type, String description) {
		super();
		this.children = children;
		this.name = name;
		this.id = id;
		this.type = type;
		this.description = description;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param children
	 * @param type
	 * @param description
	 * @param properties
	 */
	public TPEntity(List<TPEntity> children, String name, Long id, String type, String description,
			List<Property> properties) {
		super();
		this.children = children;
		this.name = name;
		this.id = id;
		this.type = type;
		this.description = description;
		this.properties = properties;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param children
	 * @param type
	 * @param description
	 * @param properties
	 */
	public TPEntity(List<TPEntity> children, String name, Long id, String type, String description,
			List<Property> properties, List<Device> devices) {
		super();
		this.children = children;
		this.name = name;
		this.id = id;
		this.type = type;
		this.description = description;
		this.properties = properties;
		this.devices = devices;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param children
	 * @param type
	 */
	public TPEntity(List<TPEntity> children, String name, Long id, String type, String parentId, Boolean aHD) {
		super();
		this.children = children;
		this.name = name;
		this.id = id;
		this.type = type;
		this.parentId = parentId;
		this.aHD = aHD;
	}

	@JsonProperty("children")
	public List<TPEntity> getChildren() {
		return children;
	}

	@JsonProperty("children")
	public void setChildren(List<TPEntity> children) {
		this.children = children;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("parentId")
	public String getParentId() {
		return parentId;
	}

	@JsonProperty("parentId")
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@JsonProperty("aHD")
	public Boolean getAHD() {
		return aHD;
	}

	@JsonProperty("aHD")
	public void setAHD(Boolean aHD) {
		this.aHD = aHD;
	}

	@JsonProperty("devices")
	public List<Device> getDevices() {
		return devices;
	}

	@JsonProperty("devices")
	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	@JsonProperty("properties")
	public List<Property> getProperties() {
		return properties;
	}

	@JsonProperty("properties")
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	public void mapperTPFacility(TPFacility tpFacility) {

		if (tpFacility.getId() != null && tpFacility.getId() != 0L) {
			this.setId(tpFacility.getId());
		}
		
		this.setName(tpFacility.getName());
		this.setType(ApplicationConstants.EntityType.FACILITY);
		
		if(tpFacility.getContractAreaId() != null){
			this.parentId = tpFacility.getContractAreaId().toString();
		}else if(tpFacility.getBasinId() != null){
			this.parentId = tpFacility.getBasinId().toString();
		}else if(tpFacility.getStateId() != null){
			this.parentId = tpFacility.getStateId().toString();
		}
		

		if (Utility.isValidString(tpFacility.getEntityId())) {
			this.parentId = tpFacility.getEntityId();
		}

		List<Property> properties = new ArrayList<Property>();

		if (Utility.isValidString(tpFacility.getContractStartDate())) {
			Property prop = new Property("Text", tpFacility.getContractStartDate(), "Contract End Date", 112);
			properties.add(prop);
		}
		if (Utility.isValidString(tpFacility.getContractStartDate())) {
			Property prop = new Property("Text", tpFacility.getContractStartDate(), "Contract End Date", 111);
			properties.add(prop);
		}
		if (tpFacility.getZoomLevel() != null) {
			Property prop = new Property("Number", tpFacility.getZoomLevel().toString(), "Zoom Level", 108);
			properties.add(prop);
		}

		Property prop1 = new Property("Number", tpFacility.getLongitude() + "", "Longitude", 24);
		properties.add(prop1);

		prop1 = new Property("Number", tpFacility.getLatitude() + "", "Latitude", 23);
		properties.add(prop1);

		if (Utility.isValidString(tpFacility.getZip())) {
			Property prop = new Property(null, tpFacility.getZip() + "", "Zip", 22);
			properties.add(prop);
		}

		if (Utility.isValidString(tpFacility.getCountryCode())) {
			Property prop = new Property("Text", tpFacility.getCountryCode() + "", "Country Code", 113);
			properties.add(prop);
		}

		if (Utility.isValidString(tpFacility.getCountry())) {
			Property prop = new Property(null, tpFacility.getCountry() + "", "Country", 21);
			properties.add(prop);
		}

		if (Utility.isValidString(tpFacility.getState())) {
			Property prop = new Property(null, tpFacility.getState() + "", "State", 20);
			properties.add(prop);
		}
		if (Utility.isValidString(tpFacility.getCity())) {
			Property prop = new Property(null, tpFacility.getCity() + "", "City", 19);
			properties.add(prop);
		}
		if (Utility.isValidString(tpFacility.getAddress())) {
			Property prop = new Property(null, tpFacility.getAddress() + "", "Address", 18);
			properties.add(prop);
		}
		/*
		 * if (Utility.isValidString(tpFacility.getZip())) { Property prop = new
		 * Property("m²", tpFacility.getArea().toString() + "", "Area", 25);
		 * properties.add(prop); }
		 */
		if (Utility.isValidString(tpFacility.getLicenseNumber())) {
			Property prop = new Property("Text", tpFacility.getLicenseNumber() + "", "License Number", 110);
			properties.add(prop);
		}
		if (Utility.isValidString(tpFacility.getCode())) {
			Property prop = new Property("Text", tpFacility.getCode() + "", "Code", 107);
			properties.add(prop);
		}
		prop1 = new Property("Number", tpFacility.getTargetProduction() + "", "Target Production (Liters)", 119);
		properties.add(prop1);

		prop1 = new Property("Number", tpFacility.getRatePerLtr() + "", "Rate per Liter", 120);
		properties.add(prop1);

		this.setProperties(properties);
	}

	public void mapperTPWell(TPWell tpWell) {

		if (tpWell.getId() != null && tpWell.getId() != 0L) {
			this.setId(tpWell.getId());
		}
		this.setName(tpWell.getName());
		this.setType(ApplicationConstants.EntityType.WELL);
		this.parentId=tpWell.getFieldName();

		if (Utility.isValidString(tpWell.getEntityId())) {
			this.parentId = tpWell.getEntityId();
		}

		List<Property> properties = new ArrayList<Property>();

		Property prop1 = new Property("Number", tpWell.getLongitude() + "", "Longitude", 59);
		properties.add(prop1);

		prop1 = new Property("Number", tpWell.getLatitude() + "", "Latitude", 58);
		properties.add(prop1);

		if (Utility.isValidString(tpWell.getElevation())) {
			Property prop = new Property(null, tpWell.getElevation() + "", "Elevation", 57);
			properties.add(prop);
		}

		if (Utility.isValidString(tpWell.getFieldName())) {
			Property prop = new Property(null, tpWell.getFieldName() + "", "Field Name", 56);
			properties.add(prop);
		}

		if (Utility.isValidString(tpWell.getLicenseNumber())) {
			Property prop = new Property(null, tpWell.getLicenseNumber() + "", "License Number", 55);
			properties.add(prop);
		}

		if (Utility.isValidString(tpWell.getWelltype())) {
			Property prop = new Property(null, tpWell.getWelltype() + "", "Type", 54);
			properties.add(prop);
		}
		if (Utility.isValidString(tpWell.getTag())) {
			Property prop = new Property(null, tpWell.getTag() + "", "Tag Name", 51);
			properties.add(prop);
		}
		if (Utility.isValidString(tpWell.getStatus())) {
			Property prop = new Property(null, tpWell.getStatus() + "", "Well Status", 60);
			properties.add(prop);
		}
		this.setProperties(properties);
	}

	public void mapperTPTank(TPTank tpTank) {

		if (tpTank.getId() != null && tpTank.getId() != 0L) {
			this.setId(tpTank.getId());
		}
		this.setName(tpTank.getName());
		this.setType(ApplicationConstants.EntityType.TANK);
		this.parentId=tpTank.getFieldName();

		if (Utility.isValidString(tpTank.getEntityId())) {
			this.parentId = tpTank.getEntityId();
		}

		List<Property> properties = new ArrayList<Property>();

		if (Utility.isValidString(tpTank.getTag())) {
			Property prop = new Property(null, tpTank.getTag() + "", "Tag Name", 121);
			properties.add(prop);
		}
		
		Property prop1 = new Property("Number", tpTank.getHeight() + "", "Height", 122);
		properties.add(prop1);

		prop1 = new Property("Number", tpTank.getDiameter() + "", "Diameter", 123);
		properties.add(prop1);

		prop1 = new Property("Number", tpTank.getVolume() + "", "Volume", 124);
		properties.add(prop1);
		
		prop1 = new Property("Number", tpTank.getPercentHeight() + "", "Percentage Height", 125);
		properties.add(prop1);

		prop1 = new Property("Number", tpTank.getLastCalibration() + "", "Last Calibration Date", 126);
		properties.add(prop1);

		this.setProperties(properties);
	}
}
