/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name" })
public class Make implements Serializable {

	private final static long serialVersionUID = 7670730409971716781L;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;

	public Make() {
	}

	@JsonProperty("id")
	public Integer getId() {
		return this.id;
	}

	@JsonProperty("name")
	public String getName() {
		return this.name;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

}
