package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"type",
"name",
"paramKey",
"stdUnit",
"alertLabel",
"label",
"paramMetric",
"molWt",
"defaultStdUnit",
"virtualStdParam",
"mtype",
"isCounter",
"isConversion",
"isAlertEnabled"
})
public class MetricParam {

@JsonProperty("id")
private Integer id;
@JsonProperty("type")
private String type;
@JsonProperty("name")
private String name;
@JsonProperty("paramKey")
private String paramKey;
@JsonProperty("stdUnit")
private String stdUnit;
@JsonProperty("alertLabel")
private Object alertLabel;
@JsonProperty("label")
private String label;
@JsonProperty("paramMetric")
private Object paramMetric;
@JsonProperty("molWt")
private Object molWt;
@JsonProperty("defaultStdUnit")
private Object defaultStdUnit;
@JsonProperty("virtualStdParam")
private List<Object> virtualStdParam = null;
@JsonProperty("mtype")
private String mtype;
@JsonProperty("isCounter")
private Boolean isCounter;
@JsonProperty("isConversion")
private Boolean isConversion;
@JsonProperty("isAlertEnabled")
private Boolean isAlertEnabled;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("type")
public String getType() {
return type;
}

@JsonProperty("type")
public void setType(String type) {
this.type = type;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("paramKey")
public String getParamKey() {
return paramKey;
}

@JsonProperty("paramKey")
public void setParamKey(String paramKey) {
this.paramKey = paramKey;
}

@JsonProperty("stdUnit")
public String getStdUnit() {
return stdUnit;
}

@JsonProperty("stdUnit")
public void setStdUnit(String stdUnit) {
this.stdUnit = stdUnit;
}

@JsonProperty("alertLabel")
public Object getAlertLabel() {
return alertLabel;
}

@JsonProperty("alertLabel")
public void setAlertLabel(Object alertLabel) {
this.alertLabel = alertLabel;
}

@JsonProperty("label")
public String getLabel() {
return label;
}

@JsonProperty("label")
public void setLabel(String label) {
this.label = label;
}

@JsonProperty("paramMetric")
public Object getParamMetric() {
return paramMetric;
}

@JsonProperty("paramMetric")
public void setParamMetric(Object paramMetric) {
this.paramMetric = paramMetric;
}

@JsonProperty("molWt")
public Object getMolWt() {
return molWt;
}

@JsonProperty("molWt")
public void setMolWt(Object molWt) {
this.molWt = molWt;
}

@JsonProperty("defaultStdUnit")
public Object getDefaultStdUnit() {
return defaultStdUnit;
}

@JsonProperty("defaultStdUnit")
public void setDefaultStdUnit(Object defaultStdUnit) {
this.defaultStdUnit = defaultStdUnit;
}

@JsonProperty("virtualStdParam")
public List<Object> getVirtualStdParam() {
return virtualStdParam;
}

@JsonProperty("virtualStdParam")
public void setVirtualStdParam(List<Object> virtualStdParam) {
this.virtualStdParam = virtualStdParam;
}

@JsonProperty("mtype")
public String getMtype() {
return mtype;
}

@JsonProperty("mtype")
public void setMtype(String mtype) {
this.mtype = mtype;
}

@JsonProperty("isCounter")
public Boolean getIsCounter() {
return isCounter;
}

@JsonProperty("isCounter")
public void setIsCounter(Boolean isCounter) {
this.isCounter = isCounter;
}

@JsonProperty("isConversion")
public Boolean getIsConversion() {
return isConversion;
}

@JsonProperty("isConversion")
public void setIsConversion(Boolean isConversion) {
this.isConversion = isConversion;
}

@JsonProperty("isAlertEnabled")
public Boolean getIsAlertEnabled() {
return isAlertEnabled;
}

@JsonProperty("isAlertEnabled")
public void setIsAlertEnabled(Boolean isAlertEnabled) {
this.isAlertEnabled = isAlertEnabled;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
