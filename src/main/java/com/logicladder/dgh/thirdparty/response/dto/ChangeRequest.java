package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "status", "createdAt", "comment", "crType", "entity",
		"changeRequestData" })
@JsonIgnoreProperties({"submittedTo", "submittedBy"})

public class ChangeRequest {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("status")
	private String status;
	@JsonProperty("submittedTo")
	private SubmittedTo submittedTo;
	@JsonProperty("submittedBy")
	private SubmittedBy submittedBy;
	@JsonProperty("createdAt")
	private String createdAt;
	@JsonProperty("comment")
	private String comment;
	@JsonProperty("crType")
	private String crType;
	@JsonProperty("entity")
	private ChangeRequestEntity entity;
	@JsonProperty("changeRequestData")
	private List<ChangeRequestData> changeRequestData = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("submittedTo")
	public SubmittedTo getSubmittedTo() {
		return submittedTo;
	}

	@JsonProperty("submittedTo")
	public void setSubmittedTo(SubmittedTo submittedTo) {
		this.submittedTo = submittedTo;
	}

	@JsonProperty("submittedBy")
	public SubmittedBy getSubmittedBy() {
		return submittedBy;
	}

	@JsonProperty("submittedBy")
	public void setSubmittedBy(SubmittedBy submittedBy) {
		this.submittedBy = submittedBy;
	}

	@JsonProperty("createdAt")
	public String getCreatedAt() {
		return createdAt;
	}

	@JsonProperty("createdAt")
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@JsonProperty("comment")
	public String getComment() {
		return comment;
	}

	@JsonProperty("comment")
	public void setComment(String comment) {
		this.comment = comment;
	}

	@JsonProperty("crType")
	public String getCrType() {
		return crType;
	}

	@JsonProperty("crType")
	public void setCrType(String crType) {
		this.crType = crType;
	}

	@JsonProperty("entity")
	public ChangeRequestEntity getEntity() {
		return entity;
	}

	@JsonProperty("entity")
	public void setEntity(ChangeRequestEntity entity) {
		this.entity = entity;
	}

	@JsonProperty("changeRequestData")
	public List<ChangeRequestData> getChangeRequestData() {
		return changeRequestData;
	}

	@JsonProperty("changeRequestData")
	public void setChangeRequestData(List<ChangeRequestData> changeRequestData) {
		this.changeRequestData = changeRequestData;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}