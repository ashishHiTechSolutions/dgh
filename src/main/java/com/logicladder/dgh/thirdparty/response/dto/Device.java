/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "description", "type", "timezone", "dataFrequency", "tag", "certification",
		"provisionStatus", "createdDate", "deletedDate", "deviceSource", "params", "model", "status", "path",
		"sno", "entityType", "make", "entityId", "entityName" })
@JsonIgnoreProperties({ "deviceType", "entityIdFromTable" })
public class Device implements Serializable {

	private final static long serialVersionUID = -261906702373014321L;
	@JsonProperty("certification")
	private Object certification;
	@JsonProperty("createdDate")
	private String createdDate;
	@JsonProperty("dataFrequency")
	private Object dataFrequency;
	@JsonProperty("deletedDate")
	private Object deletedDate;
	@JsonProperty("description")
	private Object description;
	@JsonProperty("deviceSource")
	private DeviceSource deviceSource;
	@JsonProperty("entity")
	private Entity entity;
	@JsonProperty("entityId")
	private String entityId;
	@JsonProperty("entityName")
	private String entityName;
	@JsonProperty("entityType")
	private String entityType;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("make")
	private Make make;
	@JsonProperty("model")
	private Model model;
	@JsonProperty("name")
	private String name;
	@JsonProperty("params")
	private List<Param> params = null;
	@JsonProperty("path")
	private Object path;
	@JsonProperty("provisionStatus")
	private Object provisionStatus;
	@JsonProperty("sno")
	private String sno;

	@JsonProperty("status")
	private String status;

	@JsonProperty("tag")
	private String tag;
	@JsonProperty("timezone")
	private String timezone;
	@JsonProperty("type")
	private Object type;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Device() {
	}

	@JsonProperty("certification")
	public Object getCertification() {
		return this.certification;
	}

	@JsonProperty("createdDate")
	public String getCreatedDate() {
		return this.createdDate;
	}

	@JsonProperty("dataFrequency")
	public Object getDataFrequency() {
		return this.dataFrequency;
	}

	@JsonProperty("deletedDate")
	public Object getDeletedDate() {
		return this.deletedDate;
	}

	@JsonProperty("description")
	public Object getDescription() {
		return this.description;
	}

	@JsonProperty("deviceSource")
	public DeviceSource getDeviceSource() {
		return this.deviceSource;
	}

	@JsonProperty("entityId")
	public String getEntityId() {
		return this.entityId;
	}

	@JsonProperty("entityName")
	public String getEntityName() {
		return this.entityName;
	}

	@JsonProperty("entityType")
	public String getEntityType() {
		return this.entityType;
	}

	@JsonProperty("id")
	public Integer getId() {
		return this.id;
	}

	@JsonProperty("make")
	public Make getMake() {
		return this.make;
	}

	@JsonProperty("model")
	public Model getModel() {
		return this.model;
	}

	@JsonProperty("name")
	public String getName() {
		return this.name;
	}

	@JsonProperty("params")
	public List<Param> getParams() {
		return this.params;
	}

	@JsonProperty("path")
	public Object getPath() {
		return this.path;
	}

	@JsonProperty("provisionStatus")
	public Object getProvisionStatus() {
		return this.provisionStatus;
	}

	@JsonProperty("sno")
	public String getSno() {
		return this.sno;
	}

	@JsonProperty("status")
	public String getStatus() {
		return this.status;
	}

	@JsonProperty("tag")
	public String getTag() {
		return this.tag;
	}

	@JsonProperty("timezone")
	public String getTimezone() {
		return this.timezone;
	}

	@JsonProperty("type")
	public Object getType() {
		return this.type;
	}

	@JsonProperty("certification")
	public void setCertification(Object certification) {
		this.certification = certification;
	}

	@JsonProperty("createdDate")
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@JsonProperty("dataFrequency")
	public void setDataFrequency(Object dataFrequency) {
		this.dataFrequency = dataFrequency;
	}

	@JsonProperty("deletedDate")
	public void setDeletedDate(Object deletedDate) {
		this.deletedDate = deletedDate;
	}

	@JsonProperty("description")
	public void setDescription(Object description) {
		this.description = description;
	}

	@JsonProperty("deviceSource")
	public void setDeviceSource(DeviceSource deviceSource) {
		this.deviceSource = deviceSource;
	}

	@JsonProperty("entityId")
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@JsonProperty("entityName")
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	@JsonProperty("entityType")
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("make")
	public void setMake(Make make) {
		this.make = make;
	}

	@JsonProperty("model")
	public void setModel(Model model) {
		this.model = model;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("params")
	public void setParams(List<Param> params) {
		this.params = params;
	}

	@JsonProperty("path")
	public void setPath(Object path) {
		this.path = path;
	}

	@JsonProperty("provisionStatus")
	public void setProvisionStatus(Object provisionStatus) {
		this.provisionStatus = provisionStatus;
	}

	@JsonProperty("sno")
	public void setSno(String sno) {
		this.sno = sno;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("tag")
	public void setTag(String tag) {
		this.tag = tag;
	}

	@JsonProperty("timezone")
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@JsonProperty("type")
	public void setType(Object type) {
		this.type = type;
	}

}