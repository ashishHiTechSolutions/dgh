/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "serialNo", "sensorId", "dataFrequency" })
public class DeviceSource implements Serializable {

	private final static long serialVersionUID = -4576369750723811016L;
	@JsonProperty("dataFrequency")
	private Object dataFrequency;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("sensorId")
	private String sensorId;
	@JsonProperty("serialNo")
	private String serialNo;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public DeviceSource() {
	}

	@JsonProperty("dataFrequency")
	public Object getDataFrequency() {
		return this.dataFrequency;
	}

	@JsonProperty("id")
	public Integer getId() {
		return this.id;
	}

	@JsonProperty("sensorId")
	public String getSensorId() {
		return this.sensorId;
	}

	@JsonProperty("serialNo")
	public String getSerialNo() {
		return this.serialNo;
	}

	@JsonProperty("dataFrequency")
	public void setDataFrequency(Object dataFrequency) {
		this.dataFrequency = dataFrequency;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("sensorId")
	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	@JsonProperty("serialNo")
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

}
