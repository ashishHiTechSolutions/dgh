package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "time", "value", "node" })
public class Datum {

	@JsonProperty("time")
	private ChangeRequestTime time;
	@JsonProperty("value")
	private ChangeRequestMetricValue value;
	@JsonProperty("node")
	private Node node;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("time")
	public ChangeRequestTime getTime() {
		return time;
	}

	@JsonProperty("time")
	public void setTime(ChangeRequestTime time) {
		this.time = time;
	}

	@JsonProperty("value")
	public ChangeRequestMetricValue getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(ChangeRequestMetricValue value) {
		this.value = value;
	}

	@JsonProperty("node")
	public Node getNode() {
		return node;
	}

	@JsonProperty("node")
	public void setNode(Node node) {
		this.node = node;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}