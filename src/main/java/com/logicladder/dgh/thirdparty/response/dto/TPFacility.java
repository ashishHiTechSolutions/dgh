package com.logicladder.dgh.thirdparty.response.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.logicladder.dgh.util.Utility;

import lombok.Getter;
import lombok.Setter;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({ "shifts", "escalationLevels", "budgetAllocation", "billingCycles", "timezone", "path",
	"leftValue", "rightValue", "rootValue","area" })

public class TPFacility {

	@Getter
	@Setter
	@JsonProperty("name")
	private String name;

	@Getter
	@Setter
	@JsonProperty("id")
	private Long id;

	@Getter
	@Setter
	@JsonProperty("description")
	private String description;

	@Getter
	@Setter
	@JsonProperty("devices")
	private List<Device> devices = null;

	@Getter
	@Setter
	@JsonProperty("type")
	private String type;

	@Getter
	@Setter
	@JsonProperty("locatedDevices")
	private List<Device> locatedDevices = null;

	@Getter
	@Setter
	@JsonProperty("code")
	private String code;

	@Getter
	@Setter
	@JsonProperty("latitude")
	private double latitude;

	@Getter
	@Setter
	@JsonProperty("longitude")
	private double longitude;

	@Getter
	@Setter
	@JsonProperty("ratePerLtr")
	private Double ratePerLtr;

	@Getter
	@Setter
	@JsonProperty("targetProduction")
	private Double targetProduction;

	@Getter
	@Setter
	@JsonProperty("licenseNumber")
	private String licenseNumber;

	@Getter
	@Setter
	@JsonProperty("address")
	private String address;

	@Getter
	@Setter
	@JsonProperty("city")
	private String city;

	@Getter
	@Setter
	@JsonProperty("state")
	private String state;

	@Getter
	@Setter
	@JsonProperty("country")
	private String country;

	@Getter
	@Setter
	@JsonProperty("countryCode")
	private String countryCode;

	@Getter
	@Setter
	@JsonProperty("zip")
	private String zip;

	@Getter
	@Setter
	@JsonProperty("area")
	private Double area;

	@Getter
	@Setter
	@JsonProperty("zoomLevel")
	private Integer zoomLevel;

	@Getter
	@Setter
	@JsonProperty("contractStartDate")
	private String contractStartDate;

	@Getter
	@Setter
	@JsonProperty("contractEndDate")
	private String contractEndDate;
	
	/**
	 * operatorId
	 */
	@Getter
	@Setter
	@JsonProperty("operatorId")
	private Long operatorId;
	
	
	/**
	 * Parent Id
	 */
	@Getter
	@Setter
	@JsonProperty("entityId")
	private String entityId;
	
	@Getter
	@Setter
	@JsonProperty("stateId")
	private Long stateId;
	
	@Getter
	@Setter
	@JsonProperty("basinId")
	private Long basinId;
	
	@Getter
	@Setter
	@JsonProperty("contractAreaId")
	private Long contractAreaId;
	
	
	public TPFacility() {
		super();
	}
	
	

	public TPFacility(TPEntity entity) {
		super();
		this.id = entity.getId();
		this.name = entity.getName();
		this.type = entity.getType();
		this.devices = entity.getDevices();
		this.locatedDevices = entity.getLocatedDevices();

		for (Property property : entity.getProperties()) {

			if (property.getId() == 120) {
				if (Utility.isValidString(property.getValue())) {
					this.ratePerLtr = Double.parseDouble(property.getValue());
				}
			}

			if (property.getId() == 119) {
				if (Utility.isValidString(property.getValue())) {
					this.targetProduction = Double.parseDouble(property.getValue());
				}
			}

			if (property.getId() == 107) {
				this.code = property.getValue();
			}

			if (property.getId() == 110) {
				this.licenseNumber = property.getValue();
			}

			if (property.getId() == 109) {
				if (Utility.isValidString(property.getValue())) {
					this.operatorId = Long.parseLong(property.getValue());
				}
			}

/*			if (property.getId() == 25) {
				if (Utility.isValidString(property.getValue())) {
					this.area = Double.parseDouble(property.getValue());
				}
			}
*/
			if (property.getId() == 18) {
				this.address = property.getValue();
			}

			if (property.getId() == 19) {
				this.city = property.getValue();
			}

			if (property.getId() == 20) {
				this.state = property.getValue();
			}

			if (property.getId() == 21) {
				this.country = property.getValue();
			}
			if (property.getId() == 113) {
				this.countryCode = property.getValue();
			}

			if (property.getId() == 22) {
				this.zip = property.getValue();
			}

			if (property.getId() == 23) {
				if (Utility.isValidString(property.getValue())) {
					this.latitude = Double.parseDouble(property.getValue());
				}
			}
			if (property.getId() == 24) {
				if (Utility.isValidString(property.getValue())) {
					this.longitude = Double.parseDouble(property.getValue());
				}
			}

			if (property.getId() == 108) {
				if (Utility.isValidString(property.getValue())) {
					this.zoomLevel = Integer.parseInt(property.getValue());
				}
			}

			if (property.getId() == 111) {
				this.contractStartDate = property.getValue();

			}

			if (property.getId() == 112) {

				this.contractEndDate = property.getValue();

			}
		}
	}





	public TPFacility(String name, Long id, String description, List<Device> devices, String type,
			List<Device> locatedDevices, String code, double latitude, double longitude, Double ratePerLtr,
			Double targetProduction, String licenseNumber, String address, String city, String state, String country,
			String countryCode, String zip, Double area, Integer zoomLevel, String contractStartDate,
			String contractEndDate, Long operatorId, String entityId) {
		super();
		this.name = name;
		this.id = id;
		this.description = description;
		this.devices = devices;
		this.type = type;
		this.locatedDevices = locatedDevices;
		this.code = code;
		this.latitude = latitude;
		this.longitude = longitude;
		this.ratePerLtr = ratePerLtr;
		this.targetProduction = targetProduction;
		this.licenseNumber = licenseNumber;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.countryCode = countryCode;
		this.zip = zip;
		this.area = area;
		this.zoomLevel = zoomLevel;
		this.contractStartDate = contractStartDate;
		this.contractEndDate = contractEndDate;
		this.operatorId = operatorId;
		this.entityId = entityId;
	}



	public TPFacility(String name, Long id, String description, List<Device> devices, String type,
			List<Device> locatedDevices, String code, double latitude, double longitude, Double ratePerLtr,
			Double targetProduction, String licenseNumber, String address, String city, String state, String country,
			String countryCode, String zip, Double area, Integer zoomLevel, String contractStartDate,
			String contractEndDate, Long operatorId, String entityId, Long stateId, Long basinId, Long contractAreaId) {
		super();
		this.name = name;
		this.id = id;
		this.description = description;
		this.devices = devices;
		this.type = type;
		this.locatedDevices = locatedDevices;
		this.code = code;
		this.latitude = latitude;
		this.longitude = longitude;
		this.ratePerLtr = ratePerLtr;
		this.targetProduction = targetProduction;
		this.licenseNumber = licenseNumber;
		this.address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.countryCode = countryCode;
		this.zip = zip;
		this.area = area;
		this.zoomLevel = zoomLevel;
		this.contractStartDate = contractStartDate;
		this.contractEndDate = contractEndDate;
		this.operatorId = operatorId;
		this.entityId = entityId;
		this.stateId = stateId;
		this.basinId = basinId;
		this.contractAreaId = contractAreaId;
	}
	
	

}
