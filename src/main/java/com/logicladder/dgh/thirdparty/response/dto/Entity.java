/*
 * 
 */
package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "children", "name", "id", "description", "devices", "type", "properties", "parentId",
		"aHD", "locatedDevices" })
@JsonIgnoreProperties({ "shifts", "escalationLevels", "budgetAllocation", "billingCycles", "timezone", "path",
		"leftValue", "rightValue", "rootValue", "entityIdFromTable", "userEntities" })
public class Entity implements Serializable {

	private final static long serialVersionUID = 5834811061384945427L;
	@JsonProperty("aHD")
	private Boolean aHD;

	@JsonProperty("children")
	private List<Entity> children = null;

	@JsonProperty("description")
	private String description;

	@JsonProperty("devices")
	private List<Device> devices = null;

	@JsonProperty("id")
	private Long id;

	@Getter
	@Setter
	@JsonProperty("locatedDevices")
	private List<Device> locatedDevices = null;

	@JsonProperty("name")
	private String name;

	@JsonProperty("parentId")
	private String parentId;

	@JsonProperty("properties")
	private List<Property> properties = null;

	@JsonProperty("type")
	private String type;

	public Entity() {
	}

	@JsonProperty("aHD")
	public Boolean getAHD() {
		return this.aHD;
	}

	@JsonProperty("children")
	public List<Entity> getChildren() {
		return this.children;
	}

	@JsonProperty("description")
	public String getDescription() {
		return this.description;
	}

	@JsonProperty("devices")
	public List<Device> getDevices() {
		return this.devices;
	}

	@JsonProperty("id")
	public Long getId() {
		return this.id;
	}

	@JsonProperty("name")
	public String getName() {
		return this.name;
	}

	@JsonProperty("parentId")
	public String getParentId() {
		return this.parentId;
	}

	@JsonProperty("properties")
	public List<Property> getProperties() {
		return this.properties;
	}

	@JsonProperty("type")
	public String getType() {
		return this.type;
	}

	@JsonProperty("aHD")
	public void setAHD(Boolean aHD) {
		this.aHD = aHD;
	}

	@JsonProperty("children")
	public void setChildren(List<Entity> children) {
		this.children = children;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("devices")
	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("parentId")
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@JsonProperty("properties")
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

}
