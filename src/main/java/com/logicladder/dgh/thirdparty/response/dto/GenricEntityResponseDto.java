package com.logicladder.dgh.thirdparty.response.dto;

import java.io.Serializable;
import java.util.List;

/*
{
    "id": 2461,
    "name": "Test Facility updated",
    "type": "tank",
    "description": null,
    "devices": [],
    "locatedDevices": [],
    "shifts": [],
    "escalationLevels": [],
    "budgetAllocation": [],
    "billingCycles": [],
    "timezone": null,
    "path": null,
    "leftValue": 170,
    "rightValue": 171,
    "rootValue": 2334,
    ]
}*/
public class GenricEntityResponseDto implements Serializable{

	private Integer id;
	private String name;
	private String type;
	private String description;
	private List<Device> devices;
	private List<Device> locatedDevices;
	private String leftValue;
	private String rightValue;
	private String rootValue;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Device> getDevices() {
		return devices;
	}
	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}
	public List<Device> getLocatedDevices() {
		return locatedDevices;
	}
	public void setLocatedDevices(List<Device> locatedDevices) {
		this.locatedDevices = locatedDevices;
	}
	public String getLeftValue() {
		return leftValue;
	}
	public void setLeftValue(String leftValue) {
		this.leftValue = leftValue;
	}
	public String getRightValue() {
		return rightValue;
	}
	public void setRightValue(String rightValue) {
		this.rightValue = rightValue;
	}
	public String getRootValue() {
		return rootValue;
	}
	public void setRootValue(String rootValue) {
		this.rootValue = rootValue;
	}
	
	

}
