package com.logicladder.dgh.thirdparty.response.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "paramName", "oldValue", "newValue", "dpTime" })
public class ChangeRequestData {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("paramName")
	private String paramName;
	@JsonProperty("oldValue")
	private Double oldValue;
	@JsonProperty("newValue")
	private Double newValue;
	@JsonProperty("dpTime")
	private String dpTime;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("paramName")
	public String getParamName() {
		return paramName;
	}

	@JsonProperty("paramName")
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	@JsonProperty("oldValue")
	public Double getOldValue() {
		return oldValue;
	}

	@JsonProperty("oldValue")
	public void setOldValue(Double oldValue) {
		this.oldValue = oldValue;
	}

	@JsonProperty("newValue")
	public Double getNewValue() {
		return newValue;
	}

	@JsonProperty("newValue")
	public void setNewValue(Double newValue) {
		this.newValue = newValue;
	}

	@JsonProperty("dpTime")
	public String getDpTime() {
		return dpTime;
	}

	@JsonProperty("dpTime")
	public void setDpTime(String dpTime) {
		this.dpTime = dpTime;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}