/*
 * 
 */
package com.logicladder.dgh.constant;

import com.logicladder.dgh.util.Utility;

public class ApplicationConstants {

	public static class JwtAuthenticationData {

		public static final String AUTH_TOKEN = "Auth-Token";

		public static final String IS_OTP_VERIFICATION_COMPLETED = "IsOtpVerificationCompleted";

		public static final String JWT_API_KEY = "abcdefghijklmnopqrstuvwxyz";

		public static final String ROLE = "Role";
	}

	public static class ResponseCode {

		public static final Integer OK_RESPONSE = 200;

	}

	public static class ResponseMessage {

		public static final String FAILURE = "failure";

		public static final String SUCCESS = "success";
		
		public static final String ALERT_RULE_SUCCESS = "Alert Rule created successfully";
		public static final String ALERT_RULE_FETCH_SUCCESS = "Alert Rules fetched successfully";
		public static final String TRIGGERED_ALERT_FETCH_SUCCESS = "Alerts fetched successfully";
		public static final String FACILITY_LIST_FETCH_SUCCESS = "Facilities fetched successfully";
		public static final String METRIC_DATA_FETCH_SUCCESS = "Metrics fetched successfully";
		public static final String CHANGE_REQUEST_FETCH_SUCCESS = "Change Requests fetched successfully";
		public static final String CR_FETCH_SUCCESS = "Change Request fetched successfully";
		public static final String PARAM_DATA_FETCH_SUCCESS = "Param Timeseries data fetched successfully";
	}

	public static class SpringConfig {

		public static final String SYSTEM_CREATED_DATA = "SYSTEM_CREATED_DATA";
	}
	
	public class EntityType {

		public static final String STATE = "State";
		public static final String BASIN = "Basin";
		public static final String CONTRACT_AREA = "Contract Area";
		public static final String FACILITY = "facility";
		public static final String WELL = "Well";
		public static final String TANK = "Tank";

	}

	public class EntityProperty {

		public static final String CODE = "Code";
		public static final String LATITUDE = "Latitude";
		public static final String LONGITUDE = "Longitude";
		public static final String STATUS = "Status";
		public static final String WELL_STATUS = "Well Status";
		public static final String ELEVATION = "Elevation";
		public static final String TYPE = "type";
		public static final String TAG_NAME = "Tag Name";
		public static final String WELL_TYPE = "Type";

	}
	
	public static class LogicLadderAPI {

		public final static String URL_ENTITIES = "/entities";
		public final static String URL_FULL_ENTITIES = "/fullEntities";
		public final static String URL_ENTITYPROPS = "/entityProps";
		public final static String URL_USERS = "/users";
		public final static String URL_PASSWORD = "/password";
		public final static String URL_DATA = "/entities";
		public final static String URL_ALERTS = "/alerts";
		public final static String URL_ALERT_RULES = "/alertRules";
		public final static String URL_CHANGE_REQUEST = "/changeRequest";
		
		public final static String URL_MAKE = "/makes";
		public final static String URL_MODELS = "/makes/{{MAKE-ID}}/models";

		public final static String URL_LOCATED_DEVICE = "/entities/locatedDevices";
		public final static String URL_DELETE_DEVICE = "/entities/{{ENTITY-ID}}/locatedDevices/{{DEVICE-ID}}";

		public final static String URL_UNITS = "/units";
		public final static String URL_METRICS = "/metrics";
		public final static String URL_ALERT_RULE = "/alertRules";
		public final static String URL_GET_DEVICE = "/devices/{{DEVICE-ID}}";
		public final static String URL_PARAMS = "/parameters";

	}


	// TODO

	public static final String API_BASEPATH = "";
	
	public static final String LLAUTHTOKENHEADER = (String) Utility.getApplicationDefaultProperties().get(
			"constant.logic.ladder.constants.ll_auth_token_header");
	
	public static String TP_BASE_URL = (String) Utility.getApplicationDefaultProperties().get(
			"constant.logic.ladder.constants.tp_base_url");

}
