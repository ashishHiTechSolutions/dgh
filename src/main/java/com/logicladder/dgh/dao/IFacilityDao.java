/*
 * 
 */
package com.logicladder.dgh.dao;

import com.logicladder.dgh.entity.app.Facility;

public interface IFacilityDao extends IGenricDao<Facility> {

	int updateFacilityProperty(Facility entity);
}
