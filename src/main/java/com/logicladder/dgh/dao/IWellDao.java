/*
 * 
 */
package com.logicladder.dgh.dao;

import com.logicladder.dgh.entity.app.Well;

public interface IWellDao extends IGenricDao<Well> {

	int updateWellProperty(Well entity);
}
