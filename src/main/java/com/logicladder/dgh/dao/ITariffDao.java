package com.logicladder.dgh.dao;

import java.util.Date;
import java.util.List;

import com.logicladder.dgh.entity.app.Tariff;

public interface ITariffDao extends IGenricDao<Tariff> {

	public List<Tariff> tariffList(Long id, Long facilityId, Date startDate, Date endDate);
}
