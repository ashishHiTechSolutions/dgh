/*
 * 
 */
package com.logicladder.dgh.dao;

import javax.validation.Valid;

import com.logicladder.dgh.entity.app.SuperEntity;

public interface IGenricDao<T extends SuperEntity> {

	T create(@Valid T Entityd);

	void update(@Valid T Entityd);

}
