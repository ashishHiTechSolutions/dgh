/*
 * 
 */
package com.logicladder.dgh.dao;

import com.logicladder.dgh.entity.app.ContractArea;

public interface IContractAreaDao extends IGenricDao<ContractArea> {

	int updateContractAreaProperty(ContractArea entity);
}
