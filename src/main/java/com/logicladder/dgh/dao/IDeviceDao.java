package com.logicladder.dgh.dao;

import java.util.List;

import com.logicladder.dgh.entity.app.Device;
import com.logicladder.dgh.enums.DeviceType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

public interface IDeviceDao extends IGenricDao<Device> {

	List<Device> getAllByDeviceTypeAndFacilityId(DeviceType deviceType, Long facilityId);

	int toogleStatus(EntityStatus status, Long Id);

}
