/*
 * 
 */
package com.logicladder.dgh.dao;

import com.logicladder.dgh.entity.app.Tank;

public interface ITankDao extends IGenricDao<Tank> {

	int updateTankProperty(Tank entity);
}