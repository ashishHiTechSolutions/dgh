/*
 * 
 */
package com.logicladder.dgh.dao;

import com.logicladder.dgh.entity.app.Basin;

public interface IBasinDao extends IGenricDao<Basin> {

	int updateBasinProperty(Basin entity);
}
