/*
 * 
 */
package com.logicladder.dgh.dao.impl;

import javax.persistence.EntityManager;
import javax.validation.Valid;

import org.hibernate.Session;

import com.logicladder.dgh.dao.IGenricDao;
import com.logicladder.dgh.entity.app.SuperEntity;

public abstract class AbstractGenricDaoImpl<T extends SuperEntity> implements IGenricDao<T> {

	private Class<T> clazz;

	private EntityManager entityManager;

	public AbstractGenricDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.logicladder.dgh.dao.IGenricDao#create(com.logicladder.dgh.entity.app.
	 * SuperEntity)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T create(@Valid T entity) {
		return (T) this.entityManager.unwrap(Session.class).save(entity);
	}

	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.logicladder.dgh.dao.IGenricDao#update(com.logicladder.dgh.entity.app.
	 * SuperEntity)
	 *
	 * if entity will not exits in database method will throw the
	 *
	 * @link org.hibernate.StaleObjectStateException exception which is being
	 * handled on @link GlobalExceptionHandler
	 */
	@Override
	public void update(@Valid T entity) {
		this.entityManager.unwrap(Session.class).update(entity);
	}

}
