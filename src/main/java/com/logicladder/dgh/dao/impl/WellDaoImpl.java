/*
 * 
 */
package com.logicladder.dgh.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.dao.IWellDao;
import com.logicladder.dgh.entity.app.Well;

@Repository
public class WellDaoImpl extends AbstractGenricDaoImpl<Well> implements IWellDao {

	EntityManager entityManager;

	@Autowired
	public WellDaoImpl(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	@Override
	public int updateWellProperty(Well entity) {

		int updatedEntities = this.entityManager.createQuery(
				"update Well b set b.lastModifiedOn = :lastModifiedOn,b.modifiedBy =:modifiedBy ,b.name = :name ,"
						+ "b.wellType = :wellType, b.wellProductionType= :wellProductionType, b.wellStatus= :wellStatus,"
						+ "b.status= :status, b.facilityId = :facilityId, b.coreId = :coreId where b.id = :id")
				.setParameter("lastModifiedOn", entity.getLastModifiedOn()).setParameter("modifiedBy", entity
						.getModifiedBy()).setParameter("name", entity.getName()).setParameter("wellType",
								entity.getWellType()).setParameter("wellProductionType", entity
										.getWellProductionType()).setParameter("wellStatus", entity
												.getWellStatus()).setParameter("status", entity.getStatus())
				.setParameter("facilityId", entity.getFacilityId()).setParameter("id", entity.getId())
				.setParameter("coreId", entity.getCoreId()).executeUpdate();
		this.entityManager.flush();
		if (updatedEntities == 0) {
			throw new EntityNotFoundException(String.format("Invalid Id [%s] ", entity.getId()));
		}
		return updatedEntities;
	}

}
