/*
 * 
 */
package com.logicladder.dgh.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.dao.ITankDao;
import com.logicladder.dgh.entity.app.Tank;

@Repository
public class TankDaoImpl extends AbstractGenricDaoImpl<Tank> implements ITankDao {

	EntityManager entityManager;

	@Autowired
	public TankDaoImpl(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	@Override
	public int updateTankProperty(Tank entity) {
		int updatedEntities = this.entityManager.createQuery(
				"update Tank b set b.lastModifiedOn = :lastModifiedOn,b.modifiedBy =:modifiedBy ,b.name = :name ,"
						+ "b.height = :height , b.diameter= :diameter ,"
						+ "b.lastCalibrationDate = :lastCalibrationDate ,b.status = :status ,"
						+ "b.facilityId = :facilityId, b.coreId = :coreId, b.capacity =:capacity where b.id = :id")
				.setParameter("lastModifiedOn", entity.getLastModifiedOn()).setParameter("modifiedBy", entity
						.getModifiedBy()).setParameter("name", entity.getName()).setParameter("height", entity
								.getHeight()).setParameter("diameter", entity.getDiameter()).setParameter(
										"lastCalibrationDate", entity.getLastCalibrationDate()).setParameter(
												"status", entity.getStatus()).setParameter("facilityId",
														entity.getFacilityId()).setParameter("id", entity
																.getId()).setParameter("coreId", entity
																		.getCoreId()).setParameter("capacity",
																				entity.getCapacity())
				.executeUpdate();
		this.entityManager.flush();
		if (updatedEntities == 0) {
			throw new EntityNotFoundException(String.format("Invalid Id [%s] ", entity.getId()));
		}
		return updatedEntities;
	}

}
