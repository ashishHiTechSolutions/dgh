package com.logicladder.dgh.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.dao.ITariffDao;
import com.logicladder.dgh.entity.app.Device;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.entity.app.Tariff;
import com.logicladder.dgh.enums.DeviceType;

@Repository
public class TariffDaoImpl extends AbstractGenricDaoImpl<Tariff> implements ITariffDao {

	EntityManager entityManager;

	@Autowired
	public TariffDaoImpl(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	@Override
	public List<Tariff> tariffList(Long id, Long facilityId, Date startDate, Date endDate) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Tariff> query = builder.createQuery(Tariff.class);
		Root<Tariff> root = query.from(Tariff.class);
		Join<Tariff, Device> join = root.join("device");
		Join<Device, Facility> joinFacility = join.join("facility");
		java.util.List<Predicate> predicates = new ArrayList<>();

		if (id != null) {
			predicates.add(builder.equal(joinFacility.get("id"), id));
		}

		if (startDate != null && endDate != null) {

			Predicate startDatePredicate = builder.between(root.get("tariffStartDate"), startDate, endDate);
			Predicate endDatePredicate = builder.between(join.get("tariffEndDate"), startDate, endDate);

			predicates.add(builder.or(startDatePredicate, endDatePredicate));
		} else if (startDate == null && endDate == null) {

		} else if (startDate != null) {
			predicates.add(builder.between(root.get("tariffStartDate"), startDate, endDate));
		} else if (endDate != null) {

			predicates.add(builder.between(join.get("tariffEndDate"), startDate, endDate));
		}

		query.where(builder.and(predicates.toArray(new Predicate[] {})));
		return entityManager.createQuery(query).getResultList();

	}

	public List<Device> findByDeviceTypeAndFacilityId(DeviceType deviceType, Long facilityId) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Device> query = builder.createQuery(Device.class);

		Root<Device> root = query.from(Device.class);
		Join<Device, Facility> join = root.join("facility");
		List<Predicate> predicates = new ArrayList<>();

		if (deviceType != null) {
			predicates.add(builder.equal(root.get("deviceType"), deviceType));
		}

		if (facilityId != null && facilityId > 0) {
			predicates.add(builder.equal(join.get("id"), facilityId));
		}

		query.where(builder.and(predicates.toArray(new Predicate[] {})));
		List<Device> devices = entityManager.createQuery(query).getResultList();
		return devices;
	}
}
