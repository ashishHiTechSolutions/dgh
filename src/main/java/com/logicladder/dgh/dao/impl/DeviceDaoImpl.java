package com.logicladder.dgh.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.dao.IDeviceDao;
import com.logicladder.dgh.entity.app.Device;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.enums.DeviceType;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Repository
public class DeviceDaoImpl extends AbstractGenricDaoImpl<Device> implements IDeviceDao {

	EntityManager entityManager;

	@Autowired
	public DeviceDaoImpl(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
		this.setClazz(Device.class);
	}

	@Override
	public List<Device> getAllByDeviceTypeAndFacilityId(DeviceType deviceType, Long facilityId) {

		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Device> query = builder.createQuery(Device.class);

		Root<Device> root = query.from(Device.class);
		Join<Device, Facility> join = root.join("facility");
		List<Predicate> predicates = new ArrayList<>();

		if (deviceType != null) {
			predicates.add(builder.equal(root.get("deviceType"), deviceType));
		}

		if (facilityId != null && facilityId > 0) {
			predicates.add(builder.equal(join.get("id"), facilityId));
		}

		query.where(builder.and(predicates.toArray(new Predicate[] {})));
		List<Device> devices = entityManager.createQuery(query).getResultList();
		return devices;
	}

	@Override
	public int toogleStatus(EntityStatus status, Long Id) {

		int updatedEntities = this.entityManager.createQuery("update Device d set d.status = :status where d.id = :id")
				.setParameter("status", status)
				.setParameter("id", Id)
				.executeUpdate();
		this.entityManager.flush();
		if (updatedEntities == 0) {
			throw new EntityNotFoundException(String.format("Invalid Id [%s] ", Id));
		}
		return updatedEntities;

	}
}
