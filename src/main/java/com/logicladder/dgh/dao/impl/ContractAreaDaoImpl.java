/*
 * 
 */
package com.logicladder.dgh.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.dao.IContractAreaDao;
import com.logicladder.dgh.entity.app.ContractArea;

@Repository
public class ContractAreaDaoImpl extends AbstractGenricDaoImpl<ContractArea> implements IContractAreaDao {

	EntityManager entityManager;

	@Autowired
	public ContractAreaDaoImpl(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
		this.setClazz(ContractArea.class);
	}

	@Override
	public int updateContractAreaProperty(ContractArea entity) {
		int updatedEntities = this.entityManager.createQuery(
				"update ContractArea b set b.lastModifiedOn = :lastModifiedOn , b.modifiedBy = :modifiedBy ,b.name = :name ,"
						+ "b.type = :type ,b.code = :code , b.status = :status ,"
						+ "b.areaCovered = :areaCovered , "
						+ "b.producingStatus = :producingStatus ,b.basinId = :basinId, b.coreId = :coreId where b.id = :id")
				.setParameter("lastModifiedOn", entity.getLastModifiedOn()).setParameter("modifiedBy", entity
						.getModifiedBy()).setParameter("name", entity.getName()).setParameter("type", entity
								.getType()).setParameter("code", entity.getCode()).setParameter("status",
										entity.getStatus()).setParameter("areaCovered", entity
												.getAreaCovered()).setParameter("producingStatus", entity
														.getProducingStatus()).setParameter("basinId",
																entity.getBasinId()).setParameter("id", entity
																		.getId()).setParameter("coreId",
																				entity.getCoreId())
				.executeUpdate();
		this.entityManager.flush();
		if (updatedEntities == 0) {
			throw new EntityNotFoundException(String.format("Invalid Id [%s] ", entity.getId()));
		}
		return updatedEntities;
	}
}
