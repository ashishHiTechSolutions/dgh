/*
 * 
 */
package com.logicladder.dgh.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.dao.IFacilityDao;
import com.logicladder.dgh.entity.app.Facility;

@Repository
public class FacilityDaoImpl extends AbstractGenricDaoImpl<Facility> implements IFacilityDao {

	EntityManager entityManager;

	@Autowired
	public FacilityDaoImpl(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	@Override
	public int updateFacilityProperty(Facility entity) {
		int updatedEntities = this.entityManager.createQuery(
				"update Facility b set b.lastModifiedOn = :lastModifiedOn,b.modifiedBy =:modifiedBy ,b.name = :name, "
						+ "b.fieldArea = :fieldArea ,b.code = :code , b.status = :status, "
						+ "b.targetProduction = :targetProduction , b.fieldStatus = :fieldStatus, "
						+ "b.contractAreaId = :contractAreaId, b.coreId = :coreId where b.id = :id")
				.setParameter("lastModifiedOn", entity.getLastModifiedOn()).setParameter("modifiedBy", entity
						.getModifiedBy()).setParameter("name", entity.getName()).setParameter("fieldArea",
								entity.getFieldArea()).setParameter("code", entity.getCode()).setParameter(
										"status", entity.getStatus()).setParameter("targetProduction", entity
												.getTargetProduction()).setParameter("fieldStatus", entity
														.getFieldStatus()).setParameter("contractAreaId",
																entity.getContractAreaId()).setParameter("id",
																		entity.getId()).setParameter("coreId",
																				entity.getCoreId())
				.executeUpdate();
		this.entityManager.flush();
		if (updatedEntities == 0) {
			throw new EntityNotFoundException(String.format("Invalid Id [%s] ", entity.getId()));
		}
		return updatedEntities;
	}

}
