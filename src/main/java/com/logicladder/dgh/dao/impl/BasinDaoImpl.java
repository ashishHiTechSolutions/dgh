/*
 * 
 */
package com.logicladder.dgh.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.dao.IBasinDao;
import com.logicladder.dgh.entity.app.Basin;

@Repository
public class BasinDaoImpl extends AbstractGenricDaoImpl<Basin> implements IBasinDao {

	EntityManager entityManager;

	@Autowired
	public BasinDaoImpl(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	@Override
	public int updateBasinProperty(Basin entity) {
		int updatedEntities = this.entityManager.createQuery(
				"update Basin b set b.lastModifiedOn = :lastModifiedOn,b.modifiedBy =:modifiedBy ,b.name = :name ,b.description = :description ,"
						+ "b.type = :type ,b.code = :code , b.status = :status" + "where b.id = :id")
				.setParameter("lastModifiedOn", entity.getLastModifiedOn()).setParameter("modifiedBy", entity
						.getModifiedBy()).setParameter("name", entity.getName()).setParameter("description",
								entity.getDescription()).setParameter("type", entity.getType()).setParameter(
										"code", entity.getCode()).setParameter("status", entity.getStatus())
				.setParameter("id", entity.getId()).executeUpdate();
		this.entityManager.flush();
		if (updatedEntities == 0) {
			throw new EntityNotFoundException(String.format("Invalid Id [%s] ", entity.getId()));
		}
		return updatedEntities;
	}

}
