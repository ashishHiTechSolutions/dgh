/*
 * 
 */
package com.logicladder.dgh.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.logicladder.dgh.service.IUserAuthDataService;
import com.logicladder.dgh.service.IUserService;

import io.jsonwebtoken.ExpiredJwtException;

public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {

	private JwtTokenUtil jwtTokenUtil;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private String tokenHeader;
	private UserDetailsService userDetailsService;
	private IUserAuthDataService iUserAuthDataService;

	public JwtAuthorizationTokenFilter(UserDetailsService userDetailsService, JwtTokenUtil jwtTokenUtil,
			String tokenHeader, RoleBasedAuthenticator roleBasedAuthenticator, IUserService iUserService,
			IUserAuthDataService iUserAuthDataService) {
		this.userDetailsService = userDetailsService;
		this.jwtTokenUtil = jwtTokenUtil;
		this.tokenHeader = tokenHeader;
		this.iUserAuthDataService = iUserAuthDataService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
			FilterChain chain) throws ServletException, IOException {
		this.logger.debug("processing authentication for '{}'", request.getRequestURL());

		final String requestHeader = request.getHeader(this.tokenHeader);

		String username = null;
		String authToken = null;
		if ((requestHeader != null) && requestHeader.startsWith("Bearer ")) {
			authToken = requestHeader.substring(7);
			try {
				username = this.jwtTokenUtil.getUsernameFromToken(authToken);
				if (!iUserAuthDataService.isValidAuthToken(authToken)) {
					username = null;
				}
			} catch (IllegalArgumentException e) {
				this.logger.error("an error occured during getting username from token", e);
			} catch (ExpiredJwtException e) {
				this.logger.warn("the token is expired and not valid anymore", e);
			}
			// Unable to process request via postman
		} else {
			this.logger.warn("couldn't find bearer string, will ignore the header");
		}

		/*
		 * // RoleBased Authentication boolean isActionVerified = false; if
		 * (requestHeader != null && requestHeader.startsWith("Bearer ")) {
		 * logger.info("Control recieved to check user role with function"); try { if
		 * (roleBasedAuthenticator.checkIfActionLevelAuthenticationRequired()) { User
		 * user = this.iUserService.getUserByEmail(username); isActionVerified =
		 * roleBasedAuthenticator.checkRoleBasedAuthorization(user, request); } } catch
		 * (Exception e) { logger.error("User not permitted to visit this function", e);
		 * } } else { logger.warn(
		 * "couldn't find bearer string, will ignore the header for role based authentication as well"
		 * ); }
		 */

		this.logger.debug("checking authentication for user '{}'", username);
		if ((username != null) && (SecurityContextHolder.getContext().getAuthentication() == null)
		/* && isActionVerified */) {
			this.logger.debug("security context was null, so authorizating user");

			// It is not compelling necessary to load the use details from the database. You
			// could also store the information
			// in the token and read it from it. It's up to you ;)
			UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

			// For simple validation it is completely sufficient to just check the token
			// integrity. You don't have to call
			// the database compellingly. Again it's up to you ;)

			if (this.jwtTokenUtil.validateToken(authToken, userDetails)) {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				this.logger.info("authorizated user '{}', setting security context", username);
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}
		chain.doFilter(request, response);

	}
}
