/*
 * 
 */
package com.logicladder.dgh.security;

public interface TokenExtractor {
	String extract(String payload);
}
