/*
 * 
 */
package com.logicladder.dgh.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.logicladder.dgh.entity.app.Role;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.ApprovalStatus;

public class JwtUser implements UserDetails {

	private static final long serialVersionUID = -7805479303000607025L;
	private String accessibleEntities;
	private Collection<? extends GrantedAuthority> authorities;
	private Long coreUserId;
	private String email;
	private boolean enabled;
	private long id;
	private Date lastPasswordResetDate;
	private String name;
	private Boolean otpVerified;
	private String password;

	private String phone;
	private ApprovalStatus status;
	private String userName;

	public JwtUser(long id, String userName, String name, ApprovalStatus status, String phone, String email,
			String accessibleEntities, Long coreUserId, boolean otpVerified,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = id;
		this.userName = userName;
		this.name = name;
		this.status = status;
		this.phone = phone;
		this.email = email;
		this.accessibleEntities = accessibleEntities;
		this.coreUserId = coreUserId;
		this.otpVerified = otpVerified;
		this.authorities = authorities;
	}

	public JwtUser(long id, String userName, String name, ApprovalStatus status, String phone, String email,
			String accessibleEntities, Long coreUserId, boolean otpVerified,
			Collection<? extends GrantedAuthority> authorities, boolean enabled, Date lastPasswordResetDate) {
		super();
		this.id = id;
		this.userName = userName;
		this.name = name;
		this.status = status;
		this.phone = phone;
		this.email = email;
		this.accessibleEntities = accessibleEntities;
		this.coreUserId = coreUserId;
		this.authorities = authorities;
		this.enabled = enabled;
		this.otpVerified = otpVerified;
		this.lastPasswordResetDate = lastPasswordResetDate;
	}

	public JwtUser(User user) {
		super();
		this.id = user.getId();
		this.userName = user.getUserName();
		this.name = user.getName();
		this.status = user.getStatus();
		this.phone = user.getPhone();
		this.email = user.getEmail();

		user.getFacilities().forEach(facility -> {
			this.accessibleEntities = (this.getAccessibleEntities() + facility.getId() + ",");
		});
		if (this.accessibleEntities.lastIndexOf(",") == this.accessibleEntities.length()) {
			this.accessibleEntities = this.accessibleEntities.substring(0, this.accessibleEntities.length()
					- 1);
		}
		this.coreUserId = user.getCoreUserId();
		this.otpVerified = false;// false since the user is not yet logged in and the otp verification has not
									// yet completed
		List<Role> roles = new ArrayList<>();
		roles.add(user.getUserRole());
		this.authorities = JwtUserFactory.mapToGrantedAuthorities(roles);

	}

	public String getAccessibleEntities() {
		return this.accessibleEntities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	public Long getCoreUserId() {
		return this.coreUserId;
	}

	public String getEmail() {
		return this.email;
	}

	public long getId() {
		return this.id;
	}

	@JsonIgnore
	public Date getLastPasswordResetDate() {
		return this.lastPasswordResetDate;
	}

	public String getName() {
		return this.name;
	}

	public Boolean getOtpVerified() {
		return this.otpVerified;
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		return this.password;
	}

	public String getPhone() {
		return this.phone;
	}

	public ApprovalStatus getStatus() {
		return this.status;
	}

	@Override
	public String getUsername() {
		return this.userName;
	}

	public String getUserName() {
		return this.userName;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setOtpVerified(Boolean otpVerified) {
		this.otpVerified = otpVerified;
	}

}
