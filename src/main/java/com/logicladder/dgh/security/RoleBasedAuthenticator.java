/*
 * 
 */
package com.logicladder.dgh.security;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;

import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.service.IRoleService;
import com.logicladder.dgh.util.Utility;

@Component
public class RoleBasedAuthenticator {

	protected static Logger logger = LogManager.getLogger(RoleBasedAuthenticator.class);

	@Autowired
	private IRoleService iRoleService;

	public boolean checkIfActionLevelAuthenticationRequired() {
		try {
			if (Boolean.TRUE.equals(Boolean.parseBoolean((String) Utility.getActiveSpringProfileBasedConfig()
					.get("action.level.authetication")))) {
				return true;
			}
		} catch (Exception e) {
			RoleBasedAuthenticator.logger.error("Action Level Authentication Not Defined");
		}
		return false;
	}

	public boolean checkIfLoggingRequired(String type) {
		try {
			if (Boolean.parseBoolean((String) Utility.getActiveSpringProfileBasedConfig().get("log."
					+ type))) {
				return true;
			}
		} catch (Exception e) {
			RoleBasedAuthenticator.logger.error("Action Level Authentication Not Defined");
		}
		return false;
	}

	public boolean checkRoleBasedAuthorization(User user, HttpServletRequest request) {
		ContentCachingRequestWrapper requestCacheWrapperObject = new ContentCachingRequestWrapper(request);

		return this.iRoleService.validateIfAccessPermitted(user.getUserRole().getRoleName(),
				requestCacheWrapperObject.getHeader("function"), requestCacheWrapperObject.getHeader("page"));
	}

	private Properties getLoggableProperties() {
		Properties prop = new Properties();
		InputStream input;
		input = this.getClass().getClassLoader().getResourceAsStream(
				"applicationProperties/logger.properties");
		try {
			prop.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop;
	}

	public void logRequestObject(HttpServletRequest requestCacheWrapperObject) {

		Properties prop = this.getLoggableProperties();
		StringBuilder builder = new StringBuilder();

		builder.append(
				"New Request Recieved, Request Structure defined as per logging properties is ::::=======>>>>");
		prop.keySet().forEach(item -> {
			if (((String) item).contains("request")) {
				if (((String) item).contains("request.header")) {
					try {
						builder.append(item + " : " + requestCacheWrapperObject.getHeader((String) item));
					} catch (Exception e) {
						RoleBasedAuthenticator.logger.debug((String) item
								+ "does not exist in request header.");
					}
				}
				if (((String) item).contains("request.body")) {
					try {
						builder.append(item + " : " + requestCacheWrapperObject.getParameterMap().get(item));
					} catch (Exception e) {
						RoleBasedAuthenticator.logger.debug((String) item
								+ "does not exist in paramter map.");
					}
				}
			}
		});
		RoleBasedAuthenticator.logger.debug(builder.toString());

	}

	public void logResponseObject(HttpServletResponse responseCacheWrapperObject) {
		Properties prop = this.getLoggableProperties();
		StringBuilder builder = new StringBuilder();

		builder.append(
				"New Request Recieved, Request Structure defined as per logging properties is ::::=======>>>>");
		prop.keySet().forEach(item -> {
			if (((String) item).contains("response")) {
				if (((String) item).contains("response.header")) {
					try {
						builder.append(item + " : " + responseCacheWrapperObject.getHeader((String) item));
					} catch (Exception e) {
						RoleBasedAuthenticator.logger.debug((String) item
								+ "does not exist in request header.");
					}
				}
			}
		});
		RoleBasedAuthenticator.logger.debug(builder.toString());
	}

}
