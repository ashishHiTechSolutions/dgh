/*
 * 
 */
package com.logicladder.dgh.security;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.CollectionUtils;

import com.logicladder.dgh.entity.app.Role;
import com.logicladder.dgh.entity.app.User;

public final class JwtUserFactory {

	// TODO
	public static JwtUser create(User user) {
		List<Role> authorities = new ArrayList<>();
		authorities.add(user.getUserRole());

		String entity = "";
		if (user.getFacilities() != null)
			entity = user.getFacilities().stream().map(facility -> facility.getId()).collect(Collectors
					.toList()).toString();
		return new JwtUser(user.getId(), user.getUserName(), user.getName(), user.getStatus(), user
				.getPhone(), user.getEmail(), entity, user.getCoreUserId(), Boolean.FALSE, JwtUserFactory
						.mapToGrantedAuthorities(authorities));
	}

	public static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> authorities) {
		// TODO
		if (CollectionUtils.isEmpty(authorities)) {
			return null;
		}
		return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getRoleName()))
				.collect(Collectors.toList());
	}

	private JwtUserFactory() {
	}
}
