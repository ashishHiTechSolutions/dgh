/*
 * 
 */
package com.logicladder.dgh.security.repository;

import com.logicladder.dgh.entity.app.State;

public interface IStateRepository extends IGenericRepository<State, Long> {

	public State findByName(String name);

}
