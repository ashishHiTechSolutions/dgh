package com.logicladder.dgh.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.Unit;

@Repository
public interface IUnitRepository extends JpaRepository<Unit, Long> {

}
