package com.logicladder.dgh.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.StdParam;

@Repository
public interface IStdParamRepository extends JpaRepository<StdParam, Long>{

}
