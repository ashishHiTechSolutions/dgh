/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.Collection;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.dgh.entity.app.SuperEntity;

public interface IGenericRepository<T extends SuperEntity, Long> extends JpaRepository<T, Long> {
}
