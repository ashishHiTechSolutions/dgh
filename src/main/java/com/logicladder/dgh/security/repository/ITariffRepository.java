package com.logicladder.dgh.security.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.Tariff;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Repository
public interface ITariffRepository  extends IGenericRepository<Tariff, Long> {
	
	public List<Tariff> findByStatus(EntityStatus status);

}
