/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.dgh.entity.app.Basin;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

public interface IBasinRepository extends IGenericRepository<Basin, Long> {

	List<Basin> findByStatus(EntityStatus status);

	Basin findByContractAreasId(long id);

}
