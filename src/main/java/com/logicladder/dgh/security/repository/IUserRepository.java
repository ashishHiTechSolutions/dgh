/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.List;

import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.ApprovalStatus;

public interface IUserRepository extends IGenericRepository<User, Long> {

	List<User> findByStatus(ApprovalStatus userStatus);

	User findByUserName(String userName);

	List<User> findByUserRoleRoleNameAndStatus(String string, ApprovalStatus status);

	public List<User> findByFacilitiesIdIn(List<Long> facilityIds);
	
}