package com.logicladder.dgh.security.repository;

import com.logicladder.dgh.entity.app.AccessRolePageGroup;

public interface IAccessRolePageGroupRepository extends IGenericRepository<AccessRolePageGroup, Long> {

	AccessRolePageGroup findByRoleRoleName(String role);

}
