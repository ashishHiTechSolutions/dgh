/*
 * 
 */
package com.logicladder.dgh.security.repository;

import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.Page;

@Repository
public interface IPageRepository extends IGenericRepository<Page, Long> {

}
