package com.logicladder.dgh.security.repository;

import com.logicladder.dgh.entity.app.AuditLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface IAuditRepository extends JpaRepository<AuditLog,Long>{

    Page<AuditLog> findByEntityAndChangedOnBetween(String entity, Date startDate, Date endDate, Pageable pageable);

    Page<AuditLog> findAll(Pageable pageable);
}
