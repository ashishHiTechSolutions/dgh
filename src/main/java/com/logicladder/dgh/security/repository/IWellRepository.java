/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.Well;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Repository
public interface IWellRepository extends IGenericRepository<Well, Long> {

	public List<Well> findByStatus(EntityStatus status);
}
