/*
 * 
 */
package com.logicladder.dgh.security.repository;

import com.logicladder.dgh.entity.app.JwtToken;

public interface IAuthenticationTokenRepository extends IGenericRepository<JwtToken, Long> {

}
