package com.logicladder.dgh.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.Device;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Repository
public interface IDeviceRepository extends IGenericRepository<Device, Long> {

	public List<Device> findByStatus(EntityStatus status);

}
