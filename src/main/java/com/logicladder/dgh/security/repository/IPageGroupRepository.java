package com.logicladder.dgh.security.repository;

import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.PageGroup;

@Repository
public interface IPageGroupRepository extends IGenericRepository<PageGroup, Long> {

}
