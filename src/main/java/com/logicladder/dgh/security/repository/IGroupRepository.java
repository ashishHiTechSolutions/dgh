package com.logicladder.dgh.security.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.Group;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Repository
public interface IGroupRepository extends IGenericRepository<Group, Long> {
	public List<Group> findByStatus(EntityStatus status);
}
