/*
 * 
 */
package com.logicladder.dgh.security.repository;

import com.logicladder.dgh.entity.app.Function;

public interface IFunctionRepository extends IGenericRepository<Function, Long> {

}
