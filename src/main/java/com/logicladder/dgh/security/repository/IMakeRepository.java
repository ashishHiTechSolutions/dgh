package com.logicladder.dgh.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.entity.app.Make;

@Validated
public interface IMakeRepository extends JpaRepository<Make, Long> {

	public Make getByCoreId(Long coreId);
}
