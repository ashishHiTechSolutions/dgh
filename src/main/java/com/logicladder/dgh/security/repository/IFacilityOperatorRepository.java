/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.List;

import javax.ws.rs.QueryParam;

import org.springframework.data.jpa.repository.Query;

import com.logicladder.dgh.entity.app.FacilityOperator;

public interface IFacilityOperatorRepository extends IGenericRepository<FacilityOperator, Long> {

	public List<FacilityOperator> findByFacilityId(Long id);

	@Query("select e from FacilityOperator e left join e.facility f where f.id=:id and now() BETWEEN e.contractStartDate AND e.contractEndDate")
	public FacilityOperator findByFacilityIdAndActiveDate(@QueryParam(value = "id") Long id);

	public List<FacilityOperator> findByOperatorId(Long id);

}
