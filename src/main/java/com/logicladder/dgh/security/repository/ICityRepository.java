/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.List;

import com.logicladder.dgh.entity.app.City;

public interface ICityRepository extends IGenericRepository<City, Long> {

	public List<City> findByStateName(String name);

	public List<City> findByStateId(Long id);
}
