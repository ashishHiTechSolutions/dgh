/*
 * 
 */
package com.logicladder.dgh.security.repository;

import com.logicladder.dgh.entity.app.EventLog;

public interface IEventLogRepository extends IGenericRepository<EventLog, Long> {

//	public List<EventLog> findByCreatedDateBetweenFromAndTo(Date from, Date to);
}
