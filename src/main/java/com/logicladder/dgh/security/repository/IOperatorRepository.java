package com.logicladder.dgh.security.repository;

import java.util.List;

import javax.validation.constraints.Email;

import com.logicladder.dgh.entity.app.Operator;
import com.logicladder.dgh.enums.ApprovalStatus;

public interface IOperatorRepository extends IGenericRepository<Operator, Long> {

	Operator findByEmail(@Email String email);

	List<Operator> findByStatus(ApprovalStatus status);

}
