/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.List;

import com.logicladder.dgh.entity.app.Role;

public interface IRoleRepository extends IGenericRepository<Role, Long> {

	List<Role> findByRoleName(String roleName);

}
