/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.ContractArea;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Repository
public interface IContractAreaRepository extends IGenericRepository<ContractArea, Long> {

	@Query(value = "select * from contract_area cr left outer join basin ba on cr.basin_id =ba.id", nativeQuery = true)
	Collection<ContractArea> findAllActiveContractArea();

	public List<ContractArea> findByStatus(EntityStatus status);

	/*
	 * @Query("select * from contract_area cr left outer join basin ba on cr.basin_id =ba.id where cr.id= :contractAreaId"
	 * ) ContractArea findContractAreaById( @Param("contractAreaId") long
	 * contractAreaId);
	 */
}
