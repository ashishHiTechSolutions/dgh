/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Repository
public interface IFacilityRepository extends IGenericRepository<Facility, Long> {

	public List<Facility> findByStatus(EntityStatus status);
	
}
