/*
 * 
 */
package com.logicladder.dgh.security.repository;

import com.logicladder.dgh.entity.app.UserAuthData;

public interface IUserAuthDataRepository extends IGenericRepository<UserAuthData, Long> {

	UserAuthData findByDeviceId(String deviceId);

	UserAuthData findByJwtToken(String jwtToken);

	UserAuthData findByOtpVerifiedAndUserUserName(Boolean otpVerified, String username);

	UserAuthData findByTempJwtToken(String tempJwtToken);

	UserAuthData findByUserUserName(String userName);

	UserAuthData findByUserUserNameAndDeviceId(String userName, String deviceId);

	UserAuthData findByJwtTokenOrTempJwtToken(String authToken, String tempAuthToken);

	/*
	 * @Query("SELECT ua.llAuthToken from UserAuthData ua where ua.userName : userName"
	 * ) public String findLLAuthToken(@Param("userName") String userName);
	 */
}
