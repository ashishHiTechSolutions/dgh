/*
 * 
 */
package com.logicladder.dgh.security.repository;

import com.logicladder.dgh.entity.app.Country;

public interface ICountryRepository extends IGenericRepository<Country, Long> {

}
