package com.logicladder.dgh.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.validation.annotation.Validated;

import com.logicladder.dgh.entity.app.Model;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Validated
public interface IModelRepository extends JpaRepository<Model, Long> {

	public List<Model> getAllByStatus(EntityStatus status);

	public List<Model> getByMakeId(Long id);
}
