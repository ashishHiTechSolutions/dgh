/*
 * 
 */
package com.logicladder.dgh.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logicladder.dgh.entity.app.Tank;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;

@Repository
public interface ITankRepository extends IGenericRepository<Tank, Long> {

	public List<Tank> findByStatus(EntityStatus status);

}
