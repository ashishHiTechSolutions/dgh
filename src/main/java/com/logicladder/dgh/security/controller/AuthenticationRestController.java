/*
 * 
 */
package com.logicladder.dgh.security.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.logicladder.dgh.app.exception.CustomException;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.dto.OperatorDto;
import com.logicladder.dgh.dto.UserDto;
import com.logicladder.dgh.entity.app.Basin;
import com.logicladder.dgh.entity.app.Operator;
import com.logicladder.dgh.entity.app.User;
import com.logicladder.dgh.enums.EnumUtils.EntityStatus;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.JwtAuthenticationRequest;
import com.logicladder.dgh.security.JwtTokenUtil;
import com.logicladder.dgh.service.IAuthenticationService;
import com.logicladder.dgh.service.IBasinService;
import com.logicladder.dgh.service.IOperatorService;
import com.logicladder.dgh.service.IUserService;
import com.logicladder.dgh.util.Utility;

@RestController
public class AuthenticationRestController {

	private static final Logger logger = LogManager.getLogger(AuthenticationRestController.class);

	@Autowired
	private IAuthenticationService iAuthenticationService;

	@Autowired
	private IOperatorService iOperatorService;

	@Autowired
	private IUserService iUserService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	@Qualifier("jwtUserDetailsService")
	private UserDetailsService userDetailsService;

	@Autowired
	private IBasinService iBasinService;

	@RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(
			@RequestBody JwtAuthenticationRequest authenticationRequest) throws CusAuthenticationException {
		try {
			AuthenticationRestController.logger.info(
					"Request to process received at AuthenticationRestController.createAuthenticationToken 1");

			try {
				Map<Object, Object> responseData = new HashMap<>();
				responseData.put("authToken", this.iAuthenticationService.loginUser(authenticationRequest
						.getUsername(), authenticationRequest.getPassword(), authenticationRequest
								.getDeviceId(), authenticationRequest.getIpAddress()));
				Response<?> response = new Response<>(HttpStatus.OK.value(),
						ApplicationConstants.ResponseMessage.SUCCESS, "Token Generated Successfully",
						responseData);
				return ResponseEntity.ok(response);
			} catch (CusAuthenticationException e) {
				logger.fatal(e.getMessage());
				throw new CusAuthenticationException(Response.of(ErrorCode.getEnumByErrorCode(e.getResp()
						.getErrorCode()), e.getMessage(), ApplicationConstants.ResponseMessage.FAILURE));
			} catch (HttpClientErrorException e) {
				logger.fatal(e.getMessage());
				throw new CusAuthenticationException(Response.of(ErrorCode.AUTHENTICATION,
						"Username and password do not match", ApplicationConstants.ResponseMessage.FAILURE));
			} catch (Exception e) {
				logger.fatal(e.getMessage());
				throw new CusAuthenticationException(Response.of(ErrorCode.AUTHENTICATION, e.getMessage(),
						ApplicationConstants.ResponseMessage.FAILURE));
			}
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping("/login/forgotPassword")
	public ResponseEntity<?> forgotPassword(HttpServletRequest request, @RequestParam String emailId)
			throws InterruptedException {
		try {
			AuthenticationRestController.logger.info(
					"Request to process received at AuthenticationRestController.forgotPassword");

			if (StringUtils.isNotBlank(emailId) && StringUtils.isNoneBlank(emailId)) {

				Response<?> response = this.iAuthenticationService.forgotPassword(emailId);

				return ResponseEntity.ok().body(response);
			}
			throw new HttpServerErrorException(HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@RequestMapping(path = "/register/getRegistrationData", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ResponseEntity<?> getRegistrationPreloadData(HttpServletRequest request) {
		try {
			AuthenticationRestController.logger.info(
					"Request to process received at AuthenticationRestController.getRegistrationPreloadData");

			return ResponseEntity.ok().body(this.iUserService.getRegistrationPreloadData());
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping("/login/otpverification")
	public ResponseEntity<?> OtpVerification(@RequestParam String OTP, HttpServletRequest request)
			throws CusAuthenticationException {
		try {
			AuthenticationRestController.logger.info(
					"Request to process received at AuthenticationRestController.OtpVerification");

			String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);

			if (StringUtils.isNotBlank(OTP) && StringUtils.isNoneBlank(username)) {

				Response<?> response = this.iAuthenticationService.verifyOtp(OTP, username, request.getHeader(
						this.tokenHeader).substring(7));

				return ResponseEntity.ok().body(response);

			}
			throw new CusAuthenticationException(Response.of(ErrorCode.OTP_VERIFICATION,
					"Error While verifying Otp", ApplicationConstants.ResponseMessage.FAILURE));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	@PostMapping("/register/userRegistrationRequest")
	public ResponseEntity<?> userRegistrationRequest(HttpServletRequest request,
			@RequestBody UserDto userDto) {
		try {
			AuthenticationRestController.logger.info(
					"Request to process received at AuthenticationRestController.userRegistrationRequest");

			String username = Utility.getUsernameFromAuthToken(request, this.tokenHeader, this.jwtTokenUtil);
			String authToken = this.iUserService.userRegistrationRequest(userDto, username, Utility
					.getJwtTokenFromRequest(request, this.tokenHeader));

			if (authToken != null) {
				return ResponseEntity.ok().body(Utility.getResponseForObject(
						ApplicationConstants.ResponseMessage.SUCCESS, "User Created Successfully",
						"authToken", authToken));
			} else {
				return ResponseEntity.ok().body(new Response<>(HttpStatus.OK.value(),
						ApplicationConstants.ResponseMessage.SUCCESS, "user account not created",
						new HashMap<>()));
			}
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

	/*
	 * @PostMapping(path = "/register/verifyOperatorOtp") public ResponseEntity<?>
	 * verifyOperatorOtp(
	 * 
	 * @RequestParam(value = "operatorId", required = true) Long operatorId,
	 * 
	 * @RequestParam(value = "emailOtp", required = true) String emailOtp,
	 * 
	 * @RequestParam(value = "mobileOtp", required = true) String mobileOtp,
	 * HttpServletRequest reques) {
	 * 
	 * boolean status = iOperatorService.verifyOperatorOtp(operatorId, emailOtp,
	 * mobileOtp); Map<String, Boolean> respMap = new HashMap<>();
	 * respMap.put("isVerified", status); if (status) { return
	 * ResponseEntity.ok().body(Utility.getResponseForObject(
	 * ApplicationConstants.ResponseMessage.SUCCESS, "OTP Verified", "isVerified",
	 * status)); } else { return
	 * ResponseEntity.ok().body(Utility.getResponseForObject(
	 * ApplicationConstants.ResponseMessage.FAILURE, "OTP Verification Failed",
	 * "isVerified", status)); } }
	 */

	@PostMapping(path = "/register/getAllBasins")
	public ResponseEntity<?> getAll(HttpServletRequest request, @RequestParam EntityStatus status) {
		try {
			logger.info("Application Control received at BasinController.getAll");

			List<Basin> resp = this.iBasinService.getAllByStatus(status);
			if (resp != null) {
				resp.forEach(basin -> {
					basin = Utility.processForSerialization(basin);
				});
			} else {
				resp = new ArrayList<>();
			}
			return ResponseEntity.ok(Utility.getResponseForObject(
					ApplicationConstants.ResponseMessage.SUCCESS, "Data Fetched Succesfully", "basins",
					resp));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
	}

}
