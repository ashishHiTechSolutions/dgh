/*
 * 
 */
package com.logicladder.dgh.security.controller;

import org.springframework.security.core.AuthenticationException;

import com.logicladder.dgh.response.Response;

public class CusAuthenticationException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	private Response resp;

	public CusAuthenticationException(Response resp) {
		super(resp.getMessage());
		this.resp = resp;
	}

	public CusAuthenticationException(String msg, Throwable t, Response resp) {
		super(msg, t);
		this.resp = resp;
	}

	public Response getResp() {
		return this.resp;
	}

	public void setResp(Response resp) {
		this.resp = resp;
	}

}
