/*
 * 
 */
package com.logicladder.dgh.security.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logicladder.dgh.enums.EnumUtils.ErrorCode;
import com.logicladder.dgh.response.Response;

@Component
public class CusAuthenticationFailureHandler implements AuthenticationFailureHandler {
	private final ObjectMapper mapper;

	@Autowired
	public CusAuthenticationFailureHandler(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException e) throws IOException, ServletException {

		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);

		if (e instanceof AuthenticationServiceException) {
			this.mapper.writeValue(response.getWriter(), Response.of(ErrorCode.AUTHENTICATION, e.getMessage(),
					HttpStatus.UNAUTHORIZED.name()));
		}

		this.mapper.writeValue(response.getWriter(), Response.of(ErrorCode.AUTHENTICATION,
				"Authentication failed", HttpStatus.UNAUTHORIZED.name()));
	}
}
