/*
 * 
 */
package com.logicladder.dgh.security;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;

@Component
public class JwtTokenUtil implements Serializable {

	static final String CLAIM_KEY_CREATED = "iat";
	static final String CLAIM_KEY_USERNAME = "sub";
	/**
	 *
	 */
	private static final long serialVersionUID = -1784432904298735440L;
	private Clock clock = DefaultClock.INSTANCE;

	@Value("${jwt.expiration}")
	private Long expiration;

	@Value("${jwt.secret}")
	private String secret;

	private Date calculateExpirationDate(Date createdDate) {
		return new Date(createdDate.getTime() + (this.expiration * 1000));
	}

	public Boolean canTokenBeRefreshed(String token, Date lastPasswordReset) {
		final Date created = this.getIssuedAtDateFromToken(token);
		return !this.isCreatedBeforeLastPasswordReset(created, lastPasswordReset) && (!this.isTokenExpired(
				token) || this.ignoreTokenExpiration(token));
	}

	private String doGenerateToken(Map<String, Object> claims, String subject) {
		final Date createdDate = this.clock.now();
		final Date expirationDate = this.calculateExpirationDate(createdDate);

		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(createdDate).setExpiration(
				expirationDate).signWith(SignatureAlgorithm.HS512, this.secret).compact();
	}

	public String generateToken(JwtUser userDetails) {
		Map<String, Object> claims = new HashMap<>();

		claims.put("userDetails", userDetails);

		return this.doGenerateToken(claims, userDetails.getEmail());
	}

	public String generateToken(JwtUser userDetails, boolean otpVerified) {
		Map<String, Object> claims = new HashMap<>();
		userDetails.setOtpVerified(otpVerified);
		claims.put("userDetails", userDetails);

		return this.doGenerateToken(claims, userDetails.getEmail());
	}

	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = this.getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	public Date getExpirationDateFromToken(String token) {
		return this.getClaimFromToken(token, Claims::getExpiration);
	}

	public Date getIssuedAtDateFromToken(String token) {
		return this.getClaimFromToken(token, Claims::getIssuedAt);
	}

	public String getUsernameFromToken(String token) {
		return this.getClaimFromToken(token, Claims::getSubject);
	}

	private Boolean ignoreTokenExpiration(String token) {
		// here you specify tokens, for that the expiration is ignored
		return false;
	}

	private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
		return ((lastPasswordReset != null) && created.before(lastPasswordReset));
	}

	private Boolean isTokenExpired(String token) {
		final Date expiration = this.getExpirationDateFromToken(token);
		return expiration.before(this.clock.now());
	}

	public String refreshToken(String token) {
		final Date createdDate = this.clock.now();
		final Date expirationDate = this.calculateExpirationDate(createdDate);

		final Claims claims = this.getAllClaimsFromToken(token);
		claims.setIssuedAt(createdDate);
		claims.setExpiration(expirationDate);

		return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, this.secret).compact();
	}

	public Boolean validateToken(String token, UserDetails userDetails) {
		JwtUser user = (JwtUser) userDetails;
		final String username = this.getUsernameFromToken(token);
		final Date created = this.getIssuedAtDateFromToken(token);
		// final Date expiration = getExpirationDateFromToken(token);
		boolean tokenExpired = this.isTokenExpired(token);
		return (username.equals(user.getEmail()) && !tokenExpired);
	}
}
