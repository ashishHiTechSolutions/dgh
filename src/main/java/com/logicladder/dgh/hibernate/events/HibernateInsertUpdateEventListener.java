package com.logicladder.dgh.hibernate.events;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.logicladder.dgh.components.ApplicationContextAware;
import com.logicladder.dgh.configuration.SecurityContextUserDetails;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.entity.app.SuperEntity;

@Component
public class HibernateInsertUpdateEventListener implements PreInsertEventListener, PreUpdateEventListener {

	private static final long serialVersionUID = 2147856134912345153L;

	Logger logger = LogManager.getLogger(HibernateInsertUpdateEventListener.class);

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@PostConstruct
	private void init() {
		SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
		EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(
				EventListenerRegistry.class);
		registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(this);
		registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(this);
	}

	@Override
	public boolean onPreUpdate(PreUpdateEvent event) {
		logger.info("Modified Data updated for persistent entity");
		SuperEntity entity = (SuperEntity) event.getEntity();
		String userName = null;
		try {
			userName = ApplicationContextAware.applicationContext.getBean(SecurityContextUserDetails.class)
					.getSecurityContextUserName();

		} catch (Exception e) {
			userName = ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA;
		}
		if (entity.getModifiedBy() == null && userName != null) {
			entity.setModifiedBy(userName);
		} else {
			entity.setModifiedBy(ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA);
		}
		entity.setLastModifiedOn(new Date());
		return false;
	}

	@Override
	public boolean onPreInsert(PreInsertEvent event) {
		logger.info("Created Data updated for persistent entity");
		SuperEntity entity = (SuperEntity) event.getEntity();
		String userName = null;
		try {
			userName = ApplicationContextAware.applicationContext.getBean(SecurityContextUserDetails.class)
					.getSecurityContextUserName();

		} catch (Exception e) {
			userName = ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA;
		}
		if (entity.getCreatedBy() == null && userName != null) {
			entity.setCreatedBy(userName);
		}
		entity.setCreatedDate(new Date());
		return false;
	}

}
