package com.logicladder.dgh.hibernate.events;

import java.io.Serializable;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

public class EventListenerIntegrator extends EmptyInterceptor {

	private static final long serialVersionUID = 7240628702379135329L;

	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames,
			Type[] types) {
		// TODO Auto-generated method stub
		return super.onSave(entity, id, state, propertyNames, types);
	}
}
