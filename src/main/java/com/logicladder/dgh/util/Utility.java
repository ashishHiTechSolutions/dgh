/*
 * 
 */
package com.logicladder.dgh.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.logicladder.dgh.components.ApplicationContextAware;
import com.logicladder.dgh.constant.ApplicationConstants;
import com.logicladder.dgh.entity.app.AccessRolePageGroup;
import com.logicladder.dgh.entity.app.Basin;
import com.logicladder.dgh.entity.app.ContractArea;
import com.logicladder.dgh.entity.app.Device;
import com.logicladder.dgh.entity.app.Facility;
import com.logicladder.dgh.entity.app.FacilityOperator;
import com.logicladder.dgh.entity.app.Function;
import com.logicladder.dgh.entity.app.Page;
import com.logicladder.dgh.entity.app.PageGroup;
import com.logicladder.dgh.entity.app.Role;
import com.logicladder.dgh.entity.app.Tank;
import com.logicladder.dgh.entity.app.Well;
import com.logicladder.dgh.enums.OTP_Type;
import com.logicladder.dgh.response.Response;
import com.logicladder.dgh.security.JwtTokenUtil;
import com.logicladder.dgh.thirdparty.request.dto.LogicLadderConstant;
import com.logicladder.dgh.thirdparty.response.dto.AlertMetric;
import com.logicladder.dgh.thirdparty.response.dto.AlertRule;
import com.logicladder.dgh.thirdparty.response.dto.ChangeRequest;
import com.logicladder.dgh.thirdparty.response.dto.ResponseData;
import com.logicladder.dgh.thirdparty.response.dto.TPEntity;
import com.logicladder.dgh.thirdparty.response.dto.TriggeredAlert;

public class Utility {

	private static SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

	private static Logger logger = LogManager.getLogger(Utility.class);

	public static Date createDateFromString(String date) {
		try {
			return Utility.format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String generateOTP(String mobile, OTP_Type otpType) {
		String OTP = new String("123456");
		String userName = (String) Utility.getActiveSpringProfileBasedConfig().get("smscountry.user");
		String smsCntryPasswd = (String) Utility.getActiveSpringProfileBasedConfig().get("smscountry.passwd");
		String senderId = (String) Utility.getActiveSpringProfileBasedConfig().get("smscountry.senderId");
		String mtype = (String) Utility.getActiveSpringProfileBasedConfig().get("smscountry.mtype");
		String DR = (String) Utility.getActiveSpringProfileBasedConfig().get("smscountry.dr");
		if (Boolean.parseBoolean((String) Utility.getActiveSpringProfileBasedConfig().get("app.dynamic.otp"))) {
			OTP = Utility.generateRandomOTP();
		}
		String message = null;
		if (otpType.equals(OTP_Type.LOGIN))
			message = "Use " + OTP + " as your OTP(One Time Password) to confirm Login.";
		if (otpType.equals(OTP_Type.REGISTRATION))
			message = "Use " + OTP + "as your OTP(One Time Password) to confirm User Registration.";

		// Send OTP to user's mobile via SMS Country
		logger.info("SMS text :: " + message);
		try {
			CallSmscApi.sendSms(userName, smsCntryPasswd, mobile, message, senderId, mtype, DR);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return OTP;
	}

	// TODO
	public static String generateRandomOTP() {
		String numbers = "123456789123324682376823764837648723642364723648723648224234234234";
		Random rndm_method = new Random();
		char[] otp = new char[6];
		for (int i = 0; i < 6; i++) {
			otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
		}
		return new String(otp);

	}

	public static String makeLogicLadderApisUrl(String enitityContextUrl) {
		StringBuffer url = new StringBuffer(
				(String) Utility.getApplicationDefaultProperties().get(LogicLadderConstant.LL_BASE_URL))
						.append((String) Utility.getApplicationDefaultProperties().get(enitityContextUrl));
		return url.toString();

	}

	public static String convertObjectToJsonString(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonStringCreateEntity = "";
		try {
			jsonStringCreateEntity = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonStringCreateEntity;
	}

	public static Properties getActiveSpringProfileBasedConfig() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = Utility.class.getClassLoader().getResourceAsStream("application.properties");

			if (Arrays.stream(ApplicationContextAware.applicationContext.getEnvironment().getActiveProfiles())
					.anyMatch(env -> (env.equalsIgnoreCase("dev")))) {
				input = Utility.class.getClassLoader().getResourceAsStream("application-dev.properties");
			} else if (Arrays.stream(ApplicationContextAware.applicationContext.getEnvironment().getActiveProfiles())
					.anyMatch(env -> (env.equalsIgnoreCase("prod")))) {
				input = Utility.class.getClassLoader().getResourceAsStream("application-prod.properties");
			} else if (Arrays.stream(ApplicationContextAware.applicationContext.getEnvironment().getActiveProfiles()) 
					.anyMatch(env -> (env.equalsIgnoreCase("uat")))) {
				input = Utility.class.getClassLoader().getResourceAsStream("application-uat.properties");

			}
			prop.load(input);
		} catch (Exception e) {
			Utility.logger.debug("Error reading psring active profile, cant not decifer email send config. Error: "
					+ e.getMessage());
		}
		return prop;
	}

	public static Properties getApplicationDefaultProperties() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = ApplicationContextAware.class.getClassLoader()
					.getResourceAsStream("applicationProperties/local.properties");
			prop.load(input);
		} catch (Exception e) {
			Utility.logger.debug("Error reading psring active profile, cant not decifer email send config. Error: "
					+ e.getMessage());
		}
		return prop;
	}

	public static String getJwtTokenFromRequest(HttpServletRequest request, String tokenHeader) {
		String tokenHead = request.getHeader(tokenHeader);
		if (StringUtils.isNotBlank(tokenHead)) {
			final String token = tokenHead.substring(7);
			return token;
		}
		return null;
	}

	public static Response<?> getResponseForObject(String status, String message, String keyForData, Object object) {
		Response<?> response = new Response<>();
		response.setErrorCode(HttpStatus.OK.value());
		response.setMessage(message);
		response.setStatus(status);
		Map<Object, Object> data = new HashMap<>();
		data.put(keyForData, object);
		response.setResponseData(data);
		return response;
	}

	public static String getUsernameFromAuthToken(HttpServletRequest request, String tokenHeader,
			JwtTokenUtil jwtTokenUtil) {
		String tokenHead = request.getHeader(tokenHeader);
		if (StringUtils.isNotBlank(tokenHead) && (tokenHead.length() > 7)) {

			final String token = tokenHead.substring(7);
			String username = jwtTokenUtil.getUsernameFromToken(token);
			return username;
		}
		return null;
	}

	public static boolean isPasswordValid(String password) {
		return password.matches("^(?=.{9,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$");
	}

	public static boolean isValidString(String str) {
		if ((str != null) && !"".equals(str.trim())) {
			return true;
		}

		return false;
	}

	public static Well processForSerialization(Well well) {
		if (well != null && well.getTanks() != null && !well.getTanks().isEmpty()) {
			well.getTanks().forEach(tank -> {
				tank.setWells(null);
			});
		}
		return well;
	}

	public static Tank processForSerialization(Tank tank) {
		if (tank != null && tank.getWells() != null && !tank.getWells().isEmpty()) {
			tank.getWells().forEach(well -> {
				well.setTanks(null);
				well.setDevices(null);
			});
		}

		if (CollectionUtils.isNotEmpty(tank.getDevices())) {
			tank.setDevices(null);
		}
		return tank;
	}

	public static void processForSerialization(Device deviceEntity) {
		if (deviceEntity != null) {
			if (deviceEntity.getTank() != null) {
				deviceEntity.getTank().setDevices(null);
				deviceEntity.getTank().setWells(null);
			}

			if (deviceEntity.getWell() != null) {
				deviceEntity.getWell().setDevices(null);
				deviceEntity.getWell().setTanks(null);
			}
		}
	}

	public static Facility processForSerialization(Facility facility) {
		if (facility != null) {
			if (facility.getWells() != null) {
				facility.getWells().forEach(well -> {
					well = Utility.processForSerialization(well);
				});
			}
			if (facility.getTanks() != null) {
				facility.getTanks().forEach(tank -> {
					tank = Utility.processForSerialization(tank);
				});
			}
		}
		return facility;
	}

	public static Basin processForSerialization(Basin basin) {
		if (basin != null) {
			if (basin.getContractAreas() != null) {
				basin.getContractAreas().forEach(contractArea -> {
					contractArea = Utility.processForSerialization(contractArea);
				});
			}
		}
		return basin;
	}

	public static ContractArea processForSerialization(ContractArea contractArea) {
		if (contractArea != null) {
			if (contractArea.getFacilities() != null) {
				contractArea.getFacilities().forEach(facility -> {
					facility = Utility.processForSerialization(facility);
				});
			}
		}
		return contractArea;
	}

	public static FacilityOperator processForSerialization(FacilityOperator facilityOperator) {
		if (facilityOperator != null) {
			Utility.processForSerialization(facilityOperator.getFacility());
		}
		return facilityOperator;
	}

	public static ResponseEntity<?> returnInvalidAuthToken() {
		return ResponseEntity.ok().body(Utility.createFailure(100005, "Invalid Access Token."));
	}

	public static ResponseData createFailure(Integer errorCode, String message) {

		ResponseData genericFailureRespnse = new ResponseData(errorCode, message);
		return genericFailureRespnse;
	}

	public static Object[] convertStringToTriggerArray(String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		Object[] obj = null;

		obj = mapper.readValue(json, TriggeredAlert[].class);

		return obj;

	}

	public static Object[] convertStringToAlertMetricArray(String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		Object[] obj = null;

		obj = mapper.readValue(json, AlertMetric[].class);

		return obj;

	}

	public static Object[] convertStringToAlertRuleArray(String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		Object[] obj = null;

		obj = mapper.readValue(json, AlertRule[].class);

		return obj;

	}

	public static TPEntity[] convertStringToObjectArray(String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		TPEntity[] obj = null;

		obj = mapper.readValue(json, TPEntity[].class);

		return obj;

	}

	public static void main(String[] args) {
		logger.info(createApplicationRoleBasedAuthYaml());
	}

	public static Properties getPreloadProperties() {
		Properties prop = new Properties();
		InputStream input;
		input = Utility.class.getClassLoader().getResourceAsStream("applicationProperties/authProperties.properties");
		try {
			prop.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	public static String createApplicationRoleBasedAuthYaml() {
		ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

		Set<AccessRolePageGroup> accessRolePageGroups = new HashSet<>();
		for (String roleName : ((String) Utility.getLoggableProperties().getProperty("app.role")).split(",")) {
			AccessRolePageGroup accessRolePageGroup = new AccessRolePageGroup();
			Role role = new Role(roleName, ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA);
			String groups = (String) Utility.getLoggableProperties().getProperty("app.group");
			Set<PageGroup> accessGroups = new HashSet<>();
			for (String group : groups.split(",")) {
				PageGroup accessRoleGroup = new PageGroup();
				accessRoleGroup.setName(group);

				Set<Page> pages = new HashSet<>();
				for (String page : ((String) Utility.getLoggableProperties().getProperty("app.page")).split(",")) {
					String pageName = page.contains(":") ? page.split(":")[0] : page;
					if (page.split(":")[1].equalsIgnoreCase(group)) {
						Page pageEntity = new Page(pageName, ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA);

						System.out.println(pageName);
						pageEntity.setGroupName(page.split(":")[1]);
						Set<Function> functions = new HashSet<>();
						for (String functionDetails : ((String) Utility.getLoggableProperties()
								.getProperty("app.function")).split(",")) {
							Function function = new Function(functionDetails.split(":")[0],
									functionDetails.split(":")[1],
									ApplicationConstants.SpringConfig.SYSTEM_CREATED_DATA);
							functions.add(function);
						}
						pageEntity.setFunctions(functions);
						pages.add(pageEntity);
					}
				}
				accessRoleGroup.setPages(pages);
				accessGroups.add(accessRoleGroup);
			}
			accessRolePageGroup.setRole(role);
			accessRolePageGroup.setPageGroup(accessGroups);
			accessRolePageGroups.add(accessRolePageGroup);
		}

		try {
			mapper.writeValue(new File("file.yml"), accessRolePageGroups);
			return mapper.writeValueAsString(accessRolePageGroups);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static Properties getLoggableProperties() {
		Properties prop = new Properties();
		InputStream input;
		input = Utility.class.getClassLoader().getResourceAsStream("applicationProperties/authProperties.properties");
		try {
			prop.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	public static List<AccessRolePageGroup> getYamlProperties() {
		ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
		try {
			List<AccessRolePageGroup> accessRolePageGroup = mapper.readValue(
					Utility.class.getClassLoader().getResourceAsStream("applicationProperties/roleAccessMapping.yml"),
					new TypeReference<List<AccessRolePageGroup>>() {
					});
			return accessRolePageGroup;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Object convertStringToChangeRequestObj(String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		Object obj = null;

		obj = mapper.readValue(json, ChangeRequest.class);

		return obj;

	}
	
	public static Object[] convertStringToChangeRequestArray(String json) throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		Object[] obj = null;

		obj = mapper.readValue(json, ChangeRequest[].class);

		return obj;

	}
}
