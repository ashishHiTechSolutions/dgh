/*
 * 
 */
package com.logicladder.dgh.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dozer.Mapper;
import org.springframework.util.CollectionUtils;

public abstract class DozerMapperUtils {

	public static <T> List<T> mapAsList(Collection<?> sources, Class<T> destinationClass, Mapper mapper) {

		if (CollectionUtils.isEmpty(sources)) {
			throw new IllegalArgumentException("Collection must not be null");
		}
		ArrayList<T> targets = new ArrayList<>();
		for (Object source : sources) {
			targets.add(mapper.map(source, destinationClass));
		}
		return targets;
	}

	public static <T> Set<T> mapAsSet(Collection<?> sources, Class<T> destinationClass, Mapper mapper) {

		if (CollectionUtils.isEmpty(sources)) {
			throw new IllegalArgumentException("Collection must not be null");
		}
		Set<T> targets = new HashSet<>();
		for (Object source : sources) {
			targets.add(mapper.map(source, destinationClass));
		}
		return targets;
	}

}
