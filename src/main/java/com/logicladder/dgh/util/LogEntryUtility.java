/*
 * 
 */
package com.logicladder.dgh.util;

import com.logicladder.dgh.entity.app.EventLog;
import com.logicladder.dgh.enums.EventCompletionStatus;
import com.logicladder.dgh.enums.EventSeverity;
import com.logicladder.dgh.enums.EventType;

public class LogEntryUtility {

	public static EventLog createLogEntry(String userId, EventCompletionStatus completionStatus,
			String eventDescription, EventType eventType, EventSeverity eventSeverity) {
		EventLog log = new EventLog();
		log.setCreatedBy(userId);
		log.setEventCompletionStatus(completionStatus);
		log.setEventDescription(eventDescription);
		log.setEventType(eventType);
		log.setSeverity(eventSeverity);
		return log;

	}
}
